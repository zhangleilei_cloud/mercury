"""获取代理IP"""

import random

from mercury.settings import DEPLOYTYPE, PROXIES
from mercury.downloader.http import AsyncHttpDownloader


def get_proxy():
    """获取代理IP
    如果爬虫机器在国内阿里云上, 则访问代理服务器的内网IP
    如果爬虫机器在海外或是开发测试环境下, 则访问代理服务器的公网IP
    """
    return PROXIES[DEPLOYTYPE]


async def get_ip():
    request_url = "{0}3333/ips".format(get_proxy()[:-4])
    dl = AsyncHttpDownloader(timeout=3)
    retval = await dl.fetch(request_url, rtype="json")
    try:
        json, _ = retval
        ips = json["data"]
        ip = random.choice(ips)
        return ip
    except:
        return None
