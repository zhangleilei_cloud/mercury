# -*- coding: utf-8 -*-

"""
基于HTTP的页面下载器
"""
import logging
from functools import partial

from aiohttp import ClientSession, TCPConnector, ClientTimeout
from aiohttp.resolver import AsyncResolver

from mercury.settings import logger

AF_INET = 2


class AsyncHttpDownloader:

    # 主要抓取的是国内网站，用百度和DNSPod的公共DNS解析服务（新加坡访问较快)
    resolver = AsyncResolver(nameservers=["180.76.76.76", "119.29.29.29"])

    def __init__(self, timeout=60, proxy=None, **kwargs):
        # 明确指定为IPv4，禁用dns缓存，并改用国内DNS服务，防止socket.gaierror
        conn = TCPConnector(
            family=AF_INET,
            limit=0,
            verify_ssl=False,
            resolver=self.resolver,
            use_dns_cache=False
        )
        self._session = ClientSession(
            connector=conn, timeout=ClientTimeout(total=timeout), **kwargs)
        self.cookies = {}
        self.url = None
        self.error = None
        self.proxy = proxy
        self.cookie_jar = None
        self.status = None

    async def fetch(self, url, method='get', ctype=None, data=None, rtype='text', auto_close=True, **kwargs):
        """发送HTTP请求并返回响应
        :param ctype: HTTP请求发送的数据的类型:
         json: json data, json compatible python object, dict, str.
         formurl: params in query string, mapping, iterable of tuple of k/v pairs, string.
         formdata: form-encoded data, dictionary
        """
        s = self._session
        metd = getattr(s, method)
        ctypes_mapping = {'json': 'json',
                          'formurl': 'params', 'formdata': 'data'}
        if ctype == 'json' and isinstance(data, str):
            request = partial(metd, data=data)
        else:
            request = partial(
                metd, **{ctypes_mapping[ctype]: data}) if ctype else metd
        retval = (None, None)
        try:
            async with request(url, proxy=self.proxy, **kwargs) as r:
                # HTTP状态码
                if r.status < 400:
                    # 为解决goose抽取编码识别错误问题，
                    # 显式返回aiohttp检测的编码
                    _ = getattr(r, rtype)
                    coro = _(errors='ignore') if rtype == 'text' else _()
                    retval = await coro, r.get_encoding()
                self.cookies = r.cookies
                self.url = str(r.url)
                self.cookie_jar = s.cookie_jar
                self.status = r.status
        except Exception as e:
            self.error = e
            logger.error(e, exc_info=True)
        finally:
            if auto_close or self.error:
                await self.close()
            return retval

    async def close(self):
        if self._session:
            await self._session.close()
            self._session = None
