# -*- coding: utf-8 -*-

import os
import time
import logging
import logging.config
from collections import defaultdict

import yaml

path = lambda root, *a: os.path.join(root, *a)

# ####################
# Basic paths
# ####################
DIR_SETTING = os.path.dirname(os.path.abspath(__file__))
DIR_PROJECT = os.path.dirname(os.path.dirname(DIR_SETTING))


# ####################
# Deployment types
# ####################
class DeployType:
    PROD = "PRODUCTION"
    TEST = "TEST"
    DEV = "DEVELOPMENT"
    PRODCN = "PRODCN"


if "DEPLOYTYPE" in os.environ:
    DEPLOYTYPE = os.environ["DEPLOYTYPE"].upper()
else:
    DEPLOYTYPE = DeployType.DEV

# ####################
# Third Services
# ####################
API_VIBRIO_GEN = "http://wait.delpoy.com/generator"
API_VIBRIO_RES = "http://wait.deploy.com/resolver"


# ####################
# PyYAML pre-settings
# ####################
def __pathjoin(loader, node):
    seq = loader.construct_sequence(node)
    return path(*seq)


yaml.add_constructor("!pathjoin", __pathjoin)

# ####################
# Load storage config
# ####################
STORAGE = {}

with open(path(DIR_SETTING, "storage.yml")) as f:
    _storageconf = yaml.safe_load(f)
    # 删除无关配置
    for ty in {DeployType.PROD, DeployType.TEST,
               DeployType.DEV, DeployType.PRODCN} - {DEPLOYTYPE}:
        del _storageconf[ty]
    STORAGE = _storageconf.pop(DEPLOYTYPE)

# ####################
# Logging configurations
# ####################

# use UTC time
logging.Formatter.converter = time.gmtime

with open(path(DIR_SETTING, "logging.yml")) as f:
    _logconf = yaml.load(f)
    _logconf_p = _logconf.pop(DeployType.PROD.lower())
    _ = _logconf.pop("log_dir")  # 后续无用
    if DEPLOYTYPE in {DeployType.PROD, DeployType.PRODCN}:
        _logconf.update(_logconf_p)
    logging.config.dictConfig(_logconf)

# disable verbose logging
logging.getLogger('nose').setLevel(logging.WARNING)
logging.getLogger('aioredis').setLevel(logging.INFO)
logging.getLogger('aiokafka').setLevel(logging.INFO)
logging.getLogger('boto3').setLevel(logging.WARNING)
logging.getLogger('botocore').setLevel(logging.WARNING)
logging.getLogger('chardet.charsetprober').setLevel(logging.WARNING)
logging.getLogger('jieba').setLevel(logging.WARNING)
logging.getLogger('urllib3.connectionpool').setLevel(logging.WARNING)
logging.getLogger('newspaper.images').setLevel(logging.WARNING)
logging.getLogger('PIL.Image').setLevel(logging.WARNING)
logging.getLogger('PIL.PngImagePlugin').setLevel(logging.WARNING)

logger = logging.getLogger('mercury.main')


# ####################
# RuoKuai captcha identification
# ####################
class RuoKuaiConf:
    username = "metasota"
    password = "MetaSot4!"
    softid = "106594"
    softkey = "55c8ec599a784eedba18f6815afbbf4c"
    FOUR_DIGIT_LETTER = "3040"  # 4位英数混合
    SIX_DIGIT_LETTER = "3060"  # 6位英数混合
    SIX_LETTER = "2060"  # 6位英文


# ####################
# Constant variables
# ####################
SPIDER_CONF = {}

with open(path(DIR_SETTING, "spiders.yml")) as f:
    _spidersconf = yaml.safe_load(f)
    SPIDER_CONF.update(_spidersconf)
    # 使程序内拿到的都是完整配置，不必关心怎么用默认值覆盖
    _default = SPIDER_CONF["default"]
    for k, v in SPIDER_CONF.items():
        _tmp = _default.copy()
        _tmp.update(v)
        SPIDER_CONF[k] = _tmp

# 单个进程同时下载并处理的job数量，视抓取节点性能和IP池大小而定
# 与spider并发无直接关系，若要限制单个spider的并发，请在spiders.yml中配置
MAX_JOBS = 20
JOBQUEUE_MAXSIZE = 3000
# 代理服务器内网IP地址
PROXY_ADDR_PRIVATE = 'http://172.16.0.222:3128'
# 代理服务器公网IP地址
PROXY_ADDR_PUBLIC = 'http://39.104.131.254:3128'
# 根据配置环境自动选择对应的代理IP, 默认为代理服务器的公网IP
PROXIES = defaultdict(lambda: PROXY_ADDR_PUBLIC,
                      {DeployType.PRODCN: PROXY_ADDR_PRIVATE})

# 种子检查周期, 每隔(秒)检查一次
if DEPLOYTYPE in {DeployType.PROD, DeployType.PRODCN}:
    SEED_INTERVAL = 60
else:
    SEED_INTERVAL = 10

# 北大法宝API接口 缓存服务器 地址 默认为外网IP, 部署环境为PRONCN的情况下, 使用内网IP
PKULAW_CACHE_HOST = defaultdict(lambda: 'http://39.104.166.193:3456',
                                {DeployType.PRODCN: 'http://172.16.0.78:3456'})

# 把手网账号
BS_ACCOUNT = {"mobile": "18515307922",
              "password": "lishuai1995",
              "rememberMe": True}

if __name__ == "__main__":
    print(STORAGE)
    logger = logging.getLogger("mercury.main")
    logger.info("test")
