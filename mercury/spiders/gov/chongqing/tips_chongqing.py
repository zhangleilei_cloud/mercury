# -*- coding: utf-8 -*-

import re
import time
import json
import random
import requests
import operator as op

from urllib import parse
from bs4 import BeautifulSoup

from mercury.models import Tips
from mercury.settings import logger
from mercury.spiders._base import Spider as BaseSpider
from ..gov_helper import update_deprecated_tips, update_dup_tip

domain_url = "http://zwfw.cq.gov.cn"

headless_url = "http://127.0.0.1:8000/"


class Spider(BaseSpider):
    name = "tips_chongqing"

    Model = Tips

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "User-Agent": ("Mozilla/5.0 (Windows NT 6.1; Win64; x64) "
                               "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36")
            },
            "cookies": {},
            "sleep_range_before_request": None,
            "uniqid": "urlmd5",
            "save_html": False
        }
        return configs

    async def before_request(self):
        if self.job.is_seed:
            # 种子页面的访问是post请求
            self.job.method = "post"
            signature = self.job.extra_data["signature"]
            timestamp = await self.get_unique_timestamp(signature)
            s_pattern = re.compile(r'&s=[a-z0-9A-Z]+')
            t_pattern = re.compile(r'&t=.*')
            # 每次的s和t的值不同，第一次追加，非第一次替换s和t的值
            if re.search(s_pattern, self.job.url):
                self.job.url = re.sub(s_pattern, "&s=" + signature, self.job.url)
            else:
                self.job.url = self.job.url + "&s=" + signature
            if re.search(t_pattern, self.job.url):
                self.job.url = re.sub(t_pattern, "&t=" + timestamp, self.job.url)
            else:
                self.job.url = self.job.url + "&t=" + timestamp
        else:
            self.job.url = headless_url + self.job.url
        return True

    async def handle_index(self, page):
        detail_infos, next_info = [], {}
        try:
            index_url = self.job.url
            site_level = self.job.extra_data.get("site_level")
            area_spell = self.job.extra_data.get("area_spell")
            signature = self.job.extra_data.get("signature", None)
            request_type = self.job.extra_data.get("request_type", None)
            page_json = json.loads(page)
            logger.info("[Spider]<{0}> url<{1}> site_level of<{2}>"
                        .format(self.name, self.job.url, site_level))
            isupdate = self.job.extra_data.get("isupdate", None)
            detail_url, cate_parent, title = "", "", ""
            for item in page_json["data"]:
                columns = item["columns"]
                code = columns["CODE"]
                org_code = columns["ORG_CODE"]
                org_name = parse.urlencode({"orgname": columns["ORG_NAME"]})
                title = columns["NAME"]
                if request_type == "gg":
                    code = columns["CODE"]
                    timestamp = await self.get_unique_timestamp(signature)
                    # 需要根据列表返回 先请求一次详情 再再这个详情里面真正的详情
                    temp_detail_url = f"{domain_url}/{area_spell}/api-v2/cq.app.icity.govservice." \
                                      f"GovProjectCmd/getItemListByFolderCQ2?s={signature}&t={timestamp}&" \
                                      f"PAGEMODEL&folder_code={code}&limit=50&org_code={org_code}&page=1&start"
                    response = requests.post(temp_detail_url)
                    if response.status_code == 200 and response.text:
                        temp_json = json.loads(response.text)
                        if temp_json["data"] and temp_json["total"] == 1:
                            tmp_column = temp_json["data"][0]["columns"]
                            tmp_sub_type = tmp_column["SUB_TYPE"]
                            tmp_code = tmp_column["CODE"]
                            tmp_org_name = parse.urlencode({"orgname": tmp_column["ORG_NAME"]})
                            tmp_org_code = tmp_column["ORG_CODE"]
                            cate_parent = tmp_column["FOLDER_NAME"]
                            title = tmp_column["NAME"]
                            if tmp_column["TYPE"] == "XK":
                                detail_url = f"{domain_url}/{area_spell}/icity/proinfo?" \
                                             f"code={tmp_code}&{tmp_org_name}&orgcode={tmp_org_code}"
                            else:
                                detail_url = f"http://zwfw.cq.gov.cn/{area_spell}/icity/" \
                                             f"proinfo_GG_{tmp_sub_type}?code={tmp_code}"
                else:
                    cate_parent = columns["FOLDER_NAME"]
                    if area_spell == "cq":
                        detail_url = f"{domain_url}/{area_spell}/icity/project/proinfo?" \
                                     f"code={code}&{org_name}&orgcode={org_code}"
                    else:
                        detail_url = f"{domain_url}/{area_spell}/icity/proinfo?" \
                                     f"code={code}&{org_name}&orgcode={org_code}"
                if detail_url is not "":
                    detail_infos.append(
                        {'url': detail_url,
                         'extra_data': {"site_level": self.job.extra_data["site_level"], "title": title,
                                        "cate_parent": cate_parent, "isupdate": isupdate,
                                        "area_spell": area_spell, "request_type": request_type}})
            total = page_json["total"]
            remain_page = total % 8
            page_num = int(total / 8)
            if remain_page != 0:
                page_num = page_num + 1
            if page_num > 1:
                current_page = int(re.search(r"\d+", re.search("page=\d+", index_url).group()).group())
                start = int(re.search(r"\d+", re.search("start=\d+", index_url).group()).group())
                if current_page < page_num:
                    pn_old = "page=" + str(current_page)
                    start_old = "start=" + str(start)
                    pn_new = "page=" + str(current_page + 1)
                    start_new = "start=" + str((start + 1) * 8)
                    url_next = index_url.replace(pn_old, pn_new)
                    url_next = url_next.replace(start_old, start_new)
                    extra_data = {"area_spell": area_spell, "request_type": request_type,
                                  "signature": signature, "site_level": site_level, "isupdate": isupdate}
                    next_info = {'url': url_next, 'extra_data': extra_data}
                if current_page == page_num:
                    await update_deprecated_tips(self.name, self.job.extra_data["site_level"])
                    # detail的访问是get请求
                    self.job.method = "get"
            else:
                # detail的访问是get请求
                self.job.method = "get"
            del page
        except Exception as e:
            logger.error(e, exc_info=True)
        return (detail_infos, next_info)

    async def handle_dup_detail(self):
        if not self.job.is_seed:
            await update_dup_tip(self.job, self.name)

    async def handle_detail(self, page, encoding, extra_data=None):
        detail_extra_data = []
        try:
            request_type = extra_data.pop("request_type", ""),
            self.job.url = self.job.url.replace(headless_url, "")
            retval = dict(title=extra_data.pop("title", ""), getway=self.name)
            soup = BeautifulSoup(page, 'lxml')
            li_list = soup.select(".bszn_left")[0].find_all("li")
            for li in li_list:
                # 不显示的li 不去抓取
                if li.has_attr("style") and "display:none" in li["style"]:
                    continue
                flag = 0
                data_code = li["data-code"].strip()
                if data_code == "baseInfo":
                    base_info_element = soup.select("#baseInfo")
                    if base_info_element:
                        base_info = dict(name="基本信息", content=base_info_element[0].prettify())
                        retval["organization"], retval["department"], retval[
                            "telephone"], extra_data["cate_xz"], supervise_telephone = "", "", "", "", ""
                        all_ths = base_info_element[0].find_all("th")
                        all_tds = base_info_element[0].find_all("td")
                        for index, th in enumerate(all_ths):
                            if op.eq(th.get_text().strip(), "事项类型"):
                                extra_data["cate_xz"] = all_tds[index].get_text().strip()
                            if op.eq(th.get_text().strip(), "决定机构"):
                                retval["organization"] = all_tds[index].get_text().strip()
                            if op.eq(th.get_text().strip(), "实施主体") or op.eq(th.get_text().strip(), "实施主体名称"):
                                if all_tds[index].get_text().strip() is not "":
                                    retval["department"] = all_tds[index].get_text().strip()
                            if op.eq(th.get_text().strip(), "咨询方式") or op.eq(th.get_text().strip(), "咨询电话"):
                                retval["telephone"] = all_tds[index].get_text().strip()
                            if op.eq(th.get_text().strip(), "到现场次数"):
                                retval["visits"] = all_tds[index].get_text().strip()
                            if op.eq(th.get_text().strip(), "监督电话"):
                                supervise_telephone = all_tds[index].get_text().strip()
                        if retval["organization"] is "" and retval["department"] is not "":
                            retval["organization"] = retval["department"]
                        if retval["telephone"] is "":
                            retval["telephone"] = supervise_telephone
                        if request_type is "gg" and extra_data["cate_xz"] is "":
                            extra_data["cate_xz"] = "其他公共服务"
                    else:
                        base_info = dict(name="基本信息", content="")
                    retval["base_info"] = base_info
                    flag = 1

                if data_code == "materialList":
                    materials_element = soup.select("#materialList")
                    if materials_element:
                        basis = dict(name="申请材料", content=materials_element[0].prettify())
                    else:
                        basis = dict(name="申请材料", content="")
                    retval["materials"] = basis
                    flag = 1

                if data_code == "approval_basis":
                    basis_element = soup.select("#approval_basis")
                    if basis_element:
                        basis = dict(name="设定依据", content=basis_element[0].prettify())
                    else:
                        basis = dict(name="设定依据", content="")
                    retval["basis"] = basis
                    flag = 1

                if data_code == "flow_chart":
                    work_flow_element = soup.select("#flow_chart")
                    if work_flow_element:
                        work_flow = dict(name="办理流程", content=work_flow_element[0].prettify())
                    else:
                        work_flow = dict(name="办理流程", content="")
                    retval["work_flow"] = work_flow
                    flag = 1

                if data_code == "fre_standard":
                    fees_element = soup.select("#fre_standard")
                    if fees_element:
                        fees = dict(name="收费标准", content=fees_element[0].prettify())
                    else:
                        fees = dict(name="收费标准", content="")
                    retval["fees"] = fees
                    flag = 1

                if flag == 0:
                    extra_element = soup.select("#" + data_code)
                    if extra_element:
                        extra = dict(name=li.get_text().strip(), content=extra_element[0].prettify())
                        detail_extra_data.append(extra)
            if retval and extra_data:
                retval.update({"reference": self.job.url, "extras": detail_extra_data, "sync": 0})
                retval.update({
                    "cate_parent": extra_data.pop("cate_parent", ""),
                    "cate_xz": extra_data.pop("cate_xz", ""),
                    "site_level": extra_data.pop("site_level", "")
                })
            return retval
        except Exception as e:
            logger.error(e, exc_info=True)
        return None

    async def get_unique_timestamp(self, signature):
        # 根据网站规则生成t,每一次请求t不同，每次请求需要重新生成
        char_str = "0123456789abcdef"
        key = ""
        keyIndex = -1
        for i in range(0, 6):
            c = signature[keyIndex + 1]
            key += c
            keyIndex = char_str.index(c)
            if keyIndex < 0 or keyIndex >= len(signature):
                keyIndex = i
        timestamp = str(int(random.random() * (9999 - 1000 + 1) + 1000)) + "_" + key + "_" + str(
            int(time.time() * 1000))
        return timestamp
