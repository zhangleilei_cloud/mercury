# -*- coding: utf-8 -*-

import json

from bs4 import BeautifulSoup

from mercury.spiders._base import Spider as BaseSpider
from mercury.models import Tips
from mercury.settings import logger
from mercury.models import JobQueue
from mercury.spiders.gov.gov_helper import update_deprecated_tips, get_base_telephone

domain_url = "http://www.gdzwfw.gov.cn"


class Spider(BaseSpider):
    name = "tips_guangdong"
    Model = Tips

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
            },
            "cookies": {},
            "sleep_range_before_request": None,
            "uniqid": "urlmd5",
            "save_html": False
        }
        return configs

    async def before_request(self):
        if self.job.is_seed:
            self.job.method = "post"
            if self.job.extra_data:
                referer = self.job.extra_data.get("Referer")
                if referer:
                    self.configs.get("headers").update({"Referer": referer})
            logger.info("set configs is ", self.configs)
        return True

    async def handle_download_failure(self, error):
        # 将下载失败的任务重新加入任务队列
        await JobQueue.rpush(self.job)

    async def handle_index(self, page):
        detail_infos, next_info = [], {}
        try:
            page_json = json.loads(page)
            if page_json["statusCode"] == '200':
                site_level = self.job.extra_data.pop("site_level", "")
                isupdate = self.job.extra_data.pop("isupdate", None)
                page_info = page_json["data"]["CUSTOM"]['PAGEINFO']
                data_list = page_json["data"]["CUSTOM"]['AUDIT_ITEMLIST']
                for item in data_list:
                    logger.info("item task name={0}".format(item["TASK_NAME"]))
                    extra_data = {"cate_xz": item['TASK_TYPE_TEXT'],
                                  "site_level": site_level,
                                  "title": item["TASK_NAME"],
                                  "isupdate": isupdate}
                    detail_infos.append(
                        {'url': domain_url + "/portal/guide/" + item['TASK_CODE'], 'extra_data': extra_data})

                if (int(page_info["CURRENTPAGEINDEX"]) * int(page_info["PAGESIZE"])) < page_info["TOTALNUM"]:
                    pn_new = "pageNum=" + str(int(page_info["CURRENTPAGEINDEX"]) + 1)
                    pn_old_ret = "pageNum=" + page_info["CURRENTPAGEINDEX"]
                    url_next = self.job.url.replace(pn_old_ret, pn_new)
                    extra_data = {"site_level": site_level, "Referer": self.job.extra_data.get("Referer"),
                                  "isupdate": isupdate}
                    next_info = {'url': url_next, 'extra_data': extra_data}
                else:
                    logger.info("list  finish")
                    await update_deprecated_tips(self.name, site_level)
                del page
        except Exception as e:
            logger.info("handle_index error configs is", self.configs)
            logger.info("handle_index error page is", page)
            logger.error(e, exc_info=True)
        return (detail_infos, next_info)

    async def handle_detail(self, page, encoding, extra_data=None):
        try:
            retval = extra_data
            retval.update({"getway": self.name, "reference": self.job.url, "sync": 0})
            soup = BeautifulSoup(page, 'lxml')
            detail_data = await self.get_detail_data(soup)
            if extra_data["title"] == "":
                titles = soup.select(".matters-affix-inner")
                for t in titles:
                    extra_data["title"] = t.find(class_="matters-truncate").string
            retval.update(detail_data)
            return retval
        except Exception as e:
            logger.info("handle_detail error detail== ", page)
            logger.error(e, exc_info=True)
            return None

    async def get_detail_data(self, soup):
        try:
            retval = {}
            extras = []

            matters_content_parts = soup.select(".matters-content-part")
            if matters_content_parts:
                for part in matters_content_parts:
                    h2 = part.find("h2")
                    if h2:
                        text = h2.string
                        if text == "基本信息":
                            base_info = dict(name="基本信息", content=part.prettify())
                            retval["base_info"] = base_info
                            trs = part.select("tr")
                            for tr in trs:
                                ths = tr.select("th")
                                tds = tr.select("td")
                                for index, th in enumerate(ths):
                                    if th.text == "实施主体":
                                        retval["organization"] = tds[index].text
                        elif text == "办理流程":
                            work_flow = dict(name="办理流程", content=part.prettify())
                            retval["work_flow"] = work_flow
                        elif text == "申请材料":
                            materials = dict(name="申请材料", content=part.prettify())
                            retval["materials"] = materials
                        elif text == "设定依据":
                            basis = dict(name="设定依据", content=part.prettify())
                            retval["basis"] = basis
                        else:
                            extra = dict(name=h2.string, content=part.prettify())
                            extras.append(extra)
                            if text == "咨询监督":
                                cols = part.select(".grid")[0].select(".col6")
                                for col in cols:
                                    h3 = col.find("h3")
                                    if h3.string == "咨询方式":
                                        telephone = col.find("p", class_="inquire-content")
                                        telephone_strs = await get_base_telephone(telephone.text)
                                        if len(telephone_strs) > 0:
                                            retval["telephone"] = telephone_strs[0]


            else:
                logger.error("can not find anything", exc_info=True)
                return None

            faq_boxs = soup.select(".matters-aside-box")
            for box in faq_boxs:
                title = box.find(class_="matters-aside-box-title")
                if title and title.string == "常见问题":
                    content = box.find("ul", class_="matters-aside-faq").string
                    if content == None:
                        content = "无"
                    faq = dict(name="常见问题", content=content)
                    retval["faq"] = faq
                if title and title.string == "主管部门":
                    dept = box.find("a")
                    if dept:
                        retval["department"] = dept.text
                if title and title.string == "咨询方式":
                    if "telephone" not in retval:
                        telephone = box.find("div", class_="matters-aside-box-bd").text
                        telephone_str = await get_base_telephone(telephone.strip())
                        retval["telephone"] = telephone_str
            visit_box = soup.find("ul", class_="matters-aside-promise")
            if visit_box:
                li_tags = visit_box.findAll("li")
                for li in li_tags:
                    if li.find(class_="matters-aside-promise-title").string == "到现场次数":
                        content = li.find("i").string
                        visit = dict(name="到现场次数", content=content)
                        retval["visits"] = visit
            retval["extras"] = extras
            return retval
        except Exception as e:
            logger.error(e, exc_info=True)
            return None
