# -*- coding: utf-8 -*-

import re
import time
import json
import random
import operator as op

from bs4 import BeautifulSoup

from mercury.models import Tips
from mercury.settings import logger
from mercury.spiders._base import Spider as BaseSpider
from ..gov_helper import update_deprecated_tips, update_dup_tip

domain_url = "http://www.sxzwfw.gov.cn"
headless_url = "http://127.0.0.1:8000/"
item_url = domain_url + "/icity/api-v2/app.icity.govservice.GovProjectCmd/getItemListByFolderCodeByPage?" \
                        "CAT=theme&ID"
sub_item_url = domain_url + "/icity/api-v2/app.icity.govservice.GovProjectCmd/getItemListByFolderCodeByPage?" \
                            "CAT=theme&ID"
detail_code_url = domain_url + "/icity/api-v2/app.icity.govservice.GovProjectCmd/getOndressByCode?"
detail_url = domain_url + "/icity/icity/proinfo?"


class Spider(BaseSpider):
    name = "tips_shanxi"

    Model = Tips

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "User-Agent": ("Mozilla/5.0 (Windows NT 6.1; Win64; x64) "
                               "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36")
            },
            "cookies": {},
            "sleep_range_before_request": None,
            "uniqid": "urlmd5",
            "save_html": False
        }
        return configs

    async def before_request(self):
        if self.job.is_seed:
            # index的访问市post请求
            self.job.method = "post"
            signature = self.job.extra_data["signature"]
            timestamp = await self.get_unique_timestamp(signature)
            s_pattern = re.compile(r'&s=[a-z0-9A-Z]+')
            t_pattern = re.compile(r'&t=.*')
            # 每次的s和t的值不同，第一次追加，非第一次替换s和t的值
            if re.search(s_pattern, self.job.url):
                self.job.url = re.sub(s_pattern, "&s=" + signature, self.job.url)
            else:
                self.job.url = self.job.url + "&s=" + signature
            if re.search(t_pattern, self.job.url):
                self.job.url = re.sub(t_pattern, "&t=" + timestamp, self.job.url)
            else:
                self.job.url = self.job.url + "&t=" + timestamp
        else:
            self.job.url = headless_url + self.job.url
        return True

    async def handle_index(self, page):
        detail_infos, next_info = [], {}
        try:
            site_level = self.job.extra_data.get("site_level")
            area_type = self.job.extra_data.get("area_type", None)
            bs_type = self.job.extra_data.get("area_type", None)
            signature = self.job.extra_data.get("signature", None)
            isupdate = self.job.extra_data.get("isupdate", None)
            index_url = self.job.url
            logger.info("[Spider]<{0}> index_url: {1}"
                        .format(self.name, index_url))
            page_json = json.loads(page)
            if area_type == "4":
                # 表示是乡镇
                detail_infos = await self.county_seeds(page_json, detail_infos)
            else:
                # 表示不是乡镇
                detail_infos = await self.not_county_seeds(page_json, detail_infos)
            total = page_json["total"]
            remain_page = total % 10
            page_num = int(total / 10)
            if remain_page != 0:
                page_num = page_num + 1
            if page_num > 1:
                current_page = int(re.search(r"\d+", re.search("page=\d+", index_url).group()).group())
                start = int(re.search(r"\d+", re.search("start=\d+", index_url).group()).group())
                if current_page < page_num:
                    pn_old = "page=" + str(current_page)
                    start_old = "start=" + str(start)
                    pn_new = "page=" + str(current_page + 1)
                    start_new = "start=" + str((start + 1) * 10)
                    url_next = index_url.replace(pn_old, pn_new)
                    url_next = url_next.replace(start_old, start_new)
                    extra_data = {"site_level": site_level, "area_type": area_type, "bs_type": bs_type,
                                  "signature": signature, "isupdate": isupdate}
                    next_info = {'url': url_next, 'extra_data': extra_data}
                if current_page == page_num:
                    await update_deprecated_tips(self.name, self.job.extra_data["site_level"])
                    # detail的访问是get请求
                    self.job.method = "get"
            else:
                # detail的访问是get请求
                self.job.method = "get"
            del page
        except Exception as e:
            logger.error(e, exc_info=True)
        return (detail_infos, next_info)

    async def handle_dup_detail(self):
        if not self.job.is_seed:
            await update_dup_tip(self.job, self.name)

    async def handle_detail(self, page, encoding, extra_data=None):
        detail_extra_data = []
        try:
            self.job.url = self.job.url.replace(headless_url, "")
            retval = dict(title=extra_data.pop("title", ""), getway=self.name)
            soup = BeautifulSoup(page, 'lxml')
            base_info_element = soup.find("table", class_="table-wordy")
            if base_info_element:
                base_info = dict(name="基本信息", content=base_info_element.prettify())
                item_type = soup.find("td", id="itemType")
                extra_data["cate_xz"] = item_type.get_text().strip()
                all_tds = base_info_element.find_all("td")
                for index, td in enumerate(all_tds):
                    if op.eq(td.get_text().strip(), "到现场办理次数"):
                        retval["visits"] = all_tds[index + 1].get_text().strip()
                    if op.eq(td.get_text().strip(), "权力部门") or op.eq(td.get_text().strip(), "权利部门"):
                        retval["organization"] = all_tds[index + 1].get_text().strip()
                        retval["department"] = all_tds[index + 1].get_text().strip()
                    if op.eq(td.get_text().strip(), "咨询电话"):
                        retval["telephone"] = all_tds[index + 1].get_text().strip()
            else:
                base_info = dict(name="基本信息", content="")
            retval["base_info"] = base_info

            li_list = soup.select(".yt-tabs")[0].find_all("li")
            for li in li_list:
                flag = 0
                li_id = li.a["href"].replace("#", "")
                li_text = li.a.get_text().strip()

                if li_text == "办理依据":
                    basis_element = soup.find("div", id=li_id)
                    if basis_element:
                        basis = dict(name=li_text, content=basis_element.prettify())
                    else:
                        basis = dict(name=li_text, content="")
                    retval["basis"] = basis
                    flag = 1

                if li_text == "申报材料":
                    materials_element = soup.find("div", id=li_id)
                    if materials_element:
                        materials = dict(name=li_text, content=materials_element.prettify())
                    else:
                        materials = dict(name=li_text, content="")
                    retval["materials"] = materials
                    flag = 1

                if li_text == "平台流程" or li_text == "办理程序":
                    work_flow_element = soup.find("div", id=li_id)
                    if work_flow_element:
                        work_flow = dict(name=li_text, content=work_flow_element.prettify())
                    else:
                        work_flow = dict(name=li_text, content="")
                    retval["work_flow"] = work_flow
                    flag = 1

                if li_text == "办理流程图" or li_text == "权力运行流程图":
                    work_flow_img_element = soup.find("div", id=li_id)
                    if work_flow_img_element:
                        if "work_flow" not in retval.keys():
                            work_flow = dict(name=li_text, content="")
                            retval["work_flow"] = work_flow
                        if work_flow_img_element.img:
                            retval["work_flow"]["picture"] = domain_url + work_flow_img_element.img["src"]
                        flag = 1

                if flag == 0:
                    extra_element = soup.find("div", id=li_id)
                    if extra_element:
                        extra = dict(name=li_text, content=extra_element.prettify())
                        detail_extra_data.append(extra)

            if retval and extra_data:
                retval.update({"reference": self.job.url, "extras": detail_extra_data, "sync": 0})
                retval.update({
                    "cate_parent": extra_data.pop("cate_parent", ""),
                    "cate_xz": extra_data.pop("cate_xz", ""),
                    "site_level": extra_data.pop("site_level", "")
                })
            return retval
        except Exception as e:
            logger.error(e, exc_info=True)
        return None

    async def get_unique_timestamp(self, signature):
        # 根据网站规则生成t,每一次请求t不同，每次请求需要重新生成
        char_str = "0123456789abcdef"
        key = ""
        keyIndex = -1
        for i in range(0, 6):
            c = signature[keyIndex + 1]
            key += c
            keyIndex = char_str.index(c)
            if keyIndex < 0 or keyIndex >= len(signature):
                keyIndex = i
        timestamp = str(int(random.random() * (9999 - 1000 + 1) + 1000)) + "_" + key + "_" + str(
            int(time.time() * 1000))
        return timestamp

    async def not_county_seeds(self, page_json, detail_infos):
        bs_type = self.job.extra_data["bs_type"]
        i = "1"
        if bs_type == "ent":
            i = "2"
        signature = self.job.extra_data["signature"]
        site_level = self.job.extra_data["site_level"]
        isupdate = self.job.extra_data.get("isupdate", None)
        for item in page_json["data"]:
            # 模仿点击，再次请求，重新赋值self.job.url，获取实际的种子页
            self.job.url = item_url + "&foldercode=" + item["CODE"] + \
                           "&PAGEMODEL=" + bs_type + "&s=" + signature + "&t=" + await  self.get_unique_timestamp(
                signature)
            page, encoding = await self._make_http_request()
            item_json = json.loads(page)
            for sub_item in item_json["data"]:
                self.job.url = detail_code_url + "code=" + sub_item["CODE"] + \
                               "&s=" + signature + "&t=" + await self.get_unique_timestamp(signature)
                # 再次请求，获取detail页面需要的id值
                page, encoding = await self._make_http_request()
                detail_json = json.loads(page)
                for detail_item in detail_json["data"]:
                    url = detail_url + "id=" + detail_item["ID"] + "&i=" + i
                    detail_infos.append(
                        {'url': url, 'extra_data': {"site_level": site_level, "title": detail_item["NAME"],
                                                    "cate_parent": detail_item["FOLDER_NAME"], "isupdate": isupdate}})
        return detail_infos

    async def county_seeds(self, page_json, detail_infos):
        isupdate = self.job.extra_data.get("isupdate", None)
        detail_head_url = re.search(".*api-v2", self.job.url).group()
        detail_head_url = detail_head_url.replace("api-v2", "")
        detail_head_url = detail_head_url + "/govservice/publicService/proinfo?id="
        for item in page_json["data"]:
            url = detail_head_url + item["ID"]
            detail_infos.append(
                {'url': url, 'extra_data': {"site_level": self.job.extra_data["site_level"], "title": item["NAME"],
                                            "cate_parent": item["NAME"], "isupdate": isupdate}})
        return detail_infos
