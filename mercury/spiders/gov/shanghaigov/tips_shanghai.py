# -*- coding: utf-8 -*-

import re
import json

from urllib import parse
from bs4 import BeautifulSoup

from mercury.models import Tips
from mercury.models import JobQueue
from mercury.settings import logger
from mercury.spiders._base import Spider as BaseSpider
from ..gov_helper import update_deprecated_tips, update_dup_tip

detail_url = "http://zwdt.sh.gov.cn/govPortals/bsfw/findBsfw.do"
seed_interval = 86400  # 1天
headless_url = "http://127.0.0.1:8000/"


class Spider(BaseSpider):
    name = "tips_shanghai"

    Model = Tips

    region_type_mapping = {"SH00SH": "",
                           "SH00PD": "浦东新区",
                           "SH00HP": "黄浦区",
                           "SH00JA": "静安区",
                           "SH00XH": "徐汇区",
                           "SH00CN": "长宁区",
                           "SH00PT": "普陀区",
                           "SH00HK": "虹口区",
                           "SH00YP": "杨浦区",
                           "SH00BS": "宝山区",
                           "SH00MH": "闵行区",
                           "SH00JD": "嘉定区",
                           "SH00JS": "金山区",
                           "SH00QP": "青浦区",
                           "SH00FX": "奉贤区",
                           "SH00CM": "崇明区",
                           }

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "User-Agent": ("Mozilla/5.0 (Windows NT 6.1; Win64; x64) "
                               "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36"),
                "Content-Type": "application/x-www-form-urlencoded",
                "Host": "zwdt.sh.gov.cn",
                "Origin": "http://zwdt.sh.gov.cn",
                "Referer": "http://zwdt.sh.gov.cn/govPortals/filterWorkIndex.do"
            },
            "cookies": {},
            "sleep_range_before_request": None,
            "uniqid": "urlmd5",
            "save_html": False
        }
        return configs

    async def before_request(self):
        if not self.job.is_seed:
            self.job.url = headless_url + self.job.url
        return True

    async def handle_download_failure(self, error):
        # 将下载失败的任务重新加入任务队列
        if not self.job.is_seed:
            self.job.url = self.job.url.replace(headless_url, "")
        await JobQueue.rpush(self.job)

    async def handle_index(self, page):
        detail_infos, next_info = [], {}
        try:
            page_json = json.loads(page)
            data = page_json["itemList"]
            site_level = self.job.extra_data.pop("site_level")
            cate_xz = self.job.extra_data.pop("cate_xz")
            isupdate = self.job.extra_data.get("isupdate", None)
            for value in data.values():
                for item in value:
                    extra_data = {"cate_xz": cate_xz, "site_level": site_level, "cate_parent": item['stItemName'],
                                  "title": item["stSubitemName"], "isupdate": isupdate}
                    region_param = parse.urlencode({"_organName_": self.region_type_mapping[site_level]})
                    item_type_param = parse.urlencode({"_itemType": item["stItemType"]})
                    if site_level != "SH00SH":
                        detail_infos.append(
                            {"url": "{0}?{1}&_organCode_={2}&_organType_=other&_itemId={3}&{4}&_stSubitemId="
                                .format(detail_url, region_param, site_level, item["stId"], item_type_param),
                             "extra_data": extra_data})
                    else:
                        detail_infos.append(
                            {"url": "{0}?_organName_=&_organCode_=&_organType_=&_itemId={1}&{2}&_stSubitemId="
                                .format(detail_url, item["stId"], item_type_param),
                             "extra_data": extra_data})

            if page_json["currentPage"] * page_json["pageSize"] < page_json["count"]:
                pn_new = "pageNumber={0}".format(str(page_json["currentPage"] + 1))
                pn_old_ret = "pageNumber={0}".format(str(page_json["currentPage"]))
                url_next = self.job.url.replace(pn_old_ret, pn_new)
                new_extra_data = {"site_level": site_level, "cate_xz": cate_xz, "isupdate": isupdate}
                next_info = {'url': url_next, 'extra_data': new_extra_data}
            if page_json["currentPage"] >= page_json["count"]:
                site_level = "上海市 {0}".format(self.region_type_mapping[site_level])
                await update_deprecated_tips(self.name, site_level)
            del page

        except Exception as e:
            logger.error(e, exc_info=True)
        return (detail_infos, next_info)

    async def handle_dup_detail(self):
        if not self.job.is_seed:
            await update_dup_tip(self.job, self.name)

    async def handle_detail(self, page, encoding, extra_data=None):
        self.job.url = self.job.url.replace(headless_url, "")
        retval = {}
        detail_extra_data = []
        try:
            soup = BeautifulSoup(page, 'lxml')
            logger.info("handle_detail extra_data:" + json.dumps(extra_data))
            retval = dict(title=extra_data["title"], getway=self.name)

            base_info_element = soup.select("#work-basic-info .work-box-content")
            if base_info_element:
                base_info = dict(name="基本信息", content=base_info_element[0].prettify())
                retval["base_info"] = base_info
                tds = base_info_element[0].find_all("td")
                for index, td in enumerate(tds):
                    if td.get_text().strip() == "实施主体":
                        retval["organization"] = tds[index + 1].get_text().strip()
                        break

            work_flow_element = soup.select("#work-handle-process .work-box-content")
            if work_flow_element:
                work_flow = dict(name="办理流程", content=work_flow_element[0].prettify())
                retval["work_flow"] = work_flow

            materials_element = soup.select("#work-apply-data .work-box-content")
            if materials_element:
                materials = dict(name="申请材料", content=materials_element[0].prettify())
                retval["materials"] = materials

            results_element = soup.select("#work-result-sample .work-box-content")
            if results_element:
                results = dict(name="审批证件", content=results_element[0].prettify())
                retval["results"] = results

            basis_element = soup.select("#work-basis-setting .work-box-content")
            if basis_element:
                basis = dict(name="设定依据", content=basis_element[0].prettify())
                retval["basis"] = basis

            fees_element = soup.select("#work-rule-fee .work-box-content")
            if fees_element:
                fees = dict(name="审批收费", content=fees_element[0].prettify())
                retval["fees"] = fees

            visits_element = dict(name="到现场次数", content="")
            retval["visits"] = visits_element

            faq_element = soup.select("#work-common-question .work-box-content")
            if faq_element:
                faq = dict(name="常见问题", content=faq_element[0].prettify())
                retval["faq"] = faq

            accept_element = soup.select("#work-accept-condition .work-box-content")
            if accept_element:
                accept = dict(name="受理条件", content=accept_element[0].prettify())
                detail_extra_data.append(accept)

            handle_mode_element = soup.select("#work-handle-mode .work-box-content")
            if handle_mode_element:
                handle_mode = dict(name="办理方式", content=handle_mode_element[0].prettify())
                detail_extra_data.append(handle_mode)

            work_right_element = soup.select("#work-right-duty .work-box-content")
            if work_right_element:
                work_right = dict(name="申请人权利和义务", content=work_right_element[0].prettify())
                detail_extra_data.append(work_right)

            ask_complain_element = soup.select("#work-ask-complain .work-box-content")
            if ask_complain_element:
                accept = dict(name="咨询投诉", content=ask_complain_element[0].prettify())
                detail_extra_data.append(accept)
                h3_pattern = re.compile(r'<\s*h3\s*>.*咨询电话.*<\s*h3\s*>')
                h3_split_pattern = re.compile(r'<\s*h3\s*>.*咨询电话.*<\s*/h3\s*>')
                if len(ask_complain_element[0].find_all("h3")) == 2:
                    ask_html = ask_complain_element[0].prettify().replace("\n", "")
                    if re.search(h3_pattern, ask_html):
                        ask_html = re.search(h3_pattern, ask_html).group()
                        ask_html = re.sub(h3_split_pattern, "", ask_html)
                        retval["telephone"] = ask_html.replace("<h3>", "").strip()

            place_element = soup.select("#work-handle-placetime .work-box-content")
            if place_element:
                place = dict(name="办理地点", content=place_element[0].prettify())
                detail_extra_data.append(place)

            if retval and extra_data:
                retval.update({"reference": self.job.url,
                               "extras": detail_extra_data, "sync": 0})
                cate_xz = extra_data.pop('cate_xz', "")
                if cate_xz == "行政审批":
                    cate_xz = "行政许可"
                retval.update({
                    "cate_xz": cate_xz,
                    "cate_parent": extra_data.pop('cate_parent', ""),
                    "site_level": "上海市 {0}".format(self.region_type_mapping[extra_data.pop("site_level", "")])
                })
        except Exception as e:
            logger.error(e, exc_info=True)
        return retval
