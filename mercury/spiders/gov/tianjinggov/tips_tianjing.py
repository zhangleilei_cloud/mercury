# -*- coding: utf-8 -*-

import json

from bs4 import BeautifulSoup

from mercury.spiders._base import Spider as BaseSpider
from mercury.models import Tips
from mercury.settings import logger
from mercury.models import JobQueue
from mercury.spiders.gov.gov_helper import update_deprecated_tips, get_base_telephone

domain_url = "http://zwfw.tj.gov.cn/tj_twfw"


class Spider(BaseSpider):
    name = "tips_tianjing"
    Model = Tips

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
            },
            "cookies": {},
            "sleep_range_before_request": None,
            "uniqid": "urlmd5",
            "save_html": False
        }
        return configs

    async def handle_download_failure(self, error):
        # 将下载失败的任务重新加入任务队列
        await JobQueue.rpush(self.job)

    async def handle_index(self, page):
        detail_infos, next_info = [], {}
        try:
            soup = BeautifulSoup(page, 'lxml')
            page_div = soup.find("div", class_="page")
            if page_div != None:
                page_spans = page_div.select("span")
                page_spans_length = len(page_spans)
                job_url = self.job.url
                tags_ = job_url.split("_")
                index = int(tags_[-1].split(".")[0])
                main = soup.find("div", class_="dxbt")
                site_level = self.job.extra_data.pop("site_level", "")
                isupdate = self.job.extra_data.pop("isupdate", None)
                tables = main.select("table")
                for table in tables:
                    detail_button = table.find("a", class_="show1")
                    detail_url = detail_button["href"]
                    detail_url = detail_url.replace("..", domain_url)
                    extra_data = {"isupdate": isupdate, "site_level": site_level}
                    detail_infos.append(
                        {'url': detail_url, 'extra_data': extra_data})
                # last page
                if page_spans_length == 6 and index != 1:
                    logger.info("tips tianjing one list finish")
                    await update_deprecated_tips(self.name, site_level)
                else:
                    url_next = tags_[0] + "_" + tags_[1] + "_" + tags_[2] + "_" + tags_[3] + "_" + str(index + 1) + ".html"
                    extra_data = {"isupdate": isupdate, "site_level": site_level}
                    next_info = {'url': url_next, 'extra_data': extra_data}
                del page
        except Exception as e:
            logger.info("handle_index error configs is", self.configs)
            logger.info("handle_index error page is", page)
            logger.error(e, exc_info=True)
        return (detail_infos, next_info)

    async def handle_detail(self, page, encoding, extra_data=None):
        try:
            retval = extra_data
            retval.update({"getway": self.name, "reference": self.job.url, "sync": 0})
            soup = BeautifulSoup(page, 'lxml')
            detail_data = await self.get_detail_data(soup)
            retval.update(detail_data)
            return retval
        except Exception as e:
            logger.info("tianjing  handle_detail error detail== ", page)
            logger.error(e, exc_info=True)
            return None

    async def get_detail_data(self, soup):
        try:
            retval = {}
            extras = []

            top_right_box_divs = soup.find_all("div", class_="top-right-box")
            title = top_right_box_divs[0].find("div", class_="tab01-title").text.strip()
            retval["title"] = title
            base_info_table = top_right_box_divs[0].find("table")
            for tr in base_info_table.select("tr"):
                for i, td in enumerate(tr.select("td")):
                    if td.text == "事项类型":
                        retval["cate_xz"] = tr.select("td")[i + 1].text.strip()
                    elif td.text == "办理部门":
                        retval["organization"] = tr.select("td")[i + 1].text.strip()
                    elif td.text == "咨询电话":
                        telephone = await get_base_telephone(tr.select("td")[i + 1].text.strip())
                        if len(telephone) > 0:
                            retval["telephone"] = telephone[0]
                    elif td.text == "办件公示":
                        tr.select("td")[i + 1]["colspan"] = 4
                        tr.select("td")[i + 2].attrs = {"style": "display:none"}

            base_info = dict(name="基本信息", content=base_info_table.prettify())
            retval["base_info"] = base_info
            tagContent_div = top_right_box_divs[1].find_all("div", id="tagContent")
            tag_contents = tagContent_div[0].find_all(class_="tagContent")
            for index, tagContent in enumerate(tag_contents):
                if index == 0:
                    for tr in tagContent.select("tr"):
                        for td in tr.select("td"):
                            for a in td.select("a"):
                                if a.text.strip() == "无":
                                    a.attrs = {}
                                    a.name = "span"
                    retval["materials"] = dict(name="申请材料", content=tagContent.prettify())
                elif index == 1:
                    text = tagContent.text
                    text = text.replace("\n", "<br\>")
                    tagContent.string = text
                    extra = dict(name="申请条件", content=tagContent.prettify())
                    extras.append(extra)
                elif index == 2:
                    extra = dict(name="办理形式", content=tagContent.prettify())
                    extras.append(extra)
                elif index == 3:
                    retval["basis"] = dict(name="设定依据", content=tagContent.prettify())
                elif index == 4:
                    text = tagContent.text
                    text = text.replace("\n", "<br\>")
                    tagContent.string = text
                    extra = dict(name="其他依据", content=tagContent.prettify())
                    extras.append(extra)
                elif index == 5:
                    retval["work_flow"] = dict(name="办理流程", content=tagContent.prettify())
                elif index == 6:
                    extra = dict(name="流程图", content=tagContent.prettify())
                    extras.append(extra)
            retval["extras"] = extras
            return retval
        except Exception as e:
            logger.error(e, exc_info=True)
            return None
