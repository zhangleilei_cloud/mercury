# -*- coding: utf-8 -*-
import json
import asyncio
import requests

from mercury.spiders._base import Spider as BaseSpider
from mercury.downloader import AsyncHttpDownloader
from mercury.models import Tips
from bs4 import BeautifulSoup
from mercury.models import JobQueue
from mercury.settings import logger
from ..gov_helper import update_deprecated_tips, update_dup_tip, get_base_telephone

domain_url = "http://banshi.beijing.gov.cn"
headless_url = "http://127.0.0.1:8000/"


class Spider(BaseSpider):
    name = "tips_beijing"

    Model = Tips

    nature_type_mapping = {"8655": "行政许可",
                           "8660": "行政处罚",
                           "8664": "行政强制",
                           "8658": "行政征收",
                           "8657": "行政给付",
                           "8665": "行政检查",
                           "8656": "行政确认",
                           "8663": "行政奖励",
                           "8659": "行政裁决",
                           "8662": "政府内部审批事项",
                           "8661": "其他职权",
                           "140973": "公共服务", }

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "User-Agent": (" Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:60.0)"
                               " Gecko/20100101 Firefox/60.0")
            },
            "cookies": {},
            "sleep_range_before_request": None,
            "uniqid": "urlmd5",
            "save_html": False
        }
        return configs

    async def before_request(self):
        if not self.job.is_seed:
            self.job.url = headless_url + self.job.url
        return True

    async def handle_download_failure(self, error):
        # 将下载失败的任务重新加入任务队列
        if not self.job.is_seed:
            self.job.url = self.job.url.replace(headless_url, "")
        await JobQueue.rpush(self.job)

    async def handle_index(self, page):
        detail_infos, next_info = [], {}
        try:
            page_json = json.loads(page)
            data = page_json["data"]
            site_level = self.job.extra_data.pop("site_level", "")
            isupdate = self.job.extra_data.pop("isupdate", None)
            for item in data['list']:
                extra_data = {"cate_xz": self.nature_type_mapping[item["natureType"]],
                              "natureType": item['natureType'],
                              "site_level": site_level,
                              "isupdate": isupdate}

                if item["childNode"]:
                    extra_data["cate_parent"] = item["title"]
                    for child in item["childNode"]:
                        extra_data["title"] = child["title"]
                        extra_data["isupdate"] = isupdate
                        detail_infos.append({'url': domain_url + child['guideUrl'], 'extra_data': extra_data})
                else:
                    extra_data["title"] = item["title"]
                    extra_data["isupdate"] = isupdate
                    detail_infos.append({'url': domain_url + item['guideUrl'], 'extra_data': extra_data})

            if data["pageNum"] < data["pages"]:
                pn_new = "pageNum=" + str(data["nextPage"])
                pn_old_ret = "pageNum=" + str(data["pageNum"])
                url_next = self.job.url.replace(pn_old_ret, pn_new)
                extra_data = {"site_level": site_level, "isupdate": isupdate}
                next_info = {'url': url_next, 'extra_data': extra_data}

            if data["pageNum"] == data["pages"]:
                await update_deprecated_tips(self.name, site_level)
            del page
        except Exception as e:
            logger.error(e, exc_info=True)
        return (detail_infos, next_info)

    async def handle_dup_detail(self):
        if not self.job.is_seed:
            await update_dup_tip(self.job, self.name)

    async def handle_detail(self, page, encoding, extra_data=None):
        try:
            self.job.url = self.job.url.replace(headless_url, "")
            retval = dict(title=extra_data["title"], getway=self.name, sync=0)
            soup = BeautifulSoup(page, 'lxml')
            base_info_ele = soup.select(".new_info_table")
            if base_info_ele:
                detail_data = await self.extract_detail_new_version(soup)
            else:
                detail_data = await self.extract_detail_old_version(soup)
            if not detail_data:
                return None
            retval.update(detail_data)

            if retval and extra_data:
                retval.update({"reference": self.job.url})
                retval.update({
                    "cate_xz": extra_data.pop("cate_xz", ""),
                    "cate_parent": extra_data.pop("cate_parent", ""),
                    "site_level": extra_data.pop("site_level", "")
                })

            return retval
        except Exception as e:
            logger.error(e, exc_info=True)
            return None

    async def extract_detail_new_version(self, soup):
        try:
            retval = {}
            base_info_ele = soup.select(".new_info_table")
            if base_info_ele:
                base_info = dict(name="基本信息", content=base_info_ele[0].prettify())
                retval["base_info"] = base_info
                self.get_department_organization_telphone(base_info_ele[0], retval)

            work_flow_ele = soup.select(".new_bllc_table")
            if work_flow_ele:
                work_flow = dict(name="办理流程", content=work_flow_ele[0].prettify())
                work_flow_pic_ele = soup.select(".new_list_title .a_img")
                if work_flow_pic_ele:
                    if work_flow_pic_ele[0].has_attr('href'):
                        a_img = f"{domain_url}{work_flow_pic_ele[0]['href']}"
                        work_flow["picture"] = a_img
                retval["work_flow"] = work_flow

            materials_ele = soup.select(".new_sqcl_table")
            if materials_ele:
                materials = dict(name="申请材料", content=materials_ele[0].prettify())
                retval["materials"] = materials
            if not base_info_ele or not work_flow_ele or not materials_ele:
                return None

            results_eles = soup.select(".newslist06")
            if results_eles:
                results = dict(name="结果名称及样本", content=[])
                for result_ele in results_eles:
                    result = dict(name=result_ele.dt.get_text(), content=result_ele.dd.table.prettify())
                    results["content"].append(result)
                retval["results"] = results

            basis_eles = soup.select(".newslist01")
            if basis_eles:
                basis = dict(name="设定依据", content=[])
                for basis_ele in basis_eles:
                    item = dict(name=basis_ele.dt.get_text(), content=basis_ele.dd.table.prettify())
                    basis["content"].append(item)
                retval["basis"] = basis

            fees_ele = soup.select(".new_sfbz_table")
            if fees_ele:
                fees = dict(name="收费标准及依据", content=fees_ele[0].prettify())
                retval["fees"] = fees

            visits_ele = soup.select(".new_num_time")
            if visits_ele:
                visits = dict(name="到现场次数", content=visits_ele[0].get_text())
                retval["visits"] = visits

            retval["faq"] = await self.get_faq(soup)
            return retval
        except Exception as e:
            logger.error(e, exc_info=True)
            return None

    async def get_faq(self, soup):
        faq = dict(name="常见问题", content="")
        try:
            faq_ele = soup.select(".new_changjian")
            if faq_ele:
                if faq_ele[0].has_attr('href'):
                    dl = self.dl or AsyncHttpDownloader(
                        headers=self.configs.get("headers"),
                        cookies=self.configs.get("cookies"))
                    page, encoding = await dl.fetch(
                        f"{headless_url}{faq_ele[0]['href']}", method=self.job.method,
                        ctype=self.job.ctype, data=self.job.payload,
                        rtype=self.job.rtype, auto_close=self.job.auto_close)
                    faq_page = BeautifulSoup(page, 'lxml')
                    elements = faq_page.select(".list_div div p")
                    for element in elements:
                        faq["content"] = faq["content"] + element.get_text()
        except Exception as e:
            logger.error(e, exc_info=True)
        return faq

    async def extract_detail_old_version(self, soup):
        try:
            retval = {}
            base_info_ele = soup.select(".info_table")
            if base_info_ele:
                base_info = dict(name="基本信息", content=base_info_ele[0].prettify())
                retval["base_info"] = base_info
                await self.get_department_organization_telphone(base_info_ele[0], retval)
            tab_con = soup.select(".tabCon > div")
            if tab_con:
                slyj_list = tab_con[0].select("dl")
                if slyj_list:
                    basis = dict(name="设定依据", content=[])
                    for slyj in slyj_list:
                        item = dict(name=slyj.dt.get_text(), content=slyj.dd.table.prettify())
                        basis["content"].append(item)
                    retval["basis"] = basis

                retval["extras"] = []
                sltj_list = tab_con[1].select("dl")
                if sltj_list:
                    sltj_data = dict(name="受理条件", content="")
                    for slyj in sltj_list:
                        sltj_data["content"] = f"{sltj_data['content']}{slyj.dd.get_text()}"
                    retval["extras"].append(sltj_data)

                bltj_list = tab_con[2].select("dl")
                if bltj_list:
                    bltj_data = dict(name="办理条件", content="")
                    for bltj in bltj_list:
                        bltj_data["content"] = f"{bltj_data['content']}{bltj.dd.get_text()}"
                    retval["extras"].append(bltj_data)

                work_flow_list = tab_con[3].select("dl")
                if work_flow_list:
                    work_flow = dict(name="办理流程", content=work_flow_list[0].dd.get_text())
                    src = work_flow_list[1].dd.img['src']
                    if src:
                        a_img = f"{domain_url}{src}"
                        work_flow["picture"] = a_img
                    retval["work_flow"] = work_flow

                materials_ele = tab_con[4]
                if materials_ele:
                    materials = dict(name="申请材料", content=materials_ele.prettify())
                    retval["materials"] = materials

                fees_ele = tab_con[5]
                if fees_ele:
                    fees = dict(name="收费标准及依据", content=fees_ele.prettify())
                    retval["fees"] = fees

                results_eles = tab_con[6].select("dl")
                if results_eles:
                    results = dict(name="结果名称及样本", content=[])
                    for result_ele in results_eles:
                        result = dict(name=result_ele.dt.get_text(), content=result_ele.dd.table.prettify())
                        results["content"].append(result)
                    retval["results"] = results

            return retval
        except Exception as e:
            logger.error(e, exc_info=True)
            return None

    async def get_department_organization_telphone(self, element, retval):
        trs = element.select("tr")
        for tr in trs:
            ths = tr.select("th")
            tds = tr.select("td")
            for index, th in enumerate(ths):
                if th.text == "实施机关":
                    retval["organization"] = tds[index].text
                if th.text == "实施主体":
                    retval["department"] = tds[index].text
                if th.text == "咨询电话":
                    telephone = await get_base_telephone(tds[index].text)
                    if len(telephone) > 0:
                        retval["telephone"] = telephone[0]

# if __name__ == "__main__":
#     page = requests.get("http://127.0.0.1:8000/http://banshi.beijing.gov.cn/newhall/bszn/5068/150981.html")
#     soup = BeautifulSoup(page.text, 'lxml')
#     loop = asyncio.get_event_loop()
#     loop.run_until_complete(extract_detail_old_version(soup))
