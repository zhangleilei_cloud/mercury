# -*- coding: utf-8 -*-

import re

from mercury.spiders._base import Spider as BaseSpider
from mercury.downloader import AsyncHttpDownloader
from mercury.models import Tips
from mercury.models import JobQueue
from mercury.settings import logger
from ..gov_helper import get_method_parameters, update_deprecated_tips, get_base_telephone
from bs4 import BeautifulSoup

domain_url = "http://zwfw.hunan.gov.cn"
detail_url = "http://zwfw.hunan.gov.cn/hnvirtualhall/serviceguide/jsp/serviceguideck.jsp?approve_id={0}"


class Spider(BaseSpider):
    name = "tips_hunan"
    Model = Tips

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "Host": "zwfw.hunan.gov.cn",
                "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit"
                              "/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36"
            },
            "cookies": {},
            "sleep_range_before_request": None,
            "uniqid": "urlmd5",
            "save_html": False
        }
        return configs

    async def handle_download_failure(self, error):
        # 将下载失败的任务重新加入任务队列
        await JobQueue.rpush(self.job)

    async def handle_index(self, page):
        detail_infos, next_info = [], {}
        try:
            site_level = self.job.extra_data.get("site_level", "")
            area_code = self.job.extra_data.get("area_code", "")
            isupdate = self.job.extra_data.get("isupdate")

            list_page = BeautifulSoup(page, 'lxml')

            current_page = 0
            total_page = 0
            zg_page_list = list_page.find_all("span", {"class": "zg_page"})
            for per_zg_page in zg_page_list:
                per_zg_page_html = per_zg_page.get_text()
                if re.match(r"\s*第\s*\d+\s*页\s*", per_zg_page_html):
                    com = re.compile(r"\d+")
                    current_page = int(com.findall(per_zg_page_html)[0])
                elif re.match(r"\s*共\s*\d+\s*页\s*", per_zg_page_html):
                    com = re.compile(r"\d+")
                    total_page = int(com.findall(per_zg_page_html)[0])

            if current_page == 0 or total_page == 0 or current_page > total_page:
                return

            list_content_list = list_page.find_all("div", {"class": "list_content"})
            for per_list_content in list_content_list:

                # get department
                department = ""
                cate_parent = ""
                list_content_top = per_list_content.find("div", {"class": "list_content_top"})
                if list_content_top:
                    list_content_top_h2 = list_content_top.find("h2")
                    if list_content_top_h2:
                        list_content_top_text = list_content_top_h2.get_text(strip=True)
                        cate_parent = list_content_top_text
                        department_start_index = list_content_top_text.find("【") + 1
                        department_end_index = list_content_top_text.find("】")
                        if department_start_index != 0 and department_end_index > department_start_index:
                            department = list_content_top_text[department_start_index:department_end_index]
                            cate_parent = list_content_top_text[:department_start_index-1]

                detail_extra_data_template = dict(
                    site_level=site_level,
                    area_code=area_code,
                    isupdate=isupdate,
                    department=department,
                    cate_parent=cate_parent
                )

                list_content_sub_item = per_list_content.find("input", {"name": "subPublicItem"})
                if list_content_sub_item:
                    list_content_sub_item_value = list_content_sub_item.get("value")
                    list_content_sub_item_orgid = list_content_sub_item.get("orgid")

                    sub_item_txt = await self.get_list_content_sub_item(area_code,
                                                                   list_content_sub_item_value,
                                                                   list_content_sub_item_orgid)
                    if not sub_item_txt:
                        continue

                    sub_item_page = BeautifulSoup(sub_item_txt, 'lxml')
                    sub_item_p_list = sub_item_page.find_all("p", {"class": "p1 pause_class"})
                    for per_sub_item_p in sub_item_p_list:
                        await self.append_detail_info(per_sub_item_p, detail_extra_data_template, detail_infos)
                else:
                    list_content_bottom_a = per_list_content.find("a", {"class": "a1 pause_class"})
                    if list_content_bottom_a:
                        await self.append_detail_info(list_content_bottom_a, detail_extra_data_template, detail_infos)

            if current_page < total_page:
                pn_new = "pager.offset=" + str(current_page*15)
                pn_old_ret = "pager.offset=" + str((current_page-1)*15)
                url_next = self.job.url.replace(pn_old_ret, pn_new)
                extra_data = {"site_level": site_level, "isupdate": isupdate}
                next_info = {'url': url_next, 'extra_data': extra_data}

            if current_page == total_page:
                await update_deprecated_tips(self.name, site_level)
            del page
        except Exception as e:
            logger.error(e, exc_info=True)
        return (detail_infos, next_info)

    async def append_detail_info(self, tag, detail_extra_data_template, detail_infos):
        onclick_text = tag.get("onclick")
        title = tag.get("title")
        if "checkGuide" in onclick_text:
            onclick_parameters = await get_method_parameters(onclick_text)
            if len(onclick_parameters) > 0:
                detail_id = onclick_parameters[0]
                detail_extra_data = detail_extra_data_template.copy()
                detail_extra_data.update(
                    detail_id=detail_id,
                    title=title
                )
                detail_infos.append(dict(
                    url=detail_url.format(detail_id),
                    extra_data=detail_extra_data
                ))

    async def get_list_content_sub_item(self, area_code, code, orgid):
        sub_item_url = "http://zwfw.hunan.gov.cn/hnvirtualhall/{0}/jsp/csserviceguidedata_public.jsp?" \
                       "code={1}&orgId={2}&areacode={3}"
        res = []
        try:
            dl = self.dl or AsyncHttpDownloader(
                headers=self.configs.get("headers"),
                cookies=self.configs.get("cookies"))
            find_by_directory_url = sub_item_url.format(area_code, code, orgid, area_code)
            res = await dl.fetch(
                find_by_directory_url, method="get",
                ctype=self.job.ctype, data=self.job.payload,
                rtype=self.job.rtype, auto_close=self.job.auto_close)
        except Exception as e:
            logger.error(e, exc_info=True)
            return None
        return res[0]

    async def handle_detail(self, page, encoding, extra_data=None):
        retval = {}
        try:
            site_level = extra_data.get("site_level", "")
            detail_id = extra_data.get("detail_id", "")
            if site_level == "" or detail_id == "":
                retval = None
            else:
                retval = await self.extract_detail(page)
                retval["reference"] = self.job.url
                retval["title"] = extra_data.get("title", "")
                retval["department"] = extra_data.get("department", "")
                retval["cate_parent"] = extra_data.get("cate_parent", "")
                retval["site_level"] = site_level
                retval["getway"] = self.name
        except Exception as e:
            logger.error(e, exc_info=True)
            return None
        retval["sync"] = 0
        return retval

    async def extract_detail(self, detail_page_txt):
        retval = {}
        detail_page = BeautifulSoup(detail_page_txt, 'lxml')

        # process visits
        visits = None
        part_tag = detail_page.find("div", {"class": "part-tag"})
        if part_tag:
            labels = part_tag.find_all("label")
            for label in labels:
                label_strings = list(label.stripped_strings)
                if len(label_strings) == 3:
                    label_name = label_strings[0]
                    label_content = "".join(label_strings[1:])
                    if "到现场次数" in label_name:
                        visits = dict(name=label_name, content=label_content)
                        break
        retval["visits"] = visits

        sxbt_divs = detail_page.find_all("div", {"class": "xbt2"})
        sxbt_names = []
        for sxbt_div in sxbt_divs:
            sxbt_names.append(sxbt_div.get_text(strip=True))

        sxnr_divs = detail_page.find_all("div", {"class": "sxnr"})

        extras = []
        result_content = []

        # process base_info, materials, work_flow, extra
        for i in range(len(sxbt_names)):
            sxbt_name = sxbt_names[i]
            if i > len(sxnr_divs) - 1:
                break
            sxnr_div = sxnr_divs[i]

            if sxbt_name == "事项介绍":
                if not retval.get("base_info"):
                    base_info_table = sxnr_div.find("table", {"class": "m-nrbox"})
                    if base_info_table:
                        retval["base_info"] = dict(name="基本信息", content=base_info_table.prettify())

                        # process cate_xz, department, organization, telephone
                        base_info_tds = base_info_table.find_all("td")
                        for i in range(len(base_info_tds)):
                            base_info_td = base_info_tds[i]
                            td_class = base_info_td.get("class")[0]
                            if td_class == "xbt" and i < len(base_info_tds) - 1:
                                current_td_text = base_info_td.get_text(strip=True)
                                next_td_text = base_info_tds[i + 1].get_text(strip=True)
                                if current_td_text == "事项类型":
                                    retval["cate_xz"] = next_td_text
                                if current_td_text == "实施机关":
                                    retval["organization"] = next_td_text
                                if current_td_text == "咨询电话":
                                    telephones = await get_base_telephone(next_td_text)
                                    if len(telephones) >= 1:
                                        retval["telephone"] = telephones[0]
                                if retval.get("cate_xz") and retval.get("organization") and retval.get("telephone"):
                                    break
            elif sxbt_name == "申请材料":
                if not retval.get("materials"):
                    mater_table = sxnr_div.find("table", {"id": "mater_tab"})
                    if mater_table:
                        a_tag_list = mater_table.find_all("a")
                        for a_tag in a_tag_list:
                            a_onclick_method = a_tag.get("onclick")
                            if a_onclick_method and "materialsamples" in a_onclick_method:
                                a_onclick_parameters = await get_method_parameters(a_onclick_method)
                                if len(a_onclick_parameters) == 3:
                                    del a_tag["onclick"]
                                    download_url = domain_url + "/uploadify?opt=queryAttach&material_id={0}&kind={1}&titel={2}"
                                    a_tag["href"] = download_url.format(a_onclick_parameters[1], a_onclick_parameters[0], a_onclick_parameters[2])

                        mater_text = mater_table.prettify()
                        mater_text = mater_text.replace("查看详情", "——")
                        retval["materials"] = dict(name="申请材料", content=mater_text)
            elif sxbt_name == "办理流程":
                if not retval.get("work_flow"):
                    flow_div = sxnr_div.find("div", {"id": "flowDiv"})
                    if flow_div:
                        work_flow_content = flow_div.prettify()
                        work_flow_img_path = ""
                        flow_img = flow_div.find("img", {"id": "flowImg"})
                        if flow_img:
                            work_flow_img_path = flow_img.get("value")
                        retval["work_flow"] = dict(name="办理流程", content=work_flow_content, picture=work_flow_img_path)
            elif sxbt_name == "结果样本":
                result_names = sxnr_div.find_all("div", {"class": "sxnrbt"})
                result_divs = sxnr_div.find_all("div", {"id": "resultDiv"})
                if len(result_divs) > 0 and len(result_divs) == len(result_names):
                    for k in range(len(result_divs)):
                        result_name = result_names[k].get_text(strip=True)
                        if len(result_name) > 0 and result_name != "无":
                            result_content.append(dict(
                                name=result_name,
                                content=result_divs[k].prettify()
                            ))
            elif sxbt_name == "常见问题":
                if not retval.get("faq"):
                    ask_ul_list = sxnr_div.find_all("ul", {"class": "ask-ul"})
                    if len(ask_ul_list) > 0:
                        retval["faq"] = dict(name="常见问题", content=sxnr_div.prettify())
            else:
                extras.append(dict(
                    name=sxbt_name,
                    content=sxnr_div.prettify()
                ))

        retval["extras"] = extras
        retval["results"] = dict(name="结果名称及样本", content=result_content)
        return retval
