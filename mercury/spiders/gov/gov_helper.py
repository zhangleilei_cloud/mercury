# -*- coding: utf-8 -*-
import re

from mercury.libs.mongo import mgdb
from mercury.utils import timestamp
from mercury.settings import logger

seed_interval = 86400  # 1天


async def update_deprecated_tips(name, site_level):
    try:
        db = mgdb.tips
        time_interval = seed_interval * 2 * 1000
        should_update = f"(new Date(ISODate().getTime()- {time_interval}) >= this.update_time)"

        await db.update_many({"$where": should_update, "getway": name, "status": "New", "site_level": site_level},
                             {'$set': {"status": "Deprecated"}})
    except Exception as e:
        logger.error(e, exc_info=True)


async def update_dup_tip(job, name):
    try:
        db = mgdb.tips
        await db.update_one({"urlmd5": job.urlmd5, "getway": name},
                            {'$set': {"update_time": timestamp(), "status": "New"}})
    except Exception as e:
        logger.error(e, exc_info=True)


async def get_base_telephone(str):
    number_pattern = re.compile(r'\d{3,4}\s*[-|－|—]\s*\d{7,8}|\d{11,12}|\d{4}\s*[-|－|—]\s*\d{5}')
    result = number_pattern.findall(str)
    if len(result) == 0:
        telephone_no_area_code_pattern = re.compile(r"\d{8}")
        result = telephone_no_area_code_pattern.findall(str)
    return result


async def get_method_parameters(method_string):
    parameters = []
    parameter_start_index = method_string.find("(") + 1
    parameter_end_index = method_string.find(")")
    if parameter_start_index == 0 or parameter_end_index == -1:
        return parameters
    chars = list(method_string[parameter_start_index:parameter_end_index])
    parameter_start = False
    parameter_start_index = 0
    for i, char in enumerate(chars):
        if char == "'":
            parameter_start = not parameter_start
            if parameter_start:
                parameter_start_index = i
            else:
                parameters.append("".join(chars[parameter_start_index + 1:i]))
    return parameters
