# -*- coding: utf-8 -*-

import operator as op

from bs4 import BeautifulSoup

from mercury.models import Tips
from mercury.models import JobQueue
from mercury.settings import logger
from mercury.spiders._base import Spider as BaseSpider
from ..gov_helper import update_deprecated_tips, update_dup_tip

headless_url = "http://127.0.0.1:8000/"


class Spider(BaseSpider):
    name = "tips_hebei"

    Model = Tips

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "User-Agent": ("Mozilla/5.0 (Windows NT 6.1; Win64; x64) "
                               "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36")
            },
            "cookies": {},
            "sleep_range_before_request": None,
            "uniqid": "urlmd5",
            "save_html": False
        }
        return configs

    async def before_request(self):
        if not self.job.is_seed:
            self.job.url = headless_url + self.job.url
        return True

    async def handle_download_failure(self, error):
        # 将下载失败的任务重新加入任务队列
        if not self.job.is_seed:
            self.job.url = self.job.url.replace(headless_url, "")
        await JobQueue.rpush(self.job)

    async def handle_index(self, page):
        detail_infos, next_info = [], {}
        try:
            soup = BeautifulSoup(page, 'lxml')
            site_level = self.job.extra_data.pop("site_level", "")
            isupdate = self.job.extra_data.get("isupdate", None)
            # 表示是乡镇 解析方式不一样 乡镇目前没有翻页
            type = self.job.extra_data.pop("type", "")
            if op.eq(type, "4"):
                logger.debug("这是乡镇")
                if soup.select(".service_item"):
                    td_list = soup.select(".service_item")[0].find_all("td", class_="item_name")
                    for td in td_list:
                        extra_data = {"cate_parent": td.a.get_text(), "title": td.a.get_text(),
                                      "site_level": site_level, "isupdate": isupdate}
                        detail_infos.append({'url': td.a["href"], 'extra_data': extra_data})
            # 表示省区市县
            else:
                logger.debug("不是乡镇")
                tybs_list = soup.find_all("li", class_="tybs_list")
                for r3_item in tybs_list:
                    a_href = r3_item.select(".r3")[0].find("a")
                    extra_data = {"cate_parent": a_href.get_text(), "title": a_href.get_text(),
                                  "site_level": site_level, "isupdate": isupdate}
                    detail_infos.append({'url': a_href["href"], 'extra_data': extra_data})
                bmfw_right_cell = soup.find_all("li", class_="bmfw_right_cell")
                for item in bmfw_right_cell:
                    parent_title = item.select(".r1")[0].select(".r1_tit")[0].get_text()
                    for r2_item in item.find_all("div", class_="r2"):
                        extra_data = {"cate_parent": parent_title, "title": r2_item.find("a").get_text(),
                                      "site_level": site_level}
                        detail_infos.append({'url': r2_item.find("a")["href"], 'extra_data': extra_data})
                page_li = soup.select("#page")[0].find_all("li")
                total_page = len(page_li)
                # total_page为0表示只有一页 不再翻页
                if total_page != 0:
                    for li in page_li:
                        # 当前页
                        if li.select(".right_page_aOn"):
                            current_page = li.select(".right_page_aOn")
                            if current_page < total_page:
                                pn_new = "page_num=" + str(int(current_page) + 1)
                                pn_old_ret = "page_num=" + str(int(current_page))
                                url_next = self.job.url.replace(pn_old_ret, pn_new)
                                extra_data = {"site_level": site_level, "isupdate": isupdate}
                                next_info = {'url': url_next, 'extra_data': extra_data}
                            if current_page == total_page:
                                await update_deprecated_tips(self.name, site_level)
                            break
            del page
        except Exception as e:
            logger.error(e, exc_info=True)
        return (detail_infos, next_info)

    async def handle_dup_detail(self):
        if not self.job.is_seed:
            await update_dup_tip(self.job, self.name)

    async def handle_detail(self, page, encoding, extra_data=None):
        try:
            self.job.url = self.job.url.replace(headless_url, "")
            retval = dict(title=extra_data.pop("title", ""), getway=self.name)
            soup = BeautifulSoup(page, 'lxml')
            id_map = {}
            detail_extra_data = []
            main_tab_menu = soup.select(".main_tab_menu")
            if main_tab_menu:
                spans = main_tab_menu[0].find_all("span")
                for span in spans:
                    item_id = span["id"].replace("one", "#con-one-")
                    id_map[item_id] = span.get_text().strip()

            for key in id_map:
                flag = 0
                if id_map[key] == "基本信息":
                    base_info_element = soup.select(key)
                    if base_info_element:
                        base_info = dict(name=id_map[key], content=base_info_element[0].prettify())
                        if base_info_element[0].select("table"):
                            all_tds = base_info_element[0].select("table")[0].find_all("td")
                            for index, td in enumerate(all_tds):
                                if op.eq(td.get_text().strip(), "事项类型") or op.eq(td.get_text().strip(), "权力事项类型"):
                                    if all_tds[index + 1].get_text().strip() is not "":
                                        extra_data["cate_xz"] = all_tds[index + 1].get_text().strip()
                                if op.eq(td.get_text().strip(), "到现场次数"):
                                    retval["visits"] = all_tds[index + 1].get_text().strip()
                                if op.eq(td.get_text().strip(), "实施单位") or op.eq(td.get_text().strip(), "实施机关"):
                                    if all_tds[index + 1].get_text().strip() is not "":
                                        retval["organization"] = all_tds[index + 1].get_text().strip()
                                if op.eq(td.get_text().strip(), "咨询电话"):
                                    retval["telephone"] = all_tds[index + 1].get_text().strip()
                    else:
                        base_info = dict(name=id_map[key], content="")
                    retval["base_info"] = base_info
                    flag = 1
                if id_map[key] == "受理材料" or id_map[key] == "办理材料":
                    materials_element = soup.select(key)
                    if materials_element:
                        materials = dict(name=id_map[key], content=materials_element[0].prettify())
                    else:
                        materials = dict(name=id_map[key], content="")
                    retval["materials"] = materials
                    flag = 1

                if id_map[key] == "办理流程":
                    work_flow_element = soup.select(key)
                    if work_flow_element:
                        work_flow = dict(name=id_map[key], content=work_flow_element[0].prettify())
                    else:
                        work_flow = dict(name=id_map[key], content="")
                    retval["work_flow"] = work_flow
                    flag = 1

                if id_map[key] == "办理流程图":
                    work_flow_img_element = soup.select(key)
                    if work_flow_img_element:
                        if work_flow_img_element and work_flow_img_element[0].img:
                            retval["work_flow"]["picture"] = work_flow_img_element[0].img["src"]
                    flag = 1

                if id_map[key] == "收费标准及依据" or id_map[key] == "收费情况":
                    fees_element = soup.select(key)
                    if fees_element:
                        fees = dict(name=id_map[key], content=fees_element[0].prettify())
                    else:
                        fees = dict(name=id_map[key], content="")
                    retval["fees"] = fees
                    flag = 1

                if id_map[key] == "实施依据" or id_map[key] == "设定依据":
                    basis_element = soup.select(key)
                    if basis_element:
                        basis = dict(name=id_map[key], content=basis_element[0].prettify())
                    else:
                        basis = dict(name=id_map[key], content="")
                    retval["basis"] = basis
                    flag = 1

                if id_map[key] == "办理结果样本" or id_map[key] == "办结结果样本":
                    results_element = soup.select(key)
                    if results_element:
                        results = dict(name=id_map[key], content=results_element[0].prettify())
                    else:
                        results = dict(name=id_map[key], content="")
                    retval["results"] = results
                    flag = 1

                if flag == 0:
                    extra_element = soup.select(key)
                    if extra_element:
                        extra = dict(name=id_map[key], content=extra_element[0].prettify())
                        detail_extra_data.append(extra)

            if retval and extra_data:
                retval.update({"reference": self.job.url, "extras": detail_extra_data, "sync": 0})
                retval.update({
                    "cate_parent": extra_data.pop("cate_parent", ""),
                    "cate_xz": extra_data.pop("cate_xz", ""),
                    "site_level": extra_data.pop("site_level", "")
                })
            return retval
        except Exception as e:
            logger.error(e, exc_info=True)
        return None
