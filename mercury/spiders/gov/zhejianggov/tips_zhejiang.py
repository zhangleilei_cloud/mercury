# -*- coding: utf-8 -*-

import re
import json
import time
import random
import requests
import operator as op

from bs4 import BeautifulSoup

from mercury.models import Tips
from mercury.settings import logger
from mercury.models import JobQueue
from mercury.spiders._base import Spider as BaseSpider
from ..gov_helper import update_deprecated_tips, update_dup_tip


class Spider(BaseSpider):
    name = "tips_zhejiang"

    Model = Tips

    detail_url_head = "http://www.zjzwfw.gov.cn/zjzw/item/detail/item_detail.do?"

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {},
            "cookies": {},
            "sleep_range_before_request": None,
            "uniqid": "urlmd5",
            "save_html": False
        }
        return configs

    async def before_request(self):
        if self.job.is_seed:
            self.job.url = self.job.url + "&_=" + str(int(time.time() * 1000))
            logger.info("[Spider]<{0}> self.job.url: {1}"
                        .format("tips_zhejiang", self.job.url))
        return True

    async def handle_download_failure(self, error):
        # 将下载失败的任务重新加入任务队列
        await JobQueue.rpush(self.job)

    async def handle_index(self, page):
        detail_infos, next_info = [], {}
        try:
            isupdate = self.job.extra_data.get("isupdate", None)
            site_level = self.job.extra_data.pop("site_level", "")
            organization = self.job.extra_data.pop("dept_name", "")
            list_soup = BeautifulSoup(page, 'lxml')
            rdbs_list = list_soup.find_all("div", class_="rdbs_list")
            for rdbs in rdbs_list:
                rdbs_list_titmain = rdbs.find_all("div", class_="rdbs_list_titmain")[0]
                if rdbs_list_titmain.select(".rdbs_list_tit_haveChild"):
                    await self.list_index_has_child(rdbs_list_titmain, rdbs, site_level, detail_infos, organization,
                                                    isupdate)
                else:
                    await self.list_index_no_child(rdbs, site_level, detail_infos, organization, isupdate)
            if list_soup.find("input", class_="default_pgCurrentPage") and list_soup.find("span",
                                                                                          class_="default_pgTotalPage"):
                current_page = list_soup.find("input", class_="default_pgCurrentPage")["value"]
                total_page = list_soup.find("span", class_="default_pgTotalPage").get_text()
                if current_page != "" and total_page != "":
                    if current_page < total_page:
                        pn_new = "pageno=" + str(int(current_page) + 1)
                        pn_old_ret = "pageno=" + str(int(current_page))
                        url_next = self.job.url.replace(pn_old_ret, pn_new)
                        extra_data = {"site_level": site_level, "isupdate": isupdate, "dept_name": organization}
                        next_info = {'url': url_next, 'extra_data': extra_data}
                    if current_page == total_page:
                        await update_deprecated_tips(self.name, site_level)
            del page
        except Exception as e:
            logger.error(e, exc_info=True)
        return (detail_infos, next_info)

    async def handle_dup_detail(self):
        if not self.job.is_seed:
            await update_dup_tip(self.job, self.name)

    async def handle_detail(self, page, encoding, extra_data=None):
        try:
            retval = extra_data
            retval.update({"getway": self.name, "reference": self.job.url, "sync": 0})
            detail_data = {}
            id_map = {}
            detail_extra_data = []
            soup = BeautifulSoup(page, 'lxml')
            base_info_ele = soup.select(".td_topleft")[0].parent.parent
            if base_info_ele:
                base_info = dict(name="基本信息", content=base_info_ele.prettify())
                detail_data["base_info"] = base_info
                tds = base_info_ele.find_all("td")
                for index, td in enumerate(tds):
                    if td.get_text().strip() == "权力事项类型" or td.get_text().strip() == "事项类型":
                        detail_data["cate_xz"] = tds[index + 1].get_text().strip()
                    if td.get_text().strip() == "受理机构" or td.get_text().strip() == "主管部门" or td.get_text().strip() == "实施机关":
                        detail_data["department"] = tds[index + 1].get_text().strip()
                    if td.get_text().strip() == "联系电话" or td.get_text().strip() == "咨询电话":
                        detail_data["telephone"] = tds[index + 1].get_text().strip()

                pattern = re.compile(r"\d+")
                visits_ele = base_info_ele.select("#bszd")
                if visits_ele:
                    visit_times_list = pattern.findall(visits_ele[0].get_text())
                    if visit_times_list:
                        visits = dict(name="到现场次数", content=visit_times_list[0].strip())
                    else:
                        visits = dict(name="到现场次数", content="")
                    detail_data["visits"] = visits
                else:
                    visits_ele = base_info_ele.select("#cs")
                    if visits_ele:
                        visit_times_list = pattern.findall(visits_ele[0].get_text())
                        if visit_times_list:
                            visits = dict(name="到现场次数", content=visit_times_list[0].strip())
                        else:
                            visits = dict(name="到现场次数", content="")
                        detail_data["visits"] = visits
                    else:
                        visits = dict(name="到现场次数", content="")
                        detail_data["visits"] = visits

            dang_qian = soup.select(".td_dangqian")[0]
            item_id = dang_qian["id"].replace("focusC", "#focusCInfo")
            id_map[item_id] = dang_qian.get_text()
            more = soup.select(".td_moren")
            for key in more:
                if not (key.has_attr("style") and op.contains(key["style"], "display: none")):
                    item_id = key["id"].replace("focusC", "#focusCInfo")
                    id_map[item_id] = key.get_text()

            for key in id_map:
                flag = 0
                if id_map[key] == "办理流程":
                    work_flow_ele = soup.select(key)[0]
                    if work_flow_ele.select("table"):
                        work_flow = dict(name=id_map[key], content=work_flow_ele.select("table")[0].prettify())
                    elif work_flow_ele:
                        work_flow = dict(name=id_map[key], content=work_flow_ele.prettify())
                    else:
                        work_flow = dict(name=id_map[key], content="")
                    detail_data["work_flow"] = work_flow
                    flag = 1

                if id_map[key] == "申请材料" or id_map[key] == "办理材料":
                    materials_ele = soup.select(key)[0]
                    if materials_ele.select("table"):
                        materials = dict(name=id_map[key], content=materials_ele.select("table")[0].prettify())
                    elif materials_ele:
                        materials = dict(name=id_map[key], content=materials_ele.prettify())
                    else:
                        materials = dict(name=id_map[key], content="")
                    flag = 1
                    detail_data["materials"] = materials

                results = dict(name="结果名称及样本", content="")
                detail_data["results"] = results

                if id_map[key] == "法定依据" or id_map[key] == "实施依据":
                    basis_eles = soup.select(key)[0]
                    if basis_eles.select("table"):
                        basis = dict(name=id_map[key], content=basis_eles.select("table")[0].prettify())
                    elif basis_eles:
                        basis = dict(name=id_map[key], content=basis_eles.prettify())
                    else:
                        basis = dict(name=id_map[key], content="")
                    flag = 1
                    detail_data["basis"] = basis

                if id_map[key] == "收费情况":
                    fees_ele = soup.select(key)[0]
                    if fees_ele.select("table"):
                        fees = dict(name=id_map[key], content=fees_ele.select("table")[0].prettify())
                    elif fees_ele:
                        fees = dict(name=id_map[key], content=fees_ele.prettify())
                    else:
                        fees = dict(name=id_map[key], content="")
                    flag = 1
                    detail_data["fees"] = fees

                if id_map[key] == "常见问题解答":
                    faq_ele = soup.select(key)[0]
                    if faq_ele.select("table"):
                        faq = dict(name=id_map[key], content=faq_ele.select("table")[0].prettify())
                    elif faq_ele:
                        faq = dict(name=id_map[key], content=faq_ele.prettify())
                    else:
                        faq = dict(name=id_map[key], content="")
                    flag = 1
                    detail_data["faq"] = faq

                if flag == 0:
                    extra_ele = soup.select(key)[0]
                    if extra_ele.select("table"):
                        extra = dict(name=id_map[key], content=extra_ele.select("table")[0].prettify())
                        detail_extra_data.append(extra)
                    elif extra_ele:
                        extra = dict(name=id_map[key], content=extra_ele.prettify())
                        detail_extra_data.append(extra)

            if retval and detail_extra_data:
                retval["extras"] = detail_extra_data
                retval.update(detail_data)
            return retval

        except Exception as e:
            logger.error(e, exc_info=True)
            return None

    async def list_index_has_child(self, rdbs_list_titmain, rdbs, site_level, detail_infos, organization, isupdate):
        cate_parent = rdbs_list_titmain.select(".rdbs_list_tit_haveChild")[0].get_text()
        rdbs_list_subtitmain = rdbs.find_all("div", class_="rdbs_list_subtitmain")
        for sub_rdbsin in rdbs_list_subtitmain:
            if sub_rdbsin.select(".rdbs_list_subtit"):
                onclick = sub_rdbsin.select(".rdbs_list_subtit")[0].a["onclick"]
                title = sub_rdbsin.select(".rdbs_list_subtit")[0].a.get_text()
                await self.list_index_common(site_level, detail_infos, onclick, cate_parent, title, organization,
                                             isupdate)

    async def list_index_no_child(self, rdbs, site_level, detail_infos, organization, isupdate):
        rdbs_list_tit = rdbs.find_all("div", class_="rdbs_list_tit")
        if rdbs_list_tit:
            onclick = rdbs_list_tit[0].a["onclick"]
            title = rdbs_list_tit[0].a.get_text()
            await self.list_index_common(site_level, detail_infos, onclick, "", title, organization, isupdate)

    async def list_index_common(self, site_level, detail_infos, onclick, cate_parent, title, organization, isupdate):
        if cate_parent == "":
            cate_parent = title
        onclick = onclick[14:len(onclick) - 1]
        onclick = onclick.replace("'", "")
        onclick_array = onclick.split(",")
        if not (len(onclick_array) == 3 and onclick_array[2] == '3'):
            tmp_detail_url = self.detail_url_head + "itemid=" + onclick_array[0] + "&webid=" + \
                             onclick_array[1] + "&dotype=1&rand=" + str(random.random())
            # 获取到url，还需要请求返回实际的detail_url
            response = requests.post(tmp_detail_url)
            if response.status_code == 200 and response.text and "http" in response.text:
                extra_data = {"title": title, "site_level": site_level, "cate_parent": cate_parent,
                              "organization": organization, "reference": response.text, "isupdate": isupdate}
                detail_infos.append({'url': extra_data["reference"], 'extra_data': extra_data})
                logger.debug("<{0}>tmp_detail_url<{1}>real_url<{2}>".format(self.name, tmp_detail_url,
                                                                            extra_data["reference"]))
