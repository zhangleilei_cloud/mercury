# -*- coding: utf-8 -*-
import json
import re
import bs4

from mercury.spiders._base import Spider as BaseSpider
from mercury.downloader import AsyncHttpDownloader
from mercury.models import Tips
from mercury.models import JobQueue
from mercury.settings import logger
from ..gov_helper import update_deprecated_tips, update_dup_tip, get_base_telephone
from bs4 import BeautifulSoup

domain_url = "http://www.sczwfw.gov.cn"
thing_url = "http://www.sczwfw.gov.cn/app/thing"


class Spider(BaseSpider):
    name = "tips_sichuan"
    Model = Tips

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "Host": "www.sczwfw.gov.cn",
                "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit"
                              "/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36"
            },
            "cookies": {},
            "sleep_range_before_request": None,
            "uniqid": "urlmd5",
            "save_html": False,
            "proxy_enable": True
        }
        return configs

    async def handle_download_failure(self, error):
        # 将下载失败的任务重新加入任务队列
        await JobQueue.rpush(self.job)

    async def handle_index(self, page):
        detail_infos, next_info = [], {}
        try:
            site_level = self.job.extra_data.pop("site_level", "")
            isupdate = self.job.extra_data.pop("isupdate", None)
            page_json = json.loads(page)
            paging = page_json["paging"]
            area_code = paging["areaCode"]
            rows = page_json["rows"]
            for row in rows:
                # 之前网站返回的数据结构是外层list点击后再发起另一个请求获取下层list的数据
                th_directory_name = row.get("thDirectoryName", "")
                th_directory_id = row.get("thDirectoryId", "")
                if len(th_directory_name) > 0 and len(th_directory_id) > 0:
                    # 根据thDirectoryId查展开的list
                    th_find_array = await self.find_by_directory(area_code, th_directory_id)
                    th_find_array_json = json.loads(th_find_array[0])
                    if th_find_array_json.get("code", -1) == 0:
                        th_find_array_res = th_find_array_json.get("result", [])
                        for item in th_find_array_res:
                            detail_extra_data = dict(cate_parent=th_directory_name, site_level=site_level,
                                                     isupdate=isupdate)
                            detail_extra_data["title"] = item["eventName"]
                            item_id = item["id"]
                            detail_extra_data["detail_id"] = item_id
                            detail_url = "{0}/app/workGuide/detail?id={1}".format(domain_url, item_id)
                            detail_infos.append({'url': detail_url, 'extra_data': detail_extra_data})
                else:
                    detail_extra_data = dict(cate_parent=row.get("dirName", ""), site_level=site_level,
                                             isupdate=isupdate)
                    detail_extra_data["title"] = row["eventName"]
                    row_id = row["idForStr"]
                    detail_extra_data["detail_id"] = row_id
                    detail_url = "{0}/app/workGuide/detail?id={1}".format(domain_url, row_id)
                    detail_infos.append({'url': detail_url, 'extra_data': detail_extra_data})

            if paging["currentPage"] < paging["totalPage"]:
                pn_new = "page=" + str(paging["currentPage"] + 1)
                pn_old_ret = "page=" + str(paging["currentPage"])
                url_next = self.job.url.replace(pn_old_ret, pn_new)
                extra_data = {"site_level": site_level, "isupdate": isupdate}
                next_info = {'url': url_next, 'extra_data': extra_data}

            if paging["currentPage"] == paging["totalPage"]:
                await update_deprecated_tips(self.name, site_level)
            del page
        except Exception as e:
            logger.error(e, exc_info=True)
        return (detail_infos, next_info)

    # async def handle_dup_detail(self):
    #     if not self.job.is_seed:
    #         await update_dup_tip(self.job, self.name)

    async def handle_detail(self, page, encoding, extra_data=None):
        retval = {}
        try:
            site_level = extra_data.pop("site_level", "")
            detail_id = extra_data.pop("detail_id", "")
            if site_level == "" or detail_id == "":
                retval = None
            else:
                retval = await self.extract_page(page, detail_id)
                retval["reference"] = self.job.url
                retval["title"] = extra_data.pop("title", "")
                retval["cate_parent"] = extra_data.pop("cate_parent", "")
                retval["site_level"] = site_level
                retval["getway"] = self.name
        except Exception as e:
            logger.error(e, exc_info=True)
        retval["sync"] = 0
        return retval

    async def find_by_directory(self, area_code, directory_id):
        res = []
        try:
            dl = self.dl or AsyncHttpDownloader(
                headers=self.configs.get("headers"),
                cookies=self.configs.get("cookies"))
            find_by_directory_url = thing_url + "/findByThDirectory?areaCode={0}&thDirectoryId={1}".format(area_code,
                                                                                                           directory_id)
            res = await dl.fetch(
                find_by_directory_url, method="get",
                ctype=self.job.ctype, data=self.job.payload,
                rtype=self.job.rtype, auto_close=self.job.auto_close)
        except Exception as e:
            logger.error(e, exc_info=True)
        return res

    async def extract_page(self, page, detail_id):
        page_soup = BeautifulSoup(page, 'lxml')
        retval = {}
        # process cate_xz
        event_type_input = page_soup.find("input", {"id": "eventTypeShow"})
        if event_type_input:
            retval["cate_xz"] = event_type_input.get("value").strip()

        # find base_info
        if retval["cate_xz"] in ["行政许可", "行政裁决", "行政确认", "行政给付", "行政奖励", "其他行政权力"]:
            content_div = page_soup.find("div", {"class": "content"})
            content_children_div = await self.get_children_with_tag(content_div, "div")
            for i in range(0, len(content_children_div)):
                # div class == zn_more 的上一个div是存储基本信息的div
                content_child = content_children_div[i]
                content_child_classes = content_child.get("class")
                if content_child_classes:
                    if "zn_more" in content_children_div[i].get("class"):
                        retval["base_info"] = dict(name="基本信息", content=content_children_div[i - 1].prettify())
                        await self.get_department_organization_telphone(content_children_div[i - 1], retval)
                        break
        elif retval["cate_xz"] in ["行政处罚", "行政强制", "行政征收", "行政检查"]:
            header_div = page_soup.find("div", {"class": "header_main"})
            tables = await self.get_children_with_tag(header_div, "table")
            if len(tables) > 0:
                retval["base_info"] = dict(name="基本信息", content=tables[0].prettify())
                await self.get_department_organization_telphone(tables[0], retval)
        # process 设定依据,收费标准,申请材料,常见问题办理流程,extra
        bszn_tab_div = page_soup.find("div", {"class": "bszn_tab"})
        bszn_tab_spans = bszn_tab_div.find("h3").findAll("span")
        extras = []
        for bszn_tab_span in bszn_tab_spans:
            tab_id = bszn_tab_span.get("tab-id")
            text = bszn_tab_span.get_text().strip()
            if text in ["设定依据", "法定依据"]:
                # process basis
                sdyj_table = bszn_tab_div.find("div", {"id": tab_id}).find("table")
                if sdyj_table:
                    retval["basis"] = dict(name="设定依据", content=await self.process_url(sdyj_table.prettify()))
            elif text in ["收费标准及依据", "收费标准"]:
                # process fees
                sfbz_table = bszn_tab_div.find("div", {"id": tab_id}).find("table")
                if sfbz_table:
                    retval["fees"] = dict(name="收费标准及依据", content=await self.process_url(sfbz_table.prettify()))
            elif text in ["申请材料"]:
                # process materials
                sqcl_table = bszn_tab_div.find("div", {"id": tab_id}).find("table")
                if sqcl_table:
                    sqcl_content = await self.process_url(sqcl_table.prettify())
                    sqcl_content = sqcl_content.replace("下载", "——")
                    retval["materials"] = dict(name="申请材料", content=sqcl_content)
            elif text in ["常见问题"]:
                # process faq
                cjwt_table = bszn_tab_div.find("div", {"id": tab_id}).find("table")
                if cjwt_table:
                    retval["faq"] = dict(name="常见问题", content=await self.process_url(cjwt_table.prettify()))
            elif text in ["办理流程", "流程图"]:
                # process work_flow
                if text == "办理流程":
                    bllc_div = bszn_tab_div.find("div", {"id": tab_id})
                    if bllc_div:
                        bllc_path = domain_url + "/app/egov/web/workGuide/flowCharts.html?id=" + detail_id
                        retval["work_flow"] = dict(name="办理流程", content="", picture=bllc_path)
                elif text == "流程图":
                    img = bszn_tab_div.find("div", {"id": tab_id}).find("img")
                    if img:
                        complete_path_img = BeautifulSoup(await self.process_url(img.prettify()), "lxml").find("img")
                        retval["work_flow"] = dict(name="办理流程", content="", picture=complete_path_img.get("src"))
            else:
                # others put extra
                other_div = bszn_tab_div.find("div", {"id": tab_id})
                if other_div:
                    extras.append(dict(name=text, content=await self.process_url(other_div.prettify())))
        retval["extras"] = extras

        # process visits
        visits = None
        times_div = page_soup.find("div", {"class": "times fr"})
        if times_div:
            bs_num_spans = times_div.findAll("span", {"class": "bs_num"})
            for span in bs_num_spans:
                tag_h = span.find("h1")
                tag_p = span.find("p")
                if tag_h and tag_p and tag_p.get_text().strip() == "到现场次数":
                    visits = dict(name=tag_p.get_text(), content=tag_h.get_text())
                    break
        retval["visits"] = visits
        retval["results"] = dict(name="结果名称及样本", content=[])
        return retval

    async def get_children_with_tag(self, tag, name=None):
        tag_children = tag.contents
        res = []
        if len(tag_children) > 0:
            for child_tag in tag_children:
                if isinstance(child_tag, bs4.element.Tag):
                    if name:
                        if child_tag.name == name:
                            res.append(child_tag)
                    else:
                        res.append(child_tag)
        return res

    async def process_url(self, string):
        p = r'(src\s*=\s*\"(?!http))|(href\s*=\s*\"(?!http))'
        string = re.sub(p, lambda m: m.group(0) + domain_url, string)
        return string

    async def get_department_organization_telphone(self, element, retval):
        trs = element.select("tr")
        for tr in trs:
            tds = tr.select("td")
            for index, td in enumerate(tds):
                if td.text == "实施主体" and index + 1 < len(tds):
                    retval["organization"] = tds[index + 1].text
                if (td.text == "咨询电话" or td.text == "咨询方式") and index + 1 < len(tds):
                    telephone = await get_base_telephone(tds[index + 1].text)
                    if len(telephone) == 0:
                        retval["telephone"] = "12345"
                    else:
                        retval["telephone"] = telephone[0]
