# -*- coding: utf-8 -*-
import datetime
import json
import os

from mercury.spiders._base import Spider as BaseSpider
from mercury.downloader import AsyncHttpDownloader
from mercury.models import Tips
from mercury.models import JobQueue
from mercury.settings import logger
from ..gov_helper import update_deprecated_tips, update_dup_tip

DIR_SCRIPT = os.path.dirname(os.path.abspath(__file__))

site_path = os.path.join(DIR_SCRIPT, "base_info_template.html")

domain_url = "http://center.lnzwfw.gov.cn"
child_list_main_url = "http://www.lnzwfw.gov.cn/matter/Dept"


class Spider(BaseSpider):
    name = "tips_liaoning"
    Model = Tips

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
            },
            "cookies": {},
            "sleep_range_before_request": None,
            "uniqid": "urlmd5",
            "save_html": False
        }
        return configs

    async def handle_download_failure(self, error):
        # 将下载失败的任务重新加入任务队列
        await JobQueue.rpush(self.job)

    async def handle_index(self, page):
        detail_infos, next_info = [], {}
        try:
            site_level = self.job.extra_data.pop("site_level", "")
            page_json = json.loads(page)
            if page_json["code"] == 0:
                total_page = page_json["result"]["totalPage"]
                current_page = page_json["result"]["currPage"]
                rows = page_json["result"]["list"]
                for row in rows:
                    # 之前网站返回的数据结构是外层list点击后再发起另一个请求获取下层list的数据
                    cate_parent = row.get("ql_name", "")
                    item_code = row.get("item_code", "")
                    # 根据item_code查展开的children list
                    children = await self.find_children(item_code)

                    children_json = json.loads(children[0])
                    if children_json["code"] == 0:
                        for child in children_json["result"]:
                            detail_extra_data = dict(cate_parent=cate_parent, site_level=site_level)
                            item_code = child["item_code"]
                            item_guid = child["itemguid"]
                            detail_extra_data["item_code"] = item_code
                            detail_extra_data["item_guid"] = item_guid
                            detail_url = "{0}/api/web/public/toApplyWSBS?id={1}&code={2}".format(domain_url, item_guid,
                                                                                                 item_code)
                            detail_infos.append({'url': detail_url, 'extra_data': detail_extra_data})

                if current_page < total_page:
                    pn_new = "pageNum=" + str(current_page + 1)
                    pn_old_ret = "pageNum=" + str(current_page)
                    url_next = self.job.url.replace(pn_old_ret, pn_new)
                    extra_data = {"site_level": site_level}
                    next_info = {'url': url_next, 'extra_data': extra_data}

                if current_page == total_page:
                    await update_deprecated_tips(self.name, site_level)
                del page

        except Exception as e:
            logger.info("handle_index error configs is", self.configs)
            logger.info("handle_index error page is", page)
            logger.error(e, exc_info=True)
        return (detail_infos, next_info)

    async def handle_dup_detail(self):
        if not self.job.is_seed:
            await update_dup_tip(self.job, self.name)

    async def handle_detail(self, page, encoding, extra_data=None):
        try:
            retval = extra_data
            retval.update({"getway": self.name, "reference": self.job.url})
            extras = []
            item_code = extra_data["item_code"]
            item_guid = extra_data["item_guid"]

            detail_content = await self.get_detail_content(item_code)

            detail_materials = await self.get_detail_materials(item_guid)

            detail_agency = await self.get_detail_agency(item_guid)

            detail_approve_list = await self.get_detail_approve_list(item_code)

            detail_question = await self.get_detail_question(item_guid)

            detail_charge_item = await self.get_detail_charge_item(item_guid)

            content_json = json.loads(detail_content)
            content_html = self.get_content_html(content_json)

            materials_html = self.get_materials_html(detail_materials)

            agency_html = self.get_agency_html(detail_agency)

            question_html = self.get_question_html(detail_question)

            charge_item_html = self.get_charge_item_html(detail_charge_item)

            approve_html = self.get_approve_html(detail_approve_list)

            work_flow_html = self.get_work_flow_html(content_json)

            approval_result_html = self.get_approval_result_html(content_json)

            relief_way_html = self.get_relief_way_html(content_json)

            application_condition_html = self.get_application_condition_html(content_json)

            if content_html == "":
                logger.error("base_info error", exc_info=True)
                return None

            retval["title"] = content_json["Dept"]["ql_name_yw"]

            ql_kind = content_json["Dept"]["ql_kind"]  # 职权类型

            ql_kind_dict = {"01": "行政许可", "04": "行政征收", "05": "行政给付", "07": "行政确认", "20": "公共服务", "10": "其他权力",
                            "99": "内部审批"}

            if ql_kind in ql_kind_dict:
                ql_kind = ql_kind_dict[ql_kind]
            else:
                ql_kind = "其它"

            retval["cate_xz"] = ql_kind
            base_info = dict(name="基本信息", content=content_html)
            retval["base_info"] = base_info

            if materials_html != "":
                materials = dict(name="申请材料", content=materials_html)
                retval["materials"] = materials

            if charge_item_html != "":
                fees = dict(name="收费标准及依据", content=charge_item_html)
                retval["fees"] = fees

            if work_flow_html != "":
                work_flow = dict(name="办理流程", content=work_flow_html)
                retval["work_flow"] = work_flow

            if approval_result_html != "设定依据":
                results = dict(name="结果名称及样本", content=approval_result_html)
                retval["results"] = results

            visits = dict(name="到现场次数", content=content_json["DeptExpand"]["applyermin_count"])
            retval["visits"] = visits

            if question_html != "":
                faq = dict(name="常见问题", content=question_html)
                retval["faq"] = faq

            if application_condition_html != "":
                extra = dict(name="申请条件", content=application_condition_html)
                extras.append(extra)

            if relief_way_html != "":
                extra = dict(name="救济途径", content=relief_way_html)
                extras.append(extra)

            if approve_html != "":
                extra = dict(name="办件公示", content=approve_html)
                extras.append(extra)

            if agency_html != "":
                extra = dict(name="特殊环节", content=agency_html)
                extras.append(extra)

            retval["extras"] = extras
            return retval
        except Exception as e:
            logger.info("handle_detail error extra_data== ", extra_data)
            logger.info("handle_detail error detail== ", page)
            logger.error(e, exc_info=True)

    async def find_children(self, catalog_code):
        result = None
        try:
            dl = self.dl or AsyncHttpDownloader(
                headers=self.configs.get("headers"),
                cookies=self.configs.get("cookies"))
            child_list_url = child_list_main_url + "/findByCatalogCode?qlObject=10&catalogCode={0}".format(catalog_code)
            result = await dl.fetch(
                child_list_url, method="get",
                ctype=self.job.ctype, data=self.job.payload,
                rtype=self.job.rtype, auto_close=self.job.auto_close)
        except Exception as e:
            logger.error(e, exc_info=True)
        return result

    async def get_detail_content(self, item_code):  # 基本信息+申请条件+办理流程+审批结果+救济途径++
        result = None
        try:
            dl = self.dl or AsyncHttpDownloader(
                headers=self.configs.get("headers"),
                cookies=self.configs.get("cookies"))
            content_url = domain_url + "/api/web/matter/getContent?id={0}".format(item_code)
            result = await dl.fetch(
                content_url, method="get",
                ctype=self.job.ctype, data=self.job.payload,
                rtype=self.job.rtype, auto_close=self.job.auto_close)

        except Exception as e:
            logger.error(e, exc_info=True)
        return result[0]

    async def get_detail_materials(self, item_guid):  # 申请材料
        result = None
        try:
            dl = self.dl or AsyncHttpDownloader(
                headers=self.configs.get("headers"),
                cookies=self.configs.get("cookies"))
            materials_url = domain_url + "/api/web/matter/ywfile?itemGuId={0}".format(item_guid)
            result = await dl.fetch(
                materials_url, method="get",
                ctype=self.job.ctype, data=self.job.payload,
                rtype=self.job.rtype, auto_close=self.job.auto_close)
        except Exception as e:
            logger.error(e, exc_info=True)
        return result[0]

    async def get_detail_agency(self, item_guid):  # 特殊环节
        result = None
        try:
            dl = self.dl or AsyncHttpDownloader(
                headers=self.configs.get("headers"),
                cookies=self.configs.get("cookies"))
            agency_url = domain_url + "/api/web/matter/Agency?itemGuId={0}".format(item_guid)
            result = await dl.fetch(
                agency_url, method="get",
                ctype=self.job.ctype, data=self.job.payload,
                rtype=self.job.rtype, auto_close=self.job.auto_close)
        except Exception as e:
            logger.error(e, exc_info=True)
        return result[0]

    async def get_detail_approve_list(self, item_code):  # 办件公示
        result = None
        try:
            end_time = datetime.datetime.now()
            start_time = end_time + datetime.timedelta(days=-30)
            end_time_formatter = end_time.strftime('%Y-%m-%d %H:%M:%S')
            start_time_formatter = start_time.strftime('%Y-%m-%d %H:%M:%S')

            dl = self.dl or AsyncHttpDownloader(
                headers=self.configs.get("headers"),
                cookies=self.configs.get("cookies"))
            approve_list_url = domain_url + "/api/web/public/approveList?page={0}&limit={1}&itemCode={2}&receviedTimeStart={3}&receviedTimeEnd={4}".format(
                1, 100000, item_code, start_time_formatter, end_time_formatter)
            result = await dl.fetch(
                approve_list_url, method="post",
                ctype=self.job.ctype, data=self.job.payload,
                rtype=self.job.rtype, auto_close=self.job.auto_close)
        except Exception as e:
            logger.error(e, exc_info=True)
        return result[0]

    async def get_detail_question(self, item_guid):  # 常见问题
        result = None
        try:
            dl = self.dl or AsyncHttpDownloader(
                headers=self.configs.get("headers"),
                cookies=self.configs.get("cookies"))
            question_url = domain_url + "/api/web/matter/Question?itemGuId={0}".format(item_guid)
            result = await dl.fetch(
                question_url, method="get",
                ctype=self.job.ctype, data=self.job.payload,
                rtype=self.job.rtype, auto_close=self.job.auto_close)
        except Exception as e:
            logger.error(e, exc_info=True)
        return result[0]

    async def get_detail_charge_item(self, item_guid):  # 收费情况
        result = None
        try:
            dl = self.dl or AsyncHttpDownloader(
                headers=self.configs.get("headers"),
                cookies=self.configs.get("cookies"))
            charge_item_url = domain_url + "/api/web/matter/ChargeItem?itemGuId={0}".format(item_guid)
            result = await dl.fetch(
                charge_item_url, method="get",
                ctype=self.job.ctype, data=self.job.payload,
                rtype=self.job.rtype, auto_close=self.job.auto_close)
        except Exception as e:
            logger.error(e, exc_info=True)
        return result[0]

    def get_materials_html(self, materials):
        materials_html = ""
        if materials:
            materials_json = json.loads(materials)
            if materials_json["code"] == 0 and len(materials_json["result"]) > 0:
                materials_html = '<table border="0" class="xl_talbe" cellpadding="0" cellspacing="0">'
                materials_html += '<tr>'
                materials_html += '<th width="64">序号</th>'
                materials_html += '<th width="128">材料名称</th>'
                materials_html += '<th width="115">材料来源渠道</th>'
                materials_html += '<th width="186">法定依据描述</th>'
                materials_html += '<th width="77">纸质原件<p>份数</p></th>'
                materials_html += '<th width="77">纸质复印<p>件及份数</p></th>'
                materials_html += '<th width="116">纸质原件扫描<p>电子版上传</p></th>'
                materials_html += '<th width="119">空白表格下载</th>'
                materials_html += '<th width="104">样例下载</th>'
                materials_html += '<th width="65">备注</th>'
                materials_html += '</tr>'
                for i, item in enumerate(materials_json["result"]):
                    materials_html += '<tr>'
                    materials_html += '<td>' + str(i + 1) + '</td>'
                    materials_html += '<td>' + item["materialname"] + '</td>'

                    file_source = "其他"
                    if item["file_source"] == "1":
                        file_source = "申请人自备"
                    elif item["file_source"] == "2":
                        file_source = "政府部门核发"

                    materials_html += '<td>' + file_source + '</td>'
                    materials_html += '<td>' + item["lawbasis"] + '</td>'
                    materials_html += '<td>' + str(item["page_num"]) + '</td>'
                    materials_html += '<td>' + str(item["zzfyj_page_num"]) + '</td>'

                    zzyjsmdzb = "无"
                    if item["zzyjsmdzb"] == "0":
                        zzyjsmdzb = "否"
                    elif item["zzyjsmdzb"] == "1":
                        zzyjsmdzb = "是"
                    materials_html += '<td>' + zzyjsmdzb + '</td>'

                    materialemptytable = item["materialemptytable"]
                    if len(materialemptytable) > 0:
                        materialemptytables = materialemptytable.split(",")
                        materials_html += '<td>'
                        for table in materialemptytables:
                            file_name_sp = table.split("/")
                            file_name = file_name_sp[len(file_name_sp) - 1]

                            file_url_sp = table.split("nas")
                            file_url = "http://www.lnzwfw.gov.cn/xdfile/nas" + file_url_sp[1]
                            materials_html += '<a href="' + file_url + '" target="_blank">' + file_name + '</a><br/><br/>'
                        materials_html += '</td>'
                    else:
                        materials_html += '<td>无</td>'

                    materialexampletable = item["materialexampletable"]
                    if len(materialexampletable) > 0:
                        materialexampletables = materialexampletable.split(",")
                        materials_html += '<td>'
                        for table in materialexampletables:
                            file_name_sp = table.split("/")
                            file_name = file_name_sp[len(file_name_sp) - 1]

                            file_url_sp = table.split("nas")
                            file_url = "http://www.lnzwfw.gov.cn/xdfile/nas" + file_url_sp[1]
                            materials_html += '<a href="' + file_url + '" target="_blank">' + file_name + '</a><br/><br/>'
                        materials_html += '</td>'
                    else:
                        materials_html += '<td>无</td>'

                    materials_html += '<td>' + item["baknote"] + '</td>'
                    materials_html += '</tr>'
                materials_html += '</table>'

        return materials_html

    def get_approve_html(self, approve):
        approve_list_html = ""
        if approve:
            approve_json = json.loads(approve)
            if approve_json["code"] == "200" and len(approve_json["result"]["list"]) > 0:
                approve_list_html = '<table border="0" class="xl_talbe" cellpadding="0" cellspacing="0">'
                approve_list_html += '<tr>'
                approve_list_html += '<th width="467">申报人</th>'
                approve_list_html += '<th width="165">申报时间</th>'
                approve_list_html += '<th width="182">受理时间</th>'
                approve_list_html += '<th width="182">办结时间</th>'
                approve_list_html += '<th width="230">状态</th>'
                approve_list_html += '</tr>'
                for item in approve_json["result"]["list"]:
                    approve_list_html += '<tr>'
                    approve_list_html += '<td style="text-align: center;">' + item["appPersonName"] + '</td>'
                    approve_list_html += '<td>' + item["receviedTime"] + '</td>'
                    approve_list_html += '<td>' + item["startTime"] + '</td>'
                    approve_list_html += '<td>' + item["completeTime"] + '</td>'
                    approve_list_html += '<td>' + item["insState"] + '</td>'
                    approve_list_html += '</tr>'
                approve_list_html += '</table>'

        return approve_list_html

    def get_agency_html(self, agency):
        agency_html = ""
        if agency:
            agency_json = json.loads(agency)
            if agency_json["code"] == 0 and len(agency_json["result"]) > 0:
                agency_html = '<table border="0" class="xl_talbe" cellpadding="0" cellspacing="0">'
                agency_html += "<tr>"
                agency_html += '<th width="64">序号</th>'
                agency_html += '<th width="228">中介服务或特殊环节名称</th>'
                agency_html += '<th width="152">法律依据及描述</th>'
                agency_html += '<th width="140">实施机构</th>'
                agency_html += '<th width="140">是否收费</th>'
                agency_html += '<th width="143">收费依据及描述</th>'
                agency_html += '<th>办理时限</th>'
                agency_html += '</tr>'
                for i, item in enumerate(agency_json["result"]):
                    agency_html += '<tr>'
                    agency_html += '<td>' + str(i + 1) + '</td>'
                    agency_html += '<td>' + item["zjfwtshj_name"] + '</td>'
                    agency_html += '<td>' + item["lawbasis"] + '</td>'
                    agency_html += '<td>' + item["ssjc"] + '</td>'
                    charge_flag = "是" if item["charge_flag"] == "0" else "否"
                    agency_html += '<td>' + charge_flag + '</td>'
                    agency_html += '<td>' + item["charge_law"] + '</td>'
                    agency_html += '<td>' + item["promise_day"] + '</td>'
                    agency_html += '</tr>'
                agency_html += '</table>'

        return agency_html

    def get_question_html(self, question):
        question_html = ""
        if question:
            question_json = json.loads(question)
            if question_json["code"] == 0 and len(question_json["result"]) > 0:
                question_html = '<table border="0" class="xl_talbe" cellpadding="0" cellspacing="0">'
                question_html += '<tr>'
                question_html += '<th width="539">常见错误示例</th>'
                question_html += '<th>常见问题解答</th>'
                question_html += '</tr>'
                for item in question_json["result"]:
                    question_html += '<tr>'
                    question_html += '<td>' + item["question"] + '</td>'
                    question_html += '<td>' + item["answer"] + '</td>'
                    question_html += '</tr>'

                question_html += '</table>'

        return question_html

    def get_charge_item_html(self, charge_item):
        charge_item_html = ""
        if charge_item:
            charge_item_json = json.loads(charge_item)
            if charge_item_json["code"] == 0 and len(charge_item_json["result"]) > 0:
                charge_item_html = '<table border="0" class="xl_talbe" cellpadding="0" cellspacing="0">'
                charge_item_html += '<tr>'
                charge_item_html += '<th width="64">序号</th>'
                charge_item_html += '<th width="300">收费项目</th>'
                charge_item_html += '<th width="300">收费依据</th>'
                charge_item_html += '<th>收费标准</th>'
                charge_item_html += '</tr>'
                for i, item in enumerate(charge_item_json["result"]):
                    charge_item_html += '<tr>'
                    charge_item_html += '<td>' + str(i + 1) + '</td>'
                    charge_item_html += '<td>' + item["charge_name"] + '</td>'
                    charge_item_html += '<td>' + item["charge_law"] + '</td>'
                    charge_item_html += '<td>' + item["charge_stand"] + '</td>'
                    charge_item_html += '</tr>'

                charge_item_html += '</table>'

        return charge_item_html

    def get_content_html(self, data):
        if data:
            if data["code"] == 0:
                fc = open(site_path, "r", encoding="utf-8")
                html = fc.read()
                fc.close()

                dept = data["Dept"]  # 获取项目详细内容

                deptExpand = data["DeptExpand"]  # 获取事项详细内容扩展

                ql_name = dept["ql_name"]  # 事项名称

                ql_name_yw = dept["ql_name_yw"]  # 业务名称

                item_code = dept["item_code"]  # 事项编码

                ql_kind = dept["ql_kind"]  # 职权类型

                ql_kind_dict = {"01": "行政许可", "04": "行政征收", "05": "行政给付", "07": "行政确认", "20": "公共服务", "10": "其他权力",
                                "99": "内部审批"}

                if ql_kind in ql_kind_dict:
                    ql_kind = ql_kind_dict[ql_kind]
                else:
                    ql_kind = "其它"

                itemsource = deptExpand["itemsource"]  # 权力来源

                itemsource_dict = {"1": "法定本级行使", "2": "中央下放到省级", "3": "省级下放到设区市", "4": "省级下放到县（市、区）",
                                   "5": "设区市下放到县（市、区）",
                                   "6": "中央下放到设区市",
                                   "7": "中央下放到县（市、区）", "8": "其它"}

                if itemsource in itemsource_dict:
                    itemsource = itemsource_dict[itemsource]
                else:
                    itemsource = "其它"

                shiquancj = dept["shiquancj"]  # 行使层级

                shiquancj_dict = {"2": "省级", "3": "市级", "4": "县级"}

                if shiquancj in shiquancj_dict:
                    shiquancj = shiquancj_dict[shiquancj]
                else:
                    shiquancj = "其它"

                bjtype = dept["bjtype"]  # 办理类型

                bjtype_dict = {"1": "即办件", "2": "承诺件"}

                if bjtype in bjtype_dict:
                    bjtype = bjtype_dict[bjtype]
                else:
                    bjtype = "其它"

                ql_object = dept["ql_object"]  # 服务对象及领域

                ql_object_dict = {"10": "法人", "20": "自然人", "10,20": "法人,自然人", "20,10": "自然人,法人"}

                if ql_object in ql_object_dict:
                    ql_object = ql_object_dict[ql_object]
                else:
                    ql_object = "其它"

                lawbasis = dept["lawbasis"] + dept["ql_lawbasis"]  # 审批依据

                qxhf = dept["qxhf"]  # 权限划分

                sljg = dept["sljg"]  # 受理机构

                sljgxz = dept["sljgxz"]  # 受理机构性质

                jgxz_dict = {"1": "法定机关", "2": "授权组织", "3": "受委托组织", "4": "其它"}

                if sljgxz in jgxz_dict:
                    sljgxz = jgxz_dict[sljgxz]
                else:
                    sljgxz = "其它"

                jdjg = dept["jdjg"]  # 决定机构

                jdjgxz = dept["jdjgxz"]  # 决定机构性质

                if jdjgxz in jgxz_dict:
                    jdjgxz = jgxz_dict[jdjgxz]
                else:
                    jdjgxz = "其它"

                xsnr = dept["xsnr"]  # 行使内容

                tongbanmode = deptExpand["tongbanmode"]  # 通办范围

                tongbanmode_dict = {"10": "全国", "20": "跨省", "30": "跨市", "40": "跨县"}

                if tongbanmode in tongbanmode_dict:
                    tongbanmode = tongbanmode_dict[tongbanmode]
                else:
                    tongbanmode = "其它"

                sflhsp = deptExpand["sflhsp"]  # 联合审批

                sflhsp = "是" if sflhsp == "1" else "否"

                blxs = dept["blxs"]  # 办理形式

                blxs_dict = {"1": "网上办理", "2": "窗口办理", "3": "网上或窗口办理", "4": "其他"}

                if blxs in blxs_dict:
                    blxs = blxs_dict[blxs]
                else:
                    blxs = "其它"

                if_appoint = deptExpand["if_appoint"]  # 支持预约办理

                if_appoint = "是" if if_appoint == "1" else "否"

                if_ems = deptExpand["if_ems"]  # 支持物流快递

                if_ems = "是" if if_ems == "1" else "否"

                syfw = deptExpand["syfw"]  # 适用范围

                sfjzbl = deptExpand["sfjzbl"]  # 集中办理

                sfjzbl = "是" if sfjzbl == "1" else "否"

                jzxyq = deptExpand["jzxyq"]  # 禁止性要求

                cnsx = ""  # 承诺时限

                promise_type = dept["promise_type"]
                type_dict = {"1": "工作日", "2": "自然日", "3": "月", "4": "年"}

                if promise_type in type_dict:
                    promise_type = type_dict[promise_type]
                else:
                    promise_type = "工作日"

                if dept["bjtype"] == "1":
                    cnsx = "即办"
                else:
                    if dept["promise_day"] == 0:
                        cnsx = "无"
                    else:
                        cnsx = str(dept["promise_day"]) + promise_type

                fdsx = ""  # 法定时限

                anticipate_type = dept["anticipate_type"]

                if anticipate_type in type_dict:
                    anticipate_type = type_dict[anticipate_type]
                else:
                    anticipate_type = "工作日"

                if dept["bjtype"] == "1":
                    fdsx = "即办"
                else:
                    if dept["anticipate_day"] == 0:
                        fdsx = "无"
                    else:
                        fdsx = str(dept["anticipate_day"]) + anticipate_type

                sxsm = dept["sxsm"]  # 期限说明

                dqjyjyj = deptExpand["dqjyjyj"]  # 定期检验及依据

                blgs = deptExpand["blgs"]  # 办理公示

                blcx = deptExpand["blcx"]  # 办理查询

                accept_address = dept["accept_address"]  # 办理地点

                applyermin_count = deptExpand["applyermin_count"]  # 办理办事地点次数

                accept_time = dept["accept_time"]  # 工作时间

                link_tel = dept["link_tel"]  # 咨询联系电话

                supervise_tel = dept["supervise_tel"]  # 监督投诉渠道

                content_html = html.format(ql_name, ql_name_yw, item_code, itemsource, ql_kind, shiquancj, bjtype,
                                           ql_object,
                                           fdsx, cnsx, sxsm, lawbasis, qxhf, sljg, sljgxz, jdjg, jdjgxz, tongbanmode,
                                           sflhsp,
                                           blxs, if_appoint, if_ems, sfjzbl, syfw, jzxyq, dqjyjyj, blgs, blcx,
                                           accept_address,
                                           applyermin_count,
                                           accept_time, link_tel, supervise_tel)

        return content_html

    def get_application_condition_html(self, data):
        application_condition_html = ""
        if data:
            if data["code"] == 0 and len(data["DeptExpand"]["sqtj"]) > 0:
                application_condition_html = "<div class='bs_conter' id='sqtjdiv'>{0}</div>".format(
                    data["DeptExpand"]["sqtj"])
        return application_condition_html

    def get_relief_way_html(self, data):
        relief_way_html = ""
        if data:
            if data["code"] == 0 and len(data["DeptExpand"]["jjtj"]) > 0:
                relief_way_html = "<div class='bs_conter' id='jjtjdiv'>{0}</div>".format(
                    data["DeptExpand"]["jjtj"])

        return relief_way_html

    def get_approval_result_html(self, data):
        approval_result_htm = ""
        if data:
            if data["code"] == 0:
                deptExpand = data["DeptExpand"]
                approval_result_html = '<table border="0" class="xl_talbe" cellpadding="0" cellspacing="0">'
                approval_result_html += '<tr>'
                approval_result_html += '<th width="286">审批结果类型</th>'
                approval_result_html += '<th width="369">结果名称</th>'
                approval_result_html += '<th width="200">结果送达</th>'
                approval_result_html += '<th>结果样本下载</th>'
                approval_result_html += '</tr>'

                finish_type = deptExpand["banjie_finishtype"]

                finish_type_dict = {"10": "许可证、执照或者其他许可证书", "20": "资格证、资质证或者其他合格证书", "30": "行政机关的批准文件或者证明文件",
                                    "40": "法律、法规规定的其他行政许可证件"}

                if finish_type in finish_type_dict:
                    finish_type = finish_type_dict[finish_type]
                else:
                    finish_type = "其它"

                approval_result_html += '<tr>'
                approval_result_html += '<td>' + finish_type + '</td>'
                approval_result_html += '<td>' + deptExpand["banjie_finishname"] + '</td>'
                approval_result_html += '<td>' + deptExpand["jgsd"] + '</td>'

                banjie_finishfile = deptExpand["banjie_finishfiles"]
                if len(banjie_finishfile) > 0:
                    banjie_finishfiles = banjie_finishfile.split(",")
                    approval_result_html += '<td>'
                    for table in banjie_finishfiles:
                        file_url_sp = table.split("nas")
                        file_url = "http://www.lnzwfw.gov.cn/xdfile/nas" + file_url_sp[1]
                        approval_result_html += '<a href="' + file_url + '" target="_blank">样例</a>'
                        approval_result_html += '</td>'
                else:
                    approval_result_html += '<td>无</td>'

                approval_result_html += '</tr>'
                approval_result_html += '</table>'

        return approval_result_htm

    def get_work_flow_html(self, data):
        work_flow_html = ""
        if data:
            if data["code"] == 0:
                dept = data["Dept"]
                flow_imageurl = dept["flow_imageurl"]
                if len(flow_imageurl) > 0:
                    flow_imageurls = flow_imageurl.split(",")
                    lctsm = dept["lctsm"]
                    if len(lctsm) > 0:
                        work_flow_html += '<div style=" width:1000px; height:20px; margin:10px 0; border-left: 5px solid #6598c9;text-indent:14px; line-height: 20px;">办理流程</div>';
                        work_flow_html += "<div class='bs_conter'>" + lctsm + "</div>"
                    work_flow_html += '<div style=" width:1000px; height:20px; margin:10px 0; border-left: 5px solid #6598c9;text-indent:14px; line-height: 20px;">流程图</div>'
                    for table in flow_imageurls:
                        file_url_sp = table.split("nas")
                        file_url = "http://www.lnzwfw.gov.cn/xdfile/nas" + file_url_sp[1]
                        work_flow_html += '<img src="' + file_url + '" />'
        return work_flow_html
