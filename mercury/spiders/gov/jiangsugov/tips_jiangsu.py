# -*- coding: utf-8 -*-
import re

from mercury.spiders._base import Spider as BaseSpider
from mercury.models import Tips
from mercury.models import JobQueue
from mercury.settings import logger
from ..gov_helper import update_deprecated_tips, update_dup_tip, get_base_telephone
from bs4 import BeautifulSoup
from urllib.parse import urlencode

domain_url = "http://www.jszwfw.gov.cn"
tips_detail_url = domain_url + "/jszwfw/publicservice/itemlist/showDetail.do?"


class Spider(BaseSpider):
    name = "tips_jiangsu"
    Model = Tips

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "Host": "www.jszwfw.gov.cn",
                "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit"
                              "/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36"
            },
            "cookies": {},
            "sleep_range_before_request": None,
            "uniqid": "urlmd5",
            "save_html": False,
            "proxy_enable": True
        }
        return configs

    async def handle_download_failure(self, error):
        # 将下载失败的任务重新加入任务队列
        await JobQueue.rpush(self.job)

    async def handle_index(self, page):
        detail_infos, next_info = [], {}
        try:
            # 从种子数据里取站点和服务领域
            site_level = self.job.extra_data.pop("site_level", "")
            fwly = self.job.extra_data.pop("fwly", "")
            isupdate = self.job.extra_data.pop("isupdate", None)

            # 获取总页数和当前页数
            pages = self.job.extra_data.pop("pages", 0)
            current_page = self.job.extra_data.pop("current_page", 0)

            # 如果extra_data中没有pages和current_page的信息，则从页面上取
            if pages == 0 and current_page == 0:
                pages_html = re.findall(r"pages\s*:\s*'\d+'", page, re.S)
                if len(pages_html) == 1:
                    pages_string = pages_html[0][pages_html[0].index("'") + 1:len(pages_html[0]) - 1].strip()
                    if len(pages_string) > 0:
                        pages = int(pages_string)

                current_page_html = re.findall(r"curr\s*:\s*'\d+'", page, re.S)
                if len(current_page_html) == 1:
                    current_page_string = current_page_html[0][
                                          current_page_html[0].index("'") + 1:len(current_page_html[0]) - 1].strip()
                    if len(current_page_string) > 0:
                        current_page = int(current_page_string)

            if pages > 0 and current_page > 0:
                # 解析list页面构造detail_infos
                page_soup = BeautifulSoup(page, 'lxml')

                # 找到详情页链接的function
                r_tit_a_s = page_soup.find_all("a", {"class": "r_tit_a"})
                for r_tit_a in r_tit_a_s:
                    a_onclick = r_tit_a.get("onclick")
                    tips_title = r_tit_a.get_text().strip()
                    if len(tips_title) == 0:
                        continue
                    # 取出function的参数
                    parameters = await self.get_onclick_parameters(a_onclick)
                    detail_params = {
                        "webId": parameters[0],
                        "iddept_ql_inf": parameters[1],
                        "iszx": parameters[2],
                        "ql_kind": parameters[3],
                        "g_fw_mode": parameters[4],
                        "iddept_yw_inf": parameters[5]
                    }
                    # 将onclick的参数拼接到detail_url上
                    detail_url = tips_detail_url + urlencode(detail_params)
                    detail_extra_data = dict(cate_parent='', site_level=site_level, fwly=fwly, title=tips_title,
                                             isupdate=isupdate)
                    detail_infos.append({'url': detail_url, 'extra_data': detail_extra_data})

            # 如果当前页不是最后一页，则构造下一页的url
            if current_page < pages:
                pn_new = "pageno=" + str(current_page + 1)
                pn_old_ret = "pageno=" + str(current_page)
                url_next = self.job.url.replace(pn_old_ret, pn_new)
                extra_data = dict(site_level=site_level, pages=pages, current_page=current_page + 1, fwly=fwly,
                                  isupdate=isupdate)
                next_info = {'url': url_next, 'extra_data': extra_data}

            # 如果当前页是最后一页，则更新种子数据
            if current_page == pages:
                await update_deprecated_tips(self.name, site_level)
            del page
        except Exception as e:
            logger.error(e, exc_info=True)
        return (detail_infos, next_info)

    # async def handle_dup_detail(self):
    #     if not self.job.is_seed:
    #         await update_dup_tip(self.job, self.name)

    async def handle_detail(self, page, encoding, extra_data=None):
        retval = {}
        try:
            retval = await self.extract_page(page)
            retval["reference"] = self.job.url
            retval["title"] = extra_data.pop("title", "")
            retval["cate_xz"] = extra_data.pop("fwly", "")
            retval["cate_parent"] = extra_data.pop("cate_parent", "")
            retval["site_level"] = extra_data.pop("site_level", "")
            retval["getway"] = self.name
            retval["sync"] = 0
        except Exception as e:
            logger.error(e, exc_info=True)
        return retval

    async def extract_page(self, page):
        retval = {}
        # 解析详情页
        page_soup = BeautifulSoup(page, 'lxml')

        org_div = page_soup.select(".blsj_12")
        if len(org_div):
            retval["organization"] = org_div[0].select("p")[0].text

        # 获取存储详情页内容信息的li
        nav_height_div = page_soup.find("div", {"id": "navHeight"})
        nav_lis = nav_height_div.find("ul").find_all("li")
        extras = []
        for nav_li in nav_lis:
            div_id = nav_li.find("a").get("href").replace("#", "")
            text = nav_li.find("a").get_text().strip()
            target_div = page_soup.find("div", {"id": div_id})
            if target_div:
                if text == "基本信息":
                    retval["base_info"] = dict(name="基本信息", content=target_div.prettify())
                    lis = target_div.select("li")
                    for li in lis:
                        if li.text.startswith("咨询方式"):
                            telephone = await get_base_telephone(li.select("span")[0].text.strip())
                            if len(telephone) > 0:
                                retval["telephone"] = telephone[0]
                elif text == "设定依据":
                    retval["basis"] = dict(name="设定依据", content=target_div.prettify())
                elif text == "流程图":
                    lct_div = page_soup.find("div", {"class": "xiangtu"})
                    if lct_div:
                        retval["work_flow"] = dict(name="办理流程", content=lct_div.prettify(), picture="")
                elif text == "办理材料目录":
                    retval["materials"] = dict(name="申请材料", content=target_div.prettify())
                elif text == "收费标准":
                    retval["fees"] = dict(name="收费标准及依据", content=target_div.prettify())
                elif text == "常见问题":
                    retval["faq"] = dict(name="常见问题", content=target_div.prettify())
                elif text == "办理结果形式":
                    retval["results"] = dict(name="结果名称及样本", content=target_div.prettify())
                else:
                    # others put extra
                    other_div = page_soup.find("div", {"id": div_id})
                    if other_div:
                        extras.append(dict(name=text, content=other_div.prettify()))
        retval["extras"] = extras
        return retval

    async def get_onclick_parameters(self, onclick_string):
        parameters = []
        chars = list(onclick_string[onclick_string.index("(") + 1:onclick_string.index(")")])
        parameter_start = False
        parameter_start_index = 0
        for i, char in enumerate(chars):
            if char == "'":
                parameter_start = not parameter_start
                if parameter_start:
                    parameter_start_index = i
                else:
                    parameters.append("".join(chars[parameter_start_index + 1:i]))
        return parameters
