
import re
import random
from functools import partial
from concurrent.futures import ThreadPoolExecutor

import demjson
import asyncio
import requests
from pyquery import PyQuery
from bs4 import BeautifulSoup

from mercury.libs.proxy import get_proxy
from mercury.libs.useragent import random_ua
from mercury.downloader.http import AsyncHttpDownloader
from mercury.settings import logger, MAX_JOBS
from mercury.utils import timestamp
from mercury.models import LawUpstream, JobQueue
from mercury.spiders._base import Spider as BaseSpider
from .pkulaw import Spider as PkulawSpider
from .laws_helper import (MEDIA_TYPES, RE_ATTACHMENT,
                          extract_pkulaw_v6_metainfo, extract_table_pre_img_link_content)

url_pkulaw_login = 'http://58.16.80.192:9000/login'
url_pkulaw_search = 'http://58.16.80.192:9000/law/search/RecordSearch'
url_pkulaw_v6 = 'http://58.16.80.192:9000/rwt/BDFY/http/P75YPLURNN4XZZLYF3SX85B'
url_detail = 'http://58.16.80.192:9000/rwt/BDFY/http/P75YPLURNN4XZZLYF3SX85B/{lib}/{fbid}.html'


def get_user():
    users = ['201201046', '201201048', '201201054', '201201060', '201201082', 
             '201201084', '201201088', '201201108', '201201150', '201301080', 
             '201301096', '201301113', '201301116', '201301118', '201301135', 
             '201301195', '201302111', '201302155', '201302162', '201302166', 
             '201302169', '201302170', '201302171', '201302172', '201302173', 
             '201302178']
    return random.choice(users)


class Spider(BaseSpider):
    name = 'pkulaw_gzcj'

    Model = LawUpstream

    executor = ThreadPoolExecutor(max_workers=MAX_JOBS)

    CrawledCounter = 0
    loginFailedCounter = 0

    async def fetch(self, url, method='get', **kwargs):
        def _make_request(url, method, **kwargs):
            with requests.Session() as s:
                meth = getattr(s, method)
                with meth(url, **kwargs) as r:
                    logger.debug('[_make_request]: {}'.format(url))
                    return r, s.cookies

        # proxies = {'http': get_proxy()}
        f = partial(_make_request, method=method, **kwargs)
        try:
            response, cookies = await self.loop.run_in_executor(self.executor, f, url)
            return response, cookies
        except Exception as e:
            logger.error(e, exc_info=True)
            return None, None

    def __init__(self, job):
        super().__init__(job)
        self.dl = None
        self.delegator = PkulawSpider(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Accept-Encoding': 'gzip, deflate',
                'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                'DNT': '1',
                'Host': '58.16.80.192:9000',
                'Proxy-Connection': 'keep-alive',
                'Upgrade-Insecure-Requests': '1',
                "User-Agent": random_ua()
            },
            "cookies": {},
            "sleep_range_before_request": (10, 20),
            "uniqid": "fbid",
            "save_html": True,
            "proxy_enable": False,
        }
        return configs

    async def before_request(self):
        cookies = await self.get_cookie()
        if cookies:
            self.configs['cookies'].update(cookies)
            self.configs['headers'].update({
                'Accept': '*/*',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Origin': 'http://58.16.80.192:9000',
                'Proxy-Connection': 'keep-alive',
                'Referer': 'http://58.16.80.192:9000/rwt/BDFY/http/P75YPLURNN4XZZLYF3SX85B/',
                'X-Requested-With': 'XMLHttpRequest'
            })
            return True
        return False

    async def handle_download_failure(self, error):
        if self.configs.get('cookies') and self.job.is_seed and self.dl.status == 500:
            logger.info('[Spider]<{}> cookies expired...'.format(self.name))
            await self.clear_cookie()

    async def handle_index(self, page):
        detail_infos, next_info = [], None
        for item in page:
            fbid = item['fbid']
            lib = item['lib']
            url = url_detail.format(fbid=fbid, lib=lib)
            detail_infos.append({'url': url, 'fbid': fbid,
                                 'extra_data': self.job.extra_data})

        # 抓取的次数超过2000  或者联系登录失败10次
        if Spider.CrawledCounter > 2000 or Spider.loginFailedCounter > 10:
            # 达到上限, 清理缓存
            await self.clear_cookie()
        else:
            # 查询结果为空
            next_info = {'url': self.job.url,
                         'extra_data': self.job.extra_data} if detail_infos else None
        return detail_infos, next_info

    async def handle_detail(self, page, encoding='utf-8', extra_data=None):
        # 每次请求详情页面就会加1
        Spider.CrawledCounter += 1
        logger.info('[Spider]<{}> crawling count: {}'.format(
            self.name, Spider.CrawledCounter))

        pq = PyQuery(page)
        #   被重定向到登录页面            cookie已过期, 需要先登录才能下载页面
        if pq('title').text() == '首页':
            logger.info('[Spider]<{}> cookies expired...'.format(self.name))
            await self.clear_cookie()
            return None

        # 验证页面
        if pq('#verify'):
            logger.info('[Spider]<{}> verify page...'.format(self.name))
            logger.info('[Spider]<{}> cookies expired...'.format(self.name))
            await self.clear_cookie()
            return None

        fbid = pq('input#ArticleId').val()
        # lib
        lib = pq('input#DbId').val()
        # eg: 全国人大常委会关于修改《中华人民共和国个人所得税法》的决定(2018)-北大法宝V6官网
        title = pq('title').text().rsplit('-', 1)[0]

        # 非法页面
        if not fbid or not lib or not title:
            logger.info('[Spider]<{}> cookies expired...'.format(self.name))
            await self.clear_cookie()
            return None

        retval = {
            'url': self.job.url,
            'sid': self.job.seed_urlmd5,
            'getway': self.name,
            'fbid': fbid,
            'lib': lib,
            'title': title,
        }
        # 需要先登录才能查看完整信息
        if pq('#WeChatPayUrl').val():
            logger.info('[Spider]<{}> need login: {}'.format(
                self.name, self.job.url))
            logger.info('[Spider]<{}> cookies expired...'.format(self.name))
            await self.clear_cookie()
            return None

        mainblock = pq('div#divFullText')
        html = mainblock.html()
        content = extract_table_pre_img_link_content(html)
        mediatypes = [t for t in MEDIA_TYPES if mainblock(t)]
        hasattachments = True if RE_ATTACHMENT.search(content) else False
        retval['content'] = content
        retval['mediatypes'] = mediatypes
        retval['hasattachments'] = hasattachments

        metainfo = extract_pkulaw_v6_metainfo(page, lib)
        retval.update(metainfo)
        # 是否被已被修改过
        retval['needlogin'] = False
        # 更新时间
        retval['update_time'] = timestamp()
        return retval

    async def login(self):
        logger.debug('[Spider]<{}> starting login...'.format(self.name))
        loginlock = await self.queue.get('mercury:pkulaw:loginlock')
        logger.debug('[Spider]<{}> login: {}'.format(self.name, loginlock))
        if loginlock == 1:
            return None
        logger.debug('[Spider]<{}> get login lock...'.format(self.name))
        await self.queue.set('mercury:pkulaw:loginlock', 1)

        self.configs['headers'].update({
            'Cache-Control': 'max-age=0',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Origin': 'http://58.16.80.192:9000',
            'Referer': 'http://58.16.80.192:9000/login?iplogin=0'
        })
        form_login = {'userId': get_user(),
                      'password': '888888',
                      'mac': '',
                      'x': random.randrange(20, 80),
                      'y': random.randrange(20, 80),
                      'url': ''}
        logger.debug('[Spider]<{}> send login message...'.format(self.name))
        # proxies = {'http': get_proxy()}
        resp, cookies = await self.fetch(
            url_pkulaw_login, method='post', data=form_login,
            headers=self.configs['headers'], timeout=15)
        logger.debug('[Spider]<{}> sent finished...'.format(self.name))
        if resp and resp.status_code == 200 and resp.url != url_pkulaw_login and cookies:
            logger.info('[Spider]<{}> login succeed...'.format(self.name))
            await self.queue.delete('mercury:pkulaw:loginlock')
            return cookies
        # 登录失败统计+1
        Spider.loginFailedCounter += 1
        logger.info('[Spider]<{}> login failed: {}'.format(
            self.name, Spider.loginFailedCounter))
        await self.queue.delete('mercury:pkulaw:loginlock')
        return None

    async def get_cookie(self):
        logger.debug('[Spider]<{}> get cookie...'.format(self.name))
        cookies = await self.queue.get('mercury:pkulaw:cookies')
        logger.debug('[Spider]<{}> cookies: {}'.format(self.name, cookies))
        if not cookies:
            cookies = await self.login()
            if cookies:
                await self.queue.set('mercury:pkulaw:cookies', demjson.encode(cookies))
        return demjson.decode(cookies) if isinstance(cookies, str) else cookies

    async def clear_cookie(self):
        logger.info('[Spider]<{}> clear cookie...'.format(self.name))
        await self.queue.delete('mercury:pkulaw:cookies', 'mercury:pkulaw:loginlock')
