
import re
import time
import random
import asyncio
from urllib.parse import quote, urljoin

import demjson
from pyquery import PyQuery
from aiohttp.client_exceptions import TooManyRedirects

from mercury.models import Law, Job, JobQueue
from mercury.settings import logger, MAX_JOBS, STORAGE
from mercury.utils import md5, timestamp
from mercury.libs.proxy import get_proxy
from mercury.monitor import data_amount
from mercury.libs.useragent import random_ua
from mercury.downloader.http import AsyncHttpDownloader
from mercury.spiders import BaseSpider

url_send_msg = 'http://www.ttfawu.com/user/sendMessage'
url_register = 'http://www.ttfawu.com/user/subRegisterMobile'
url_detail = 'http://www.ttfawu.com/fagui/detail/058567c94d40414e9907de7edc68e91a'

# 获取手机号
api_get_mobile = 'http://api.fxhyd.cn/UserInterface.aspx?action=getmobile&token=0084977440138742acb90b5947ee3f2fcd34e5b8&itemid=24518&excludeno='
# 获取短信验证码
api_get_sms = 'http://api.fxhyd.cn/UserInterface.aspx?action=getsms&token=0084977440138742acb90b5947ee3f2fcd34e5b8&itemid=24518&mobile={0}'
# 释放手机号
api_release_mobile = 'http://api.fxhyd.cn/UserInterface.aspx?action=release&token=0084977440138742acb90b5947ee3f2fcd34e5b8&itemid=24518&mobile={0}'
# 拉黑手机号
api_block_mobile = 'http://api.fxhyd.cn/UserInterface.aspx?action=addignore&token=0084977440138742acb90b5947ee3f2fcd34e5b8&itemid=24518&mobile={0}'


RE_MCODE = re.compile('\d{6}')


async def get_mobile():
    """获取手机号"""
    logger.info('[Spider]<ttfawu> get mobile...')
    dl = AsyncHttpDownloader(
        proxy=get_proxy() if STORAGE.get('sync') == -1 else None)
    counter = 0
    while True:
        text, _ = await dl.fetch(api_get_mobile, auto_close=False)
        if '|' in text:
            status, mobile = text.split('|')
            if status == 'success':
                await dl.close()
                logger.info('[Spider]<ttfawu> get mobile {}'.format(mobile))
                return mobile
        counter += 1
        if counter > 3:
            break
        await asyncio.sleep(5)
    logger.info('[Spider]<ttfawu> get mobile failed')
    await dl.close()
    return None


async def get_mcode(mobile):
    """获取验证码"""
    logger.info('[Spider]<ttfawu> get mobile {} mcode...'.format(mobile))
    counter = 0
    dl = AsyncHttpDownloader(
        proxy=get_proxy() if STORAGE.get('sync') == -1 else None)
    while True:
        text, _ = await dl.fetch(api_get_sms.format(mobile), auto_close=False)
        if '|' in text:
            status, content = text.strip().split('|')
            logger.info('[Spider]<ttfawu> [text] {}'.format(text))
            if status == 'success':
                r = RE_MCODE.search(content)
                if r:
                    await dl.close()
                    mcode = r.group(0)
                    logger.info(
                        '[Spider]<ttfawu> get mobile {} mcode {}'.format(mobile, mcode))
                    return mcode
        counter += 1
        if counter > 10:
            break
        await asyncio.sleep(5)
    logger.info(
        '[Spider]<ttfawu> get mobile {} mcode failed...'.format(mobile))
    await dl.close()
    return None


def get_random_username():
    """获取随机用户名"""
    first_name = [
        '赵', '钱', '孙', '李', '周', '吴', '郑', '王', '冯', '陈', '褚', '卫', '蒋', '沈', '韩', '杨', '朱', '秦',
        '何', '吕', '施', '张', '孔', '曹', '严', '华', '金', '魏', '陶', '姜', '戚', '谢', '邹', '喻', '柏', '水',
        '云', '苏', '潘', '葛', '奚', '范', '彭', '郎', '鲁', '韦', '昌', '马', '苗', '凤', '花', '方', '俞', '任',
        '酆', '鲍', '史', '唐', '费', '廉', '岑', '薛', '雷', '贺', '倪', '汤', '滕', '殷', '罗', '毕', '郝', '邬',
        '乐', '于', '时', '傅', '皮', '卞', '齐', '康', '伍', '余', '元', '卜', '顾', '孟', '平', '黄', '和', '穆',
        '姚', '邵', '堪', '汪', '祁', '毛', '禹', '狄', '米', '贝', '明', '臧', '计', '伏', '成', '戴', '谈', '宋',
        '熊', '纪', '舒', '屈', '项', '祝', '董', '梁']
    last_name = [
        '劳', '型', '烈', '姑', '陈', '莫', '鱼', '异', '抱', '宝', '权', '鲁', '简', '态', '级', '票', '怪', '寻',
        '胜', '份', '汽', '右', '洋', '范', '床', '舞', '秘', '午', '登', '楼', '贵', '吸', '责', '例', '追', '较',
        '渐', '左', '录', '丝', '牙', '党', '继', '托', '赶', '章', '智', '冲', '叶', '胡', '吉', '卖', '坚', '喝',
        '救', '修', '松', '临', '藏', '担', '戏', '善', '卫', '药', '悲', '敢', '靠', '伊', '村', '戴', '词', '森',
        '短', '祖', '云', '规', '窗', '散', '迷', '油', '旧', '适', '乡', '架', '恩', '投', '弹', '铁', '博', '雷',
        '超', '负', '勒', '杂', '醒', '洗', '采', '毫', '嘴', '毕', '九', '冰', '既', '状', '乱', '景', '席', '珍',
        '派', '素', '脱', '农', '疑', '练', '野', '按', '犯', '拍', '征', '坏', '骨', '余', '承', '置', '臓', '彩',
        '琴', '免', '环', '姆', '暗', '换', '技', '翻', '束', '增', '忍', '餐', '洛', '塞', '缺', '忆', '判', '欧',
        '阵', '玛', '批', '岛', '项', '狗', '休', '懂', '武', '革', '良', '恶', '恋', '委', '拥', '娜', '妙', '探',
        '退', '摇', '弄', '桌', '熟', '诺', '宣', '银', '势', '奖', '宫', '忽', '套', '康', '供', '优', '课', '鸟',
        '夏', '困', '刘', '罪', '亡', '鞋', '健', '模', '败', '伴', '守', '挥', '鲜', '财', '孤', '枪', '禁', '恐',
        '迹', '妹', '藸', '遍', '盖', '副', '坦', '牌', '江', '顺', '秋', '萨', '菜', '划', '授', '归', '浪', '听',
        '奶', '雄', '升', '碃', '编', '典', '袋', '莱', '含', '盛', '济', '蒙', '棋', '端', '腿', '招', '释', '介',
        '乾', '坤']
    return random.choice(first_name) + random.choice(last_name) + random.choice(last_name)


def get_random_email():
    """获取随机邮箱"""
    domains = ['qq.com', '163.com', '126.com', 'hotmail.com',
               'gmail.com', 'sina.com', 'aol.com', 'yahoo.com']
    s = 'abcdefghijklmnopqrstuvwxyz01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    return '{0}@{1}'.format(''.join(random.sample(s, random.randrange(6, 12))), random.choice(domains))


def get_random_passowrd():
    """获取随机密码"""
    s = "abcdefghijklmnopqrstuvwxyz01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()?"
    password = ''.join(random.sample(s, random.randrange(8, 10)))
    return password


def get_random_company():
    """获取随机公司名称"""
    lawoffices = [
        "北京金标律师事务所", "河北昭庆律师事务所", "福建旭丰律师事务所", "广东经国律师事务所", "广东国欣律师事务所",
        "河南融业律师事务所", "天津四方君汇律师事务所", "重庆乾信律师事务所", "河北天壹律师事务所", "山西龙盛律师事务所",
        "黑龙江油都金城律师事务所", "湖北浩法律师事务所", "天津则立律师事务所", "黑龙江嵩岩律师事务所", "广东天穗律师事务所",
        "重庆精韬律师事务所", "河北盛誉律师事务所", "北京德恒（重庆）律师事务所", "福建重宇合众律师事务所", "天津景贤律师事务所",
        "北京盈科(上海)律师事务所", "山东博港律师事务所", "黑龙江庆大律师事务所", "湖北武当律师事务所", "福建汇丰联盟律师事务所",
        "北京京坤律师事务所", "上海市欣隆律师事务所", "河南通冠律师事务所", "上海顺盈律师事务所", "陕西乐友律师事务所",
        "重庆龙都律师事务所", "北京市博天律师事务所", "广东民赐律师事务所", "广东深田律师事务所", "河北领航律师事务所",
        "重庆大祥律师事务所", "陕西秦瀚律师事务所", "福建熹龙律师事务所", "陕西兴振业律师事务所", "陕西法智律师事务所"]
    return random.choice(lawoffices)


class Spider(BaseSpider):
    name = 'ttfawu'

    Model = Law

    cookies = None
    loginlock = None

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {},
            "cookies": {},
            "sleep_range_before_request": (5, 10),
            "uniqid": "urlmd5",
            "save_html": True,
            "proxy_enable": True
        }
        return configs

    async def handle_download_failure(self, error):
        """将下载失败的任务重新加入任务队列中"""
        await JobQueue.rpush(self.job)
        # 多次重定向错误, 说明触发反爬, 清除cookie
        if isinstance(error, TooManyRedirects):
            await self.clear_cookie()

    async def before_request(self):
        if self.job.is_seed:
            self.job.method = 'post'
            self.job.ctype = 'formdata'
            self.job.rtype = 'json'
            self.configs['headers'].update({
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'Accept-Encoding': 'gzip, deflate',
                'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                'Connection': 'keep-alive',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'DNT': '1',
                'Host': 'www.ttfawu.com',
                'Origin': 'http://www.ttfawu.com',
                'Referer': 'http://www.ttfawu.com',
                'User-Agent': random_ua(),
                'X-Requested-With': 'XMLHttpRequest'
            })
            self.dl = AsyncHttpDownloader(
                headers=self.configs['headers'], proxy=get_proxy() if STORAGE.get('sync') == -1 else None)
        else:
            cookies = await self.get_cookie()
            if not cookies:
                return False
            self.dl = AsyncHttpDownloader(
                headers=self.configs['headers'], cookies=cookies, proxy=get_proxy() if STORAGE.get('sync') == -1 else None)
            self.configs['headers'].update({
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Accept-Encoding': 'gzip, deflate',
                'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                'Cache-Control': 'max-age=0',
                'Connection': 'keep-alive',
                'DNT': '1',
                'Host': 'www.ttfawu.com',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': random_ua()
            })
        return True

    async def handle_index(self, page):
        detail_infos, next_info = [], None
        if page.get('success'):
            faguis = page['faguis']
            for fagui in faguis:
                title = PyQuery(fagui['title']).text().rstrip('（天天法务梳理版）')
                cond0 = title in self.job.extra_data['title']
                cond1 = self.job.extra_data.get('documentno') and fagui.get(
                    'fwzh') and self.job.extra_data['documentno'] == fagui['fwzh']
                cond2 = self.job.extra_data.get('issuedate') and fagui.get(
                    'estDate') and self.job.extra_data['issuedate'] == fagui['estDate'].replace('-', '.')
                cond3 = self.job.extra_data.get('implementdate') and fagui.get(
                    'impDate') and self.job.extra_data['implementdate'] == fagui['impDate'].replace('-', '.')

                if cond0 and any((cond1, cond2, cond3)):
                    detail_infos.append({
                        'url': 'http://www.ttfawu.com/fagui/detail/' + fagui['id'],
                        'extra_data': {'isupdate': True, **self.job.extra_data}
                    })
                    break
        if not detail_infos:
            await self.queue.sadd('mercury:pkulaw:ttfawu:notfound', demjson.decode(self.job.extra_data))
            # 非实时统计Redis中未能命中的查询条件的数量
            if random.randint(0, 10) == 1:
                data_amount.labels(getway="ttfawu").set(await self.queue.scard('mercury:pkulaw:ttfawu:notfound'))
        return detail_infos, next_info

    async def handle_detail(self, page, encoding, extra_data=None):
        pq = PyQuery(page)
        content = pq('div.xq_ml').text().strip()
        if content:
            data = {
                'content': pq('div.xq_ml').text(),
                'update_time': timestamp(),
                'update_source': {
                    'getway': self.name,
                    'url': self.job.url,
                    's3': "{spider}/{date}/{uniqid}.html.gz".format(
                        spider=self.name, date=timestamp().strftime("%Y%m%d"), uniqid=self.job.uniqid)
                }
            }
            return data
        return None

    async def register(self):
        logger.info('[Spider]<{}> register...'.format(self.name))
        mobile = await get_mobile()
        if mobile:
            self.configs['headers'].update(
                {'Accept': 'application/json, text/javascript, */*; q=0.01',
                 'Accept-Encoding': 'gzip, deflate',
                 'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                 'DNT': '1',
                 'Host': 'www.ttfawu.com',
                 'Origin': 'http://www.ttfawu.com',
                 'Proxy-Connection': 'keep-alive',
                 'Referer': 'http://www.ttfawu.com/user/applyIndex',
                 'User-Agent': random_ua(),
                 'X-Requested-With': 'XMLHttpRequest'})

            dl = AsyncHttpDownloader(
                headers=self.configs['headers'], proxy=get_proxy() if STORAGE.get('sync') == -1 else None)
            form_mobile = {'mobile': mobile}
            retval, _ = await dl.fetch(url_send_msg, method='post', ctype='formdata', data=form_mobile, rtype='json', auto_close=False)
            if retval and retval.get('status') == 200:
                mcode = await get_mcode(mobile)
                if mcode:
                    form_reg = {'name': get_random_username(),
                                'sex': '1',
                                'mobile': mobile,
                                'company': get_random_company(),
                                'position': '律师',
                                'email': get_random_email(),
                                'pswd': get_random_passowrd(),
                                'mcode': mcode}
                    retval, _ = await dl.fetch(url_register, method='post', ctype='formdata', rtype='json', data=form_reg)
                    if retval and retval.get('status') == 200:
                        cookies = {c.key: c.value for c in dl.cookie_jar}
                        logger.info('[Spider]<{}> register succeed, cookies: {}'.format(
                            self.name, cookies))
                        return cookies
            else:
                await dl.close()
        return None

    async def get_cookie(self):
        logger.info('[Spider]<{}> get cookie...'.format(self.name))
        # cookies = await self.queue.hgetall('mercury:ttfawu:cookies')
        cookies = Spider.cookies
        if not cookies and not Spider.loginlock:
            Spider.loginlock = object()
            cookies = await self.register()
            if cookies:
                # await self.queue.hmset('mercury:ttfawu:cookies', cookies)
                Spider.cookies = cookies
            Spider.loginlock = None
        if cookies:
            logger.info('[Spider]<{}> get cookies {}'.format(
                self.name, cookies))
        return cookies

    async def clear_cookie(self):
        logger.info('[Spider]<{}> cookie {} expired, clear...'.format(
            self.name, Spider.cookies))
        # await self.queue.delete('mercury:ttfawu:cookies')
        Spider.cookies = None

    @staticmethod
    def encodeuri(uri):
        return quote(quote(uri))
