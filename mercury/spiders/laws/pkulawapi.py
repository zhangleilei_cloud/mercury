

import os
import re
from urllib.parse import quote, urlparse, urljoin
from datetime import datetime, timedelta
from operator import itemgetter
from concurrent.futures import ThreadPoolExecutor

import requests
from pyquery import PyQuery
from bs4 import BeautifulSoup

from mercury.libs import s3
from mercury.libs.useragent import random_ua
from mercury.settings import logger, MAX_JOBS, PKULAW_CACHE_HOST, DEPLOYTYPE, STORAGE
from mercury.models import Law, Job, JobQueue, LawPkulawMeta, LawPkulawDetail
from mercury.utils import md5, timestamp, AESCipher
from mercury.exceptions import PkulawApiMaxAccessTimes
from mercury.downloader.http import AsyncHttpDownloader
from mercury.spiders import BaseSpider
from .laws_helper import (extract_content, convert_date_format, _get_html_text,
                          validate_sorted_field, extract_sorted_field,
                          CATEGORIES, EFFECTIVEDIS, DEPARTMENTS, RE_POSTFIX)

TIME_STR = '{year}/{month}/{day} {hour}:{minute}:{second}'
URL_PKULAW_API = 'http://api.pkulaw.com'
URL_INDEX = 'http://api.pkulaw.com/Db/LibraryRecordList?Library={lib}&PageSize={pagesize}&PageIndex={pageindex}'
URL_TITLE_SEARCH = URL_INDEX + "&Model.Title={title}"

URL_DETAIL = 'http://api.pkulaw.com/Db/GetSingleRecord?Library={lib}&Gid={gid}'
URL_TOKEN = '{url}&token={token}'
URL_SIGN = '{url}&sign={sign}'
RE_PAGEINDEX = re.compile('PageIndex=(\d+)')
RE_LIB = re.compile(r'Library=(?P<lib>\w+)')
RE_GID = re.compile(r'[gG]id=(?P<gid>\w+)')
RE_DATE = re.compile(r'(\d{4}-\d{2}-\d{2})')

RE_EXPR = """(<table.*?>[\s\S]*?<\/table>)|""" + \
          """(<pre.*?>[\s\S]*?<\/pre>)|""" + \
          """(<img src=[\'\"](?P<url>\S+)[\'\"] class=[\'\"]fjLink[\'\"].*?>)|""" + \
          """(<a href=[\'\"](?P<link>\S+)[\'\"] class=[\'\"]fjLink[\'\"] target=[\'\"]_blank[\'\"]>(?P<explain>\S*?)<\/a>)"""

RE_TABLE_PRE_IMG_LINK = re.compile(RE_EXPR)

RE_TITLE_UPDATED = re.compile("\(20\d{2}修[正改订]\)$")

PKULAW_KEY = 'bdyhpkulawcn2017'

KEYS = {
    "chl": "H82DRVP0VD20F24L",
    "lar": "4NVD842X4466TFJ4",
}

S3_CONF = STORAGE['s3']
S3_CONF['bucket'] = 'attachments'
FILE_SOURCE = 'https://files.metasotalaw.com/attachments'
PKULAW_SOURCE = 'http://resources.pkulaw.cn'


cpu_num = os.cpu_count()


class Spider(BaseSpider):
    executor = ThreadPoolExecutor(max_workers=cpu_num)
    name = 'pkulawapi'

    Model = Law

    def __init__(self, job):
        super().__init__(job)
        self.dl = self.HttpDownloader()

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {},
            "cookies": {},
            # sleep 的时间与并发的任务数量线性相关,
            # 尽量保持每一秒只有一个请求发送到北大法宝
            "sleep_range_before_request": (5, 6),
            "uniqid": "fbid",
            "save_html": True,
            "proxy_enable": False
        }
        return configs

    class HttpDownloader:
        """自定义HTTP下载器"""

        def __init__(self):
            self.error = None

        def _fetch(self, url):
            logger.info("[Requests] download: %s", url)
            retval, encoding = None, None
            try:
                r = requests.get(url)
                retval, encoding = r.json(), r.encoding
            except Exception as e:
                logger.info(e)
                self.error = e
            return retval, encoding

        async def fetch(self, url, **kwargs):
            retval, encoding = await Spider.loop.run_in_executor(
                Spider.executor, self._fetch, url)
            return retval, encoding

    async def before_request(self):
        self.job.rtype = 'json'
        url = self.job.url.rsplit('&sign=', 1)[0].rsplit('&token=', 1)[0]
        token = self.get_token()
        url_with_token = URL_TOKEN.format(url=url, token=token)
        sign = self.calculate_signature(url_with_token)
        url_with_sign = URL_SIGN.format(url=url_with_token, sign=sign)
        # 将请求北大法宝API接口的 地址 改为 请求 代理缓存服务器的地址
        self.job.url = url_with_sign.replace(
            URL_PKULAW_API, PKULAW_CACHE_HOST[DEPLOYTYPE])
        return True

    async def handle_download_failure(self, error):
        # 记录详情页面下载失败的任务Gid
        # await JobQueue.rpush(self.job)
        logger.info("[Error] %s", error)
        # raise PkulawApiMaxAccessTimes("超出最大访问次数")

    async def handle_index(self, page):
        detail_infos, next_info = [], None
        if page.get('Code') == 'OK':
            data = page.get('Data', {})
            items = data.get('Collection', [])
            for item in items:
                gid = item['Gid']
                lib = self.extract_lib()
                url = URL_DETAIL.format(lib=lib, gid=gid)
                urlmd5 = md5(url)

                # 将获取的数据首先在laws_pkulawmeta存一份
                model = LawPkulawMeta(url, urlmd5)
                await model.update({'Gid': gid}, data={**item, 'url': url, 'urlmd5': urlmd5, 'lib': lib})
                logger.info(
                    '[Spider]<{}> upsert Gid {} in <laws_pkulawmeta>'.format(self.name, gid))

                if self.job.extra_data.get("type") == "check_timelinessdic":
                    timelinessdic = item.get("TimelinessDic", {})
                    timelinessdic = "".join(list(timelinessdic.values()))
                    conds = {"fbid": gid}
                    doc = await self.Model.find_one(conds)
                    if doc and doc.get("timelinessdic") != timelinessdic:
                        await self.Model.update_one(
                            conds, {"timelinessdic": timelinessdic,
                                    "update_time": timestamp(), "sync": 0})
                        # 同时, 修改laws_pkulawdetail集合中该字段的时效性
                        await LawPkulawDetail.update_one(
                            {"Gid": gid}, {"TimelinessDic": item.get("TimelinessDic")})
                    continue

                detail_infos.append({'url': url, 'fbid': gid,
                                     'extra_data': self.job.extra_data})

            total = data.get('RecordCount', 0)
            pageindex = data.get('PageIndex', 0)
            pagecount = data.get('PageCount', 0)
            logger.debug('[Spider]<{}> total: {}, pagecount: {}, pageindex: {}'.format(
                self.name, total, pagecount, pageindex))
            if total > 0 and pageindex + 1 < pagecount:
                url = RE_PAGEINDEX.sub(
                    'PageIndex={}'.format(pageindex+1), self.job.url)
                next_info = {
                    'url': url, 'extra_data': self.job.extra_data}
        return detail_infos, next_info

    async def handle_detail(self, page, encoding='utf-8', extra_data=None):
        if page.get('Code') == 'OK' and page.get('Data'):
            data = page['Data']
            gid = data['Gid']
            logger.debug('[Spider]<{}> detail: {}'.format(self.name, gid))
            url = self.job.url.rsplit('&', 1)[0]
            urlmd5 = md5(url)
            lib = self.extract_lib()

            # 将获取的数据首先在laws_pkulawdetail集合存一份
            model = LawPkulawDetail(url, urlmd5)
            await model.update({'Gid': gid}, data={**data, 'url': url, 'urlmd5': urlmd5, 'lib': lib})
            logger.info(
                '[Spider]<{}> upsert Gid {} in <laws_pkulawdetail>'.format(self.name, gid))

            retval = {
                'url': url,
                'lib': lib,
                'getway': self.name,
                'sid': self.job.seed_urlmd5,
                'fbid': gid,
                'title': data['Title'],
                'documentno': data['DocumentNO'],
                'issuedate': convert_date_format(data['IssueDate']),
                'implementdate': convert_date_format(data['ImplementDate']),
                'ratifydate': convert_date_format(data['RatifyDate']),
                'lasttimelinesschangedate': convert_date_format(data['LastTimelinessChangeDate']),
                'modifybasis': data['RevisedBasis'],
                'invalidbasis': data['FailureBasis'],
                'partinvalidbasis': data['PartialFailureBasis'],
                'ratifydepartment': validate_sorted_field(extract_sorted_field(data['RatifyDepartment']), 'd'),
                'issuedepartment': validate_sorted_field(extract_sorted_field(data['IssueDepartment']), 'd'),
                'category': validate_sorted_field(extract_sorted_field(data['Category']), 'c'),
                'effectivenessdic': validate_sorted_field(extract_sorted_field(data['EffectivenessDic']), 'e'),
                'timelinessdic': extract_sorted_field(data['TimelinessDic']),
                'create_time': timestamp(),
                'update_time': timestamp(),
            }
            retval['effectivenessdic'] = retval['effectivenessdic'].split(
                ' ')[-1]

            full_text = data.get('FullText') or data.get('CheckFullText')
            html = await self.pre_process_full_text(full_text)
            content = extract_table_pre_img_link_content(html)
            if not content:
                retval['content'] = None
            else:
                logger.debug('[Spider]<%s> content: %s', self.name, content)
                res = extract_content(content)
                res['content'] = content
                res['pkulawinfos']['tips'] = data['FaBaoTips']
                retval.update(res)
            await self.check_title(retval)
            await self.check_repeat(retval)
            return retval
        return None

    async def check_title(self, doc):
        """按照title重新生成查询任务"""
        title = doc["title"]
        lib = doc["lib"]
        if RE_TITLE_UPDATED.search(title):
            title = title[:-8]

        logger.info("[Spider]<%s> title: %s, lib: %s", self.name, title, lib)
        await self.new_job(title, lib)

    async def new_job(self, title, lib):
        """依据待查询的title, 生成一个任务"""
        url = URL_TITLE_SEARCH.format(
            lib=lib, title=quote(title), pagesize=20, pageindex=0)
        urlmd5 = md5(url)
        job = Job(url=url,
                  urlmd5=urlmd5,
                  rtype="json",
                  spider_name="pkulawapi",
                  anti_cfg={},
                  priority=0,
                  is_seed=True,
                  max_tries=2,
                  extra_data={"type": "check_timelinessdic"},
                  seed_urlmd5=self.job.seed_urlmd5)
        await JobQueue.rpush(job)

    @classmethod
    async def check_repeat(cls, doc):
        """查重判断
        """
        title = doc["title"]
        documentno = doc["documentno"]
        issuedate = doc["issuedate"]
        implementdate = doc["implementdate"]
        conds = {
            "$or": [
                {"title": title, "documentno": documentno},
                {"title": title, "issuedate": issuedate},
                {"title": title, "implementdate": implementdate},
                {"documentno": documentno, "issuedate": issuedate},
                {"documentno": documentno, "implementdate": implementdate},
            ]
        }
        retval = await cls.Model.find_one(conds)
        if retval and retval["fbid"].isdigit():
            logger.info("[Spider]<%s> repeat: %s", cls.name, retval["fbid"])
            await cls.Model.delete_one({"fbid": retval["fbid"]})

    async def pre_process_full_text(self, full_text):
        """full_text 预处理
        """

        logger.debug("[Spider]<%s> full_text: %s", self.name, full_text)
        pq = PyQuery(full_text)
        links = pq('a')
        for i, link in enumerate(links):
            e = PyQuery(link)
            href = e.attr('href')
            if not href:
                continue
            if 'javascript' in href or 'JAVASCRIPT' in href:
                continue

            url = await self.move_file(href)
            if url:
                pq(f'a:eq({i})').attr('href', url)

        images = pq('img')
        for i, img in enumerate(images):
            e = PyQuery(img)
            src = e.attr('src')
            if not src:
                continue
            url = await self.move_file(src)
            if url:
                pq(f'img:eq({i})').attr('src', url)

        return pq.outer_html()

    async def move_file(self, url):
        """下载并转存文件
        """

        if not url.startswith(PKULAW_SOURCE):
            url = urljoin(PKULAW_SOURCE, url)
        logger.info('[Spider]<%s> download file: %s', self.name, url)
        dl = AsyncHttpDownloader()
        file_obj, _ = await dl.fetch(url, rtype='read')
        if file_obj:
            parser = urlparse(url)
            path = parser.path.replace('\\', '/').replace('//', '/')
            if path[:8] == '/upload/':
                path = path[8:]
            await s3.upload_file(file_obj, path, s3conf=S3_CONF)
            rurl = os.path.join(FILE_SOURCE, path)
            logger.info("[Spider]<%s> convert url: %s", self.name, rurl)
            return rurl
        return None

    def extract_lib(self):
        """在self.job.url中抽取lib字段
        """
        r = RE_LIB.search(self.job.url)
        return r.groupdict()['lib'].lower()

    def get_token(self):
        """根据北大法宝提供的key生成token
        """

        lib = self.extract_lib()
        key = KEYS[lib.lower()]
        cipher = AESCipher(PKULAW_KEY)
        now = datetime.now()
        time = TIME_STR.format(year=now.year,
                               month=now.month,
                               day=now.day,
                               hour=now.hour,
                               minute=now.minute,
                               second=now.second)
        secret_data = f"{key}_{time}"
        enc_bytes = cipher.encrypt(secret_data)
        enc_str = enc_bytes.decode('utf-8')
        token = quote(enc_str)
        return token

    @staticmethod
    def calculate_signature(url):
        """根据参数生成签名
        """

        query_string = url.split('?', 1)[-1]
        querys = query_string.split('&')
        params = [q.split('=') for q in querys]
        reorder_params = sorted(params, key=itemgetter(0), reverse=True)
        reorder_string = ''.join([''.join(p) for p in reorder_params])
        reorder_string_key = reorder_string + PKULAW_KEY
        md5sum = md5(reorder_string_key)
        return md5sum


def convert_url(url):
    """将当前url中的日期范围指定到 (30 days ago, now)
    """

    today = datetime.today()
    one_month_before = today - timedelta(days=30)
    end_string = today.strftime("%Y-%m-%d")
    start_string = one_month_before.strftime("%Y-%m-%d")
    start, end = RE_DATE.findall(url)
    url = url.replace(start, start_string, 1)
    url = url.replace(end, end_string, 1)
    return url


def extract_table_pre_img_link_content(html):
    """提取正文内容中的文字,表格,图片,超链接等信息
    """

    logger.debug('html: %s', html)
    iters = RE_TABLE_PRE_IMG_LINK.finditer(html)
    if not iters:
        text = _get_html_text(PyQuery(html))
        return text

    retval = []
    idx = 0
    for e in iters:
        start, end = e.span()
        text = _get_html_text(html[idx: start])
        raw = html[start: end]
        idx = end
        retval.append(text)
        retval.append(raw)

    if idx < len(html):
        text = _get_html_text(html[idx:])
        retval.append(text)

    fulltext = '\n'.join(retval)
    ret = RE_POSTFIX.search(fulltext[-100:])
    if ret:
        fulltext = fulltext[:-100] + fulltext[-100: -100 + ret.span()[0]]
    return fulltext
