
import re

from pyquery import PyQuery
from bs4 import BeautifulSoup

from mercury.libs.proxy import get_proxy
from mercury.libs.useragent import random_ua
from mercury.downloader.http import AsyncHttpDownloader
from mercury.settings import logger, MAX_JOBS
from mercury.models import LawUpstream, JobQueue
from mercury.spiders import BaseSpider
from .laws_helper import (
    MEDIA_TYPES, RE_ATTACHMENT,
    extract_table_pre_img_link_content, extract_pkulaw_v6_metainfo)

url_pkulaw = 'http://www.pkulaw.com'
url_login = url_pkulaw + '/login?menu=&isAutoLogin=false'


class Spider(BaseSpider):
    name = 'pkulaw'

    Model = LawUpstream

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
                "Accept-Encoding": "gzip, deflate",
                "Accept-Language": "zh-CN,zh;q=0.9",
                "Connection": "keep-alive",
                "Content-Type": "application/x-www-form-urlencoded",
                "DNT": "1",
                "Host": "www.pkulaw.com",
                "Origin": url_pkulaw,
                "Referer": url_pkulaw,
                "User-Agent": random_ua()
            },
            "cookies": {},
            "sleep_range_before_request": (0, MAX_JOBS),
            "uniqid": "fbid",
            "save_html": True,
            "proxy_enable": True
        }
        return configs

    async def before_request(self):
        if self.job.is_seed:
            self.job.method = 'post'
            self.job.ctype = 'formdata'
        return True

    async def handle_download_failure(self, error):
        # 将下载失败的任务重新加入任务队列
        await JobQueue.rpush(self.job)

    async def handle_index(self, page):
        soup = BeautifulSoup(page, 'lxml')
        hrefs = (x.attrs['href'].rsplit('?', 1)[0] for x in soup.select(
            'div.list-wrap > ul > li a') if x.attrs.get('flink') == 'true')
        detail_infos = [{'url': url_pkulaw + href,
                         'fbid': href.rstrip('.html').rsplit('/', 1)[-1]} for href in hrefs]
        if soup.select('div.pagination > ul > li > a') and \
                soup.select('div.pagination > ul > li > a')[-1].attrs.get('pageindex'):
            self.job.payload['Pager.PageIndex'] += 1
            next_info = {'url': self.job.url, 'payload': self.job.payload}
        else:
            next_info = None
        return detail_infos, next_info

    async def handle_detail(self, page, encoding='utf-8', extra_data=None):
        pq = PyQuery(page)
        # 验证页面
        if pq('#verify'):
            logger.info('[Spider]<{}> verify page...'.format(self.name))
            return None
        fbid = pq('input#ArticleId').val()
        # lib
        lib = pq('input#DbId').val()
        # eg: 全国人大常委会关于修改《中华人民共和国个人所得税法》的决定(2018)-北大法宝V6官网
        title = pq('title').text().rsplit('-', 1)[0]

        # 非法页面
        if not fbid or not lib or not title:
            return None

        retval = {
            'url': self.job.url,
            'sid': self.job.seed_urlmd5,
            'getway': self.name,
            'fbid': fbid,
            'lib': lib,
            'title': title,
        }
        # 需要先登录才能查看完整信息
        if pq('#WeChatPayUrl').val():
            await self.queue.sadd('mercury:pkulaw:detail:urls', self.job.url)
            logger.info('[Spider]<{}> need login: {}'.format(
                self.name, self.job.url))
            # 所有需要先登录才能查看完整信息的页面都有needlogin字段,并且其值为True
            # 所有不需要登录就能查看完整信息的页面要么没有该字段,要么有该字段值但值为None
            retval['needlogin'] = True

        mainblock = pq('div#divFullText')
        html = mainblock.html()
        content = extract_table_pre_img_link_content(html)

        mediatypes = [t for t in MEDIA_TYPES if mainblock(t)]
        hasattachments = True if RE_ATTACHMENT.search(content) else False
        retval['content'] = content
        retval['mediatypes'] = mediatypes
        retval['hasattachments'] = hasattachments
        # 正文内容为空
        if not content:
            return None
        metainfo = extract_pkulaw_v6_metainfo(page, lib)
        retval.update(metainfo)
        return retval
