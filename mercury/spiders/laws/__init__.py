from .itslaw import ItslawSpider
from .ttfawu import Spider as FawuSpider
from .pkulaw import Spider as PkulawSpider
from .pkulawapi import Spider as PkulawApiSpider
from .pkulaw_gzcj import Spider as PkulawGzcjSpider
from .pkulaw_xndx import Spider as PkulawXndxSpider
