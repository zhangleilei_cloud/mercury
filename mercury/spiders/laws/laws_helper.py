import os
import re
import csv
import time
import yaml
from operator import itemgetter
from datetime import datetime
from urllib.parse import urljoin, unquote

import ftfy
import execjs
import pymongo
from pyquery import PyQuery
from bs4 import BeautifulSoup

from mercury.settings import logger, DEPLOYTYPE, DIR_SETTING, DeployType
from mercury.libs import s3

# DATABASE
URI_MONGO = 'mongodb://mercury:MercurY4dm1n!@172.16.0.224:38017,172.16.0.225:38017/mercury?replicaSet=rs0'
DB = pymongo.MongoClient(URI_MONGO).get_database('mercury')
LAWS_UPSTREAM = DB['laws_upstream']
LAWS_ORIGIN = DB['laws_origin']
LAWS = DB['laws']


URL_RESOURCE = 'http://resources.pkulaw.cn'
URL_TAXLAW = 'http://124.192.33.50:6008/Law/FullText/ViewFullText?library={0}&gid={1}'
URL_GZCJ_PREFIX = 'http://58.16.80.192:9000/rwt/BDFY/http/PJTYG55WPJSXK63PPBWYK5DBP6YGG5C'
URL_XNDX_PREFIX = 'http://pcrsile.swu.edu.cn:80/REWRITER/GR5DRNU6NB4HI6B4F6YYE3LUN74YEZ5FPMYHA45WNSRYPLUDNG/'
URL_TAXLAW_PREFIX = 'http://124.192.33.50:6008'

RE_SPACE = re.compile(r'\s+')
RE_FIELD = re.compile(r'【(?P<field>\w+)】')
RE_ATTACHMENT = re.compile('附件[:：]?[一1]?')
RE_CLI = re.compile(r'(chl|lar|aom|cas|eag|eagn|hnt|iel|lfbj|scs)_\d+')
RE_EXPR = """(<table.*?>[\s\S]*?<\/table>)|""" + \
          """(<pre.*?>[\s\S]*?<\/pre>)|""" + \
          """(<img src=[\'\"](?P<url>\S+)[\'\"] class=[\'\"]fjLink[\'\"].*?>)|""" + \
          """(<a href=[\'\"](?P<link>\S+)[\'\"] (class=[\'\"]fjLink[\'\"] target=[\'\"]_blank[\'\"]|""" + \
          """style=[\'\"].+[\'\"])(.*?)>(?P<explain>\S*?)<\/a>)"""

RE_TABLE_PRE_IMG_LINK = re.compile(RE_EXPR)
RE_POSTFIX = re.compile(r"(chl|lar)_\w+")

BASE_PATH = '/data/minio/mercury/extras'
PATH_CATEGORIES = os.path.join(BASE_PATH, 'categories.csv')
PATH_DEPARTMENTS = os.path.join(BASE_PATH, 'departments.csv')
PATH_EFFECTIVEDIS = os.path.join(BASE_PATH, 'effectivenessdic.csv')
PATH_JS_FIELDS = os.path.join(BASE_PATH, 'listfields.js')

if not os.path.exists(BASE_PATH):
    os.makedirs(BASE_PATH)

# 如果依赖的文件不存在, 就下载文件
_files = list(map(lambda x: ('extras/{}'.format(x), os.path.join(BASE_PATH, x)),
                  filter(lambda x: not os.path.exists(os.path.join(BASE_PATH, x)),
                         ['categories.csv', 'departments.csv', 'effectivenessdic.csv', 'listfields.js'])))

if _files:
    logger.info('Download Dependency Files...')

    _s3_conf = {
        'bucket': 'mercury',
        'key': '3B1BAPL1IEGJNM2B3QHY',
        'secret': 'QKEqkqkFGzue5ZxR8V9AVVSr8BTXyuaiB8YCkCxN',
        'region': 'us-west-1',
        'endpoint': 'http://39.104.237.216:9000'
    } if DEPLOYTYPE in (DeployType.DEV, DeployType.TEST) else None

    import asyncio

    async def _download_files(_files):
        for (src, des) in _files:
            await s3.download_file(src, des, s3conf=_s3_conf)

    _loop = asyncio.get_event_loop()
    _loop.run_until_complete(_download_files(_files))


MEDIA_TYPES = ['table', 'img', 'pre']
LIBS = ['aom', 'chl', 'con', 'eagn', 'fmt', 'hkd', 'iel', 'lar', 'lfbj', 'twd']

# 字段中英文对照
FIELDS_MAPPING = {
    'chl': {
        '法规标题': 'title',
        '类别': 'category',
        '法规类别': 'category',
        '发文字号': 'documentno',
        '批准部门': 'issuedepartment',
        '批准日期': 'ratifydate',
        '发布部门': 'issuedepartment',
        '发布日期': 'issuedate',
        '实施日期': 'implementdate',
        '时效性': 'timelinessdic',
        '效力级别': 'effectivenessdic',
        '唯一标志': 'gid',
        '全文': 'content',
        # 党内法规的字段
        '专题': 'category',
        '失效依据': 'invalidbasis',
        '修改依据': 'modifybasis',
        '部分失效依据': 'partinvalidbasis',
        '法宝提示': 'pkulawtips',
    },
    'lar': {
        '法规标题': 'title',
        '类别': 'category',
        '法规类别': 'category',
        '发文字号': 'documentno',
        '批准部门': 'issuedepartment',
        '批准日期': 'ratifydate',
        '发布部门': 'issuedepartment',
        '发布日期': 'issuedate',
        '实施日期': 'implementdate',
        '时效性': 'timelinessdic',
        '效力级别': 'effectivenessdic',
        '唯一标志': 'gid',
        '全文': 'content',
        '专题': 'category',
        '失效依据': 'invalidbasis',
        '修改依据': 'modifybasis',
        '部分失效依据': 'partinvalidbasis',
        '法宝提示': 'pkulawtips',
    },
    'iel': {
        '相关组织': 'originization',
        '颁布日期': 'issuedate',
        '生效日期': 'implementdate',
        '类别': 'category',
        '唯一标志': 'gid',
        '全文': 'content'
    },
    'fmt': {
        '文书编号': 'documentno',
        '文书分类': 'category',
        '发布日期': 'issuedate',
        '发布部门': 'issuedepartment',
        '时效性': 'timelinessdic',
        '发文名称': 'documenttitle',
        '唯一标志': 'gid',
        '全文': 'content'
    },
    'hkd': {
        '分类': 'category',
        '英文标题': 'englishtitle',
        '唯一标志': 'gid',
        '全文': 'content'
    },
    'aom': {
        '法规分类': 'legalcategory',
        '类别': 'category',
        '刊登日期': 'issuedate',
        '相关信息': 'relationinfo',
        '唯一标志': 'gid',
        '全文': 'content'
    },
    'twd': {
        '类别': 'category',
        '颁布日期': 'issuedate',
        '唯一标志': 'gid',
        '全文': 'content'
    },
    'con': {
        '合同分类': 'category',
        '合同条数': 'number',
        '发布日期': 'issuedate',
        '发文字号': 'documentno',
        '时效性': 'timelinessdic',
        '行业类别': 'industrycategory',
        '字数区间': 'wordnumberrange',
        '发文名称': 'documenttitle',
        '合同编号': 'contractno',
        '官方合同': 'officialcontract',
        '唯一标志': 'gid',
        '全文': 'content'
    },
    'lfbj': {
        '发布部门': 'issuedepartment',
        '类别': 'category',
        '发布日期': 'issuedate',
        '唯一标志': 'gid',
        '全文': 'content',
    },
    'eag': {
        '国家与国际组织': 'originization',
        '条约分类': 'category',
        '签订日期': 'issuedate',
        '生效日期': 'implementdate',
        '批准日期': 'ratifydate',
        '批准机关': 'ratifydepartment',
        '时效性': 'timelinessdic',
        '条约种类': 'treatytype',
        '签订地点': 'signplace',
        '保存机关': 'preservedepartment',
        '交存时间': 'deliverydate',
        '中国生效时间': 'chinaeffectivedate',
        '适用于港澳': 'applyforhkmo',
        '失效日期': 'expiratedate',
        '签署时间': 'signdate',
        '声明与保留': 'declarationreservation',
        '修订日期': 'revisiondate',
        '唯一标志': 'gid',
        '全文': 'content',
    },
    'eagn': {
        '国家与国际组织': 'originization',
        '条约分类': 'category',
        '签订日期': 'issuedate',
        '生效日期': 'implementdate',
        '批准日期': 'ratifydate',
        '批准机关': 'ratifydepartment',
        '时效性': 'timelinessdic',
        '条约种类': 'treatytype',
        '签订地点': 'signplace',
        '保存机关': 'preservedepartment',
        '交存时间': 'deliverydate',
        '中国生效时间': 'chinaeffectivedate',
        '适用于港澳': 'applyforhkmo',
        '失效日期': 'expiratedate',
        '签署时间': 'signdate',
        '声明与保留': 'declarationreservation',
        '修订日期': 'revisiondate',
        '唯一标志': 'gid',
        '全文': 'content'
    },
    'news': {
        '发布日期': 'issuedate',
        '来源': 'source',
        '作者': 'author',
        '关键词语': 'keywords',
        '唯一标志': 'gid',
        '全文': 'content'
    },
    'fnl': {
        # 案例指导, 不录入
    }
}

TIMELINESSDIC = {
    '01': '现行有效',
    '02': '失效',
    '03': '已被修改',
    '04': '尚未生效',
    '05': '部分失效',
}


def load_csv(path):
    retval = dict()
    with open(path, 'r', encoding='utf-8') as f:
        reader = csv.reader(f)
        for item in reader:
            if item:
                retval[item[0]] = item[1]
    return retval


CATEGORIES = load_csv(PATH_CATEGORIES)
DEPARTMENTS = load_csv(PATH_DEPARTMENTS)
EFFECTIVEDIS = load_csv(PATH_EFFECTIVEDIS)


def js_list_fileds_to_dict(list_fields):
    retval = dict()
    size = len(list_fields)
    for i in range(0, size, 2):
        retval[list_fields[i]] = list_fields[i+1]
    return retval


def load_js(path):
    with open(path, 'r', encoding='utf-8') as f:
        page = f.read()
    js_compiler = execjs.compile(page)
    return js_compiler


def load_js_fields(js_compiler):
    """加载包含各个字段的JS文件
    """

    retval = dict()
    js_fieldes = js_compiler.eval('m_ListFieldList')
    for lib in LIBS:
        if lib == 'eagn':
            lib = 'eag'
        lib_fieldes = js_fieldes[lib]
        shixiao = lib_fieldes.get('shixiao_id', [])
        xiaoli = lib_fieldes.get('xiaoli_id', [])
        pdep = lib_fieldes.get('pdep_id', [])
        fdep = lib_fieldes.get('fdep_id', [])
        sort = lib_fieldes.get('sort_id', [])
        timelinessdic = js_list_fileds_to_dict(shixiao)
        effectivenessdic = js_list_fileds_to_dict(xiaoli)
        category = js_list_fileds_to_dict(sort)
        pdep_dict = js_list_fileds_to_dict(pdep)
        fdep_dict = js_list_fileds_to_dict(fdep)
        pdep_dict.update(fdep_dict)
        issuedepartment = pdep_dict
        ratifydepartment = pdep_dict
        retval[lib] = {
            'timelinessdic': timelinessdic,
            'effectivenessdic': effectivenessdic or EFFECTIVEDIS,
            'category': category or CATEGORIES,
            'issuedepartment': issuedepartment or DEPARTMENTS,
            'ratifydepartment': ratifydepartment or DEPARTMENTS,
        }
    return retval


JS_COMPILER = load_js(PATH_JS_FIELDS)
JS_FIELDS = load_js_fields(JS_COMPILER)


def convert_field(field, collection, delimiter='\s+'):
    """对指定字符串中的n个key找到对应的value并返回拼接后的字符串
    """

    retval = []
    vals = re.split(delimiter, field.strip())
    for val in vals:
        if val in collection:
            retval.append(collection[val])
        else:
            retval.append(val)
    return ' '.join(retval)


def strip_expired_title(title):
    """去除title末尾[失效]|[废止]两个字
    """

    if title.endswith('[失效]'):
        title = title[:-4]
    elif title.endswith('[废止]'):
        title = title[:-4]
    return title.strip().replace('\n', '').replace(' ', '')


def strip_department(department):
    """去除部门末尾的 "机构沿革"
    """

    if department.endswith('机构沿革'):
        department = department[:-4].strip()
    return department


def convert_date_format(date_str):
    """日期格式转换
    """

    try:
        d = datetime.strptime(date_str, '%Y.%m.%d')
    except:
        try:
            d = datetime.strptime(date_str, '%Y.%m')
        except:
            try:
                d = datetime.strptime(date_str, '%Y')
            except Exception as e:
                logger.info('[Invalid Date]: {}'.format(date_str))
                return date_str
            else:
                return d.strftime('%Y')
        else:
            return d.strftime('%Y-%m')
    else:
        return d.strftime('%Y-%m-%d')


FIELD_PROCESSOR_LINK = {
    'issuedate': convert_date_format,
    'implementdate': convert_date_format,
    'ratifydate': convert_date_format,
    'title': strip_expired_title,
    'issuedepartment': strip_department,
    'ratifydepartment': strip_department,
}


def process_fields(fields):
    """fields字段提取后处理
    """

    for (k, v) in fields.items():
        if k in FIELD_PROCESSOR_LINK:
            fields[k] = FIELD_PROCESSOR_LINK[k](v)
    return fields


def _get_html_text(html):
    try:
        text = PyQuery(html.strip()).text().strip()
    except:
        try:
            text = BeautifulSoup(html.strip(), "lxml").get_text().strip()
        except:
            return html.strip()
        else:
            return text
    else:
        return text


def extract_table_pre_img_link_content(html):
    """提取正文内容中的文字,表格,图片,超链接等信息
    """

    logger.debug('[Html]: {}'.format(html))
    iters = RE_TABLE_PRE_IMG_LINK.finditer(html)
    if not iters:
        text = _get_html_text(html)
        return text

    retval = []
    idx = 0
    for e in iters:
        start, end = e.span()
        text = _get_html_text(html[idx: start])
        raw = html[start: end]
        if e.groupdict().get('url') is not None:
            _url = e.groupdict()['url']
            url = _url.replace('\\', '/')
            url = url.lstrip(URL_GZCJ_PREFIX).lstrip(URL_TAXLAW_PREFIX)
            if not url.startswith(URL_RESOURCE):
                url = urljoin(URL_RESOURCE, url)
            url = ftfy.fix_text(url)
            # raw = "<img src='{url}' class='fjLink'/>".format(url=url)
            raw = html[start: end].replace(_url, url)
        elif e.groupdict().get('link') is not None:
            _link = e.groupdict()['link']
            link = _link
            for prefix in (URL_GZCJ_PREFIX, URL_XNDX_PREFIX, URL_TAXLAW_PREFIX):
                if link.startswith(prefix):
                    link = link[len(prefix):]
                    break
            if not link.startswith(URL_RESOURCE):
                link = urljoin(URL_RESOURCE, link)
            link = unquote(link)
            # explain = e.groupdict()['explain']
            # raw = "<a href='{link}' class='fjLink' target='_blank'>{explain}</a>".format(
            #     link=link, explain=explain)
            raw = html[start: end].replace(_link, link)
        idx = end
        retval.append(text)
        retval.append(raw)

    if idx < len(html):
        text = _get_html_text(html[idx:])
        retval.append(text)

    fulltext = '\n'.join(retval)
    ret = RE_POSTFIX.search(fulltext[-100:])
    if ret:
        fulltext = fulltext[:-100] + fulltext[-100: -100 + ret.span()[0]]
    return fulltext


def extract_pkulaw_v6_metainfo(html, lib):
    """提取北大法宝V6官网详情页面的元信息
    """

    pq = PyQuery(html)
    cli = pq('div.info > span > a').text()
    fields = dict([i.strip() for i in x.text().split('：', 1)]
                  for x in (PyQuery(e) for e in pq('div.fields > ul > li')))
    fields = {FIELDS_MAPPING[lib][k]: v for k, v in fields.items()}
    fields = process_fields(fields)
    changeblock = [PyQuery(e) for e in
                   pq('div.change-wrap:first > div.box > ul > li')][::-1]
    changes = [[*e.text().split(' ', 1), e('a').attr['href']]
               for e in changeblock]
    pkulawinfos = {'cli': cli, 'tips': fields.pop('pkulawtips', None)}

    return {
        'changes': changes,
        'pkulawinfos': pkulawinfos,
        **fields
    }


def extract_content(html):
    """解析法规数据的正文内容
    """

    # 修复html中存在的非法字符
    html = ftfy.fix_text(html)
    mainblock = PyQuery(html)
    # 本法变迁
    changes = [PyQuery(e).text() for e in
               mainblock('font.MTitle').parent().prev_all('a.alink')]
    # 法宝联想
    mainblock.remove('div.TiaoYinV2')
    # 本法变迁史
    mainblock.remove('font.TiaoYin:contains("【本法变迁史】")')
    # 本法变迁史
    mainblock('font.MTitle').parent().prev_all('a.alink').remove()
    # 目录
    mainblock.remove('a.miaodian')
    # 目录
    mainblock.remove('div[@align=center]:contains("目 录")')

    # 正文内容
    html = mainblock.html()
    content = extract_table_pre_img_link_content(html)

    cli = None
    parts = content.rsplit('\n', 1)
    if len(parts) == 2:
        prev, last = parts
        ret = RE_CLI.search(last)
        if ret:
            cli = ret.group(0)
            content = prev.rstrip()

    mediatypes = [t for t in MEDIA_TYPES if mainblock(t)]
    hasattachments = True if RE_ATTACHMENT.search(content) else False

    retval = {
        'content': content,
        'changes': changes,
        'mediatypes': mediatypes,
        'hasattachments': hasattachments,
        'pkulawinfos': {
            'cli': cli
        }
    }
    return retval


def extract_taxlaw_metainfo(lib, gid, page):
    """提取北大法宝镜像站税务局数据源的元信息
    """

    pq = PyQuery(page)
    # 元信息
    fields = {}
    trs = pq('table.articleInfo tr')
    for tr in trs:
        tds = [PyQuery(td) for td in PyQuery(tr)('td')]
        if tds:
            for i in range(0, len(tds), 2):
                k, v = tds[i].text(), tds[i+1].text()
                if not k:
                    continue
                r = RE_FIELD.search(k)
                if r:
                    field = r.groupdict().get('field')
                    f = FIELDS_MAPPING[lib][field]
                    fields[f] = v

    if fields.get('category'):
        fields['category'] = convert_field(fields['category'], CATEGORIES)
    if fields.get('issuedepartment'):
        fields['issuedepartment'] = convert_field(
            fields['issuedepartment'], DEPARTMENTS)
    if fields.get('ratifydepartment'):
        fields['ratifydepartment'] = convert_field(
            fields['ratifydepartment'], DEPARTMENTS)
    if fields.get('effectivenessdic'):
        fields['effectivenessdic'] = convert_field(
            fields['effectivenessdic'], EFFECTIVEDIS)

    if fields.get('issuedate'):
        d = convert_date_format(fields['issuedate'])
        if d:
            fields['issuedate'] = d
        else:
            return None
    if fields.get('implementdate'):
        d = convert_date_format(fields['implementdate'])
        if d:
            fields['implementdate'] = d
        else:
            return None
    if fields.get('ratifydate'):
        d = convert_date_format(fields['ratifydate'])
        if d:
            fields['ratifydate'] = d
        else:
            return None

    if lib == 'eagn':
        if fields.get('signdate'):
            d = convert_date_format(fields['signdate'])
            if d:
                fields['signdate'] = d
            else:
                return None
        if fields.get('expiratedate'):
            d = convert_date_format(fields['expiratedate'])
            if d:
                fields['expiratedate'] = d
            else:
                return None
        if fields.get('revisiondate'):
            d = convert_date_format(fields['revisiondate'])
            if d:
                fields['revisiondate'] = d
            else:
                return None
        if fields.get('chinaeffectivedate'):
            d = convert_date_format(fields['chinaeffectivedate'])
            if d:
                fields['chinaeffectivedate'] = d
            else:
                return None
        if fields.get('deliverydate'):
            d = convert_date_format(fields['deliverydate'])
            if d:
                fields['deliverydate'] = d
            else:
                return None
    return fields


def extract_sorted_field(fields):
    """按照字典顺序抽取字典中的值并合并在一起

    :param dict fields: 字段键值对
    :return: 排序后的字段组合
    :rtype: str
    """

    items = sorted(fields.items(), key=itemgetter(0))
    return ' '.join(map(itemgetter(1), items))


def validate_sorted_field(field, field_type):
    """检查字段中是否存在编码，如果存在把编码变成对应的值

    :param str field: 排序后的字段 "value1  value2 value3 ..."
    :return: 最后一个字段对应的值
    :rtype: str
    """

    if field_type.lower()[0] == 'c':
        fields_mapping = CATEGORIES
    elif field_type.lower()[0] == 'e':
        fields_mapping = EFFECTIVEDIS
    elif field_type.lower()[0] == 'd':
        fields_mapping = DEPARTMENTS
    else:
        return field
    retval = ' '.join([fields_mapping.get(f, f) for f in field.split(' ')])
    return retval
