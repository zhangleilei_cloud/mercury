# -*- coding: utf-8 -*-

import re
import json
from datetime import datetime

from mercury.downloader import AsyncHttpDownloader
from mercury.libs.proxy import get_proxy
from mercury.settings import logger
from mercury.utils import timestamp
from mercury.models import Law
from mercury.spiders import BaseSpider


no_data_itslaw = "mercury:pkulaw:itslaw:{urlmd5}"
index_referer_url_origin = ("https://www.itslaw.com/search/lawsAndRegulations?"
                            "searchMode=lawsAndRegulations&sortType=5&")
detail_referer_url_origin = ("https://www.itslaw.com/search/lawsAndRegulations/lawAndRegulation?"
                             "searchMode=lawsAndRegulations&lawAndRegulationId={lawAndRegulationId}&conditions=")
detail_url = ("https://www.itslaw.com/api/v1/lawRegulations/lawRegulation?"
              "lawAndRegulationId={lawAndRegulationId}")


class ItslawSpider(BaseSpider):
    name = "itslaw"
    Model = Law

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "User-Agent": ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 "
                               "(KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
            },
            "cookies": {},
            "uniqid": "urlmd5",
            "sleep_range_before_request": (2, 10),
            "save_html": True,
            "proxy_enable": True,
        }
        return configs

    async def before_request(self):
        headers = self.configs.get("headers")
        # index detail 页面的referer
        if self.job.is_seed:
            referer_search_word = re.search(
                r"&conditions=(.*)", self.job.url).group(1)
            referer_url = index_referer_url_origin + referer_search_word
            headers.update({"Referer": referer_url})
        elif self.job.extra_data:
            referer = self.job.extra_data.pop("Referer", None)
            if referer:
                headers.update({"Referer": referer})

        self.dl = AsyncHttpDownloader(
            timeout=60,
            headers=headers,
            proxy=get_proxy() if self.configs.get("proxy_enable") else None)
        return True

    async def handle_detail(self, page, encoding, extra_data=None):
        info = json.loads(page)
        data, content, appendix = {}, [], []
        try:
            if info.get("result", None):
                if info["result"]["code"] == 0:
                    detail_text = info["data"]["lawRegulationDetail"]
                    content_text = detail_text["lawRegulationParagraphs"]
                    for text_info in content_text:
                        content.append((text_info.get("text", "")))
                    # 是否有附件,取出附件内容
                    if detail_text.get("appendixParagraphs"):
                        for appendix_info in detail_text["appendixParagraphs"]:
                            appendix.append((appendix_info.get("text", "")))

                    content = " ".join(content)
                    appendix = " ".join(appendix)
                    if content:
                        reg = re.compile("<[^>]*>")
                        content = reg.sub("", content)
                        appendix = reg.sub("", appendix)
                        if self.configs.get('save_html'):
                            fname = "{spider}/{date}/{urlmd5}.json.gz".format(
                                spider=self.name,
                                date=timestamp().strftime("%Y%m%d"),
                                urlmd5=self.job.urlmd5)
                        else:
                            fname = None
                        data.update({
                            "content": content,
                            "update_time": timestamp(),
                            "update_source": {
                                "getway": self.name,
                                "url": self.job.url,
                                "s3": fname,
                                "appendix": appendix
                            }
                        })
        except Exception as e:
            await self.queue.sadd('mercury:pkulaw:itslaw:error', json.dumps(self.job.extra_data, ensure_ascii=False))
            logger.error(e, exc_info=True)
        return data

    async def handle_index(self, page):
        detail_infos, next_info = [], {}
        info = json.loads(page)
        try:
            if (not info.get("result")) or (info["result"].get("code") != 0):
                return (detail_infos, next_info)

            total_count = info["data"]["lawRegulationArticleSearchResult"]["totalCount"]
            if total_count > 0:
                articles = info["data"]["lawRegulationArticleSearchResult"]["lawRegulationArticles"]
                for content in articles:
                    # 取出元信息 itslaw 格式: 2010-10-18  pkulaw 格式: 1986.06.18
                    title = content.get("lawRegulationTitle")
                    publish_date = datetime.strptime(
                        content.get("publishDate"), "%Y-%m-%d")
                    effective_date = datetime.strptime(
                        content.get("effectiveDate"), "%Y-%m-%d")

                    if self.job.extra_data:
                        title__pkulaw = self.job.extra_data.get("title")
                        publish_date_pkulaw = datetime.strptime(
                            self.job.extra_data.get("issuedate"), "%Y.%m.%d")
                        effective_date_pkulaw = datetime.strptime(
                            self.job.extra_data.get("implementdate"), "%Y.%m.%d")

                        # 查询时两个网站的 生效日期 与 发布日期两项 相差3天之内的视为同一个
                        if (title__pkulaw == title and abs((publish_date_pkulaw - publish_date).days) <= 3
                                and abs((effective_date_pkulaw - effective_date).days) <= 3):

                            # 一致的话取出详情id, 拼接详情页和referer
                            detail_id = content["lawRegulationId"]
                            search_word = re.search(
                                r"&conditions=(.*)", self.job.url).group(1)
                            link_url = detail_url.format(
                                lawAndRegulationId=detail_id)
                            link_referer_url = detail_referer_url_origin.format(
                                lawAndRegulationId=detail_id) + search_word
                            self.job.extra_data.update({
                                "Referer": link_referer_url,
                                'isupdate': True
                            })
                            detail_infos.append({
                                "url": link_url,
                                "extra_data": self.job.extra_data
                            })
                        break
            else:
                await self.queue.sadd('mercury:pkulaw:itslaw:notfound', json.dumps(self.job.extra_data, ensure_ascii=False))
        except Exception as e:
            await self.queue.sadd('mercury:pkulaw:itslaw:error', json.dumps(self.job.extra_data, ensure_ascii=False))
            logger.error(e, exc_info=True)
        return (detail_infos, next_info)
