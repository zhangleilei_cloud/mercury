"""华律网解决方案
"""

import re
from pyquery import PyQuery

from mercury.spiders._base import Spider as BaseSpider
from mercury.models import Solution


RE_WHITE_SPEACE = re.compile(r'\s+')
RE_IMAGE = re.compile(
    r'''<img src=\"(?P<src>\S+?)\"\s*?style=\"\S*?\"\s*?title=\"(?P<title>\S+)\"[\s\S]*?>''')
RE_DIGIT = re.compile(r'''(?P<count>\d+)''')
RE_ANLI = re.compile(r'''anli_\d+''')

url_66law = 'http://www.66law.cn'


class Spider(BaseSpider):
    name = "hualv_solution"

    Model = Solution

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "User-Agent": (" Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:60.0)"
                               " Gecko/20100101 Firefox/60.0")
            },
            "cookies": {},
            "sleep_range_before_request": (10, 30),
            "uniqid": "urlmd5",
            "save_html": True,
            "proxy_enable": True
        }
        return configs

    async def handle_index(self, page):
        detail_infos, next_info = [], None
        pq = PyQuery(page)
        lis = pq('ul.sol-list3 > li')
        for li in lis:
            e = PyQuery(li)
            href = e('a.fr.ml50.mt10').attr('href')
            url = f'{url_66law}{href}'
            title = e('p > a.s-be.f16').attr('title')
            visit = e('p.clearfix.mt10 > span.s-c999.fl').remove('i').text()
            r = RE_DIGIT.search(visit)
            visit_count = int(r.gourp(0)) if r else None
            like = e('p.clearfix.mt10 > a.fl.s-c999.sol-dz.praise > span').text()
            r = RE_DIGIT.search(like)
            like_count = int(r.gourp(0)) if r else None
            extra_data = {'title': title,
                          'visit_count': visit_count,
                          'like_count': like_count}
            detail_infos.append({'url': url, 'extra_data': extra_data})
        next_url = pq('div.m-page.tc.mt40 > a.m-page-next').attr('href')
        next_info = {'url': f'{url_66law}{next_url}'} if next_url else None
        return detail_infos, next_info


    async def handle_detail(self, page, encoding, extra_data=None):
        pq = PyQuery(page)
        title = extra_data['title']
        visit_count = extra_data['visit_count']
        like_count = extra_data['like_count']
        nav = pq('div.u-weizhi.mt15 > a').text()
        catalog = pq('div.flo-list > p > a').remove('em').text()
        catalog = RE_WHITE_SPEACE.split(nav)
        section = []
        mainblock = pq('div.w660.fl')
        for child in mainblock.children():
            # 点赞 收藏 分享 终止
            if child.tag == 'div' and child.attrib['class'] == 'm-pe-ct-se mt50 pl120':
                break
            # 去除 "如果您需要进入诉讼流程为自己维权，我们将推荐专业的律师为您服务"
            elif child.tag == 'p' and child.attrib['class'] == 'f14 mt30':
                continue
            # 开头的标题, 直接添加
            elif child.tag == 'h1' and child.attrib['class'] == 'f28 lh32':
                section.append(PyQuery(child).text)
            # 法律攻略
            elif child.tag == 'div' and child.attrib['class'] == 'mt30':
                methods = []
                tabs = PyQuery(child).find(
                    'div.ce-tab > a').remove('i.ico-sol.i-sol19').text()
                tabs = RE_WHITE_SPEACE.split(tabs)
                lis = PyQuery(child).find('ul.ce-list > li')
                for li in lis:
                    e = PyQuery(li)
                    q = e('p > img')
                    if q:
                        src = q.attr('src')
                        _title = q.attr('title')
                        img = f'<img src="http:{src}", title="{_title}">'
                        methods.append(img)
                    else:
                        text = e.text()
                        methods.append(text)
                policy = []
                for (tab, method) in zip(tabs, methods):
                    policy.append({tab: method})
                section.append(policy)
            # 律师
            elif child.tag == 'ul' and child.attrib['class'] == 'sol-list8 mt20 clearfix':
                lawyers = []
                lis = PyQuery(child).find('li')
                for li in lis:
                    e = PyQuery(li)
                    photo = e('p.clearfix > a > img').attr('src')
                    name = e('p.f14.tc.mt10 > a').attr('title')
                    domain = e('p.s-c999.tc.mt5').text()
                    phone = e('p.mt5 > span.f12 > em.s-oe.fn-tel').text()
                    refer = e('p.mt10.btn-block > a').attr('href')
                    lawyer = {
                        'name': name,
                        'photo': f'http:{photo}',
                        'phone': phone,
                        'domain': domain,
                        'refer': refer,
                    }
                    lawyers.append(lawyer)
                section.append(lawyers)
            # 通用提取
            else:
                res = []
                idx = 0
                pq = PyQuery(child)
                html = pq.html()
                iters = RE_IMAGE.finditer(html)
                if not iters:
                    res.append(pq.text())
                else:
                    for it in iters:
                        start, end = it.span()
                        img = html[start: end]
                        src = it.groupdict()['src']
                        img = img.replace(src, f'http:{src}')
                        res.append(PyQuery(html[idx: start]).text())
                        idx = end
                        res.append(img)
                    res.append(PyQuery(html[idx:]).text())
                section.append('\n'.join(res))

        content = [{key: value} for key, value in zip(catalog, section)]
        retval = {
            'title': title,
            'nav': nav,
            'visit_count': visit_count,
            'like_count': like_count,
            'catalog': catalog,
            'content': content,
        }
        return retval
