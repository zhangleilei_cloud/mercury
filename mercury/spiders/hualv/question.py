"""华律网问题解答
"""

import sys
sys.path.insert(0, '../../../extras')

from pyquery import PyQuery

from mercury.spiders._base import Spider as BaseSpider
from mercury.models import Question, JobQueue
from mercury.settings import logger, MAX_JOBS
from mercury.libs.useragent import random_ua
from extras.hualv.parse_page import parse_detail


url_66law = 'http://www.66law.cn'
url_question = 'http://www.66law.cn/question/{i}.aspx'


class Spider(BaseSpider):
    name = 'hualv_question'

    Model = Question

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "User-Agent": random_ua(),
            },
            "cookies": {},
            # "sleep_range_before_request": (10, 30),
            "uniqid": "qid",
            "save_html": True,
            # "proxy_enable": True
        }
        return configs

    async def handle_download_failure(self, error):
        """用于下载失败的后处理"""
        # 将下载失败的任务重新加入任务队列
        await JobQueue.rpush(self.job)

    async def handle_index(self, page):
        if isinstance(page, str):
            return self.handle_index_page(page)
        else:
            return await self.handle_index_items(page)

    async def handle_detail(self, page, encoding, extra_data=None):
        data = parse_detail(page)
        return data

    def handle_index_page(self, page):
        detail_infos, next_info = [], None
        pq = PyQuery(page)
        lis = pq('ul.advisory-list > li > p.t > a')
        for li in lis:
            e = PyQuery(li)
            href = e.attr('href')
            title = e.attr('title')
            url = f'{url_66law}{href}'
            qid = url.rsplit('.', 1)[0].rsplit('/', 1)[-1]
            detail_infos.append({'url': url,
                                 'title': title,
                                 'qid': f'question_{qid}'})
        next_url = pq('div.u-page.mt30.mb20.tc > a.u-p-next').attr('href')
        next_info = {'url': f'{url_66law}{next_url}'} if next_url else None
        logger.debug(detail_infos)
        logger.debug(next_info)
        return detail_infos, next_info

    async def handle_index_items(self, page):
        lastid = page['lastid']
        newid = lastid + MAX_JOBS
        await self.queue.set('mercury:hualv:lastid', newid)
        detail_infos = [{'url': url_question.format(i=i)}
                        for i in range(lastid + 1, newid + 1)]
        next_info = {'url': self.job.url}
        return detail_infos, next_info
