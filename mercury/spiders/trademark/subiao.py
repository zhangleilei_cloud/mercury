# -*- coding: utf-8 -*-

import re
from datetime import datetime

import lxml

from mercury.downloader import AsyncHttpDownloader
from mercury.libs import s3
from mercury.libs.proxy import get_proxy
from mercury.models import Trademark
from mercury.monitor import time_on_upload_s3
from mercury.settings import logger
from mercury.spiders._base import Spider as BaseSpider


s3_trademark_url = "trademark/{spider}/{reg_code}{suffix}"


class SubiaoSpider(BaseSpider):
    name = "subiao"
    Model = Trademark

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "User-Agent": ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 "
                               "(KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
            },
            "cookies": {},
            "uniqid": "urlmd5",
            "sleep_range_before_request": (2, 10),
            "save_html": False,
            "proxy_enable": True,
            "save_image": False
        }
        return configs

    def _extract_html_page(self, page):
        name_list = ["reg_code", "inter_class", "date_apply", "date_reg", "reg_person_zh", "services_list",
                     "reg_person_en", "reg_addr_zh", "reg_addr_en", "notice_no_first_trial", "notice_no_reg",
                     "date_notice_first_trial", "date_notice_reg", "date_proprietary", "type", "date_late_desig",
                     "date_inter_reg", "agency", "services", "reg_process", "remarks"]
        data = {}
        info_list = []
        try:
            html = lxml.etree.HTML(page)
            result_list = html.xpath("//table[@class='table']//tr")
            for i in range(len(result_list)):
                # 第1行  3个td: 商标名称, name, img_url
                if i == 0:
                    data["name"] = result_list[i].xpath("./td[position()=2]/span/text()")[0]
                    data["url_image"] = result_list[i].xpath("./td[position()=3]/span/img/@src")[0]
                    # 图像要素
                    image_feature = result_list[i].xpath("./td[position()=3]/span/text()")
                    image_feature = [info.strip("图形要素：\r\n ") for info in image_feature if info.strip()]
                    data["features"] = list(set(image_feature))
                else:
                    info_list.append(result_list[i].xpath("./td[position()=2]//text()"))
                # 以下行数每行 4个td标签
                if i in (5, 9, 10, 11, 12):
                    info_list.append(result_list[i].xpath("./td[position()=4]//text()"))
            info_list = [" ".join([x.strip() for x in info]).strip() for info in info_list]
            for i in range(len(name_list)):
                data[name_list[i]] = "".join(info_list[i])

            if data.get("reg_process"):
                data["reg_process"] = self._extract_reg_process(data["reg_process"])

            if data.get("services_list"):
                data["services_list"], data["similar_group"] = self._extract_services_list(data["services_list"])

            if data.get("remarks"):
                data["remarks"] = data["remarks"].split()

            data = self._extract_date(data)
        except Exception as e:
            logger.error(e, exc_info=True)
        return data

    async def _upload_image_to_s3(self, img_url, reg_code):
        dl = AsyncHttpDownloader(timeout=180, headers=self.configs.get("headers"), proxy=get_proxy())
        try:
            img, _ = await dl.fetch(img_url, rtype="read")
            if img:
                suffix = img_url[img_url.rfind("."):]
                fname = s3_trademark_url.format(
                    spider=self.name,
                    reg_code=reg_code,
                    suffix=suffix
                )
                ts_start = self.loop.time()
                await s3.upload_file(img, fname)
                ts_end = self.loop.time()
                time_on_upload_s3.labels(
                    getway=self.name).observe(ts_end - ts_start)
                logger.info("[Spider]<{0}> upload {1} to s3 finished"
                            .format(self.name, fname))
                return fname
        except Exception as e:
            logger.error(e, exc_info=True)
        return None

    async def handle_detail(self, page, encoding, extra_data=None):
        url = self.job.url
        if self.executor:
            data = await self.loop.run_in_executor(
                self.executor, self._extract_html_page, page)
        else:
            data = self._extract_html_page(page)

        if data and extra_data:
            if data.get("url_image") and data.get("reg_code"):
                if self.configs.get("save_image"):
                    data["url_image_s3"] = await self._upload_image_to_s3(data["url_image"], data["reg_code"])
                else:
                    img_url = data["url_image"]
                    data["url_image_s3"] = s3_trademark_url.format(spider=self.name,
                                                               reg_code=data["reg_code"],
                                                               suffix=img_url[img_url.rfind("."):])
            status = extra_data.get("status")
            data.update({
                "url": url,
                "getway": "subiao",
                "status": status,
                "sid": self.job.seed_urlmd5,
            })
        return data

    async def handle_index(self, page):
        detail_infos, next_info = [], {}
        domain_url = "http://www.subiao.com"
        try:
            html = lxml.etree.HTML(page)
            trademark_infos = html.xpath("//ul[@class='result-list']/li/dl")
            for info in trademark_infos:
                url = info.xpath("./dt/a/@href")[0]
                detail_url = domain_url + url
                status = info.xpath(".//span[@class='d4']/em/text()")[0]
                extra_data = {"status": status}
                detail_infos.append({"url": detail_url, "extra_data": extra_data})

            next_page = html.xpath("//ul[@id='yw0']/li[@class='next']/a/@href")
            if next_page:
                url_next = domain_url + next_page[0]
                next_info = {"url": url_next}
        except Exception as e:
            logger.error(e, exc_info=True)
        return (detail_infos, next_info)

    @staticmethod
    def _extract_date(data):
        # 字符串转化时间 "2014-09-07" ==》 datetime.date(2014, 9, 7)
        if data.get("date_apply"):
            data["date_apply"] = datetime.strptime(data["date_apply"], "%Y-%m-%d")
        if data.get("date_notice_first_trial"):
            data["date_notice_first_trial"] = datetime.strptime(data["date_notice_first_trial"], "%Y-%m-%d")
        if data.get("date_notice_reg"):
            data["date_notice_reg"] = datetime.strptime(data["date_notice_reg"], "%Y-%m-%d")

        # 专有权日期  2002-04-07 至 2012-04-06 ==》 (datetime.date(2002, 4, 7), datetime.date(2012, 4, 6))
        if data.get("date_proprietary"):
            data["date_proprietary"] = data["date_proprietary"].split("至")
            start = datetime.strptime(data["date_proprietary"][0].strip(), "%Y-%m-%d")
            stop = datetime.strptime(data["date_proprietary"][1].strip(), "%Y-%m-%d")
            data["date_proprietary"] = (start, stop)
        return data

    @staticmethod
    def _extract_reg_process(reg_process):
        reg_process = reg_process.split()
        if reg_process[0] == "商标无效":
            process_time = [""] + reg_process[1::2]
            process_desp = ["商标无效"] + reg_process[2::2]
        else:
            process_time = reg_process[::2]
            process_desp = reg_process[1::2]
        reg_process = list(zip(process_time, process_desp))
        return reg_process

    @staticmethod
    def _extract_services_list(services_list):
        services_list = services_list.split()
        code = services_list[::2]
        desp = services_list[1::2]
        services_list = list(zip(code, desp))
        similar_group = list(set(code))
        return services_list, similar_group
