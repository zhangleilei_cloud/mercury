# -*- coding: utf-8 -*-

import re
from datetime import datetime

import lxml
from bs4 import BeautifulSoup

from mercury.downloader import AsyncHttpDownloader
from mercury.libs import s3
from mercury.libs.proxy import get_proxy
from mercury.models import Trademark
from mercury.monitor import time_on_upload_s3
from mercury.settings import logger
from mercury.spiders._base import Spider as BaseSpider


url_next_index = "http://www.cha-tm.cn/chatmbs/tmwx/baitusoft/sbtxwx/sbtxwxList.jsp?g={g}&c={c}&k={k}&p={p}"
s3_trademark_url = "trademark/{spider}/{reg_code}{suffix}"


class BaituSpider(BaseSpider):
    name = "baitu"
    Model = Trademark

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "User-Agent": ("Mozilla/5.0 (iPhone; CPU iPhone OS 10_2 like Mac OS X) AppleWebKit/602.3.12 (KHTML, like Gecko) "
                               "Mobile/14C92 Safari/601.1 wechatdevtools/1.02.1806120 MicroMessenger/6.5.7 Language/zh_CN "
                               "webview/15320663050926431 webdebugger port/11533")
            },
            "cookies": {},
            "uniqid": "urlmd5",
            "sleep_range_before_request": (18, 23),
            "save_html": False,
            "proxy_enable": True,
            "save_image": False
        }
        return configs

    async def before_request(self):
        self.dl = AsyncHttpDownloader(
            timeout=120,  proxy=get_proxy(),
            headers=self.configs.get("headers"))
        return True

    def _extract_html_page(self, page):
        name = ["status", "notice_no_first_trial", "reg_code", "inter_class",
                "name_zh", "name_en", "", "", "date_apply", "date_notice_first_trial",
                "date_notice_reg", "date_exp", "reg_person_zh", "reg_addr_zh",
                "agency", "services"]
        try:
            soup = BeautifulSoup(page, "lxml")
            registe_infos = soup.select("table table table tr td")
            image_url = soup.select("img#sbimage")[0].get("src")
        except Exception as e:
            logger.error(e, exc_info=True)
        else:
            infos = [info.get_text() for info in registe_infos]
            cleared_infos = infos[1::2]
            value = [x.strip(" \t\n\r\x0b\x0c\xa0：") for x in cleared_infos if "\r\n" not in x]
            reg_info = dict(zip(name, value))
            reg_info["name"] = reg_info["name_zh"] if reg_info["name_zh"] else reg_info["name_en"]
            reg_info = self._extract_date(reg_info)
            # 申请流程解析
            rule = (r"<td.*>(.*)</td>")
            infos_tmp = []
            try:
                for child in soup.table.table.table:
                    infos_tmp.append(re.findall(rule, str(child)))
                reg_process = infos_tmp[-2]
                # 有一部分详情页没有申请流程这一项
                if "流程名称" in reg_process:
                    index = reg_process.index("流程名称")
                    desp = reg_process[index + 2::2]
                    date = reg_process[index + 3::2]
                    reg_info["reg_process"] = list(zip(date, desp))
                reg_info["url_image"] = image_url
                reg_info["inter_class"] = re.search(r"\d{2}", reg_info["inter_class"]).group()
            except Exception as e:
                logger.error(e, exc_info=True)
            return reg_info

    async def _upload_image_to_S3(self, data):
        dl = AsyncHttpDownloader(timeout=180, headers=self.configs.get("headers"), proxy=get_proxy())
        img_url = data.get("url_image")
        try:
            img, _ = await dl.fetch(img_url, rtype="read")
            if img:
                suffix = img_url[img_url.rfind("."):]
                fname = s3_trademark_url.format(
                    spider=self.name,
                    reg_code=data.get("reg_code"),
                    suffix=suffix
                )
                ts_start = self.loop.time()
                await s3.upload_file(img, fname)
                ts_end = self.loop.time()
                time_on_upload_s3.labels(
                    getway=self.name).observe(ts_end - ts_start)
                logger.info("[Spider]<{0}> upload {1} to s3 finished"
                            .format(self.name, fname))
                return fname
        except Exception as e:
            logger.error(e, exc_info=True)
        return None

    async def handle_dup_detail(self):
        feature = self.job.extra_data.get("features")
        trademark = await self.Model.get_by_uniqid(self.job.uniqid)
        trademark.features.append(feature)
        trademark.features = list(set(trademark.features))
        await trademark.update({"features": trademark.features})
        return trademark

    async def handle_detail(self, page, encoding, extra_data=None):
        url = self.job.url
        if self.executor:
            data = await self.loop.run_in_executor(
                self.executor, self._extract_html_page, page)
        else:
            data = self._extract_html_page(page)

        if data:
            if data.get("url_image") and data.get("reg_code"):
                if self.configs.get("save_image"):
                    data["url_image_s3"] = await self._upload_image_to_S3(data)
                else:
                    img_url = data["url_image"]
                    data["url_image_s3"] = s3_trademark_url.format(spider=self.name,
                                                                   reg_code=data["reg_code"],
                                                                   suffix=img_url[img_url.rfind("."):])
            features = extra_data.get("features")
            data.update({
                "url": url,
                "getway": "baitu",
                "features": [features],
                "sid": self.job.seed_urlmd5,
            })
        return data

    async def handle_index(self, page):
        detail_infos, next_info = [], {}
        domain_url = "http://www.cha-tm.cn"
        try:
            htmlElement = lxml.etree.HTML(page)
            trademark_elem = htmlElement.xpath("//table//table//table[last()]/tr[position()>1]/td")
            features = htmlElement.xpath("//input[@id='k']/@value")[0]
            for elem in trademark_elem:
                detail_url = elem.xpath(".//a/@href")[0]
                detail_url = domain_url + detail_url
                extra_data = {"features": features}
                detail_infos.append({"url": detail_url, "extra_data": extra_data})

            # 一页30个商标,等于30认为有下一页
            if len(detail_infos) == 30:
                try:
                    guid = htmlElement.xpath("//input[@id='guid']/@value")[0]
                    page = htmlElement.xpath("//input[@id='p']/@value")[0]
                    category = htmlElement.xpath("//input[@id='c']/@value")[0]
                    url_next = url_next_index.format(g=guid, c=category, k=features, p=int(page) + 1)
                    next_info = {"url": url_next}
                except Exception as e:
                    logger.error(e, exc_info=True)
        except Exception as e:
            logger.error(e, exc_info=True)
        return (detail_infos, next_info)

    @staticmethod
    def _extract_date(data):
        if data.get("date_apply"):
            data["date_apply"] = datetime.strptime(data["date_apply"], "%Y-%m-%d")
        if data.get("date_notice_reg"):
            data["date_notice_reg"] = datetime.strptime(data["date_notice_reg"], "%Y-%m-%d")
        if data.get("date_notice_first_trial"):
            data["date_notice_first_trial"] = datetime.strptime(data["date_notice_first_trial"], "%Y-%m-%d")
        if data.get("date_exp"):
            data["date_exp"] = datetime.strptime(data["date_exp"], "%Y-%m-%d")

        if data.get("date_notice_reg") and data.get("date_exp"):
            data["date_proprietary"] = (data["date_notice_reg"], data["date_exp"])
        return data
