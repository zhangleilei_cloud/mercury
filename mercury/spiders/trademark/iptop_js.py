import execjs

token_cookie_js = """
    function stringToCharArray(str) {
        var uiArr = [];
        var i = 0;
        for (; i < str.length; i++) {
            uiArr[i] = str.charAt(i);
        }
        return uiArr;
    }

    function charArrayToString(arr) {
        var styleStr = "";
        var i = 0;
        for (; i < arr.length; i++) {
            styleStr = styleStr + arr[i];
        }
        return styleStr;
    }

    function encrypt(message, data) {
        var comments = stringToCharArray(message);
        var i = 0;
        for (; i < comments.length; i++) {
            var c = comments[i];
            var y = data.charCodeAt(i % data.length) % comments.length;
            comments[i] = comments[y];
            comments[y] = c;
        }
        return charArrayToString(comments);
    }

    function appendToken(s) {
        var startIndex = s.lastIndexOf("/");
        var index = s.lastIndexOf(".");
        if (index < startIndex) {
            index = s.length;
        }
        var termOutput = s.substring(startIndex + 1, index);
        var token = encrypt(resourceToken, termOutput + encryptKey);
        s = s + ("?token=" + token);
        return s;
    }

    String["prototype"]["MD5"] = function (canCreateDiscussions) {

        function parse(x, n) {
            return x << n | x >>> 32 - n;
        }

        function callback(a, b) {
            var this_bool;
            var other_bool;
            var tmp30;
            var tmp24;
            var tmp35;
            tmp30 = a & 2147483648;
            tmp24 = b & 2147483648;
            this_bool = a & 1073741824;
            other_bool = b & 1073741824;
            tmp35 = (a & 1073741823) + (b & 1073741823);
            if (this_bool & other_bool) {
                return tmp35 ^ 2147483648 ^ tmp30 ^ tmp24;
            }
            if (this_bool | other_bool) {
                if (tmp35 & 1073741824) {
                    return tmp35 ^ 3221225472 ^ tmp30 ^ tmp24;
                } else {
                    return tmp35 ^ 1073741824 ^ tmp30 ^ tmp24;
                }
            } else {
                return tmp35 ^ tmp30 ^ tmp24;
            }
        }

        function getValue(k, g, j) {
            return k & g | ~k & j;
        }

        function pad(n, t, a) {
            return n & a | t & ~a;
        }

        function merge(entities, config, val) {
            return entities ^ config ^ val;
        }

        function apply(c, e, a) {
            return e ^ (c | ~a);
        }

        function f(value, obj, id, str, val, name, r) {
            value = callback(value, callback(callback(getValue(obj, id, str), val), r));
            return callback(parse(value, name), obj);
        }

        function format(val, s, name, str, d, v, opt) {
            val = callback(val, callback(callback(pad(s, name, str), d), opt));
            return callback(parse(val, v), s);
        }

        function fn(val, target, prop, arg, url, type, deps) {
            val = callback(val, callback(callback(merge(target, prop, arg), url), deps));
            return callback(parse(val, type), target);
        }

        function log(value, source, id, options, msg, key, opt) {
            value = callback(value, callback(callback(apply(source, id, options), msg), opt));
            return callback(parse(value, key), source);
        }

        function PL$11(PL$13) {
            var lWordCount;
            var lMessageLength = PL$13["length"];
            var lNumberOfWords_temp1 = lMessageLength + 8;
            var _0xd54dx23 = (lNumberOfWords_temp1 - lNumberOfWords_temp1 % 64) / 64;
            var lNumberOfWords = (_0xd54dx23 + 1) * 16;
            var lWordArray = Array(lNumberOfWords - 1);
            var lBytePosition = 0;
            var lByteCount = 0;
            for (; lByteCount < lMessageLength;) {
                lWordCount = (lByteCount - lByteCount % 4) / 4;
                lBytePosition = lByteCount % 4 * 8;
                lWordArray[lWordCount] = lWordArray[lWordCount] | PL$13["charCodeAt"](lByteCount) << lBytePosition;
                lByteCount++;
            }
            lWordCount = (lByteCount - lByteCount % 4) / 4;
            lBytePosition = lByteCount % 4 * 8;
            lWordArray[lWordCount] = lWordArray[lWordCount] | 128 << lBytePosition;
            lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
            lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
            return lWordArray;
        }

        function parseFloat(str) {
            var ret = "";
            var PL$22 = "";
            var default_favicon;
            var i;
            i = 0;
            for (; i <= 3; i++) {
                default_favicon = str >>> i * 8 & 255;
                PL$22 = "0" + default_favicon.toString(16);
                ret = ret + PL$22["substr"](PL$22["length"] - 2, 2);
            }
            return ret;
        }

        var map = this;
        var args = Array();
        var i;
        var rotationQuaternion;
        var filename;
        var string;
        var progress;
        var value;
        var key;
        var name;
        var val;
        var A = 7;
        var end = 12;
        var w = 17;
        var z = 22;
        var a7 = 5;
        var items = 9;
        var a6 = 14;
        var r = 20;
        var type = 4;
        var t = 11;
        var source = 16;
        var store = 23;
        var data = 6;
        var hp = 10;
        var last = 15;
        var m = 21;
        args = PL$11(map);
        value = 1732584193;
        key = 4023233417;
        name = 2562383102;
        val = 271733878;
        i = 0;
        for (; i < args["length"]; i = i + 16) {
            rotationQuaternion = value;
            filename = key;
            string = name;
            progress = val;
            value = f(value, key, name, val, args[i + 0], A, 3614090360);
            val = f(val, value, key, name, args[i + 1], end, 3905402710);
            name = f(name, val, value, key, args[i + 2], w, 606105819);
            key = f(key, name, val, value, args[i + 3], z, 3250441966);
            value = f(value, key, name, val, args[i + 4], A, 4118548399);
            val = f(val, value, key, name, args[i + 5], end, 1200080426);
            name = f(name, val, value, key, args[i + 6], w, 2821735955);
            key = f(key, name, val, value, args[i + 7], z, 4249261313);
            value = f(value, key, name, val, args[i + 8], A, 1770035416);
            val = f(val, value, key, name, args[i + 9], end, 2336552879);
            name = f(name, val, value, key, args[i + 10], w, 4294925233);
            key = f(key, name, val, value, args[i + 11], z, 2304563134);
            value = f(value, key, name, val, args[i + 12], A, 1804603682);
            val = f(val, value, key, name, args[i + 13], end, 4254626195);
            name = f(name, val, value, key, args[i + 14], w, 2792965006);
            key = f(key, name, val, value, args[i + 15], z, 1236535329);
            value = format(value, key, name, val, args[i + 1], a7, 4129170786);
            val = format(val, value, key, name, args[i + 6], items, 3225465664);
            name = format(name, val, value, key, args[i + 11], a6, 643717713);
            key = format(key, name, val, value, args[i + 0], r, 3921069994);
            value = format(value, key, name, val, args[i + 5], a7, 3593408605);
            val = format(val, value, key, name, args[i + 10], items, 38016083);
            name = format(name, val, value, key, args[i + 15], a6, 3634488961);
            key = format(key, name, val, value, args[i + 4], r, 3889429448);
            value = format(value, key, name, val, args[i + 9], a7, 568446438);
            val = format(val, value, key, name, args[i + 14], items, 3275163606);
            name = format(name, val, value, key, args[i + 3], a6, 4107603335);
            key = format(key, name, val, value, args[i + 8], r, 1163531501);
            value = format(value, key, name, val, args[i + 13], a7, 2850285829);
            val = format(val, value, key, name, args[i + 2], items, 4243563512);
            name = format(name, val, value, key, args[i + 7], a6, 1735328473);
            key = format(key, name, val, value, args[i + 12], r, 2368359562);
            value = fn(value, key, name, val, args[i + 5], type, 4294588738);
            val = fn(val, value, key, name, args[i + 8], t, 2272392833);
            name = fn(name, val, value, key, args[i + 11], source, 1839030562);
            key = fn(key, name, val, value, args[i + 14], store, 4259657740);
            value = fn(value, key, name, val, args[i + 1], type, 2763975236);
            val = fn(val, value, key, name, args[i + 4], t, 1272893353);
            name = fn(name, val, value, key, args[i + 7], source, 4139469664);
            key = fn(key, name, val, value, args[i + 10], store, 3200236656);
            value = fn(value, key, name, val, args[i + 13], type, 681279174);
            val = fn(val, value, key, name, args[i + 0], t, 3936430074);
            name = fn(name, val, value, key, args[i + 3], source, 3572445317);
            key = fn(key, name, val, value, args[i + 6], store, 76029189);
            value = fn(value, key, name, val, args[i + 9], type, 3654602809);
            val = fn(val, value, key, name, args[i + 12], t, 3873151461);
            name = fn(name, val, value, key, args[i + 15], source, 530742520);
            key = fn(key, name, val, value, args[i + 2], store, 3299628645);
            value = log(value, key, name, val, args[i + 0], data, 4096336452);
            val = log(val, value, key, name, args[i + 7], hp, 1126891415);
            name = log(name, val, value, key, args[i + 14], last, 2878612391);
            key = log(key, name, val, value, args[i + 5], m, 4237533241);
            value = log(value, key, name, val, args[i + 12], data, 1700485571);
            val = log(val, value, key, name, args[i + 3], hp, 2399980690);
            name = log(name, val, value, key, args[i + 10], last, 4293915773);
            key = log(key, name, val, value, args[i + 1], m, 2240044497);
            value = log(value, key, name, val, args[i + 8], data, 1873313359);
            val = log(val, value, key, name, args[i + 15], hp, 4264355552);
            name = log(name, val, value, key, args[i + 6], last, 2734768916);
            key = log(key, name, val, value, args[i + 13], m, 1309151649);
            value = log(value, key, name, val, args[i + 4], data, 4149444226);
            val = log(val, value, key, name, args[i + 11], hp, 3174756917);
            name = log(name, val, value, key, args[i + 2], last, 718787259);
            key = log(key, name, val, value, args[i + 9], m, 3951481745);
            value = callback(value, rotationQuaternion);
            key = callback(key, filename);
            name = callback(name, string);
            val = callback(val, progress);
        }
        if (canCreateDiscussions == 32) {
            return parseFloat(value) + parseFloat(key) + parseFloat(name) + parseFloat(val);
        } else {
            return parseFloat(key) + parseFloat(name);
        }
    };

    function nima() {
        _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        this["encode"] = function (data) {
            var keyframeString = "";
            var dword2;
            var aStatedRank;
            var normalized_images;
            var ARByte;
            var animationCss;
            var knobTranslate;
            var fillTranslate;
            var i = 0;
            data = _utf8_encode(data);
            for (; i < data["length"];) {
                dword2 = data["charCodeAt"](i++);
                aStatedRank = data["charCodeAt"](i++);
                normalized_images = data["charCodeAt"](i++);
                ARByte = dword2 >> 2;
                animationCss = (dword2 & 3) << 4 | aStatedRank >> 4;
                knobTranslate = (aStatedRank & 15) << 2 | normalized_images >> 6;
                fillTranslate = normalized_images & 63;
                if (isNaN(aStatedRank)) {
                    knobTranslate = fillTranslate = 64;
                } else {
                    if (isNaN(normalized_images)) {
                        fillTranslate = 64;
                    }
                }
                keyframeString = keyframeString + _keyStr["charAt"](ARByte) + _keyStr["charAt"](animationCss) + _keyStr["charAt"](knobTranslate) + _keyStr["charAt"](fillTranslate);
            }
            return keyframeString;
        };

        this["decode"] = function (data) {
            var output = "";
            var sql_init_admin_user;
            var initialMargin;
            var expectedSecureOpts;
            var sextet1;
            var sextet2;
            var _0xd54dx4a;
            var opCipher;
            var i = 0;
            for (; i < data["length"];) {
                sextet1 = _keyStr["indexOf"](data["charAt"](i++));
                sextet2 = _keyStr["indexOf"](data["charAt"](i++));
                _0xd54dx4a = _keyStr["indexOf"](data["charAt"](i++));
                opCipher = _keyStr["indexOf"](data["charAt"](i++));
                sql_init_admin_user = sextet1 << 2 | sextet2 >> 4;
                initialMargin = (sextet2 & 15) << 4 | _0xd54dx4a >> 2;
                expectedSecureOpts = (_0xd54dx4a & 3) << 6 | opCipher;
                output = output + String["fromCharCode"](sql_init_admin_user);
                if (_0xd54dx4a != 64) {
                    output = output + String["fromCharCode"](initialMargin);
                }
                if (opCipher != 64) {
                    output = output + String["fromCharCode"](expectedSecureOpts);
                }
            }
            output = _utf8_decode(output);
            return output;
        };

        _utf8_encode = function (options) {
            options = options.toString();
            var utftext = "";
            var i = 0;
            for (; i < options["length"]; i++) {
                var c = options.toString()["charCodeAt"](i);
                if (c < 128) {
                    utftext = utftext + String["fromCharCode"](c);
                } else {
                    if (c > 127 && c < 2048) {
                        utftext = utftext + String["fromCharCode"](c >> 6 | 192);
                        utftext = utftext + String["fromCharCode"](c & 63 | 128);
                    } else {
                        utftext = utftext + String["fromCharCode"](c >> 12 | 224);
                        utftext = utftext + String["fromCharCode"](c >> 6 & 63 | 128);
                        utftext = utftext + String["fromCharCode"](c & 63 | 128);
                    }
                }
            }
            return utftext;
        };

        _utf8_decode = function (data) {
            var string = "";
            var i = 0;
            var c = c1 = c2 = 0;
            for (; i < data["length"];) {
                c = data["charCodeAt"](i);
                if (c < 128) {
                    string = string + String["fromCharCode"](c);
                    i++;
                } else {
                    if (c > 191 && c < 224) {
                        c2 = data["charCodeAt"](i + 1);
                        string = string + String["fromCharCode"]((c & 31) << 6 | c2 & 63);
                        i = i + 2;
                    } else {
                        c2 = data.toString()["charCodeAt"](i + 1);
                        c3 = data.toString()["charCodeAt"](i + 2);
                        string = string + String["fromCharCode"]((c & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                        i = i + 3;
                    }
                }
            }
            return string;
        };
    }

    if (keyparam["lastIndexOf"](",") == keyparam["length"] - 1) {
        keyparam = keyparam["substring"](0, keyparam["length"] - 1);
    }
    var dataArr = keyparam["split"](",");
    var result = [];
    var value = [];
    var n = 3;
    var remaider = dataArr["length"] % n;
    var number = dataArr["length"] / n;
    var offset = 0;
    var i = 0;
    for (; i < n; i++) {
        if (remaider > 0) {
            value = dataArr["slice"](i * number + offset, (i + 1) * number + offset + 1);
            remaider--;
            offset++;
        } else {
            value = dataArr["slice"](i * number + offset, (i + 1) * number + offset);
        }
        result["push"](value);
        value = [];
    }
    var key1 = (new nima)["encode"](result[0]);
    var key2 = (new nima)["encode"](result[1]);
    var key3 = result[2].toString();
    key3 = key3.MD5(32)["toUpperCase"]();

    function getKey1() {
        return key1
    };
    function getKey2() {
        return key2
    };
    function getKey3() {
        return key3
    };

"""

field_js = """
    function stringToCharArray(value) {
        var cs = [];
        for (var i = 0; i < value.length; i++) {
            cs[i] = value.charAt(i)
        }
        return cs
    }

    function charArrayToString(cs) {
        var result = "";
        for (var i = 0; i < cs.length; i++) {
            result += cs[i]
        }
        return result
    }

    function decipher(msg) {
        var arr1 = stringToCharArray(msg);
        var myDate = new Date();
        var day = myDate.getDate();
        var reduce = Math.ceil(day / 7);
        for (var i = 0; i < arr1.length; i++) {
            if (i % 3 == 0) continue;
            var code = arr1[i].charCodeAt();
            arr1[i] = String.fromCharCode(code - reduce)
        }
        msg = charArrayToString(arr1);
        msg = decodeURIComponent(msg);
        var arr = stringToCharArray(msg);
        arr.reverse();
        if (arr.length % 2 == 0) {
            var index = Math.ceil(arr.length / 2);
            var oddArr = arr.slice(0, index);
            var evenArr = arr.slice(index);
            var arr2 = new Array(arr.length);
            for (var i = 0; i < arr.length; i++) {
                if (i % 2 == 0) {
                    arr2[i] = evenArr[Math.ceil(i / 2)]
                } else {
                    arr2[i] = oddArr[Math.ceil(i / 2) - 1]
                }
            }
        } else {
            var index = Math.ceil(arr.length / 2);
            var evenArr = arr.slice(0, index);
            var oddArr = arr.slice(index);
            var arr2 = new Array(arr.length);
            for (var i = 0; i < arr.length; i++) {
                if (i % 2 == 0) {
                    arr2[i] = evenArr[Math.ceil(i / 2)]
                } else {
                    arr2[i] = oddArr[Math.ceil(i / 2) - 1]
                }
            }
        }
        return charArrayToString(arr2)
    }
    """

get_field_js = """
    function getAgentName() {
        return agentName
    };
    function getApplyAddress() {
        return applyaddresscn
    };
    function getProcesses() {
        return processes
    };
"""

node_js = execjs.get()
field_decrypt_js = node_js.compile(field_js)
