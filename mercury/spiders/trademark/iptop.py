# -*- coding: utf-8 -*-
"""
商标解析流程：
index页面 --》携带Referer访问 --》全是js的detail页面（下面简称：detail的跳板页） --》解析js,拼接真正的detail的url和cookie，携带Referer --》访问detail页面
--》部分字段调用js代码解密

访问detail页面时，detail 页面上是一堆js，js混淆找个网站给破解一下，分析js：
（1）在原url上新增了token参数，并重定向。
（2）访问新的重定向页面需要携带cookie（必须携带token1，token2，token3），cookie是由js生成的
"""

from datetime import datetime

import execjs
from lxml import etree

from mercury.downloader import AsyncHttpDownloader
from mercury.libs import s3
from mercury.libs.proxy import get_proxy
from mercury.models import Trademark
from mercury.monitor import time_on_upload_s3
from mercury.settings import logger
from mercury.spiders._base import Spider as BaseSpider
from mercury.spiders.trademark import field_decrypt_js, get_field_js, token_cookie_js

s3_trademark_url = "trademark/{spider}/{reg_code}{suffix}"
domain_url = "https://www.iptop.cn"
node_js = execjs.get()


class IptopSpider(BaseSpider):
    name = "iptop"
    Model = Trademark

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "User-Agent": ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 "
                               "(KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
            },
            "cookies": {},
            "uniqid": "urlmd5",
            "sleep_range_before_request": (2, 10),
            "save_html": False,
            "proxy_enable": True,
            "save_image": False
        }
        return configs

    async def before_request(self):

        # 根据中间页获取真实的详情页url
        if not self.job.is_seed:
            headers = self.configs.get("headers")
            if self.job.extra_data:
                referer = self.job.extra_data.get("Referer", None)
                cookie = self.job.extra_data.get("Cookie", None)
                if referer:
                    headers.update({"Referer": referer})
                if cookie:
                    headers.update({"Cookie": cookie})
            pre_dl = AsyncHttpDownloader(timeout=60, headers=headers, proxy=get_proxy())
            # 获取中间页
            page, encoding = await pre_dl.fetch(
                self.job.url, method=self.job.method,
                ctype=self.job.ctype, data=self.job.payload,
                rtype=self.job.rtype, auto_close=self.job.auto_close)
            # 如果下载的中间页是None,则返回False
            if page is None:
                return False
            # 处理cookie和token
            await self._handle_token_and_cookie(page)
            referer = self.job.extra_data.get("Referer", None)
            cookie = self.job.extra_data.get("Cookie", None)
            if referer:
                headers.update({"Referer": referer})
            if cookie:
                headers.update({"Cookie": cookie})
            self.dl = AsyncHttpDownloader(timeout=60, headers=headers, proxy=get_proxy())
        return True

    def _extract_html_page(self, page):
        hd_info_name = ["reg_code", "inter_class", "reg_person_zh", "_",
                        "reg_person_en", "reg_addr_zh", "reg_addr_en"]

        bd_info_name = ["notice_no_first_trial", "date_notice_first_trial", "notice_no_reg",
                        "date_notice_reg", "date_proprietary", "joint_agent", "date_late_desig",
                        "date_inter_reg", "date_priority", "agency", "colors", "type"]
        data = {}
        try:
            html = etree.HTML(page)
            cmdt_hd = html.xpath("//div[@class='cmdt-hd']")
            script = html.xpath("//script")[19]
            agent_name, apply_addr, processes = self._extract_js_param(script)
            for hd in cmdt_hd:
                link = hd.xpath(".//img[@id='tmlogo']/@src")[0]
                data["url_image"] = domain_url + link
                data["name"] = hd.xpath(".//span[@class='cmdt-name']/text()")[0]
                data["features"] = hd.xpath("./div[last()]//li//text()")

                hd_info = hd.xpath(".//ul[@class='cmdt-bdiv']/li//text()")
                value = [x.strip() for x in hd_info if x.strip()]
                hd_info_pre = dict(zip(hd_info_name[:3], value[1:6:2]))
                hd_info_suf = dict(zip(hd_info_name[4:], value[-1:-6:-2]))
                data.update(hd_info_pre)
                data.update(hd_info_suf)

            cmdt_bd = html.xpath("//div[@class='cmdt-bd']")
            for bd in cmdt_bd:
                # 服务列表
                services_list = bd.xpath(".//ul[@class='cmdt-ser']//text()")
                if services_list:
                    data["services_list"] = self._extract_services_list(services_list)

                # 类似群
                simi_group = bd.xpath(".//div[@class='cmdt-ls']//li//text()")
                if simi_group:
                    simi_group = [num for num in simi_group[0].split(";") if num]
                    data["similar_group"] = simi_group

                # 注册信息
                reg_info_ele = bd.xpath(".//div[@class='cmdt-b']/table//tr")
                reg_data = []
                for info in reg_info_ele:
                    reg_data.append(info.xpath("./td[position()=2]//text()")[0].strip())
                    reg_data.append(info.xpath("./td[position()=4]//text()")[0].strip())
                reg_info = dict(zip(bd_info_name, reg_data))
                data.update(reg_info)

                # 注册流程
                reg_process = bd.xpath(".//ul[@id='processesUl']/li/span/text()")
                if reg_process:
                    processes = processes.split("|")
                    data["reg_process"] = self._extract_reg_process(reg_process, processes)

                # 商标公告
                notice = bd.xpath(".//div[position()=4]//li/span/text()")
                if notice:
                    data["remarks"] = list(zip(notice[::2], notice[1::2]))

                # 字段整理 "" 代替 "--"
                for key, value in data.items():
                    if value == "--":
                        data[key] = ""

                # 字符串转时间处理
                data = self._extract_date(data)

                # 加密字段处理：
                if data.get("agency"):
                    data["agency"] = agent_name
                    data["agency"] = self._js_decipher(data["agency"])
                if data.get("reg_addr_zh"):
                    data["reg_addr_zh"] = apply_addr
                    data["reg_addr_zh"] = self._js_decipher(data["reg_addr_zh"])

        except Exception as e:
            logger.error(e, exc_info=True)
        return data

    async def _upload_image_to_s3(self, img_url, reg_code):
        dl = AsyncHttpDownloader(timeout=180, headers=self.configs.get("headers"), proxy=get_proxy())
        try:
            img, _ = await dl.fetch(img_url, rtype="read")
            if img:
                suffix = img_url[img_url.rfind("."):]
                fname = s3_trademark_url.format(
                    spider=self.name,
                    reg_code=reg_code,
                    suffix=suffix
                )
                ts_start = self.loop.time()
                await s3.upload_file(img, fname)
                ts_end = self.loop.time()
                time_on_upload_s3.labels(
                    getway=self.name).observe(ts_end - ts_start)
                logger.info("[Spider]<{0}> upload {1} to s3 finished"
                            .format(self.name, fname))
                return fname
        except Exception as e:
            logger.error(e, exc_info=True)
        return None

    async def _handle_token_and_cookie(self, page):
        script_js = self._extract_js(page)
        key1, key2, key3 = self._js_keyparam(script_js)
        detail_url = self._append_token(self.job.url, script_js)
        if self.job.extra_data:
            # 保存token
            self.job.extra_data.update({
                "Referer": self.job.url,
                "Cookie": "token1={token1}; token2={token2}; token3={token3}"
                    .format(token1=key1, token2=key2, token3=key3)
            })
        # 因为job的url为原始的中间页url,这里更新为真正的详情页url再去下载详情页,同时保存原始中间页的url
        # 原因是为了与之前持久化的数据保持一直
        self.job.extra_data.update({"origin_url": self.job.url})
        self.job.url = detail_url

    async def handle_detail(self, page, encoding, extra_data=None):
        # 将原始的url替换回来
        url = self.job.extra_data["origin_url"]
        data = self._extract_html_page(page)
        if data and extra_data:
            if data.get("url_image") and data.get("reg_code"):
                if self.configs.get("save_image"):
                    data["url_image_s3"] = await self._upload_image_to_s3(data["url_image"], data["reg_code"])
                else:
                    img_url = data["url_image"]
                    data["url_image_s3"] = s3_trademark_url.format(
                        spider=self.name,
                        reg_code=data["reg_code"],
                        suffix=img_url[img_url.rfind("."):]
                    )
            status = extra_data.get("status")
            date_apply = extra_data.get("date_apply")
            if date_apply:
                date_apply = datetime.strptime(date_apply, "%Y-%m-%d")
            data.update({
                "url": url,
                "getway": "iptop",
                "status": status,
                "date_apply": date_apply,
                "sid": self.job.seed_urlmd5,
            })
        return data

    async def handle_index(self, page):
        detail_infos, next_info = [], {}
        try:
            html = etree.HTML(page)
            result_list = html.xpath("//div[contains(@class,'search_result_data')]//li")
            for info in result_list:
                url = info.xpath("./h2/a/@href")[0]
                detail_url = domain_url + url
                status = info.xpath("./div[position()=2]/p[position()=2]/span[last()]/text()")
                status = status[0] if status else ""
                date_apply_tmp = info.xpath("./div[position()=3]/p[position()=2]/span[last()]/text()")
                date_apply = date_apply_tmp[0] if date_apply_tmp else ""
                # 商标的法律状态（已注册，失效..）和申请日期只有index页面有，需要传递到detail页面
                extra_data = {"Referer": self.job.url, "status": status, "date_apply": date_apply}
                detail_infos.append({"url": detail_url, "extra_data": extra_data})

            page_now = html.xpath("//div[@class='search_result_l_t_r']//b[@class='page_now']//text()")
            page_all = html.xpath("//div[@class='search_result_l_t_r']//b[@class='page_all']//text()")
            if page_now and page_all:
                if int(page_now[0]) < int(page_all[0]):
                    page_old = "page={}".format(page_now[0])
                    page_new = "page={}".format(int(page_now[0]) + 1)
                    url_next = self.job.url.replace(page_old, page_new)
                    extra_data = {"Referer": self.job.url}
                    next_info = {"url": url_next, "extra_data": extra_data}
        except Exception as e:
            logger.error(e, exc_info=True)
        return (detail_infos, next_info)

    def _extract_reg_process(self, msg, process_name):
        """
        解密 "注册流程" 字段，数组内每一项都需要解密
        :param msg: 数组 [时间，注册流程名称，时间，名称，...]
        :param process_name: 需要解密的注册流程名称
        :return:
        """
        msg = [x.strip() for x in msg]
        process_time = msg[::2]
        process_name = map(self._js_decipher, process_name)
        result = list(zip(process_time, process_name))
        return result

    @staticmethod
    def _extract_date(data):
        if data.get("date_notice_first_trial"):
            data["date_notice_first_trial"] = datetime.strptime(data["date_notice_first_trial"], "%Y-%m-%d")
        if data.get("date_notice_reg"):
            data["date_notice_reg"] = datetime.strptime(data["date_notice_reg"], "%Y-%m-%d")

        if data.get("date_proprietary"):
            data["date_proprietary"] = data["date_proprietary"].split("至")
            start = datetime.strptime(data["date_proprietary"][0].strip(), "%Y-%m-%d")
            stop = datetime.strptime(data["date_proprietary"][1].strip(), "%Y-%m-%d")
            data["date_proprietary"] = (start, stop)
        return data

    @staticmethod
    def _extract_services_list(services_list):
        services = [x.strip() for x in services_list if "\n\t" not in x]
        services_list = []
        for info in services:
            info = tuple(info.split("-"))
            services_list.append(info)
        return services_list

    @staticmethod
    def _js_decipher(msg):
        """
        str 类型参数解密
        """
        try:
            msg = field_decrypt_js.call("decipher", msg)
        except Exception as e:
            logger.error(e, exc_info=True)
        return msg

    @staticmethod
    def _extract_js_param(script_js):
        """
        :param script_js: 最终detail页面中的js, 从该js中提取到加密之后的字段："代理人名称" "申请地址" "注册流程"
        :return:
        """
        script = script_js.text + get_field_js
        ctx = node_js.compile(script)
        agent_name, apply_addr, processes = '', '', ''
        try:
            agent_name = ctx.call("getAgentName")
            apply_addr = ctx.call("getApplyAddress")
            processes = ctx.call("getProcesses")
        except Exception as e:
            logger.error(e, exc_info=True)
        return agent_name, apply_addr, processes

    @staticmethod
    def _extract_js(page):
        """
        每个detail跳板页的js中的keyparam参数不同，先拿到js中定义的变量keyparam，跟我们js反混淆处理后的代码进行组合
        :param page: detail跳板页面的content
        :return:
        """
        try:
            html = etree.HTML(page)
            script_all = html.xpath("//script/text()")
            start_index = script_all[0].find("resourceToken")
            end_index = script_all[0].find("eval")
            token_key = script_all[0][start_index:end_index]
            keyparam_start_index = script_all[1].find("var keyparam")
            keyparam_end_index = script_all[1].find("if(keyparam")
            keyparam = script_all[1][keyparam_start_index:keyparam_end_index]
            script_js = token_key + keyparam + token_cookie_js
        except Exception as e:
            logger.error(e, exc_info=True)
        else:
            return script_js

    @staticmethod
    def _js_keyparam(script_js):
        """
        通过detail跳板页js代码拿到key, 拼接cookie
        :param script_js: detail跳板页的js
        :return:
        """
        try:
            ctx = node_js.compile(script_js)
            key1 = ctx.call("getKey1")
            key2 = ctx.call("getKey2")
            key3 = ctx.call("getKey3")
        except Exception as e:
            logger.error(e, exc_info=True)
        else:
            return key1, key2, key3

    @staticmethod
    def _append_token(href, script_js):
        """利用当前detail跳板页的url，组装最后要访问的详情页url
        :param 当前详情页链接：href: "https://www.iptop.cn/trademark/info/18468209_41.html?goodsId="
        :return: 最终的详情页链接："https://www.iptop.cn/trademark/item/18468209_41.html?token_test=XE6gSmgz5gOZv8RX&goodsId="
        """
        href = href.split("?")
        try:
            ctx = node_js.compile(script_js)
            detail_tmp = ctx.call("appendToken", href[0])
        except Exception as e:
            logger.error(e, exc_info=True)
        else:
            detail_url = detail_tmp + "&" + href[1]
            detail_url = detail_url.replace("info", "item")
            return detail_url
