import re
import lxml
import newspaper
from pyquery import PyQuery
from goose3 import Goose
from goose3.text import StopWordsChinese
from mercury.models import JobQueue, Job, Seed
from mercury.models.high_frequency import HighFrequency
from mercury.monitor import cnt_extract_fail, time_on_extract_index
from mercury.settings import logger, SPIDER_CONF
from mercury.spiders._base import Spider as BaseSpider
from urllib.parse import urlparse as url_pass
from mercury.utils import siphash, md5


class HighFrequencySpider(BaseSpider):
    name = "high_frequency"
    Model = HighFrequency

    """ 站点和publisher映射表
        添加站点的时候注意更新该映射表
    """
    publisher_mapping = {
        "www.scio.gov.cn": "中华人民共和国国务院新闻办公室",
        "www.mof.gov.cn": "中华人民共和国财政部",
        "www.shanghai.gov.cn": "上海市人民政府",
        "fdi.gov.cn": "中国投资指南网",
        "www.ndrc.gov.cn": "中华人民共和国国家发展和改革委员会",
        "www.mohurd.gov.cn": "中华人民共和国住房和城乡建设部",
        "www.chinaacc.com": "中华会计网校",
        "www.mofcom.gov.cn": "中华人民共和国商务部",
        "www.ajxxgk.jcy.gov.cn": "人民检察院案件信息公开网",
        "www.spp.gov.cn": "中华人民共和国最高人民检察院",
        "bxjg.circ.gov.cn": "中华保险监督管理委员会",
        "www.cbrc.gov.cn": "中国银行保险监督管理委员会",
        "www.npc.gov.cn": "中国人大网",
        "www.csrc.gov.cn": "中国证券监督管理委员会",
        "www.court.gov.cn": "中华人民共和国最高人民法院",
        "bfsu.fdi.gov.cn": "中国投资指南网",
        "www.gov.cn": "中国政府网",
        "xbkfs.ndrc.gov.cn": "西部开发司",
        "dbzxs.ndrc.gov.cn": "东北等老工业振兴司",
        "shs.ndrc.gov.cn": "社会发展司",
        "www.sz.gov.cn": "深圳政府在线",
        "home.saic.gov.cn": "国家市场监督管理总局",
        "www.mnr.gov.cn": "中国政府网—自然资源部",
        "f.mnr.gov.cn": "中国政府网—政策法规库",
        "futures.jrj.com.cn": "金融界",
        "opinion.jrj.com.cn": "金融界",
        "www.moa.gov.cn": "中华人民共和国农业农村部",
        "www.chinacourt.org": "中国法院网",
        "tjfy.chinacourt.org": "天津法院网",
        "bjgy.chinacourt.org": "北京法院网",
        "www.shezfy.com": "上海市第二中级人民法院",
        "www.amac.org.cn": "中国证券投资基金业协会",
        "legal.china.com.cn": "中国网",
        "www.dffyw.com": "东方法眼",
        "www.legaldaily.com.cn": "法制网",
        "finance.ifeng.com": "凤凰财经",
        "www.sinotf.com": "中国贸易金融网",
        "www.shui5.cn": "亿企赢",
        "www.chinalawedu.com": "法律教育网",
        "news.qq.com": "腾讯网",
        "news.ifeng.com": "凤凰网"
    }

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "User-Agent": ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 "
                               "(KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
            },
            "cookies": {},
            "uniqid": "siphash",
            "sleep_range_before_request": (1, 2),
            "save_html": False,
            "proxy_enable": True,
            "save_image": False,
            "save_file": True,
            "need_extract_tags": ["img", "video", "audio", "table", "pre", "map", "blockquote"],
            "auto_extract": True,
            "need_replace_key": "content"
        }
        return configs

    async def handle_index(self, page):
        detail_infos, index_infos = [], []
        formated_urls = []
        a_tags = PyQuery(page).find("a")
        for a in a_tags:
            a = PyQuery(a)
            detail_url = a.attr("href")
            formated_url = await self._format_url(detail_url)
            if formated_url and self.job.url != formated_url:
                if self._check_last_path(formated_url) and self._check_detail_url(detail_url):
                    detail_infos.append({"url": formated_url, "extra_data": {"title": a.text()}})
                formated_urls.append({"url": formated_url,
                                      "score": self.index_url_dice_coefficient(formated_url)})
        index_infos = self._generate_index_url(formated_urls)
        return detail_infos, index_infos

    def _generate_index_url(self, formated_urls):
        index_infos = []
        filtered_urls = []
        temp_urls = set()
        job_url_parsed = url_pass(self.job.url)
        for item in formated_urls:
            if item["url"] in temp_urls:
                continue
            index_url_parsed = url_pass(item['url'])
            if index_url_parsed.netloc != job_url_parsed.netloc:
                continue
            pattern = r'(content|' \
                      r'document|' \
                      r'/(19|20)\d{2}/\d{2}/\d{14}|' \
                      r'/(19|20)\d{2}/(19|20)\d{4}/|' \
                      r'/(19|20)\d{2}-\d{2}-\d{2}/)|' \
                      r'/(19|20)\d{2}/\d{2}/\d{12}'
            if re.search(pattern, item["url"], re.IGNORECASE):
                continue
            temp_urls.add(item["url"])
            filtered_urls.append(item)
        filtered_urls.sort(key=lambda x: x["score"], reverse=True)
        for i in range(len(filtered_urls)):
            index_infos.append({"url": filtered_urls[i]["url"], "extra_data": None})
            if i == 4:
                break
        return index_infos

    async def _format_url(self, detail_url):
        if not isinstance(detail_url, str):
            return None
        if re.search(r"(javascript|#)", detail_url):
            return None
        if detail_url == "/":
            return None
        if not detail_url:
            return None
        if detail_url.find("@") >= 0:
            return None
        detail_url = self._format_url_path(detail_url)
        if detail_url.endswith((".pdf", ".doc", ".docx", ".xls", ".xlsx", ".zip", ".rar")):
            return None
        if not detail_url.startswith(("http://", "https://")):
            detail_url = f"http://{detail_url}"
        if url_pass(self.job.url).netloc != url_pass(detail_url).netloc:
            return None
        return detail_url

    @staticmethod
    def _check_last_path(detail_url):
        """检测url当前资源是否是详情页"""
        detail_url = detail_url[:-1]
        res = detail_url.rsplit("/", 1)
        if len(res) > 1:
            last_path = res.pop()
        else:
            return False
        if bool(re.match(r'.*index.*', last_path, re.IGNORECASE)):
            return False
        return bool(re.match(r'(.*[0-9]+|content|detail)', last_path.split("?")[0], re.IGNORECASE))

    @staticmethod
    def _check_detail_url(detail_url):
        if bool(re.match(r'(.*index.*|'
                         r'.*ajxxgk.*/html/[a-z]+/[0-9]+\.html|'
                         r'.*page=[0-9]+|'
                         r'.*tab[0-9]+.*|'
                         r'.*column/[0-9]+.*|'
                         r'.*nw2/nw2314/.*|'
                         r'.*gengduo-[0-9]+.*|'
                         r'.*(20[0-9]{4}|19[0-9]{4})/(20[0-9]{6}|19[0-9]{6})_[0-9]+.*|'
                         r'.*list.*|'
                         r'.*dffyw.*/[a-z]+/[a-z]+/[a-z]+[0-9]+.*|'
                         r'.*jrj.*/[a-z]+/(19[0-9]{2}|20[0-9]{2}).*|'
                         r'.*moa.gov.cn.*/[a-z]+/[a-z]+_[0-1]+/?$|'
                         r'.*fdi.gov.*/[0-9]+_[0-9]+_[0-9]+.*|'
                         r'.*fdi.gov.*/[a-z]+/[a-z]+_[0-9]+(_[0-9]+|).html.*|'
                         r'.*fdi.gov.*/[0-9]+_.*.html|'
                         r'.*mnr.gov.*/[a-z]+/[a-z]+/[a-z]+/[a-z]+/[0-9]+/?$|'
                         r'.*mnr.gov.*/[a-z]+/[a-z]+/[a-z]+/(19[0-9]{2}|20[0-9]{2})/?$)',
                         detail_url,
                         re.IGNORECASE)):
            return False
        return True

    async def handle_detail(self, page, encoding, extra_data=None):
        self.job.extra_data.update({"isupdate": True})
        url = self.job.url
        page = self.handle_special_page(page)
        if self.executor:
            article_info = await self.loop.run_in_executor(
                self.executor, self._extract_html_page, url, page, encoding)
        else:
            article_info = self._extract_html_page(url, page, encoding)
        if not article_info:
            cnt_extract_fail.labels(getway='high_frequency').inc()
            logger.error(f"Extract Content Error,URL:{url}")
            return None
        data = {
            "content": article_info["content"],
            "author": article_info["authors"],
            "title": self.job.extra_data.get("title") or article_info["title"],
            "siphash": siphash(article_info["content"].replace(" ", "")),
            "publish_date": article_info["publish_date"],
            "getway": "high_frequency",
            "sid": self.job.seed_urlmd5,
            "publisher": self.get_publisher()
        }
        return data

    @staticmethod
    def _extract_by_newspaper(url, page):
        try:
            article = newspaper.Article(url, memoize_articles=False,
                                        language='zh', fetch_images=False)
            article.set_html(page)
            article.parse()
        except Exception as e:
            logger.error(e, exc_info=True)
        else:
            if article.text:
                return {"title": article.title,
                        "content": article.text,
                        "publish_date": article.publish_date,
                        "authors": article.authors,
                        "tags": article.tags}

    @staticmethod
    def _extract_by_goose(page, encoding):
        g = Goose({"stopwords_class": StopWordsChinese,
                   "strict": False})
        article = None
        try:
            # goose 自行识别编码并处理
            article = g.extract(raw_html=page)
        except (UnicodeEncodeError, LookupError):
            # 若因编码出错，采用传递进来的编码值
            try:
                article = g.extract(
                    raw_html=page.encode(encoding, errors='ignore'))
            except Exception as e:
                logger.error(e, exc_info=True)
        except lxml.etree.ParserError:
            pass
        except Exception as e:
            logger.error(e, exc_info=True)

        if article and article.cleaned_text:
            return {"title": article.title,
                    "content": article.cleaned_text,
                    "publish_date": article.publish_date,
                    "authors": article.authors,
                    "tags": article.tags}

    def _extract_html_page(self, url, page, encoding):
        """extract title and content by given raw html stings
        :param str page: raw html strings
        :param str encoding: raw html char encoding
        :return dict result: main fields of news
        """
        # TODO: 深入检查 newspaper 的 parse 机制以防出现如下异常
        # concurrent.futures.process.BrokenProcessPool
        # 应该是它在解析时又派生了子进程，而ProcessPoolExecuter不支持
        data = self._extract_by_newspaper(url, page)
        if not data:
            data = self._extract_by_goose(page, encoding)
        return data

    async def _start_index(self, page, encoding):
        logger.info("[Spider]<{0}> handle index page of: {1}"
                    .format(self.name, self.job.url))

        ts_start = self.loop.time()
        detail_infos, next_infos = await self.handle_index(page)
        ts_end = self.loop.time()
        time_on_extract_index.labels(
            getway=self.name).observe(ts_end - ts_start)

        default_conf = SPIDER_CONF.get("default")
        priority = SPIDER_CONF.get(self.name, default_conf).get(
            "priority", default_conf["priority"])
        while next_infos:
            index_info = next_infos.pop()
            # 种子去重
            seed = await Seed.find_one({"urlmd5": md5(index_info['url'])})
            if not seed:
                await Seed(**{
                    "url": index_info.pop('url'),
                    "getway": "high_frequency",
                    "_sched_interval": 86400,
                    "status": "OK",
                    "priority": 10,
                    "anti_cfg": {
                        "proxy": True,
                        "cookie": False,
                        "random_ua": False
                    }
                }).save()

        while detail_infos:
            detail = detail_infos.pop()
            job = Job(url=detail.pop('url'),
                      seed_urlmd5=self.job.seed_urlmd5,
                      spider_name=self.job.spider_name,
                      anti_cfg=self.job.anti_cfg,
                      priority=priority,
                      is_seed=False,
                      uniqid=detail.pop(self.configs.get('uniqid'), None),
                      **detail)
            await JobQueue.rpush(job)

    async def handle_download_failure(self, error):
        try:
            if error and error.http_status in {401, 403, 404}:
                return
        except Exception:
            pass
        self.job.extra_data = self.job.extra_data or {}
        failed_count = self.job.extra_data.get("failed_count", 1)
        if failed_count > 2:
            return
        self.job.extra_data["failed_count"] = (failed_count + 1)
        await JobQueue.rpush(self.job)

    def index_url_dice_coefficient(self, url):
        """计算按照？|&分割后的url与当前job的url相似度"""
        url = re.split(r"[?&]", url, 1)[0]
        job_url_bigrams = set(self.job.url)
        url_bigrams = set(url)
        overlap = len(job_url_bigrams & url_bigrams)
        return overlap * 2.0 / (len(job_url_bigrams) + len(url_bigrams))

    def handle_special_page(self, page):
        """对于某些特殊的网站的详情页，
           需要提取出正文的部分的html才能被newspaper或Goose正确抽取
        """
        if self.job.url.find("www.ajxxgk.jcy.gov.cn") >= 0:
            sub_html = PyQuery(page)
            page = str(sub_html.find("#Article"))
        if self.job.url.find("www.scio.gov.cn") >= 0:
            sub_html = PyQuery(page)
            page = str(sub_html.find("#content"))
        return page

    def get_publisher(self):
        parsed_url = url_pass(self.job.url)
        return self.publisher_mapping.get(parsed_url.netloc, None)
