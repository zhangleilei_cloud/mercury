# -*- coding: utf-8 -*-
from mercury.spiders.high_frequency.high_frequency import HighFrequencySpider
from mercury.spiders.zhihu.zhihucolumn_article import ZhihuColumnArticleSpider
from mercury.spiders.zhihu.zhihucolumn_summary import ZhihuColumnSummarySpider
from ._base import DefaultSpider, Spider as BaseSpider
from .news import BaiduNewsSpider, WeixinSpider
from .wscourt import WSCourtSpider
from .trademark.baitu import BaituSpider
from .trademark.subiao import SubiaoSpider
from .trademark.iptop import IptopSpider
from .laws import (PkulawSpider, PkulawApiSpider, FawuSpider,
                   ItslawSpider, PkulawGzcjSpider, PkulawXndxSpider)
from .gov import TipsBeijing, TipsShanghai, TipsSiChuan, TipsGuangDong, TipsJiangSu, TipsZhejiang, TipsLiaoNing, \
    TipsHebei, TipsShanxi, TipsTianjing, TipsHuNan, TipsChongqing
from mercury.spiders.zhihu.zhihuask import ZhihuAskSpider
from .hualv import SolutionSpider, QuestionSpider
from .google import GoogleSearchResultKeywordsTop10, GoogleSearchResultDocument
from mercury.spiders.news.roll_news import RollNewsSpider
from .wscourt.gongbao_case import GongbaoCaseSpider
from .wscourt.important_case import ImportantCaseSpider
from .wscourt.journal_case import JournalCaseSpider, JournalSpider
from .wscourt.guiding_case import GuidingCaseSpider
from .wscourt.pkulaw_case import PkulawCaseSpider
from mercury.spiders.wscourt.bs_case import BSCaseSpider
