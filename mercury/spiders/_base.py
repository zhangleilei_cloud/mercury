# -*- coding: utf-8 -*-

import os
import gzip
import asyncio
import logging
import random
import re
from concurrent.futures import ProcessPoolExecutor
import demjson
from pymongo.errors import DuplicateKeyError
from pyquery import PyQuery
from urllib.parse import urlparse
from mercury.downloader import AsyncHttpDownloader
from mercury.models import Job, JobQueue
from mercury.libs import s3
from mercury.libs.mongo import mgdb
from mercury.libs.proxy import get_proxy
from mercury.utils import md5, timestamp
from mercury.settings import logger, SPIDER_CONF, MAX_JOBS
from mercury.monitor import (cnt_db_saved,
                             cnt_download_failures,
                             cnt_extract_fail,
                             time_on_download,
                             time_on_write_mongo,
                             time_on_upload_s3,
                             time_on_extract_detail,
                             time_on_extract_index)

cpu_num = os.cpu_count()


class Spider:
    loop = asyncio.get_event_loop()
    # 如果CPU核心数量大于 1 才用进程池
    executor = ProcessPoolExecutor(
        max_workers=cpu_num) if cpu_num > 1 else None
    queue = JobQueue.pool

    def __init__(self, job):
        self.job = job
        self.configs = {}
        self.configs.update(job.anti_cfg)
        self.configs.update(self.setup_configs())
        self.dl = None

    def setup_configs(self):
        """爬虫的配置，子类中只需返回个性化定制的配置项

        :supported config items:

        :config `headers` dict: custom HTTP request headers
        :config `sleep_range_before_request` tuple: (start, stop), sleep
            some time at top of `_make_request`
        """
        raise NotImplementedError("Must setup configurations for spider.")

    async def handle_index(self, page):
        """解析索引页(又称列表页)，能够获取文章详情链接的页面
        如，搜索返回的文章列表、网站子栏目里的文章列表

        返回需要抓取的所有详情链接，分页也在此处理

        :param page: 列表页面内容字符串
        :return: 详情链接列表与下一页网址 ([url1, url2, ...], nextpage_url)
            如果没有下一页，nextpage_url 值为None
        """
        raise NotImplementedError("Must implement how to handle index page.")

    async def handle_detail(self, page, encoding, extra_data=None):
        """解析文章详情页，返回相应的详情内容，字段见models中的模块
        :param page: 详情页面内容字符串
        :param encoding: 详情页面内容字符编码
        :param extra_data: index页面传递给detai页面的数据, 默认为None
        :return: 代表文章详情的字典
        """
        raise NotImplementedError("Must implement how to handle detail page.")

    def _extract_tags(self, html, encoding):
        """抽取spider的config配置中配置好的tags标签"""
        tags_info = {}
        tags = list(set(self.configs.get("need_extract_tags", [])))
        # 让table最后抽取，目的是为了保证table中的元素也能被正确抽取到
        if "table" in tags:
            tags.remove("table")
            tags.append("table")
        for tag in tags:
            if tag in self.configs.get("need_extract_tags", []):
                if tag == "table":
                    tags_info.update({tag: self._extract_tag_by_name(tag, html, encoding)})
                else:
                    tags_info.update(self._extract_tag_by_name(tag, html, encoding))
        return tags_info

    def _extract_tag_by_name(self, name, html, encoding=None):
        """根据标签名抽取html中所有的标签，并返回该标签信息
           table , pre, img, video, audio, map, blockquote
        """

        tag_info = {}
        if html is None or not isinstance(html, PyQuery):
            return tag_info
        _tags = html.find(name)
        for index, tag in enumerate(_tags):
            key = f"###{name.upper()}_{index}###"
            new_tag = PyQuery(f"<p>{key}</p>")
            tag = PyQuery(tag)
            try:
                if name in {"img", "audio", "audio"}:
                    tag.attr("src", self._format_url_path(tag.attr("src")))
                    value = str(tag)
                else:
                    value = str(tag)
            except Exception:
                continue
            tag_info.update({key: value})
            tag.replace_with(new_tag)
        return tag_info

    def _format_url_path(self, detail_url):
        """格式化uri或url,拼接成完整的url"""
        if detail_url.startswith("./"):
            detail_url = detail_url.replace("./", "", 1)
        if detail_url.startswith("//"):
            detail_url = detail_url.replace("//", "")
        elif detail_url.startswith("/"):
            detail_url = f'{urlparse(self.job.url).netloc}/{detail_url[1:]}'
        elif detail_url.startswith("../"):
            splited_url = self.job.url.split("/")[:-(detail_url.count("../") + 1)]
            splited_url.append(detail_url.replace("../", ""))
            detail_url = "/".join(splited_url)
        elif not re.search(r'http(s|)://', detail_url):
            detail_url = f'{self.job.url.rsplit("/", 1)[0]}/{detail_url}'
        if not detail_url.startswith(("http://", "https://")):
            parsed_url = urlparse(self.job.url)
            detail_url = f"{parsed_url.scheme}://{detail_url}"
        return detail_url

    def before_handle_detail(self, page, encoding):
        """处理前需要执行的函数"""
        if self.configs.get("auto_extract"):
            try:
                html = PyQuery(page)
                tags_info = self._extract_tags(html, encoding)
                self.job.extra_data = self.job.extra_data or {}
                self.job.extra_data.update({"tags_info": tags_info})
                page = str(html)
            except Exception as e:
                pass
        return page

    def after_handle_detail(self, extracted_data):
        """处理正文后需要执行的函数"""
        if self.configs.get("auto_extract") and extracted_data:
            need_replace_key = self.configs.get("need_replace_key", "content")
            try:
                text = extracted_data[need_replace_key]
                self.job.extra_data = self.job.extra_data or {}
                tags_info = self.job.extra_data.pop("tags_info", {})
                replaced_text = self._restore_content_tags(text, tags_info)
                extracted_data[need_replace_key] = replaced_text
            except Exception as e:
                logger.error(
                    "[Spider]<{0}> handle content error,url: {1} and need_replace_key:{2}".format(self.name,
                                                                                                  self.job.url,
                                                                                                  need_replace_key))
        return extracted_data

    def _restore_content_tags(self, text, tags_info):
        """恢复正文中标签内容"""
        tag_names = list(tags_info.keys())
        # 如果含有table标签，则将table最先替换保证table中的其他元素能被正确替换
        if "table" in tag_names:
            tables_info = tags_info.pop("table")
            for key in tables_info:
                tables_info[key] = self._restore_table(tables_info[key], tags_info)
            tags_info.update(tables_info)

        for key in list(tags_info.keys()):
            text = text.replace(key, tags_info[key])
            del tags_info[key]
        return text

    @staticmethod
    def _restore_table(table, tags_info):
        """恢复表格内容"""
        table_elem = PyQuery(table)
        p_tags = table_elem.find("p")

        for index, p in enumerate(p_tags):
            if not p.text.startswith("###"):
                continue
            p = PyQuery(p)
            elem_key = p.text().strip()
            html_str = tags_info.get(elem_key)
            new_tag = PyQuery(html_str)
            p.replace_with(new_tag)
        return str(table_elem)

    async def _make_http_request(self):
        """构造合适的HTTP请求，并下载页面
        """
        spider_conf = SPIDER_CONF[self.name]
        sleep_range = spider_conf.get("sleep_range") or self.configs.get("sleep_range_before_request")
        if sleep_range:
            await asyncio.sleep(random.uniform(*sleep_range))
        if self.configs.get("cookie"):
            should_close_cookie = True
        if self.configs.get("random_ua"):
            ua = "randomly select a User-Agent from somewhere"
        page, encoding = None, None
        if await self.before_request():
            dl = self.dl or AsyncHttpDownloader(
                headers=self.configs.get("headers"),
                cookies=self.configs.get("cookies"),
                proxy=get_proxy() if self.configs.get('proxy_enable') else None)
            page, encoding = await dl.fetch(
                self.job.url, method=self.job.method,
                ctype=self.job.ctype, data=self.job.payload,
                rtype=self.job.rtype, auto_close=self.job.auto_close)
            return page if isinstance(page, (str, list, dict)) else dl.error, encoding
        return page, encoding

    async def _make_redis_request(self):
        params = dict([p.split('=') for p in
                       self.job.url.rsplit('?', 1)[-1].split('&')])
        collection = params['collection']
        operation = params['operation']
        number = params['number']
        command = getattr(self.queue, operation)
        infos = await command(collection, number)
        return infos, 'utf-8'

    async def _make_mongo_request(self):
        params = dict([p.split('=') for p in
                       self.job.url.rsplit('?', 1)[-1].split('&')])
        collection = params['collection']
        filters = demjson.decode(params['filters'])
        fields = demjson.decode(params['fields'])
        db = getattr(mgdb, collection)
        retval = []
        cursor = db.find(filters, fields).limit(MAX_JOBS)
        async for doc in cursor:
            retval.append(doc)
        return retval, 'utf-8'

    async def _make_request(self):
        if self.job.url.startswith('redis://'):
            return await self._make_redis_request()
        elif self.job.url.startswith('mongodb://'):
            return await self._make_mongo_request()
        else:
            return await self._make_http_request()

    async def before_request(self):
        return True

    async def handle_dup_detail(self):
        pass

    async def handle_download_failure(self, error):
        """用于下载失败的后处理"""
        pass

    async def after_insert(self):
        """用于插入成功的后处理"""
        pass

    async def clean_data(self, data):
        """页面提取成功后的数据清洗操作,包含乱码检测"""
        return data

    async def check_repeat(self, data):
        """数据入库前的判重操作"""
        return False

    async def _start_index(self, page, encoding):
        # 本次任务是列表页，则将列表页中解析出的
        # 文章详情 url 生成下载作业并推送到下载队列
        logger.info("[Spider]<{0}> handle index page of: {1}"
                    .format(self.name, self.job.url))

        ts_start = self.loop.time()
        detail_infos, next_info = await self.handle_index(page)
        ts_end = self.loop.time()
        time_on_extract_index.labels(
            getway=self.name).observe(ts_end - ts_start)
        if next_info:
            detail_infos.append(next_info)
            is_seed = True
        else:
            is_seed = False

        default_conf = SPIDER_CONF.get("default")
        priority = SPIDER_CONF.get(self.name, default_conf).get(
            "priority", default_conf["priority"])
        while detail_infos:
            detail = detail_infos.pop()
            job = Job(url=detail.pop('url'),
                      seed_urlmd5=self.job.seed_urlmd5,
                      spider_name=self.job.spider_name,
                      anti_cfg=self.job.anti_cfg,
                      priority=priority,
                      is_seed=is_seed,
                      uniqid=detail.pop(self.configs.get('uniqid'), None),
                      **detail)
            await JobQueue.rpush(job)
            is_seed = False

    async def _start_detail(self, page, encoding):
        logger.info("[Spider]<{0}> handle detail page of: {1}"
                    .format(self.name, self.job.url))

        ts_start = self.loop.time()
        extracted_page = self.before_handle_detail(page, encoding)
        extracted_data = await self.handle_detail(extracted_page, encoding, self.job.extra_data)
        result_data = self.after_handle_detail(extracted_data)
        ts_end = self.loop.time()
        time_on_extract_detail.labels(
            getway=self.name).observe(ts_end - ts_start)

        # 页面有内容，但无目标数据视为抽取失败
        if extracted_page and (not result_data):
            cnt_extract_fail.labels(getway=self.name).inc()
            return

        # 数据清洗
        cleaned_data = await self.clean_data(result_data)
        if not cleaned_data:
            logger.info("[Spider]<%s> clean data failed", self.name)
            return

        # 数据判重
        if await self.check_repeat(cleaned_data):
            logger.info("[Spider]<%s> data repeat", self.name)
            return

        if cleaned_data:
            url = cleaned_data.pop('url', None) or self.job.url
            urlmd5 = self.job.urlmd5
            inst = self.Model(url, urlmd5, **cleaned_data)
            ts_start = self.loop.time()
            if self.job.extra_data.pop('isupdate', None):
                uid = self.configs['uniqid']
                filters = {uid: inst.uniqid}
                cleaned_data["url"] = url
                retval = await inst.update(filters, cleaned_data)
                logger.debug('[Spider]<{0}> update model {1} - {2}'.format(
                    self.name, type(inst).__name__, inst.uniqid))
            else:
                retval = await inst.save()
            ts_end = self.loop.time()
            time_on_write_mongo.labels(
                getway=self.name).observe(ts_end - ts_start)

            if isinstance(retval, DuplicateKeyError):
                return None
            else:
                cnt_db_saved.labels(getway=self.name).inc()
            await self.after_insert()
            # 入库完成后标记本详情页面已经抓取过
            uniqid = inst.uniqid
            spider_name = self.name
            spider_conf = SPIDER_CONF[spider_name]
            expire = spider_conf.get('dedup_expire')
            if expire:
                await self.queue.setex(f"job:crawled:{spider_name}:{uniqid}", expire, 1)
            else:
                await self.queue.set(f"job:crawled:{spider_name}:{uniqid}", 1)

            if self.configs.get("save_html"):
                # 存原始HTML页面到S3服务
                fname = "{spider}/{date}/{uniqid}.html.gz".format(
                    spider=self.name,
                    date=timestamp().strftime("%Y%m%d"),
                    uniqid=uniqid)

                if isinstance(extracted_page, dict):
                    extracted_page = demjson.encode(extracted_page)
                    fname = fname.replace('.html.gz', '.json.gz')
                try:
                    binary = extracted_page.encode(encoding)
                except UnicodeEncodeError:
                    logger.error("file encoding error: %s, %s", self.job.url, encoding)
                else:
                    tarfile = gzip.compress(binary)
                    ts_start = self.loop.time()
                    await s3.upload_file(tarfile, fname)
                    ts_end = self.loop.time()
                    time_on_upload_s3.labels(getway=self.name).observe(ts_end - ts_start)
                    logger.info("[Spider]<%s> upload %s to s3 finished", self.name, fname)

    async def start(self):
        logger.info("[Spider]<{0}> start crawling url: {1}"
                    .format(self.name, self.job.url))

        ts_start = self.loop.time()
        page, encoding = await self._make_request()
        ts_end = self.loop.time()
        time_on_download.labels(getway=self.name).observe(ts_end - ts_start)

        # page 里若无内容认为下载失败，做一次统计
        if not isinstance(page, (str, list, dict)):
            cnt_download_failures.labels(getway=self.name).inc()
            await self.handle_download_failure(page)
        else:
            if self.job.is_seed:
                await self._start_index(page, encoding)
            else:
                await self._start_detail(page, encoding)


class DefaultSpider(Spider):
    """默认 Spider，做成自动处理的
    """

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        return {}

    async def start(self):
        # 遇到未知类型的Job,把Job放回任务队列中
        await JobQueue.rpush(self.job)

    async def handle_index(self, resp):
        pass

    async def handle_detail(self, resp):
        pass
