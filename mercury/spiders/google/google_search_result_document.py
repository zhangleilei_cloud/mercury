# -*- coding: utf-8 -*-

import demjson

from mercury.models.google_search_result import GoogleSearchResult
from mercury.spiders._base import Spider as BaseSpider
from mercury.utils import md5
from bs4 import BeautifulSoup
from mercury.models import JobQueue
from mercury.settings import logger

headless_url = "http://127.0.0.1:3000/"
google_url = "https://www.google.com"
LIMIT_MAX_PAGE = 10


class Spider(BaseSpider):
    name = "google_search_result_document"
    Model = GoogleSearchResult

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "User-Agent": (" Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:60.0)"
                               " Gecko/20100101 Firefox/60.0")
            },
            "cookies": {},
            "sleep_range_before_request": (35, 67),
            "uniqid": "urlmd5",
            "save_html": False,
            "proxy_enable": False
        }
        return configs

    async def before_request(self):
        if self.job.is_seed:
            self.job.url = headless_url + self.job.url
        return True

    async def handle_download_failure(self, error):
        # 将下载失败的任务重新加入任务队列
        if self.job.is_seed:
            self.job.url = self.job.url.replace(headless_url, "")
        await JobQueue.rpush(self.job)

    async def handle_index(self, page):
        detail_infos, next_info = [], {}
        try:
            soup = BeautifulSoup(page, 'lxml')

            keywords = self.job.extra_data.pop("keywords", "")
            extra_data = {"keywords": keywords}
            list_info_ele = soup.select("div.srg div.rc")
            if list_info_ele:
                for item in list_info_ele:
                    retval = {}
                    # 提取搜索结果指向的URL
                    url = item.find("div", {"class": "r"}).find("a")['href']
                    # 提取搜索结果标题
                    retval["title"] = item.find("div", {"class": "r"}).find("h3").get_text()
                    # 提取搜索结果摘要
                    retval["abstract"] = item.find("div", {"class": "s"}).find("span", {"class": "st"}).prettify()
                    retval.update({"reference": self.job.url})
                    retval.update({"keywords": keywords})
                    retval.update({"getway": self.name})

                    urlmd5 = md5(url + self.job.query + (demjson.encode(self.job.payload) if self.job.payload else ''))
                    inst = GoogleSearchResult(url, urlmd5, **retval)
                    await inst.save()
            # 提取当前页码
            index_ele = soup.select("#nav td.cur")
            real_max_page = len(soup.select("#nav td")) - len(soup.select("#nav td.navend"))
            max_page = LIMIT_MAX_PAGE if LIMIT_MAX_PAGE < real_max_page else real_max_page
            index = int(index_ele[0].get_text())
            if index < max_page:
                url_next = google_url + index_ele[0].find_next_sibling("td").find("a", href=True)['href']
                next_info = {'url': url_next, 'extra_data': extra_data}
            del page
        except Exception as e:
            logger.error(e, exc_info=True)
        return (detail_infos, next_info)

    async def handle_detail(self, page, encoding, extra_data=None):
        try:
            retval = {}
            return retval
        except Exception as e:
            logger.error(e, exc_info=True)
            return None
