# -*- coding: utf-8 -*-
import json
from pyquery import PyQuery as pq
from mercury.models import JobQueue
from mercury.models.zhihucolumn import ZhihuColumnArticle
from mercury.settings import logger
from mercury.spiders._base import Spider as BaseSpider


class ZhihuColumnArticleSpider(BaseSpider):
    name = "zhihucolumn_article"
    Model = ZhihuColumnArticle

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "User-Agent": ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 "
                               "(KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
            },
            "cookies": {},
            "uniqid": "urlmd5",
            "sleep_range_before_request": (1, 2),
            "save_html": False,
            "proxy_enable": True,
            "save_image": False
        }
        return configs

    async def handle_index(self, page):
        detail_infos, next_info = [], {}
        try:
            page_json = json.loads(page)
            data_json = page_json.get("data", [])
            for item in data_json:
                extra_data = {"title": item["title"],
                              "created": item["created"],
                              "updated": item["updated"],
                              "cover_image_url": item["image_url"],
                              "article_id": item["id"],
                              "url": item["url"],
                              "comment_count": item["comment_count"],
                              "voteup_count": item["voteup_count"],
                              "column_id": self.job.extra_data["column_id"]
                              }
                detail_infos.append({"url": item["url"], "extra_data": extra_data})
            if not page_json["paging"]["is_end"]:
                next_info.update({"url": page_json["paging"]["next"],
                                  "extra_data": {"column_id": self.job.extra_data["column_id"]}})
            return detail_infos, next_info
        except Exception as e:
            return detail_infos, next_info

    async def handle_detail(self, page, encoding, extra_data=None):
        try:
            content_html = pq(page).find(".Post-RichText")
            content = content_html.text()
            tags_html = pq(page).find(".Tag")
            tags = []
            for tag in tags_html:
                tag_html = pq(tag)
                tags.append({"name": tag_html.text(),
                             "url": tag_html.find(".TopicLink").attr("href")})
            images_urls, video_urls = self.extract_media_info(content_html)
            extra_data.update({"images_urls": images_urls,
                               "video_urls": video_urls,
                               "url": extra_data["url"],
                               "getway": "zhihucolumn_article",
                               "sid": self.job.seed_urlmd5,
                               "content": content,
                               "tags": tags
                               })
            return extra_data
        except Exception as e:
            logger.error(e)
            return None

    @staticmethod
    def extract_media_info(html):
        images_urls, video_urls = {}, {}
        img_index, video_index = 0, 0
        imgs_html, videos_html = html.find("img"), html.find("video")
        for img in imgs_html:
            img_key = "###IMAGE_" + str(img_index) + "###"
            tag = pq(img)
            # 过滤img为base64格式的空图片
            if tag.attr("src").startswith("data"):
                tag.replace_with(pq("<span></span>"))
                continue
            images_urls.update({img_key: tag.attr("src")})
            tag.replace_with(pq("<span>" + img_key + "</span>"))
            img_index += 1

        for video in videos_html:
            video_key = "###VIDEO_" + str(video_index) + "###"
            tag = pq(video)
            video_urls.update({video_key: tag.attr("src")})
            tag.replace_with(pq("<span>" + video_key + "</span>"))
            video_index += 1
        return images_urls, video_urls

    async def handle_download_failure(self, error):
        # 将下载失败的任务重新加入任务队列
        await JobQueue.rpush(self.job)
