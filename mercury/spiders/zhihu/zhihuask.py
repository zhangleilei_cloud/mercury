# -*- coding: utf-8 -*-
import json
import re

from pyquery import PyQuery as pq
from mercury.downloader import AsyncHttpDownloader
from mercury.libs.proxy import get_proxy
from mercury.models import JobQueue
from mercury.models.zhihuask import ZhihuAsk
from mercury.settings import logger
from mercury.spiders._base import Spider as BaseSpider

detail_base_uri_parm = "/answers?include=data[*].comment_count,content,voteup_count,created_time,updated_time,question.detail&limit=20&offset=0&sort_by=default"


class ZhihuAskSpider(BaseSpider):
    name = "zhihuask"
    Model = ZhihuAsk

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "User-Agent": ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 "
                               "(KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
            },
            "cookies": {},
            "uniqid": "urlmd5",
            "sleep_range_before_request": (1, 2),
            "save_html": False,
            "proxy_enable": True,
            "save_image": False
        }
        return configs

    async def handle_index(self, page):
        detail_infos, next_info = [], {}
        try:
            page_json = json.loads(page)
            paging = page_json["paging"]
            base_url = "http://www.zhihu.com/api/v4"
            data_length = len(page_json["data"])
            for index in range(data_length - 10):
                page_json["data"].pop(0)
            for data in page_json["data"]:
                # index结果中过滤article类型的结果
                if data.get("card_type") == "corrected":
                    continue
                if data.get("section_type") in ["topic", "people"]:
                    continue
                if data.get("type") == "relevant_query":
                    continue
                if data["object"]["type"] == "article":
                    continue
                try:
                    # 如果取不到id则认为该问答还未有人回答，直接过滤
                    question_id = data["object"]["question"]["id"]
                    answer_id = data["object"]["id"]
                    question_topic_url = f'https://www.zhihu.com/question/{question_id}/answer/{answer_id}'
                except KeyError:
                    continue
                detail_url = base_url + "/questions/" + question_id + detail_base_uri_parm
                extra_data = {}
                extra_data.update({"title": data["object"]["question"]["name"],
                                   "url": detail_url,
                                   "question_topic_url": question_topic_url})
                detail_infos.append({"url": detail_url, "extra_data": extra_data})

            if not page_json["paging"]["is_end"]:
                # next_url中的search_hash_id参数是一个变量，移除该参数
                next_url = paging["next"]
                pattern = re.compile(r'&search_hash_id=[a-z0-9]{32}')
                next_url = pattern.sub("", next_url)
                next_info.update({"url": next_url})
                next_info.update({"extra_data": None})
        except Exception as e:
            logger.error(page[1])
            logger.error(e, exc_info=True)
        return detail_infos, next_info

    async def before_request(self):
        if not self.job.is_seed and self.job.extra_data.get("question_topic_url"):
            return await self._crawl_tags()
        return True

    async def _crawl_tags(self):
        _tags = AsyncHttpDownloader(
            headers=self.configs.get("headers"),
            cookies=self.configs.get("cookies"),
            proxy=get_proxy() if self.configs.get('proxy_enable') else None)
        try:
            tags_page, encode = await _tags.fetch(url=self.job.extra_data["question_topic_url"],
                                                  ctype=self.job.ctype, data=self.job.payload,
                                                  rtype=self.job.rtype, auto_close=self.job.auto_close)
            question_tags = pq(tags_page).find(".QuestionTopic")
            tags = []
            for tag in question_tags:
                tag_html = pq(tag)
                tags.append({"name": tag_html.text(),
                             "url": tag_html.find(".TopicLink").attr("href")})
            self.job.extra_data["tags"] = tags
        except Exception as e:
            logger.error(e)
            return False
        return True

    async def _crawl_question_times(self):
        _tags = AsyncHttpDownloader(
            headers=self.configs.get("headers"),
            cookies=self.configs.get("cookies"),
            proxy=get_proxy() if self.configs.get('proxy_enable') else None)
        try:
            question_id = re.search(r'.*questions/([0-9]+)', self.job.url).group(1)
            question, encode = await _tags.fetch(url=f"https://api.zhihu.com/questions/{question_id}",
                                                 ctype=self.job.ctype, data=self.job.payload,
                                                 rtype=self.job.rtype, auto_close=self.job.auto_close)
            question_json = json.loads(question)
            return question_json["created"], question_json["updated_time"]
        except Exception as e:
            logger.error(e)
            return None, None

    async def handle_detail(self, page, encoding, extra_data=None):
        try:
            page_json = json.loads(page)
            data = {}
            data.update({
                "url": extra_data["url"],
                "getway": "zhihuask",
                "sid": self.job.seed_urlmd5,
            })
            if page_json["data"] == []:
                pubtime, reptime = await self._crawl_question_times()
                if pubtime and reptime:
                    data.update({
                        "title": extra_data["title"],
                        "answers": [],
                        "pubtime": pubtime,
                        "reptime": reptime
                    })
                    return data
                return None

            data_json = page_json["data"]
            titel = data_json[0]["question"]["title"]
            detail_html = pq(data_json[0]["question"]["detail"] or "<p></p>")
            detail_imgs, detail_videos = self.extract_media_info(detail_html)
            detail = detail_html.text()
            pubtime = data_json[0]["question"]["created"]
            reptime = data_json[0]["question"]["updated_time"]
            question_type = data_json[0]["question"]["question_type"]
            question_id = data_json[0]["question"]["id"]
            tags = self.job.extra_data.get("tags")
            answers = []
            for answer_info in data_json:
                answer = {}
                content_html = pq(answer_info["content"] or "<span></span>")
                image_urls, video_urls = self.extract_media_info(content_html)
                answer.update({"images_urls": image_urls,
                               "video_urls": video_urls,
                               "created_time": answer_info["created_time"],
                               "updated_time": answer_info["updated_time"],
                               "comment_count": answer_info["comment_count"],
                               "voteup_count": answer_info["voteup_count"],
                               "answer_url": answer_info["url"],
                               "content": content_html.text(),
                               "author": answer_info["author"]})
                answers.append(answer)
            data.update({"title": titel,
                         "detail": detail,
                         "detail_imgs": detail_imgs,
                         "detail_videos": detail_videos,
                         "pubtime": pubtime,
                         "reptime": reptime,
                         "answers": answers,
                         "question_type": question_type,
                         "question_id": question_id,
                         "tags": tags})

            return data

        except Exception as e:
            logger.error(e, exc_info=True)

    @staticmethod
    def extract_media_info(html):
        images_urls, video_urls = {}, {}
        img_index, video_index = 0, 0
        imgs_html, videos_html = html.find("img"), html.find("video")
        for img in imgs_html:
            img_key = "###IMAGE_" + str(img_index) + "###"
            tag = pq(img)
            # 过滤img为base64格式的空图片
            if tag.attr("src").startswith("data"):
                tag.replace_with(pq("<span></span>"))
                continue
            images_urls.update({img_key: tag.attr("src")})
            tag.replace_with(pq("<span>" + img_key + "</span>"))
            img_index += 1

        for video in videos_html:
            video_key = "###VIDEO_" + str(video_index) + "###"
            tag = pq(video)
            video_urls.update({video_key: tag.attr("src")})
            tag.replace_with(pq("<span>" + video_key + "</span>"))
            video_index += 1
        return images_urls, video_urls

    async def handle_download_failure(self, error):
        # 将下载失败的任务重新加入任务队列
        await JobQueue.rpush(self.job)
