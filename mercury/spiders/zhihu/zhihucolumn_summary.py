# -*- coding: utf-8 -*-
import asyncio
import json
import random
import re
from mercury.downloader import AsyncHttpDownloader
from mercury.libs.proxy import get_proxy
from mercury.models import JobQueue, Job
from mercury.models.zhihucolumn import ZhihuColumnSummary
from mercury.spiders._base import Spider as BaseSpider
from mercury.utils import md5
from mercury.settings import logger


class ZhihuColumnSummarySpider(BaseSpider):
    name = "zhihucolumn_summary"
    Model = ZhihuColumnSummary

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "User-Agent": ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 "
                               "(KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
            },
            "cookies": {},
            "uniqid": "urlmd5",
            "sleep_range_before_request": (1, 2),
            "save_html": False,
            "proxy_enable": True,
            "save_image": False
        }
        return configs

    async def handle_index(self, page):

        detail_infos, next_info = [], {}
        try:
            page_json = json.loads(page)
            data = page_json["data"]
            for item in data:
                extra_data = {}
                pattern = re.compile(r"<em>|</em>")
                extra_data.update({"title": re.sub(pattern, "", (item["object"]["title"])),
                                   "column_id": item["object"]["id"],
                                   "description": re.sub(pattern, "", (item["object"]["description"])),
                                   "followers": item["object"]["followers"],
                                   "articles_count": item["object"]["articles_count"],
                                   "url": item["object"]["url"],
                                   "author": item["object"]["author"]
                                   })
                detail_infos.append({"url": item["object"]["url"], "extra_data": extra_data})

            if not page_json["paging"]["is_end"]:
                next_info.update({"url": page_json["paging"]["next"].replace(r'search_hash_id[0-9a-z]{32}', ''),
                                  "extra_data": None})
        except Exception as e:
            logger.error(e, exc_info=True)
            logger.error(f"page:{page}")
        return detail_infos, next_info

    async def _make_http_request(self):
        """重构此方法解决爬虫的逻辑步骤一致性问题
        """
        sleep_range = self.configs.get("sleep_range_before_request")
        if sleep_range:
            await asyncio.sleep(random.uniform(*sleep_range))
        page, encoding = None, None
        if self.job.is_seed:
            if await self.before_request():
                dl = self.dl or AsyncHttpDownloader(
                    headers=self.configs.get("headers"),
                    cookies=self.configs.get("cookies"),
                    proxy=get_proxy() if self.configs.get('proxy_enable') else None)
                page, encoding = await dl.fetch(
                    self.job.url, method=self.job.method,
                    ctype=self.job.ctype, data=self.job.payload,
                    rtype=self.job.rtype, auto_close=self.job.auto_close)
                return page if isinstance(page, (str, list, dict)) else dl.error, encoding
            return page, encoding
        else:
            return self.job.extra_data, "utf-8"

    async def handle_detail(self, page, encoding, extra_data=None):
        page.update({
            "url": extra_data["url"],
            "getway": "zhihucolumn_summary",
            "sid": self.job.seed_urlmd5,
        })
        return page

    async def handle_download_failure(self, error):
        # 将下载失败的任务重新加入任务队列
        await JobQueue.rpush(self.job)

    async def after_insert(self):
        # 专栏摘要保存后创建专栏文章种子用于爬取专栏所有文章
        column_artilces_url = f'{self.job.url}/articles?include=data[*].comment_count,voteup_count,topics,content&limit=20&offset=0'
        # 将column_id传递到具体某个专栏的索引页,
        # 用于关联专栏摘要和具体某一篇文章
        extra_data = {"column_id": self.job.extra_data["column_id"]}
        seed_urlmd5 = md5(column_artilces_url)
        job = Job(url=column_artilces_url,
                  seed_urlmd5=seed_urlmd5,
                  spider_name="zhihucolumn_article",
                  anti_cfg=self.job.anti_cfg,
                  priority=self.job.priority,
                  is_seed=True,
                  uniqid=seed_urlmd5,
                  extra_data=extra_data)
        await JobQueue.rpush(job)
