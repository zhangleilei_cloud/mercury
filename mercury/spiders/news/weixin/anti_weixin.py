# -*- coding: utf-8 -*-

import json
import os
import random
import time

import requests
import lxml.etree

from mercury.settings import logger

DIR_SCRIPT = os.path.dirname(os.path.abspath(__file__))
VERIFY_CODE_URL = "http://223.223.203.118:5500/api/captcha"
SECCODE_URL = "https://weixin.sogou.com/antispider/"
SECCODE_RESULT_URL = "https://weixin.sogou.com/antispider/thank.php"

code_str = "用户您好，您的访问过于频繁，为确认本次访问为正常用户行为，需要您协助验证。"

headers = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) "
                  "Chrome/69.0.3497.100 Safari/537.3",
}


class WeiXinAnti:

    def __init__(self, request_url, page, referer=None):
        """
        :param request_url: 触发反爬之前的页面url
        :param page: 反爬页面：出现验证码的页面
        """
        self.session = requests.Session()
        self.url = request_url
        self.page = page
        headers.pop("Referer", None)
        if referer:
            headers.update({"Referer": referer})

    def get_seccode_url(self):
        """
        解析出现验证码的html
        :return: r：页面上的某个参数，在提交验证码结果时需提交该参数
                 img_url：验证码的url
        """
        try:
            html = lxml.etree.HTML(self.page)
            r = html.xpath("//input[@id='from']/@value")[0]
            seccode_url = html.xpath("//img[@id='seccodeImage']/@src")[0]
            img_url = SECCODE_URL + seccode_url
            return r, img_url
        except Exception as e:
            logger.error(e, exc_info=True)
        return "", ""

    def download_seccode(self, r, img_url):
        try:
            response = self.session.get(img_url, headers=headers)
            if response.content:
                captcha = self.verify_code(response.content)
                if captcha:
                    data = {
                        "c": captcha,
                        "r": r,
                        "v": 5
                    }
                    logger.debug(
                        "[WeiXinAnti] post captcha data {}".format(data))
                    response = self.session.post(
                        SECCODE_RESULT_URL, data=data, headers=headers)
                    response.encoding = "utf-8"
                    return response
        except Exception as e:
            logger.error(e, exc_info=True)
        return None

    def verify_code(self, img):
        response = requests.post(VERIFY_CODE_URL, data=img)
        return response.text

    def make_cookies(self, response):
        try:
            response = json.loads(response.text)
            if response.get("code") == 0:
                SNUID = response.get("id", "")
                SUV = int(time.time() * 1000) * 1000 + \
                    round(random.random() * 1000)
                cookies = self.session.cookies.get_dict()
                logger.debug("[WeiXinAnti] cookies {}".format(cookies))
                cookies.update({
                    "SUV": SUV,
                    "SNUID": SNUID
                })

                cookie = ["{}={};".format(key, value)
                          for key, value in cookies.items()]
                headers.update({"Cookie": " ".join(cookie)})
                return cookie
        except Exception as e:
            logger.error(e, exc_info=True)
        return None

    def get_normal_html(self):
        """
        提交完验证码再次请求。能否回到正常页面
        """
        try:
            response = requests.get(self.url, headers=headers)
            response.encoding = "utf-8"
            return response.text
        except Exception as e:
            logger.error(e, exc_info=True)
        return ""

    def solve(self):
        while 1:
            logger.info("[WeiXinAnti] is anti page {}".format(
                code_str in self.page))
            if code_str in self.page:
                r, image_url = self.get_seccode_url()
                res = self.download_seccode(r, image_url)
                if not res:
                    continue
                self.make_cookies(res)
                self.page = self.get_normal_html()
            else:
                return self.page, headers
            time.sleep(3)


if __name__ == '__main__':

    # 测试结果： 目前只有 index 页面反爬，detail 页面可以直接访问，但是有timestamp=1539928509这个参数存在过期
    request_index = ("https://weixin.sogou.com/weixin?usip=&query=%E4%BB%8A%E6%97%A5%E5%A4%B4%E6%9D%A1"
                     "&ft=2018-10-18&tsn=5&et=2018-10-18&interation=&type=2&wxid=&page=2&ie=utf8")
    request_detail = ("http://mp.weixin.qq.com/s?src=11&timestamp=1539928509&ver=1191"
                      "&signature=uXJBpYmjW36LezXdN6it6FIDWUx0i85-0TvRPVkNvCZXVDu70r5WsZC3ZHKuBeHy2uz"
                      "XExeEadaSixJVhT3kY0-BU0AP1kwMjlBtFC71VbPqSH-JsVgjdWcRHtSxFmRL&new=1")

    response = requests.get(request_index, headers=headers)
    response.encoding = "utf-8"
    print(response.text)
