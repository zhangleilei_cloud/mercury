# -*- coding: utf-8 -*-

import uuid
import gzip
from datetime import datetime
from concurrent.futures import ThreadPoolExecutor

import requests
import newspaper
from pyquery import PyQuery
from bs4 import BeautifulSoup as bs

from mercury.libs import s3
from mercury.libs.useragent import random_ua
from mercury.libs.proxy import get_proxy, get_ip
from mercury.models import WxNews, JobQueue
from mercury.settings import logger, MAX_JOBS
from mercury.utils import md5, timestamp, siphash
from mercury.monitor import time_on_upload_s3
from mercury.spiders._base import Spider as BaseSpider
from ..util import clean_data

code_str = "验证码"


class WeixinSpider(BaseSpider):
    name = "wxnews"
    Model = WxNews

    executor = ThreadPoolExecutor(max_workers=MAX_JOBS)

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                'Accept': ('text/html,application/xhtml+xml,application/xml;'
                           'q=0.9,image/webp,image/apng,*/*;'
                           'q=0.8,application/signed-exchange;v=b3'),
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                'Connection': 'keep-alive',
                'DNT': '1',
                'Host': 'weixin.sogou.com',
                'Cache-Control': 'no-cache',
                'Pragma': 'no-cache',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': random_ua(),
            },
            "uniqid": "mixid",
            "save_html": True,
            "proxy_enable": True
        }
        return configs

    @staticmethod
    def generate_cookie():
        suv = "00" + str(uuid.uuid4()).replace("-", "").upper()[:-2]
        return {"SUV": suv}

    @staticmethod
    def _fetch(url, headers=None, cookies=None,
               proxies=None, allow_redirects=True):
        logger.info("[Requests] download: %s", url)
        logger.info("headers: %s, cookies: %s, proxies: %s",
                    headers, cookies, proxies)
        try:
            r = requests.get(url, headers=headers, proxies=proxies,
                             cookies=cookies, allow_redirects=allow_redirects,
                             timeout=10)
        except:
            return None
        else:
            logger.info("[Request]: response: %d, cookies: %s",
                        r.status_code, r.cookies.get_dict())
            if r.status_code < 400:
                return r

        return None

    async def fetch(self, url, headers=None, cookies=None,
                    proxies=None, allow_redirects=True):
        resp = await self.loop.run_in_executor(
            self.executor, self._fetch, url, headers,
            cookies, proxies, allow_redirects)
        return resp


    async def before_request(self):
        if self.job.is_seed:
            self.configs["headers"]["Referer"] = self.job.url

        return True

    async def _make_http_request(self):
        if self.job.is_seed:
            ip = await get_ip()
            if not ip:
                return None, None
            proxy = f"https://201808031844454498:31686474@{ip}"
            proxies = {"https": proxy}
            cookies = self.generate_cookie()
            resp = await self.fetch(
                self.job.url, headers=self.configs["headers"],
                cookies=cookies, proxies=proxies)
            if resp and resp.status_code == 200:
                resp.encoding = "utf-8"
                return resp.text, "utf-8"
        else:
            proxy = get_proxy()
            proxies = {"http": proxy}
            resp = await self.fetch(self.job.url, proxies=proxies)
            if resp and resp.status_code == 200:
                resp.encoding = "utf-8"
                return resp.text, "utf-8"

        return None, None

    async def after_insert(self):
        if self.job.extra_data.get("type") == "update":
            collection = self.job.extra_data["collection"]
            key = self.job.extra_data["key"]
            logger.info("[Spider]<%s> delete key: %s", self.name, key)
            await self.queue.srem(collection, key)

    async def handle_download_failure(self, error):
        if not self.job.is_seed:
            logger.info("[Spider]<%s> failed job: %s", self.name, self.job.url)
            await JobQueue.rpush(self.job)

    @staticmethod
    def _get_img_index():
        for index in range(500):
            yield index

    # html 将图片和视频的位置替换成为字符串
    def _replace_img_tag(self, soup):
        link_index = self._get_img_index()
        link_url = []
        try:
            image = soup.select("img")
            video = soup.select("iframe")
            stream_media = image + video
            for tag in stream_media:
                src = tag.get("data-src", None) or tag.get("src", None)
                if src:
                    new_tag = soup.new_tag("span")
                    if tag.name == "img":
                        data = "image"
                    else:
                        data = "vedio"
                    index = next(link_index)
                    new_tag.string = "##{0}_{1}##".format(data, index)
                    tag.replace_with(new_tag)
                    link_url.append({new_tag.string: src})
        except Exception as e:
            logger.error(e, exc_info=True)
        else:
            soup_prettify = soup.prettify()
            return (soup_prettify, link_url)

    @staticmethod
    def _extract_by_newspaper(url, page):
        try:
            article = newspaper.Article(url, memoize_articles=False,
                                        language="zh", fetch_images=False)
            article.set_html(page)
            article.parse()
        except Exception as e:
            logger.error(e, exc_info=True)
        else:
            return {"title": article.title,
                    "content": article.text}

    @staticmethod
    def _extract_content(page):
        pq = PyQuery(page)
        content = pq("#js_content").text()
        title = pq("title").text()
        return {"content": content, "title": title}

    # 保存的是替换过图片和视频的 html 网页
    async def _upload_image_to_s3(self, page, encoding):
        fname = "{spider}/{date}/{urlmd5}.html.gz".format(
            spider=self.name,
            date=timestamp().strftime("%Y%m%d"),
            urlmd5=self.job.urlmd5)
        return fname
        try:
            ts_start = self.loop.time()
            await s3.upload_file(gzip.compress(page.encode(encoding)), fname)
            ts_end = self.loop.time()
        except Exception as e:
            logger.error(e, exc_info=True)
        else:
            time_on_upload_s3.labels(
                getway=self.name).observe(ts_end - ts_start)
            logger.info(
                "[Spider]<{0}> upload {1} to s3 finished".format(self.name, fname))
            return fname

    def _extract_html_page(self, url, page):
        try:
            soup = bs(page, "lxml")
            page_prettify, link_url = self._replace_img_tag(soup)
            # data = self._extract_by_newspaper(url, page_prettify)
            data = self._extract_content(page_prettify)
            # 解析作者和出版方
            info_tmp = soup.select(
                "div#img-content >  div#meta_content > span.rich_media_meta")
            origin_tmp = []
            for info in info_tmp:
                origin_tmp.append(info.get_text())
            if len(origin_tmp) == 3:
                author = origin_tmp[1].strip("\n ")
            elif len(origin_tmp) == 2:
                author = origin_tmp[0].strip("\n ")
            else:
                author = ""
            # publisher_tag = soup.select("a#js_name")
            # if publisher_tag:
            #     publisher = publisher_tag[0].get_text().strip("\n ")
            # else:
            #     publisher = soup.select(".account_nickname_inner")[
            #         0].get_text() .strip("\n ")
        except Exception as e:
            logger.error(e, exc_info=True)
        else:
            if link_url:
                data.update({
                    "author": author,
                    # "publisher": publisher,
                    "link_url": link_url,
                })
            return (data, page_prettify)

    async def clean_data(self, data):
        if isinstance(data, dict) and isinstance(data.get("content"), str):
            cleaned_content = clean_data(data["content"])
            if cleaned_content:
                data["content"] = cleaned_content
                return data
        return None

    async def check_repeat(self, data):
        if self.job.extra_data.get("type") == "update":
            return False
        content = data["content"]
        content_hash = siphash(content)
        key = f"job:crawled:{self.name}:{content_hash}"
        if await self.queue.get(key):
            logger.info("[Spider]<%s> repeat page: %s", self.name, content_hash)
            return True
        await self.queue.set(key, 1)
        data["content"] = content
        data["content_hash"] = content_hash
        return False

    async def handle_detail(self, page, encoding, extra_data=None):
        if code_str in page:
            logger.info("[Spider]<%s> anti page: %s", self.name, self.job.url)
            return None
        url = self.job.url
        try:
            data, page = self._extract_html_page(url, page)
        except Exception as e:
            logger.error(e, exc_info=True)
        else:
            if data and extra_data:
                fname = await self._upload_image_to_s3(page, encoding)
                share_link = extra_data['share_link']
                mixid = extra_data['mixid']
                publisher = extra_data['publisher']
                title = extra_data['title']
                pubtime = datetime.utcfromtimestamp(int(extra_data['pubtime']))
                data.update({
                    "url": self.job.url,
                    "getway": "wxnews",
                    "pubtime": pubtime,
                    'publisher': publisher,
                    'title': title,
                    "page": fname,
                    "sid": self.job.seed_urlmd5,
                    "share_link": share_link,
                    'mixid': mixid,
                })
                data["content_hash"] = siphash(data["content"])
                if self.job.extra_data.get("type") == "update":
                    data["sync"] = 0
                    data["update_time"] = timestamp()

                return data

        return None

    async def handle_index(self, page):
        if self.job.extra_data.get("type") == "update":
            return await self.handle_updation(page)

        if code_str in page:
            return [], {}

        detail_infos, next_info = [], {}
        try:
            soup = bs(page, "lxml")
            pubinfos = soup.select(
                "div.news-box > ul.news-list > li > div.txt-box")
            for pubinfo in pubinfos:
                share_link = pubinfo.select('h3 > a')[0].get('data-share')
                pubtime = pubinfo.select("div.s-p")[0].get("t")
                pubtime = int(pubtime)
                title = pubinfo.select("h3 > a")[0].get_text().strip()
                publisher = pubinfo.select(
                    'div.s-p > a.account')[0].get_text().strip()
                # title, publisher, pubtime 三个字段必须均不为空
                if title and publisher and pubtime:
                    mixid = self.calc_uniqid(title, publisher, pubtime)
                    extra_data = {"pubtime": pubtime,
                                  'publisher': publisher,
                                  'title': title,
                                  'mixid': mixid,
                                  'share_link': share_link}
                    detail_infos.append({"url": share_link,
                                         'mixid': mixid,
                                         "extra_data": extra_data})
            # 解析下一页
            next_link = soup.select("div#pagebar_container > a#sogou_next")
            if next_link:
                url_next = next_link[-1].get("href")
                next_info = {
                    "url": f'https://weixin.sogou.com/weixin{url_next}'}
        except Exception as e:
            logger.error(e, exc_info=True)
        return (detail_infos, next_info)

    async def handle_updation(self, page):
        detail_pages = []
        for item in page:
            mixid, title, publisher, pubtime, share_link = item.split('\t')
            detail_pages.append({"url": share_link,
                                 "mixid": mixid,
                                 "extra_data": {
                                     **self.job.extra_data,
                                     "mixid": mixid,
                                     "title": title,
                                     "publisher": publisher,
                                     "pubtime": int(pubtime),
                                     "share_link": share_link,
                                     "key": item}})

        next_page = {"url": self.job.url, "extra_data": self.job.extra_data}
        return detail_pages, next_page

    @staticmethod
    def calc_uniqid(title, publisher, pubtime):
        """用 title publisher pubtime 来生成唯一ID
        """
        fmt = f'{title}-{publisher}-{pubtime}'
        md5sum = md5(fmt)
        return md5sum

