import re

RE_PARAS = re.compile(r"\n+")

invalid_words = ("郑重声明", "免责声明")


def clean_data(content):
    """检查content内容的合法性
    """

    if not isinstance(content, str):
        return None

    invalid = False

    paras = RE_PARAS.split(content)
    for p in paras[::-1]:
        for word in invalid_words:
            if word in p:
                invalid = True
                break

    if invalid:
        retval = []
        for p in paras:
            flag = True
            for word in invalid_words:
                if word in p:
                    flag = False
                    break
            if flag:
                retval.append(p)

        text = '\n'.join(retval)
        content = text if len(text) >= 100 else None

    # 如果 "热门搜索"出现在后100个字符中,就把 "热门搜索"及其之后的文本删除掉
    if content and "热门搜索" in content[-100:]:
        content = content[:-100] + content[-100:].split("热门搜索", 1)[0]

    return content
