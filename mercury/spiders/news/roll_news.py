import json
import re
from urllib.parse import urlparse
from mercury.models import News
from mercury.spiders.news import BaiduNewsSpider
from pyquery import PyQuery
from mercury.utils import timestamp


class RollNewsSpider(BaiduNewsSpider):
    name = 'roll_news'
    Model = News

    def __init__(self, job):
        super().__init__(job)

    async def handle_detail(self, page, encoding, extra_data=None):
        url = self.job.url

        if self.executor:
            data = await self.loop.run_in_executor(
                self.executor, self._extract_html_page, url, page, encoding)
        else:
            data = self._extract_html_page(url, page, encoding)

        if data and extra_data:
            title = extra_data.get('title')
            publisher = extra_data.get('publisher')
            pubtime = extra_data.get("pubtime", timestamp())
            data.update({
                "url": url,
                "getway": "roll_news",
                "title": title,
                "publisher": publisher,
                "pubtime": pubtime,
                "sid": self.job.seed_urlmd5
            })
            return data

        return None

    async def handle_index(self, page):
        content_type = self.job.extra_data.get("content_type")
        origin_infos = self.extract_origin_infos(content_type, page)
        origin_infos = self.filter_detail_url(origin_infos)
        detail_infos = self.generate_detail_infos(origin_infos)
        return detail_infos, {}

    def extract_origin_infos(self, content_type, page):
        urls = []
        if content_type == 'js':
            # 对于js/json需要特殊处理得到title数据
            index_parsed_url = urlparse(self.job.url)
            if index_parsed_url.netloc == 'news.people.com.cn':
                items = json.loads(page).get("items")
                urls = [{"url": item["url"], "title": item["title"]} for item in items]
            elif index_parsed_url.netloc == 'news.163.com':
                from itertools import chain
                items = chain(*(json.loads(page.split("=")[1].replace(";", '')).get("news")))
                urls = [{"url": item["l"], "title": item["t"]} for item in items]
            elif index_parsed_url.netloc == "feed.mix.sina.com.cn":
                items = json.loads(page)["result"]["data"]
                urls = [{"url": item["url"], "title": item["title"],
                         "publisher": item["media_name"]} for item in items]
        elif content_type == 'html':
            html = PyQuery(page)
            a_tags = html.find("a")
            for a in a_tags:
                a_tag = PyQuery(a)
                url = self.verify_url(a_tag.attr('href'))
                if url is None:
                    continue
                urls.append({"url": url, "title": a_tag.text()})
        return urls

    def filter_detail_url(self, origin_infos):
        for info in origin_infos:
            # 过滤网易新闻中图文新闻
            if info["url"].find("photoview"):
                origin_infos.remove(info)
        return origin_infos

    def generate_detail_infos(self, origin_infos):
        detail_infos = []
        for info in origin_infos:
            extra_data = {"publisher": info.get("publisher") or self.job.extra_data.get("publisher"),
                          "title": info["title"],
                          "pubtime": info.get("pubtime")}
            detail_infos.append({"url": info["url"], "extra_data": extra_data})
        return detail_infos

    def verify_url(self, detail_url):
        if not isinstance(detail_url, str):
            return None
        if re.search(r"(javascript|#)", detail_url):
            return None
        if detail_url == "/":
            return None
        if not detail_url:
            return None
        if detail_url.find("@") >= 0:
            return None
        detail_url = self._format_url_path(detail_url)
        if detail_url.endswith((".pdf", ".doc", ".docx", ".xls", ".xlsx")):
            return None
        if not detail_url.startswith(("http://", "https://")):
            detail_url = f"http://{detail_url}"
        if urlparse(self.job.url).netloc != urlparse(detail_url).netloc:
            return None
        return detail_url

    async def before_request(self):
        # 人民网国内版种子链接请求需要加上时间戳
        if self.job.is_seed and urlparse(self.job.url).netloc == "news.people.com.cn":
            no_parm_url = re.sub(r'\?_=[0-9]{10}', "", self.job.url)
            self.job.url = f'{no_parm_url}?_={timestamp(is_utc=False, is_int=True)}'
        return True
