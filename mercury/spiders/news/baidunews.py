# -*- coding: utf-8 -*-

import re
import lxml
import logging
from urllib.parse import urlsplit, parse_qsl
from datetime import datetime, timedelta, timezone

import newspaper
from goose3 import Goose
from goose3.text import StopWordsChinese
from bs4 import BeautifulSoup

from mercury.spiders._base import Spider as BaseSpider
from mercury.models import News
from mercury.libs.useragent import random_ua
from mercury.utils import md5, siphash
from mercury.settings import logger
from mercury.exceptions import GooseEncodeError
from mercury.monitor import cnt_extract_fail
from .util import clean_data

PUBLISHER = r'^(?P<publisher>\S+)'
YEAR_MONTH_DAY = r'^(?P<year>\d{4})年(?P<month>\d{2})月(?P<day>\d{2})日'
HOUR_MINUTE = r'^(?P<hour>\d{2}):(?P<minute>\d{2})$'
SHORT_TIME = r'^(?P<digit>\d+)(?P<hmtype>小时|分钟)前$'


PAT_PUBLISHER = re.compile(PUBLISHER)
PAT_YEAR_MONTH_DAY = re.compile(YEAR_MONTH_DAY)
PAT_HOUR_MINUTE = re.compile(HOUR_MINUTE)
PAT_SHORT_TIME = re.compile(SHORT_TIME)
PAT_TITLE_YEAR = re.compile(r"20\d{2}年")

tz_utc_8 = timezone(timedelta(hours=8))
YEAR_2000 = datetime(2000, 1, 1, tzinfo=tz_utc_8)


class Spider(BaseSpider):
    name = "baidunews"

    Model = News

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                'Accept': ('text/html,application/xhtml+xml,application/xml;'
                           'q=0.9,image/webp,image/apng,*/*;'
                           'q=0.8,application/signed-exchange;v=b3'),
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                'Cache-Control': 'no-cache',
                'Connection': 'keep-alive',
                'DNT': '1',
                'Pragma': 'no-cache',
                'Referer': '',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': random_ua(),
            },
            "cookies": {},
            "sleep_range_before_request": None,
            "uniqid": "urlmd5",
            "save_html": True,
            "need_extract_tags": ["img", "video", "audio", "table", "pre", "map", "blockquote"],
            "auto_extract": True,
            "need_replace_key": "content"
        }
        return configs

    async def before_request(self):
        if self.job.is_seed:
            self.configs["headers"]["Referer"] = self.job.url
            self.configs["headers"]["Host"] = "www.baidu.com"
        return True

    @staticmethod
    def _extract_by_newspaper(url, page):
        try:
            article = newspaper.Article(url, memoize_articles=False,
                                        language='zh', fetch_images=False)
            article.set_html(page)
            article.parse()
        except Exception as e:
            logger.error(e, exc_info=True)
        else:
            return {"title": article.title,
                    "content": article.text}

    @staticmethod
    def _extract_by_goose(page, encoding):
        g = Goose({"stopwords_class": StopWordsChinese,
                   "strict": False})
        article = None
        try:
            # goose 自行识别编码并处理
            article = g.extract(raw_html=page)
        except (UnicodeEncodeError, LookupError):
            # 若因编码出错，采用传递进来的编码值
            try:
                article = g.extract(
                    raw_html=page.encode(encoding, errors='ignore'))
            except Exception as e:
                logger.error(e, exc_info=True)
        except lxml.etree.ParserError:
            pass
        except Exception as e:
            logger.error(e, exc_info=True)

        if article and article.cleaned_text:
            return {"title": article.title,
                    "content": article.cleaned_text}

    def _extract_html_page(self, url, page, encoding):
        """extract title and content by given raw html stings
        :param str page: raw html strings
        :param str encoding: raw html char encoding
        :return dict result: main fields of news
        """
        # TODO: 深入检查 newspaper 的 parse 机制以防出现如下异常
        # concurrent.futures.process.BrokenProcessPool
        # 应该是它在解析时又派生了子进程，而ProcessPoolExecuter不支持
        data1 = self._extract_by_goose(page, encoding)
        data2 = self._extract_by_newspaper(url, page)
        if not data1:
            return data2
        if not data2:
            return data1

        return data1 if len(data1["content"]) > len(data2["content"]) else data2

    async def clean_data(self, data):
        if isinstance(data, dict) and isinstance(data.get("content"), str):
            cleaned_content = clean_data(data["content"])
            if cleaned_content:
                data["content"] = cleaned_content
                return data
        return None

    async def check_repeat(self, data):
        content = data["content"]
        content_hash = siphash(content)
        key = f"job:crawled:{self.name}:{content_hash}"
        if await self.queue.get(key):
            logger.info("[Spider]<%s> repeat page: %s",
                        self.name, content_hash)
            return True
        await self.queue.set(key, 1)
        data["content"] = content
        data["content_hash"] = content_hash
        return False

    async def handle_detail(self, page, encoding, extra_data=None):
        url = self.job.url

        if self.executor:
            data = await self.loop.run_in_executor(
                self.executor, self._extract_html_page, url, page, encoding)
        else:
            data = self._extract_html_page(url, page, encoding)

        if data and extra_data:
            title = extra_data.get('title')
            publisher = extra_data.get('publisher')
            pubtime = extra_data.get('pubtime')
            data.update({
                "url": url,
                "getway": "baidunews",
                "title": title,
                "publisher": publisher,
                "pubtime": datetime.utcfromtimestamp(pubtime),
                "sid": self.job.seed_urlmd5
            })
            data["content_hash"] = siphash(data["content"])
            return data

        return None

    async def handle_index(self, page):
        detail_infos, next_info = [], {}

        soup = BeautifulSoup(page, 'lxml')
        pages = soup.select('div.result')

        for page in pages:
            cauthor = page.select('p.c-author')[0].get_text(strip=True)
            publisher, pubtime = self._extract_pubinfo(cauthor)
            link = page.select('h3 > a')[0].get('href')
            title = page.select('h3 > a')[0].get_text(strip=True)
            # 确保title,publisher,pubtime字段不为空
            if not (title and publisher and pubtime and pubtime >= YEAR_2000):
                continue
            # 针对已经入库而且标题中包含的年份与发布日期提取的年份不一致的情况, 需要重新爬取入库.
            # 该操作通过设置isupdate=True来实现更新已入库的文章.
            # 针对未爬取过的文章, isupdate默认为False, 表示不需要更新数据库中的文章, 而是直接插入.
            isupdate = False
            ret = PAT_TITLE_YEAR.search(title)
            if ret:
                re_year = int(ret.group()[:-1])
                urlmd5 = md5(link)
                if await self.queue.get(f"mercury:crawled:baidunews:{urlmd5}"):
                    doc = await self.Model.find_one(
                        {"urlmd5": urlmd5}, {"pubtime": 1})
                    if doc and isinstance(doc.get("pubtime"), datetime):
                        year = doc["pubtime"].year
                        if re_year != year:
                            isupdate = True

            extra_data = {'title': title,
                          'publisher': publisher,
                          'pubtime': pubtime.timestamp(),
                          'isupdate': isupdate}
            detail_infos.append({'url': link, 'extra_data': extra_data})

        parse_result = urlsplit(self.job.url)
        params = dict(parse_qsl(parse_result.query))
        pn = int(params.get("pn", 100))
        if pn < 60:
            url = re.sub(f"pn={pn}", f"pn={pn+10}", self.job.url)
            next_info = {"url": url, "extra_data": self.job.extra_data}

        del soup, page
        return (detail_infos, next_info)

    @staticmethod
    def _extract_pubinfo(pubinfo):
        """
        :param pubinfo: str, contains publisher and pubtime
        :rtype (publisher, pubtime)
        """

        publisher, pubtime = None, None

        split_args = re.split(r'\s+', pubinfo)
        split_len = len(split_args)
        if split_len == 3:
            publisher = Spider._extract_publisher(split_args[0])
            pubtime = Spider._extract_time(split_args[1], split_args[2])
        elif split_len == 2:
            if ':' in split_args[-1]:
                pubtime = Spider._extract_time(split_args[0], split_args[1])
            else:
                publisher = Spider._extract_publisher(split_args[0])
                pubtime = Spider._extract_time(None, None, split_args[1])
        elif split_len == 1:
            if PAT_YEAR_MONTH_DAY.match(split_args[-1]):
                pubtime = Spider._extract_time(split_args[0])
            elif PAT_SHORT_TIME.match(split_args[-1]):
                pubtime = Spider._extract_time(None, None, split_args[-1])

        if pubtime:
            pubtime = pubtime.astimezone(tz=timezone.utc)

        return (publisher, pubtime)

    @staticmethod
    def _extract_publisher(pub_str):
        """
        extract publisher from pub_str
        """
        try:
            publisher = PAT_PUBLISHER.match(pub_str).group('publisher')
        except:
            return None
        else:
            return publisher.strip()

    @staticmethod
    def _extract_time(ymd_str, hm_str=None, st_str=None):
        """
        extract pubtime from date str
        """
        tz_utc_8 = timezone(timedelta(hours=8))
        if st_str:
            t = Spider._extract_st(st_str)
        else:
            year, month, day = Spider._extract_ymd(ymd_str)
            if hm_str:
                hour, minute = Spider._extract_hm(hm_str)
                t = datetime(year, month, day, hour, minute, tzinfo=tz_utc_8)
            else:
                t = datetime(year, month, day, tzinfo=tz_utc_8)

        return t

    @staticmethod
    def _extract_ymd(ymd_str):
        """
        extract year, month, day from ymd_str
        """
        ymd_ret = PAT_YEAR_MONTH_DAY.match(ymd_str)
        year = int(ymd_ret.group('year'))
        month = int(ymd_ret.group('month'))
        day = int(ymd_ret.group('day'))
        return (year, month, day)

    @staticmethod
    def _extract_hm(hm_str):
        """
        extract hour, minute from hm_str
        """
        hm_ret = PAT_HOUR_MINUTE.match(hm_str)
        hour = int(hm_ret.group('hour'))
        minute = int(hm_ret.group('minute'))
        return (hour, minute)

    @staticmethod
    def _extract_st(st_str):
        """
        extract short time format form st_str
        """
        tz_utc_8 = timezone(timedelta(hours=8))
        st_ret = PAT_SHORT_TIME.match(st_str)
        digit = int(st_ret.group('digit'))
        hmtype = st_ret.group('hmtype')
        if hmtype == '分钟':
            t = datetime.now(tz=tz_utc_8) - timedelta(seconds=digit * 60)
        elif hmtype == '小时':
            t = datetime.now(tz=tz_utc_8) - timedelta(hours=digit)
        return t
