import logging

import aiohttp
from aiohttp import ClientTimeout
import execjs
import requests
from pyquery import PyQuery

logger = logging.getLogger("mercury.wscourt.fetch")
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
logger.addHandler(ch)


headers = {
    'Accept': ('text/html,application/xhtml+xml,application/xml;'
               'q=0.9,image/webp,image/apng,*/*;'
               'q=0.8,application/signed-exchange;v=b3'),
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
    'DNT': '1',
    'Host': 'wenshu.court.gov.cn',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) '
                   'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/'
                   '73.0.3683.103 Safari/537.36'),
}


js_function = """
function getUrl() {
    //var atob = require('atob');
    var window = Object();
    %s
    try {
        %s
    } catch (error) {
        var atob = window.atob;
        %s
    }
    return window.location;
}
"""


domain = "http://wenshu.court.gov.cn"


def getUrl(js_code):
    try:
        compiler = execjs.compile(js_code)
        url = compiler.call("getUrl")
    except Exception as e:
        logger.info(e)
    else:
        return url


def try_parse(page):
    try:
        pq = PyQuery(page)
    except:
        return None
    else:
        return pq("#hidDocID")


def _get_ip():
    r = requests.get("http://39.104.131.254:3333/ips")
    ips = r.json()["data"]
    return ips[11]


def _get_proxies(ip):
    proxies = {"http": f"http://201808031844454498:31686474@{ip}"}
    return proxies


def fetch(docid, proxies=None):
    with requests.Session() as s:
        if proxies:
            logger.info(proxies)
            s.proxies.update(proxies)
        url_content = f"{domain}/content/content?DocID={docid}"
        r1 = s.get(url_content, headers=headers)
        cookies1 = dict(r1.cookies)
        logger.info("step 1\nrequest url\n%s\nresponse header\n%s\n",
                    url_content, r1.headers)
        if r1.status_code == 200:
            r1.encoding = "utf-8"
            headers2 = headers.copy()
            # headers2["Cookie"] = "wzws_cid=" + cookies1["wzws_cid"]
            headers2["Referer"] = url_content
            paragraphs = r1.text.splitlines()
            data_js = paragraphs[8]
            logic_js = paragraphs[10]
            js_code = js_function % (data_js, logic_js, logic_js)
            url = getUrl(js_code)
            if not url:
                logger.info("failed at get dynamic url")
                return None, None
            dynamic_url = f"{domain}{url}"
            r2 = s.get(dynamic_url, headers=headers2,
                       cookies=cookies1, allow_redirects=False)
            cookies2 = dict(r2.cookies)
            logger.info(
                "step 2\nrequest url\n%s\nresponse header\n %s\n", dynamic_url, r2.headers)
            if r2.status_code == 200:
                r2.encoding = "utf-8"
                page = r2.text
                if page.startswith("""$(function(){$("#con_llcs").html("""):
                    return page, "utf-8"
                if try_parse(page) == docid:
                    logger.info("return context page")
                logger.info("failed at step 2, status_code: %s",
                            r2.status_code)
            elif r2.status_code == 302:
                url_js = f"{domain}/CreateContentJS/CreateContentJS.aspx?DocID={docid}"
                headers2["X-Requested-With"] = "XMLHttpRequest"
                r3 = s.get(url_js, headers=headers2)
                logger.info(
                    "step 3\nrequest url\n%s\nresponse header\n%s", url_js, r3.headers)
                if r3.status_code == 200:
                    r3.encoding = "utf-8"
                    page = r3.text
                    if page.startswith("""$(function(){$("#con_llcs").html("""):
                        return page, "utf-8"
                    logger.info("failed at step 3")
            logger.info("failed at get data")
    return None, None


async def fetch_detail(docid, proxy=None, timeout=30):
    async with aiohttp.ClientSession(timeout=ClientTimeout(total=timeout)) as s:
        url_content = f"{domain}/content/content?DocID={docid}"
        r1 = await s.get(url_content, headers=headers, proxy=proxy)
        cookies1 = {k: r1.cookies[k].value for k in r1.cookies}
        logger.info("step 1\nrequest url\n%s\nresponse header\n%s\ncookies:\n%s\n",
                    url_content, dict(r1.headers), cookies1)
        if r1.status == 200:
            headers2 = headers.copy()
            # headers2["Cookie"] = "wzws_cid=" + cookies1["wzws_cid"]
            headers2["Referer"] = url_content
            text = await r1.text()
            paragraphs = text.splitlines()
            data_js = paragraphs[8]
            logic_js = paragraphs[10]
            js_code = js_function % (data_js, logic_js, logic_js)
            url = getUrl(js_code)
            if not url:
                logger.info("failed at get dynamic url")
                return None, None
            dynamic_url = f"{domain}{url}"
            r2 = await s.get(dynamic_url, headers=headers2,
                             proxy=proxy, allow_redirects=False)
            cookies2 = {k: r2.cookies[k].value for k in r2.cookies}
            logger.info(
                "step 2\nrequest url\n%s\nresponse header\n %s\ncookies:\n%s\n",
                dynamic_url, dict(r2.headers), cookies2)
            if r2.status == 200:
                page = await r2.text()
                if page.startswith("""$(function(){$("#con_llcs").html("""):
                    return page, "utf-8"
                if try_parse(page) == docid:
                    logger.info("return context page")
                logger.info("failed at step 2, status_code: %s", r2.status)
            elif r2.status == 302:
                url_js = f"{domain}/CreateContentJS/CreateContentJS.aspx?DocID={docid}"
                headers2["X-Requested-With"] = "XMLHttpRequest"
                r3 = await s.get(url_js, headers=headers2, proxy=proxy)
                logger.info(
                    "step 3\nrequest url\n%s\nresponse header\n%s", url_js, dict(r3.headers))
                if r3.status == 200:
                    page = await r3.text()
                    if page.startswith("""$(function(){$("#con_llcs").html("""):
                        return page, "utf-8"
                    logger.info("failed at step 3")
            logger.info("failed at get data")
    return None, None


if __name__ == "__main__":
    page, encoding = fetch("4a5c7734-fbb6-447b-a036-02191d3ee2b7")
    if page:
        print(page)
