"""
https://cgc.law.stanford.edu/zh-hans/guiding-cases/
"""

from pyquery import PyQuery

from mercury.settings import logger
from mercury.models import JobQueue
from mercury.utils import timestamp
from mercury.libs.useragent import random_ua
from mercury.spiders._base import Spider as BaseSpider
from mercury.models import GovCase


class GuidingCaseSpider(BaseSpider):
    name = "guiding_case"
    Model = GovCase

    domain = "https://cgc.law.stanford.edu"

    def setup_configs(self):
        configs = {
            "headers": {
                'Accept': ('text/html,application/xhtml+xml,application/xml;'
                           'q=0.9,image/webp,image/apng,*/*;'
                           'q=0.8,application/signed-exchange;v=b3'),
                'Accept-Encoding': 'gzip, deflate',
                'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                'Cache-Control': 'max-age=0',
                'DNT': '1',
                'Host': 'cgc.law.stanford.edu',
                'Proxy-Connection': 'keep-alive',
                'Referer': '',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': random_ua(),
            },
            "uniqid": "urlmd5",
            "proxy_enable": True,
        }
        return configs

    async def handle_download_failure(self, error):
        if not self.job.is_seed:
            await JobQueue.rpush(self.job)

    async def before_request(self):
        self.configs["headers"]["Referer"] = self.job.url
        if not self.job.is_seed:
            self.job.rtype = "text"
        return True

    async def handle_index(self, page):
        details, next_link = [], None
        for item in page:
            label = item["title"]
            title = item["meta"]["alternate_post_title"]
            pubtime = item["meta"].get("release_date", "")
            url = item["link"]
            detail = {"url": url, "extra_data": {
                **self.job.extra_data,
                "label": label, "title": title, "pubtime": pubtime,
                "_source": item}}
            details.append(detail)

        return details, next_link

    async def handle_detail(self, page, encoding, extra_data=None):
        pq = PyQuery(page)
        content = pq("div#abstract").text()
        title = extra_data["title"]
        label = extra_data["label"]
        pubtime = extra_data["pubtime"]
        data = {
            "url": self.job.url,
            "title": title,
            "label": label,
            "content": content,
            "pubtime": pubtime,
            "getway": self.name,
            "_source": extra_data.get("_source"),
        }

        return data
