"""http://www.court.gov.cn/fabu-gengduo-15.html
"""

import re
from concurrent.futures import ThreadPoolExecutor

import requests
import aiohttp
import execjs
from bs4 import BeautifulSoup
from aiohttp import ClientTimeout
from pyquery import PyQuery

from mercury.utils import siphash
from mercury.settings import logger, MAX_JOBS
from mercury.models import JobQueue
from mercury.libs.proxy import get_ip
from mercury.libs.useragent import random_ua
from mercury.spiders._base import Spider as BaseSpider
from mercury.models import GovCase


RE_IMG = re.compile(r"<img src=[\'\"](?P<url>\S+)[\'\"] .*?>")


class ImportantCaseSpider(BaseSpider):
    name = "important_case"
    Model = GovCase

    domain = "http://www.court.gov.cn"
    executor = ThreadPoolExecutor(max_workers=MAX_JOBS)

    def setup_configs(self):
        configs = {
            "headers": {
                'Accept': ('text/html,application/xhtml+xml,application/xml;'
                           'q=0.9,image/webp,image/apng,*/*;'
                           'q=0.8,application/signed-exchange;v=b3'),
                'Accept-Encoding': 'gzip, deflate',
                'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                'Cache-Control': 'max-age=0',
                'DNT': '1',
                'Host': 'www.court.gov.cn',
                'Proxy-Connection': 'keep-alive',
                'Referer': 'http://www.court.gov.cn/fabu-gengduo-15.html',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': random_ua(),
            },
            "uniqid": "urlmd5",
            "proxy_enable": False,
        }
        return configs

    async def handle_download_failure(self, error):
        if not self.job.is_seed:
            logger.info("[Spider]<%s> failed job %s re queue", self.name, self.job.url)
            await JobQueue.rpush(self.job)

    async def _make_http_request(self):
        ip = await get_ip()
        if not ip:
            return None, None

        logger.info("[Spider]<%s> get ip: %s", self.name, ip)

        proxy = f"http://201808031844454498:31686474@{ip}"
        proxies = {"http": proxy}
        try:
            text, encoding = await self.fetch_detail(self.job.url, proxy)
            # text, encoding = await self.loop.run_in_executor(
                # self.executor, self.fetch, self.job.url, proxies, 10)
        except:
            text, encoding = None, None
        return text, encoding

    async def check_repeat(self, data):
        content_hash = siphash(data["content"])
        key = f"job:crawled:{self.name}:{content_hash}"
        if await self.queue.get(key):
            logger.info("[Spider]<%s> repeat page: %s",
                        self.name, content_hash)
            return True
        await self.queue.set(key, 1)
        data["content_hash"] = content_hash
        return False

    async def handle_index(self, page):
        details, next_link = [], None
        pq = PyQuery(page)
        items = pq("div#container > div.sec_list > ul > li")
        for item in items:
            e = PyQuery(item)
            title = e("a").text()
            href = e("a").attr("href")
            label = e("i.date").text()
            detail = {"url": self.domain + href, "extra_data":
                      {"title": title, "label": label}}
            logger.info("[Spider]<%s> detail url: %s",
                        self.name, detail["url"])
            details.append(detail)
        pager = pq("li.next > a")
        if pager:
            href = PyQuery(pager).attr("href")
            next_link = {"url": self.domain + href}

        return details, next_link

    async def handle_detail(self, page, encoding, extra_data=None):
        pq = PyQuery(page)

        html = pq("div#container > div.detail div.txt_txt").outer_html()
        if not html:
            logger.info("[Spider]<%s> anti page: %s", self.name, page)
            return None
        anchor = pq("div#container > span.fl:last").text()
        source, pubtime = pq("div#container > div.detail ul.clearfix.fl.message > li.fl")
        source = PyQuery(source).text().lstrip("来源：").strip()
        pubtime = PyQuery(pubtime).text().lstrip("发布时间：").strip()
        editor = pq("div#container  div.txt_etr > span.fr").text().lstrip("责任编辑：").strip()
        content = self.extract_img_content(html)
        title = extra_data["title"]
        label = extra_data["label"]
        data = {
            "url": self.job.url,
            "anchor": anchor,
            "title": title,
            "label": "重大案件",
            "tag": label,
            "pubtime": pubtime,
            "source": source,
            "content": content,
            "editor": editor,
            "getway": self.name,
        }

        return data

    def extract_img_content(self, html):
        """提取正文内容中的图片信息
        """
        iters = RE_IMG.finditer(html)
        if not iters:
            text = _get_html_text(html)
            return text

        retval = []
        idx = 0
        for e in iters:
            start, end = e.span()
            text = _get_html_text(html[idx: start])
            raw = html[start: end]
            if e.groupdict().get('url') is not None:
                url = e.groupdict()['url']
                raw = html[start: end].replace(url, self.domain + url)
            idx = end
            retval.append(text)
            retval.append(raw)

        if idx < len(html):
            text = _get_html_text(html[idx:])
            retval.append(text)

        content = '\n'.join(retval)
        return content

    async def fetch_detail(self, url, proxy=None, timeout=30):
        headers = self.configs["headers"]
        async with aiohttp.ClientSession(timeout=ClientTimeout(total=timeout)) as s:
            r1 = await s.get(url, headers=headers, proxy=proxy)
            cookies1 = {k: r1.cookies[k].value for k in r1.cookies}
            logger.info("step 1\nrequest url\n%s\nresponse header\n%s\ncookies:\n%s\n",
                        url, dict(r1.headers), cookies1)
            if r1.status == 200:
                headers2 = headers.copy()
                headers2["Referer"] = url
                text = await r1.text()
                paragraphs = text.splitlines()
                data_js = paragraphs[8]
                logic_js = paragraphs[10]
                js_code = self.js_function % (data_js, logic_js, logic_js)
                url = self.getUrl(js_code)
                if not url:
                    logger.info("failed at get dynamic url")
                    return None, None
                dynamic_url = f"{self.domain}{url}"
                r2 = await s.get(dynamic_url, headers=headers2,
                                 proxy=proxy)
                cookies2 = {k: r2.cookies[k].value for k in r2.cookies}
                logger.info(
                    "step 2\nrequest url\n%s\nresponse code: %s\nresponse header\n %s\ncookies:\n%s\n",
                    dynamic_url, r2.status, dict(r2.headers), cookies2)
                if r2.status == 200:
                    page = await r2.text()
                    return page, "utf-8"
                if r2.status == 302:
                    headers2["X-Requested-With"] = "XMLHttpRequest"
                    r3 = await s.get(url, headers=headers2, proxy=proxy)
                    logger.info(
                        "step 3\nrequest url\n%s\nresponse code: %s\nresponse header\n%s", r3.status, url, dict(r3.headers))
                    if r3.status == 200:
                        page = await r3.text()
                        return page, "utf-8"
                else:
                    logger.info("failed at get data")
        return None, None

    js_function = """
        function getUrl() {
            //var atob = require('atob');
            var window = Object();
            %s
            try {
                %s
            } catch (error) {
                var atob = window.atob;
                %s
            }
            return window.location;
        }
        """

    @staticmethod
    def getUrl(js_code):
        try:
            compiler = execjs.compile(js_code)
            url = compiler.call("getUrl")
        except Exception as e:
            logger.info(e)
        else:
            return url


def _get_html_text(html):
    try:
        text = PyQuery(html.strip()).text().strip()
    except:
        try:
            text = BeautifulSoup(html.strip(), "lxml").get_text().strip()
        except:
            return html.strip()
        else:
            return text
    else:
        return text
