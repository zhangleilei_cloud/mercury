import json
import re
import aiohttp
from pyquery import PyQuery
from mercury.downloader import AsyncHttpDownloader
from mercury.libs.proxy import get_proxy
from mercury.models import JobQueue, Seed
from mercury.models.bs_case import BSCase
from mercury.spiders import BaseSpider
from mercury.utils import url_concat
from mercury.settings import logger, BS_ACCOUNT


class BSCaseSpider(BaseSpider):
    """
    把手文书爬虫
    """

    name = "bs_case"
    Model = BSCase
    _FIELDS = {"当事人": "parties",
               "原判内容": "original_judgment_content",
               "抗诉理由": "protest_reason",
               "审查经过": "review_process",
               "本院查明": "court_ascertained",
               "本院认为": "court_views",
               "尾部": "ending",
               "附录": "appendix",
               "附": "appendix",
               "检察院": "procuratorate",
               "文书号": "doc_num",
               "文书类型": "doc_type",
               "案由": "cause",
               "案例来源": "case_source",
               "落款日期": "settlement_date"
               }

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "headers": {
                "User-Agent": ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 "
                               "(KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
            },
            "uniqid": "siphash",
            "sleep_range_before_request": (1, 2),
            "save_html": False,
            "proxy_enable": True,
            "save_image": False,
            "save_file": True,
            "auto_extract": False,
        }
        return configs

    async def handle_index(self, page):
        detail_infos, index_infos = [], {}
        try:
            data = json.loads(page)
            docs = data['docs']
            # 由于随机组合各个检索条件，所以存在列表长度为0的种子，出于效率的影响，所以删除此类种子
            if not docs:
                await Seed.delete_one({"getway": "bs_case", "urlmd5": self.job.urlmd5})
            for doc in docs:
                docid = doc['id']
                page = {'url': f"http://www.lawsdata.com/detail/indictment?id={docid}"}
                detail_infos.append(page)
            payload = self.job.payload
            page_num = int(re.match(r'.*pageNo.*:.*([0-9]+).*', payload['q']).group(1))
            pageTotal = data['pageTotal']
            if page_num < pageTotal:
                payload = self.job.payload
                payload['q'] = re.sub(r'"pageNo".*:.*([0-9]+)', f'"pageNo": {page_num + 1}', payload['q'])
                next_url = url_concat(self.job.url, json.loads(payload['q']))
                index_infos.update({'url': next_url, 'payload': payload})
            return detail_infos, index_infos
        except Exception:
            return {}, []

    async def handle_detail(self, page, encoding, extra_data=None):
        try:
            docid = re.match(r'.*id=([0-9a-z]+)', self.job.url).group(1)
            html_content = PyQuery(page)
            title = html_content.find(".alert-heading").text()
            detail_content = html_content.find(".detail-content")
            sections = {}
            _filed = None
            for tag in detail_content.children():
                if tag.tag == "div":
                    field_cn = self._FIELDS[PyQuery(tag).text()]
                    _filed = field_cn
                    sections.update({field_cn: ""})
                else:
                    sections[_filed] = f"{sections[_filed]}\n{PyQuery(tag).text()}"
            basic_info = {}
            basic_msg_list = html_content.find(".basic-msg-list tr")
            for tr in basic_msg_list:
                field_cn = PyQuery(tr).find("td").text()
                if field_cn == "落款日期":
                    filed = self._FIELDS[field_cn]
                    value = PyQuery(tr).find("th").text()
                    pattern = r'(19|20)[0-9]{2}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])'
                    if not re.match(pattern, value):
                        await self.handle_detail_failure()
                        return None
                    basic_info[filed] = value
                else:
                    value = PyQuery(tr).find("th").text()
                    filed = self._FIELDS[field_cn]
                    basic_info[filed] = value
        except KeyError as e:
            logger.error("filed is not exists, please check this field and url is %s cause:%s", self.job.url, e)
            return None
        bs_case = dict(url=self.job.url, getway="bs_case",
                       title=title, sections=sections, sid=self.job.seed_urlmd5,
                       basic_info=basic_info, docid=docid)
        return bs_case

    async def before_request(self):
        # 如果是详情页job不用获取并设置cookie
        if not self.job.is_seed:
            return True
        else:
            async with aiohttp.ClientSession() as s:
                # 如果是种子则使用账号登录获取cookie才能在使用多参数检索接口
                payload = BS_ACCOUNT
                async with s.post("http://www.lawsdata.com/login", data=payload) as r:
                    if r.status == 200:
                        resp_data = await r.json()
                        if resp_data.get("returnState") == "OK":
                            cookies = {}
                            for k in r.cookies:
                                cookies[k] = r.cookies[k].value
                            self.configs['cookies'] = cookies
                            return True

    async def _make_http_request(self):
        """构造合适的HTTP请求，并下载页面
        """
        page, encoding = None, None
        if await self.before_request():
            dl = self.dl or AsyncHttpDownloader(
                headers=self.configs.get("headers"),
                cookies=self.configs.get("cookies"),
                proxy=get_proxy() if self.configs.get('proxy_enable') else None)
            page, encoding = await dl.fetch(
                self.job.url, method="post" if self.job.is_seed else "get",
                ctype="formurl" if self.job.is_seed else None, data=self.job.payload,
                rtype=self.job.rtype, auto_close=self.job.auto_close)
            return page if isinstance(page, (str, list, dict)) else dl.error, encoding
        return page, encoding

    async def handle_detail_failure(self):
        # 将抽取失败的任务重新加入任务队列
        await JobQueue.rpush(self.job)
