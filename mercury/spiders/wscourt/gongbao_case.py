"""
http://gongbao.court.gov.cn/ArticleList.html?serial_no=cpwsxd
http://gongbao.court.gov.cn/ArticleList.html?serial_no=al
"""

from pyquery import PyQuery

from mercury.utils import siphash
from mercury.settings import logger
from mercury.models import JobQueue
from mercury.utils import timestamp
from mercury.libs.useragent import random_ua
from mercury.spiders._base import Spider as BaseSpider
from mercury.models import GovCase


class GongbaoCaseSpider(BaseSpider):
    name = "gongbao_case"
    Model = GovCase

    domain = "http://gongbao.court.gov.cn"

    def setup_configs(self):
        configs = {
            "headers": {
                'Accept': ('text/html,application/xhtml+xml,application/xml;'
                           'q=0.9,image/webp,image/apng,*/*;'
                           'q=0.8,application/signed-exchange;v=b3'),
                'Accept-Encoding': 'gzip, deflate',
                'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                'Cache-Control': 'max-age=0',
                'DNT': '1',
                'Host': 'gongbao.court.gov.cn',
                'Proxy-Connection': 'keep-alive',
                'Referer': '',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': random_ua(),
            },
            "uniqid": "urlmd5",
            "save_html": True,
            "proxy_enable": True,
        }
        return configs

    async def check_repeat(self, data):
        content_hash = siphash(data["content"])
        key = f"job:crawled:{self.name}:{content_hash}"
        if await self.queue.get(key):
            logger.info("[Spider]<%s> repeat page: %s",
                        self.name, content_hash)
            return True
        await self.queue.set(key, 1)
        data["content_hash"] = content_hash
        return False

    async def handle_download_failure(self, error):
        if not self.job.is_seed:
            await JobQueue.rpush(self.job)

    async def before_request(self):
        self.configs["headers"]["Referer"] = self.job.url
        if self.job.is_seed:
            param, value = self.job.url.rsplit("&", 1)[-1].split("=")
            if param == "page" and int(value) > 1:
                self.job.method = "post"
                self.job.ctype = "formdata"
                self.job.payload = {"X-Requested-With": "XMLHttpRequest"}
        return True

    async def handle_index(self, page):
        details, next_link = [], None
        pq = PyQuery(page)
        items = pq("ul#datas > li > span")
        for item in items:
            e = PyQuery(item)
            title = e("a").text()
            href = e("a").attr("href")
            label = e("lable").text()
            detail = {"url": self.domain + href,
                      "extra_data": {**self.job.extra_data,
                                     "title": title, "label": label}}
            logger.info("[Spider]<%s> detail url: %s",
                        self.name, detail["url"])
            details.append(detail)
        pager = pq("#pager a")
        for link in pager:
            e = PyQuery(link)
            href = e.attr("href")
            text = e.text()
            if text == "下一页" and href:
                next_link = {"url": self.domain + href}
                break

        return details, next_link

    async def handle_detail(self, page, encoding, extra_data=None):
        pq = PyQuery(page)
        anchor = pq("div.fl.com_a_box > p").text()
        content = pq("#gb_content").text()
        title = extra_data["title"]
        label = extra_data["label"]
        data = {
            "url": self.job.url,
            "anchor": anchor,
            "title": title,
            "label": "公报案例",
            "tag": label,
            "content": content,
            "getway": self.name,
            "create_time": timestamp(),
        }

        return data
