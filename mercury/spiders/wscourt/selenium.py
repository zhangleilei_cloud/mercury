# -*- coding: utf-8 -*-

import re
import os
import uuid
import yaml
import random
import asyncio
from datetime import datetime, timezone, timedelta

import execjs
import demjson
from bs4 import BeautifulSoup
from pyquery import PyQuery
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from mercury.utils import md5, timestamp
from mercury.libs import s3, ruokuai
from mercury.libs.mongo import mgdb
from mercury.libs.proxy import get_proxy, get_ip
from mercury.libs.useragent import random_ua
from mercury.spiders import BaseSpider
from mercury.models import Case, JobQueue
from mercury.downloader.http import AsyncHttpDownloader
from mercury.downloader.chrome import DynamicDownloader
from mercury.settings import logger, DIR_PROJECT, RuoKuaiConf, MAX_JOBS


re_chinese = re.compile('[^\u4e00-\u9fa5]')
re_tag = re.compile('''<a type="dir" name="(?P<name>\w+)"/>''')
re_update = re.compile("Date\((?P<timestamp>\d+)\)")


class Spider(BaseSpider):
    """
    裁判文书爬虫
    """

    name = "wscourt_selenium"

    Model = Case

    # 案件类型
    casetypes_mapping = {"1": "刑事案件", "2": "民事案件",
                         "3": "行政案件", "4": "赔偿案件", "5": "执行案件"}
    # 审判人员类别
    admin_types = ('审判长', '法官助理', '人民陪审员', '陪审员',  '代理审判员',
                   '审判员', '实习书记员', '代理书记员', '代书记员', '书记员')
    # 无效的页面字符串特征
    invalid_words = ('remind', 'HtmlNotExist', 'VisitRemind', '访问验证',
                     '开启JavaScript', '刷新该页', '文书不存在', '服务不可用')

    # 乱码字符
    invalid_codes = ('�', '', '?')

    # 不合法的标题
    doc_types = ('刑事裁定书', '执行裁定书', '民事裁定书', '民事判决书',
                 '民事判决书(判决书', '民事判决书(判决书)', '判决书')

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {},
            "cookies": {},
            "sleep_range_before_request": (100, 150),
            "uniqid": "docid",
            "save_html": True,
            "proxy_enable": True,
        }
        return configs

    async def before_request(self):
        if not self.job.is_seed:
            try:
                host, port = await get_ip()
            except:
                return False

            proxy = [host, port, "201808031844454498", "31686474"]
            self.dl = DynamicDownloader(proxy=proxy)
            await self.dl.fetch("http://wenshu.court.gov.cn", "gover_search_key", should_close=False)
            if self.dl.error:
                return False
            await asyncio.sleep(1)
        return True

    async def _make_http_request(self):
        sleep_range = self.configs.get("sleep_range_before_request")
        if sleep_range:
            await asyncio.sleep(random.uniform(*sleep_range))
        if self.configs.get("cookie"):
            should_close_cookie = True
        if self.configs.get("random_ua"):
            ua = "randomly select a User-Agent from somewhere"
        page, encoding = None, None
        if await self.before_request():
            proxy = ""
            page, encoding = await self.dl.fetch(self.job.url, eid="DivContent")
        return page if isinstance(page, (str, dict)) else dl.error, encoding

    async def after_insert(self,):
        docid = self.job.url.rsplit('=', 1)[-1]
        await self.queue.srem('mercury:wscourt:uncrawled:docids', docid)

    async def handle_index(self, page):
        detail_infos = [{'url': f"http://wenshu.court.gov.cn/content/content?DocID={docid}",
                         'docid': docid,
                         'extra_data': {'isupdate': True}} for docid in page]
        next_info = {'url': self.job.url}
        return detail_infos, next_info

    async def handle_detail(self, page, encoding, extra_data=None):
        if any(map(lambda w: w in page, self.invalid_words)):
            return None

        pq = PyQuery(page)
        docid = pq("#hidDocID").val()
        title = pq("#hidCaseName").val()
        cn = pq("#hidCaseNumber").val()

        if not (docid and title and cn):
            return None

        casetype = pq("#hidCaseType").val()
        casetype = self.casetypes_mapping[casetype]
        caseinfo = pq("#hidCaseInfo").val()
        caseinfo = demjson.decode(caseinfo)
        pubdate = pq("#tdFBRQ").text().split("：")[-1]
        update = caseinfo.get("上传日期")
        update = self._update_convert(update)

        court = {
            "id": caseinfo.get("法院ID"),
            "name": caseinfo.get("法院名称"),
            "province": caseinfo.get("法院省份"),
            "city": caseinfo.get("法院地市"),
            "county": caseinfo.get("法院区县"),
            'area': caseinfo.get("法院区域"),
            'level': caseinfo.get("法院层级"),
        }
        docinfo = {
            "type": caseinfo.get("文书全文类型"),
            "preamble": caseinfo.get("文本首部段落原文"),
            "baseinfo": caseinfo.get("案件基本情况段原文"),
            "adjudication": caseinfo.get("裁判要旨段原文"),
            "litigants": caseinfo.get("诉讼参与人信息部分原文"),
            "verdict": caseinfo.get("判决结果段原文"),
            "judicialrecords": caseinfo.get("诉讼记录段原文"),
            "tail": caseinfo.get("文本尾部原文"),
            "addition": caseinfo.get("附加原文"),
        }

        html = pq("#DivContent").html()
        content = self._extract_html(html)
        if content:
            html = None

        if content and "WBWB" in content[-1]:
            wbwb = content[-1]["WBWB"]
            admins = self._extract_judgerinfo(wbwb)
        else:
            admins = {}

        retval = {
            "docid": docid,
            "cn": cn,
            "title": title,
            "docinfo": docinfo,
            "court": court,
            "casetype": self.casetypes_mapping.get(caseinfo.get("案件类型")),
            "proceedings": caseinfo.get("审判程序"),
            "doctype": caseinfo.get("文书类型"),
            "cause": None,
            "admins": admins,
            "mends": caseinfo.get("补正文书"),
            "pubdate": pubdate,
            "uploaddate": update,
            "closeway": caseinfo.get("结案方式"),
            "trialdate": caseinfo.get("裁判日期"),
            "legalbasis": None,
            "html": html,
            "content": content,
            "litigants": "当事人",
            "lawoffice": "律所",
            "lawyers": "律师",
            "nonpublicreason": caseinfo.get("不公开理由"),
            "effectlevel": caseinfo.get("效力层级"),
            "doccontent": caseinfo.get("DocContent"),
            "adminscope": None,
            "adminact": None,
            "prosecutor": None,
            "relatedoc": None,
        }
        return self.clean_doc(retval)

    def clean_doc(self, doc):
        """数据提取后对数据做进一步检查,并且修改部分字段, 如果数据不合法, 返回None
        """

        if not isinstance(doc, dict):
            return doc
        title = doc.get('title') or ''
        cn = doc.get('cn') or ''
        if title[-4:] == '.doc':
            title = title[:-4]
        elif title[-5:] == '.docx':
            title = title[:-5]
        if cn and title in self.doc_types:
            title = cn + title
        doc['title'] = title
        return doc

    @staticmethod
    def _update_convert(date_str):
        digit_ret = re_update.search(date_str)
        if digit_ret:
            digit = int(digit_ret.group("timestamp")) / 1000
            tz_utc_8 = timezone(timedelta(hours=8))
            t = datetime.fromtimestamp(digit, tz=tz_utc_8)
            return t.astimezone(tz=timezone.utc)
        return None

    @staticmethod
    def _extract_html(html):
        htmls = re_tag.split(html)[1:]
        if len(htmls) < 2:
            return []
        docs = []
        for i in range(0, len(htmls), 2):
            soup = BeautifulSoup(htmls[i + 1], "lxml")
            item = {htmls[i]: soup.get_text("\n", strip=True)}
            docs.append(item)
        return docs

    @staticmethod
    def _extract_judgerinfo(judge_str):
        admins = {}
        for line in judge_str.splitlines():
            line = re_chinese.sub('', line)
            for field in Spider.admin_types:
                if field in line:
                    admins[field] = line.replace(field, '')
                    break
        return admins
