# -*- coding: utf-8 -*-

import re
import os
import uuid
import yaml
import random
import asyncio
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime, timezone, timedelta

import execjs
import demjson
from bs4 import BeautifulSoup

from mercury.utils import md5, timestamp
from mercury.libs import s3, ruokuai
from mercury.libs.mongo import mgdb
from mercury.libs.proxy import get_proxy, get_ip
from mercury.libs.useragent import random_ua
from mercury.spiders import BaseSpider
from mercury.models import Case, JobQueue
from mercury.downloader.http import AsyncHttpDownloader
from mercury.settings import logger, DIR_PROJECT, RuoKuaiConf, MAX_JOBS
from ._ws_util import parse_content_js
from .crack.downloader import SpiderManager
from .downloader import fetch_detail

url_base = "http://wenshu.court.gov.cn"
url_cookie = url_base + "/list/list/"
url_case = url_base + "/content/content?DocID={docid}"
url_captcha_img = url_base + "/waf_captcha/?{}"
url_captcha_api = url_base + "/waf_verify.htm?captcha={captcha}"
url_ackcode = url_base + "/ValiCode/GetCode"
url_indexapi = url_base + "/List/ListContent"
url_relatedocs = url_base + "/Content/GetDocRelationAndFuse"
url_detailapi = url_base + \
    "/CreateContentJS/CreateContentJS.aspx?DocID={docid}"

re_chinese = re.compile('[^\u4e00-\u9fa5]')
re_index = re.compile("Index=(?P<Index>(\d+))")
re_wenshu = re.compile("""<a type='dir' name='\w+'></a>""")
re_tag = re.compile("""<a type='dir' name='(?P<name>\w+)'></a>""")
re_update = re.compile("Date\((?P<timestamp>\d+)\)")


FIELDS = {
    'docid': '文书ID',
    'title': '案件名称',
    'court': {
        'id': '法院ID',
        'name': '法院名称',
        'nation': '法院国家',
        'province': '法院省份',
        'county': '法院区县',
        'area': '法院区域',
        'level': '法院层级',
    },
    'casetype': '案件类型',
    'proceedings': '审理程序',
    'doctype': '文书类型',
    'cn': '案号',
    'cause': '案由',
    'admins': '行政人员',
    'mends': '补正文书',
    'pubdate': '发布日期',
    'uploaddate': '上传日期',
    'closeway': '结案方式',
    'trialdate': '裁判日期',
    'legalbasis': '法律依据',
    'html': '裁判文书Html',
    'content': '裁判文书全文',
    'docinfo': {
        'type': '文书全文类型',
        'preamble': '文本首部段落原文',
        'baseinfo': '案件基本情况段原文',
        'adjudication': '裁判要旨段原文',
        'litigants': '诉讼参与人信息部分原文',  # ???
        'verdict': '判决结果段原文',
        'judicialrecords': '诉讼记录段原文',
        'tail': '文本尾部原文',
        'addition': '附加原文',
    },
    'litigants': '当事人',  # ???
    'lawoffice': '律所',
    'lawyers': '律师',
    'nonpublicreason': '不公开理由',
    'effectlevel': '效力层级',
    'doccontent': 'DocContent',  # 一般不存在
    'adminscope': '行政管理范围',  # 一般不存在
    'adminact': '行政行为种类',  # 一般不存在
    'prosecutor': '公诉机关',  # 一般不存在 public prosecution organ
    'relatedoc': '关联文书',
}


def _load_captcha():
    captcha_path = os.path.join(
        DIR_PROJECT, 'mercury/spiders/wscourt/captcha.yml')
    with open(captcha_path, 'r', encoding='utf-8') as f:
        return yaml.load(f)


def _compile_jscode():
    """ compile js code
    """
    js_path = os.path.join(DIR_PROJECT, 'mercury/spiders/wscourt/case.js')
    with open(js_path, mode='r', encoding='utf-8') as f:
        js_code = f.read()
    js_compiler = execjs.compile(js_code)
    return js_compiler


class Spider(BaseSpider):
    """
    裁判文书爬虫
    """

    executor = ThreadPoolExecutor(max_workers=MAX_JOBS)

    name = "wscourt"

    Model = Case

    captcha_identifer = _load_captcha()
    js_executor = _compile_jscode()

    # 案件类型
    casetypes_mapping = {"1": "刑事案件", "2": "民事案件",
                         "3": "行政案件", "4": "赔偿案件", "5": "执行案件"}
    # 审判人员类别
    admin_types = ('审判长', '法官助理', '人民陪审员', '陪审员',  '代理审判员',
                   '审判员', '实习书记员', '代理书记员', '代书记员', '书记员')
    # 无效的页面字符串特征
    invalid_words = ('remind', 'HtmlNotExist', 'VisitRemind', '访问验证',
                     '开启JavaScript', '刷新该页', '文书不存在', '服务不可用')

    # 乱码字符
    invalid_codes = ('�', '', '??')

    # 不合法的标题
    doc_types = ('刑事裁定书', '执行裁定书', '民事裁定书', '民事判决书',
                 '民事判决书(判决书', '民事判决书(判决书)', '判决书')

    _hans_digit_mappings = {
        "〇": 0, "一": 1, "二": 2, "三": 3, "四": 4, "五": 5, "六": 6, "七": 7,
        "八": 8, "九": 9, "O": 0, "0": 0, "零": 0,
    }

    _hans_day_mappings = {
        "〇": 0, "一": 1, "二": 2, "三": 3, "四": 4, "五": 5, "六": 6, "七": 7,
        "八": 8, "九": 9, "十": 10, "十一": 11, "十二": 12, "十三": 13,
        "十四": 14, "十五": 15, "十六": 16, "十七": 17, "十八": 18, "十九": 19,
        "二十": 20, "二十一": 21, "二十二": 22, "二十三": 23, "二十四": 24,
        "二十五": 25, "二十六": 26, "二十七": 27, "二十八": 28, "二十九": 29,
        "三十": 30, "三十一": 31}

    _hans_month_mappings = {
        "一": 1, "二": 2, "三": 3, "四": 4, "五": 5, "六": 6, "七": 7, "八": 8,
        "九": 9, "十": 10, "十一": 11, "十二": 12
    }

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "Accept": "text/javascript, application/javascript, */*",
                "Accept-Encoding": "gzip, deflate",
                "Accept-Language": "zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7",
                "Host": "wenshu.court.gov.cn",
                "User-Agent": random_ua(),
            },
            "cookies": {},
            "sleep_range_before_request": None,
            "uniqid": "docid",
            "save_html": True,
            "proxy_enable": True
        }
        return configs

    async def after_insert2(self):
        # 将成功插入的文书ID从集合中删除
        docid = self.job.url.rsplit('=', 1)[-1]
        await self.queue.srem('mercury:wscourt:not:exists:docids', docid)
        if docid.lower() == docid:
            docid = docid.upper()
        else:
            docid = docid.lower()
        await self.queue.srem('mercury:wscourt:not:exists:docids', docid)

    async def after_insert(self):
        # 将成功插入的文书ID从集合中删除
        docid = self.job.url.rsplit('=', 1)[-1]
        await self.queue.srem('mercury:wscourt:uncrawled:docids', docid)

    async def before_request(self):
        if not self.job.is_seed:
            docid = self.job.url.rsplit('=', 1)[-1]
            self.configs['headers']['Referer'] = url_case.format(docid=docid)
        return True

    async def handle_download_failure(self, error):
        # 将下载失败的任务重新加入任务队列
        # await JobQueue.rpush(self.job)
        pass

    async def handle_extract_failure1(self, page):
        if "文书不存在" in page or "HtmlNotExist" in page:
            docid = self.job.url.rsplit('=', 1)[-1]
            await self.queue.sadd('mercury:wscourt:not:exists:docids', docid)
            # 从原下载集合中清除
            await self.queue.srem('mercury:wscourt:uncrawled:docids', docid)

    async def handle_extract_failure2(self, page):
        if "文书不存在" in page or "HtmlNotExist" in page:
            docid = self.job.url.rsplit('=', 1)[-1]
            if docid.lower() == docid:
                docid = docid.upper()
            else:
                docid = docid.lower()
            if not self.queue.sismember("mercury:wscourt:not:exists:docids", docid):
                url = url_detailapi.format(docid=docid)
                self.job.url = url
                self.job.urlmd5 = docid
                self.job.uniqid = docid
                await JobQueue.rpush(self.job)

    async def _make_http_request(self):
        ip = await get_ip()
        if not ip:
            return None, None

        logger.info("[Spider]<wscourt> get ip: %s", ip)
        host, port = ip.split(":")
        # proxy = f"http://{ip}"
        # auth = ("201808031844454498", "31686474")
        # proxies = {"http": f"http://201808031844454498:31686474@{ip}"}
        # dl = SpiderManager(debug=True, timeout=30, proxy=proxy, auth=auth)
        docid = self.job.url.split("=")[-1]
        # text, encoding = await dl.fetch(docid)
        proxy = f"http://201808031844454498:31686474@{ip}"
        try:
            text, encoding = await fetch_detail(docid, proxy)
        except:
            text, encoding = None, None
        return text, encoding

    async def handle_index(self, page):
        """
        handle list page
        """
        detail_infos = [{'url': url_detailapi.format(docid=docid),
                         'docid': docid,
                         'extra_data': {'isupdate': True}} for docid in page]
        next_info = {'url': self.job.url}
        return detail_infos, next_info

    async def handle_detail(self, page, encoding, extra_data=None):
        """
        handle detail page
        """
        retval = None

        # 下载失败或者触发反爬                                  # 页面出现验证码
        if any(map(lambda w: w in page, self.invalid_words)):
            # await JobQueue.rpush(self.job)
            logger.info(page)
            return retval

        infos = self._parse_content(page)
        if infos:
            # 关联文书的抓取暂时停止
            # relate_doc = await self.handle_relate_doc(case_info)
            relate_doc = None
            retval = await self._extract_detail(*infos, relate_doc)
            retval = self.clean_doc(retval)
            if not retval:
                logger.info(page)
        return retval

    async def handle_relate_doc(self, case_info):
        """下载关联文书"""
        payload = {
            'docid': case_info['文书ID'],
            'court': case_info['法院名称'],
            'caseNumber': case_info['案号'],
            'caseType': case_info['案件类型']
        }
        # 关联文书
        relate_doc = None
        dl = AsyncHttpDownloader(timeout=15, headers=self.configs['headers'])
        relate_src, _ = await dl.fetch(
            url_relatedocs, method='post',
            ctype='formdata', data=payload, rtype='json')
        # 返回的json格式是字符串的字符串, 所以需要再次decode
        relatedocs = self.decode_json(relate_src)
        # 关联文书获取失败, 重新发起一次请求
        if not relatedocs:
            dl = AsyncHttpDownloader(
                timeout=15, headers=self.configs['headers'])
            relate_src, _ = await dl.fetch(
                url_relatedocs, method='post',
                ctype='formdata', data=payload, rtype='json')
            relatedocs = self.decode_json(relate_src)
        if relatedocs:
            relate_doc = self._extract_relatedoc(relatedocs)
        return relate_doc

    async def handle_captcha(self):
        """
        handle captcha image
        """
        logger.info('[Spider]<{}> handle captcha'.format(self.name))
        page = None
        img_url = url_captcha_img.format(str(random.random()))
        img, _ = await self.dl.fetch(img_url, rtype='read', auto_close=False)
        if not img:
            logger.info(
                '[Spider]<{}> get captcha image failed'.format(self.name))
            return page

        captcha = self.captcha_identifer.get(md5(img))
        # 图片验证码不在哈希表中
        if not captcha:
            # 使用若快提供的验证码接口识别
            result = await ruokuai.identify(img, RuoKuaiConf.FOUR_DIGIT_LETTER)
            # 若快识别错误
            if result.get('Error'):
                await ruokuai.report_error(result['id'])
                logger.error('[Spider]<{}> ruokuai error: {}'.format(
                    self.name, result['Error']))
            # 若快识别成功
            else:
                captcha = result.get('Result')
                # 将新的验证码图片存到s3上
                await s3.upload_file(img, 'captcha/court_gov_cn/{guid}_{captcha}.jpg'.format(
                    guid=result['id'], captcha=captcha))
                logger.info(
                    '[Spider]<{}> new captcha: {}'.format(self.name, captcha))

        if captcha:
            # 提交验证码识别结果
            verify_url = url_captcha_api.format(captcha=captcha)
            page, _ = await self.dl.fetch(verify_url, auto_close=False)
        logger.info('[Spider]<{}> handle captcha {}'.format(
            self.name, 'succeed' if page and self.dl.url == self.job.url else 'failed'))
        return page

    def _fields_convert(self, case):
        """convert case keys from chinese to english"""
        url = url_case.format(docid=case["文书ID"])
        retval = {
            'url': url,
            'sid': md5(url),
            'getway': self.name,
        }
        # 中英字段转换
        retval.update({k: case.get(v if isinstance(v, str) else k)
                       for (k, v) in FIELDS.items()})
        return retval

    async def check_invalid_codes(self, content):
        """将乱码的文书ID记录到Redis中
        """

        has_invalid_code = False
        for code in self.invalid_codes:
            if code in content:
                docid = self.job.url.rsplit('=', 1)[-1]
                await self.queue.sadd('mercury:wscourt:extract:failed:docids', docid)
                await self.queue.srem('mercury:wscourt:uncrawled:docids', docid)
                has_invalid_code = True
                break
        return has_invalid_code

    async def _extract_detail(self, meta_info, related_info, html_info, relate_doc=None):
        """
        extract detail page info
        """

        meta_info["案件类型"] = self._extract_casetype(meta_info)
        meta_info["上传日期"] = self._update_convert(meta_info["上传日期"])
        meta_info["审理程序"] = meta_info.pop("审判程序", None)

        # process relate info
        relate_info = {}
        for item in related_info.get("RelateInfo", []):
            relate_info[item["name"]] = item["value"]
        # appellor
        if "当事人" in relate_info:
            relate_info["当事人"] = list(
                map(str.strip, relate_info["当事人"].split(",")))
        else:
            relate_info["当事人"] = []

        # process legalbase info
        legal_base = related_info.pop("LegalBase", [])
        for dicitem in legal_base:
            dicitem["legalname"] = dicitem.pop("法规名称", None)
            dicitem['legalbasis'] = dicitem.pop('Items', [])
            for item in dicitem["legalbasis"]:
                item["legalspec"] = item.pop("法条名称", None)
                item["legalcontent"] = item.pop("法条内容", None)

        # 检查文书的正文内容  检查文书的标题
        if await self.check_invalid_codes(html_info['Html'] + html_info['Title']):
            return None

        # process doc info
        html_info["Html"] = html_info["Html"].replace("01lydyh01", "\'")
        html_info["content"] = self._extract_html(html_info["Html"])
        if html_info["content"] and "WBWB" in html_info["content"][-1]:
            wbwb = html_info["content"][-1]["WBWB"]
            admins = self._extract_judgerinfo(wbwb)
            trail_date_string = self._search_trail_date(wbwb)
            if trail_date_string:
                trail_date = self._convert_hans_date(trail_date_string)
                if trail_date:
                    meta_info["裁判日期"] = trail_date
        else:
            admins = {}

        case = meta_info
        case["发布日期"] = html_info.pop("PubDate", None)
        case["案件标题"] = html_info.pop("Title", None)
        case['行政人员'] = admins
        case["裁判文书全文"] = html_info.pop("content")
        # 如果裁判文书全文没有内容, 则存储裁判文书的原始Html
        if not case["裁判文书全文"]:
            case['裁判文书Html'] = html_info.pop('Html')
        case["法律依据"] = legal_base
        case['关联文书'] = relate_doc
        case.update(relate_info)

        case['court'] = {
            'id': case.pop('法院ID', None),
            'name': case.pop('法院名称', None),
            'nation': case.pop('法院国家', None),
            'province': case.pop('法院省份', None),
            'county': case.pop('法院区县', None),
            'area': case.pop('法院区域', None),
            'level': case.pop('法院层级', None)
        }
        case['docinfo'] = {
            'type': case.pop('文书全文类型', None),
            'preamble': case.pop('文本首部段落原文', None),
            'baseinfo': case.pop('案件基本情况段原文', None),
            'adjudication': case.pop('裁判要旨段原文', None),
            'litigants': case.pop('诉讼参与人信息部分原文', None),  # ???
            'verdict': case.pop('判决结果段原文', None),
            'judicialrecords': case.pop('诉讼记录段原文', None),
            'tail': case.pop('文本尾部原文', None),
            'addition': case.pop('附加原文', None)
        }
        ret = self._fields_convert(case)
        if not ret.get('docid'):
            return None
        if ret:
            ret['sync'] = 0
            if not await self.queue.get("job:crawled:{0}:{1}".format(self.name, ret["docid"])):
                ret['create_time'] = timestamp()
            ret['update_time'] = timestamp()
        return ret

    def clean_doc(self, doc):
        """数据提取后对数据做进一步检查,并且修改部分字段, 如果数据不合法, 返回None
        """
        if not isinstance(doc, dict):
            return doc
        title = doc.get('title') or ''
        cn = doc.get('cn') or ''
        if title[-4:] == '.doc':
            title = title[:-4]
        elif title[-5:] == '.docx':
            title = title[:-5]
        if cn and (title in self.doc_types):
            title = cn + title
        doc['title'] = title
        return doc

    def _extract_casetype(self, case):
        # get case type chinese discription, it's maybe a string of the number
        ty1 = case.get('案件类型')
        # if string number, then, get chinese edition by itself
        ty2 = self.casetypes_mapping.get(ty1)
        return ty2 or ty1

    @staticmethod
    def _extract_relatedoc(relate_doc):
        # 关联文书字段转换
        ret = []
        for item in relate_doc['RelateFile']['RelateFile']:
            doc = {}
            doc['docid'] = item.pop('文书ID')
            doc['doctype'] = item.pop('Type')
            doc['proceedings'] = item.pop('审判程序')
            doc['court'] = item.pop('审理法院')
            doc['cn'] = item.pop('案号')
            doc['closeway'] = item.pop('结案方式')
            doc['trialdate'] = item.pop('裁判日期')
            ret.append(doc)
        return ret

    @staticmethod
    def _update_convert(date_str):
        digit_ret = re_update.search(date_str)
        if digit_ret:
            digit = int(digit_ret.group("timestamp")) / 1000
            tz_utc_8 = timezone(timedelta(hours=8))
            t = datetime.fromtimestamp(digit, tz=tz_utc_8)
            return t.astimezone(tz=timezone.utc)
        return None

    @staticmethod
    def _extract_judgerinfo(judge_str):
        admins = {}
        for line in judge_str.splitlines():
            line = re_chinese.sub('', line)
            for field in Spider.admin_types:
                if field in line:
                    admins[field] = line.replace(field, '')
                    break
        return admins

    @staticmethod
    def _extract_html(html):
        docs = []
        htmls = re_tag.split(html)[1:]
        for i in range(0, len(htmls), 2):
            soup = BeautifulSoup(htmls[i + 1], "lxml")
            item = {htmls[i]: soup.get_text("\n", strip=True)}
            docs.append(item)
        return docs

    @staticmethod
    def _parse_content(page):
        """解析文书网返回的JS详情页数据，包含 案件元信息、相关信息、案件详情
        三大部分，若某部分内容解析失误则停止处理

        eg: http://wenshu.court.gov.cn/CreateContentJS/CreateContentJS.aspx?DocID=d8952be5-e5a2-4b8b-b554-cccf5824617f

        :param page: wenshu.court.gov.cn 网站返回的创建文书内容的JS代码

        :return case_info, relate_info, html_info: 返回JS代码中携带的文书相关信息
        """

        # 返回内容可能含非法换行符
        page = page.replace('\\n', '\n').replace(
            '\n', '\\n').replace('01lydyh01', '\'')
        case_start, case_stop = ('caseinfo=JSON.stringify(', ');$(document')
        relate_start, relate_stop = ('dirData = ', ';if ($("#divTool_Summary')
        html_start, html_stop = ('jsonHtmlData = ', '}";')

        case_info, next_index = parse_content_js(
            page, case_start, case_stop)
        if not isinstance(case_info, dict):
            return
        # 关联信息为空
        if page.find(relate_start) == -1 or page.find(relate_stop) == -1:
            relate_info = {}
        else:
            relate_info, next_index = parse_content_js(
                page, relate_start, relate_stop, next_index)
            if not isinstance(relate_info, dict):
                return
        # jsonHtmlData 的结束标记应包含在有用范围内，故把 '}"' 这两个字符的长度填补上
        html_info, next_index = parse_content_js(
            page, html_start, html_stop, next_index, 2)
        if not isinstance(html_info, dict):
            return
        return case_info, relate_info, html_info

    @staticmethod
    def gen_code():
        uid = str(uuid.uuid4())
        return uid[:18] + uid[19:]

    @staticmethod
    def decode_json(input):
        if not isinstance(input, str):
            return input
        retval = None
        try:
            retval = demjson.decode(input)
        except (demjson.JSONDecodeError, Exception) as e:
            logger.error(
                '[Spider]<{}> decode_json failed, input: {}'.format(self.name, input))
        finally:
            return retval

    def _search_trail_date(self, text):
        r = re.search("(?P<date_string>\w{4}年\w{1,2}月\w{1,3}日)", text)
        return r.group() if r else ""

    def _convert_hans_date(self, date_string):
        """二〇一四年十一月二十八日 => 2014-11-28
        """

        retval = ''

        parts = re.split("[年月日]", date_string)
        if len(parts) == 4:
            try:
                year = self._parse_hans_year(parts[0])
                month = self._parse_hans_month(parts[1])
                day = self._parse_hans_day(parts[2])
            except:
                pass
            else:
                return '%4d-%02d-%02d' % (year, month, day)

        return retval

    @classmethod
    def _parse_hans_year(cls, year_string):
        year = 0
        for digit in year_string:
            year = year * 10 + cls._hans_digit_mappings[digit]
        return year

    @classmethod
    def _parse_hans_month(cls, month_string):
        month = cls._hans_month_mappings[month_string]
        return month

    @classmethod
    def _parse_hans_day(cls, day_string):
        day = cls._hans_day_mappings[day_string]
        return day
