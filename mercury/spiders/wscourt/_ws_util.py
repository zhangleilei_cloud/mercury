# -*- coding: utf-8 -*-

from mercury.settings import logger
from mercury.utils import fast_json_decode


def parse_content_js(content, start, stop, next_index=0, fix=0):
    """根据文书网返回的详情以及待提取内容的 (开始, 结束) 字符串提取相关内容
    """
    begin = content.find(start, next_index) + len(start)
    end = content.find(stop, begin) + fix
    ret = None
    try:
        ret = fast_json_decode(content[begin:end])
    except Exception as e:
        logger.error(e, exc_info=True)
        logger.info("[%s], (%s, %s)", content, begin, end)
    if isinstance(ret, str):
        try:
            ret = fast_json_decode(ret)
        except Exception as e:
            logger.error(e, exc_info=True)
            logger.info("[%s], (%s, %s)", content, begin, end)
    return ret, end


if __name__ == '__main__':
    import requests
    headers = {"User-Agent": ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) "
                              "AppleWebKit/537.36 (KHTML, like Gecko Chrome/67."
                              "0.3396.87 Safari/537.36")}
    url = 'http://wenshu.court.gov.cn/CreateContentJS/CreateContentJS.aspx?DocID=d8952be5-e5a2-4b8b-b554-cccf5824617f'

    resp = requests.get(url, headers=headers)
    ret = parse_content_js(resp.text)
    print(ret)
