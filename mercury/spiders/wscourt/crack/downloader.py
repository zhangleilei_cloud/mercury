#!/usr/bin/env python
# coding=utf-8

import os
import re
import time
import json
import random
from lxml import etree
from urllib import parse

import execjs
import requests
from aiohttp import BasicAuth

from mercury.settings import logger
from mercury.downloader.http import AsyncHttpDownloader

folder = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(folder, 'encrypt.js'), 'r', encoding="utf-8") as f:
    js1 = f.read()
    ctx1 = execjs.compile(js1)
with open(os.path.join(folder, 'ywtu.js'), 'r', encoding="utf-8") as f:
    js2 = f.read()
    ctx2 = execjs.compile(js2)
with open(os.path.join(folder, 'vl5x.js'), 'r', encoding="utf-8") as fp:
    js = fp.read()
    ctx = execjs.compile(js)
with open(os.path.join(folder, 'get_docid.js'), 'r', encoding="utf-8") as fp:
    js = fp.read()
    get_docid = execjs.compile(js)


class SpiderManager(object):
    def __init__(self, debug=False, proxy=None, auth=None, timeout=30):
        self.proxy = proxy
        self.proxy_auth = BasicAuth(auth[0], auth[1]) if auth else None
        self.timeout = timeout

        self.f80t = ""
        self.f80t_n = ""
        self.meta = ""
        self.f80s = ""
        self.ywtu = ""
        self.vjkl5 = ""
        self.list = ""
        self.debug = debug
        self.conditions = ""
        self.url = "http://wenshu.court.gov.cn/List/List?sorttype=1&conditions={}"
        self.url_for_content = "http://wenshu.court.gov.cn/List/ListContent"
        self.headers = {
            "Accept": "*/*",
            "Accept-Encoding": "gzip, deflate",
            "Accept-Language": "zh-CN,zh;q=0.9",
            "Connection": "keep-alive",
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
            "Host": "wenshu.court.gov.cn",
            "Origin": "http://wenshu.court.gov.cn",
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) "
                          "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"
        }
        self.cookies = {
            "ccpassport": "1ff98c661b8f424096c234ce889da9b0",
            "_gscu_2116842793": "47626758817stt18",
            "_gscs_2116842793": "47659453ttzz3o20|pv:14",
            "_gscbrs_2116842793": "1",
            "wzwsconfirm": "0e561c10c60c2f0d44410644eb3c2403",
            "wzwstemplate": "NQ==",
            "wzwschallenge": "-1",
            "wzwsvtime": ""
        }
        self.data = {
            "Param": "案件类型:民事案件",
            "Index": "",
            "Page": "20",
            "Order": "法院层级",
            "Direction": "asc",
            "vl5x": "",
            "number": "wens",
            "guid": ""
        }

    def setconditions(self, conditions: str):
        self.conditions = conditions

    async def init(self):
        self.f80t = ""
        self.f80t_n = ""
        self.meta = ""
        self.f80s = ""
        self.ywtu = ""
        self.vjkl5 = ""
        if not self.conditions:
            if self.debug:
                logger.info("条件不能为空")
                return False
        request_url = self.url.format(self.conditions)
        headers = self.headers
        cookies = self.cookies
        cookies['wzwsvtime'] = str(int(time.time()))

        dl = AsyncHttpDownloader(
            timeout=self.timeout, headers=headers, cookies=cookies, proxy=self.proxy)
        text, _ = await dl.fetch(request_url, proxy_auth=self.proxy_auth)
        if dl.error:
            logger.info("发起请求失败")
            return False

        try:
            self.f80s = dl.cookies['FSSBBIl1UgzbN7N80S'].value
            self.f80t = dl.cookies['FSSBBIl1UgzbN7N80T'].value
            html = etree.HTML(text)
            self.meta = html.xpath(
                '//*[@id="9DhefwqGPrzGxEp9hPaoag"]/@content')[0]
            self.ywtu = ctx2.call("getc", self.meta)
        except (AttributeError, KeyError, ValueError):
            logger.info("Cookie获取失败")
            return False

        return True

    async def getvjkl5(self):
        request_url = self.url.format(self.conditions)
        headers = self.headers
        cookies = self.cookies
        cookies['wzwsvtime'] = str(int(time.time()))
        # self.ywtu = ctx2.call("getc", self.meta)
        self.f80t_n = ctx1.call("getCookies", self.meta, self.f80t, self.ywtu)

        cookies['FSSBBIl1UgzbN7Nenable'] = "true"
        cookies['FSSBBIl1UgzbN7N80S'] = self.f80s
        cookies['FSSBBIl1UgzbN7N80T'] = self.f80t_n
        cookies['wzwsvtime'] = str(int(time.time()))
        dl = AsyncHttpDownloader(
            timeout=self.timeout, headers=headers, cookies=cookies, proxy=self.proxy)
        dl.fetch(request_url, proxy_auth=self.proxy_auth)
        if dl.error:
            logger.info("获取vjkl5失败:网络连接出错")
            return False

        if dl.status == 200 and "vjkl5" in dl.cookies:
            self.vjkl5 = dl.cookies['vjkl5'].value
            return True

        logger.info("获取vjkl5失败")
        return False

    def get_vl5x(self):
        """
        根据vjkl5获取参数vl5x
        """
        vl5x = (ctx.call('getKey', self.vjkl5))
        return vl5x

    def createGuid(self):
        return str(hex((int(((1 + random.random()) * 0x10000)) | 0)))[3:]

    def getguid(self):
        return '{}{}-{}-{}{}-{}{}{}' \
            .format(
                self.createGuid(), self.createGuid(),
                self.createGuid(), self.createGuid(),
                self.createGuid(), self.createGuid(),
                self.createGuid(), self.createGuid()
            )

    async def getList(self, page):
        url = self.url_for_content
        self.f80t_n = ctx1.call("getCookies", self.meta, self.f80t, self.ywtu)
        vl5x = self.get_vl5x()
        data = self.data
        data['Index'] = str(page)
        data['vl5x'] = vl5x
        data['guid'] = self.getguid()
        cookies = self.cookies
        cookies['wzwsvtime'] = str(int(time.time()))
        cookies['FSSBBIl1UgzbN7Nenable'] = "true"
        cookies['FSSBBIl1UgzbN7N80S'] = self.f80s
        cookies['FSSBBIl1UgzbN7N80T'] = self.f80t_n
        cookies['vjkl5'] = self.vjkl5
        headers = self.headers
        headers['Referer'] = self.url.format(parse.quote(self.conditions))
        dl = AsyncHttpDownloader(
            timeout=self.timeout, headers=headers, cookies=cookies, proxy=self.proxy)
        text, _ = await dl.fetch(url, method="post", data=data, proxy_auth=self.proxy_auth)
        if dl.error:
            logger.info("获取列表页时网络请求出错")
            return False

        if dl.status == 200 and "验证码" not in text:
            self.list = text
            return True

        logger.info("获取列表页出错,code:%s,若code为200，可能出现了验证码", dl.status)
        return False

    def getListData(self):
        return self.list

    def getDocIds(self):
        list_json = json.loads(json.loads(self.list))
        if len(list_json) > 1:
            runeval = list_json[0]['RunEval']
            docids = []
            for item in list_json[1:]:
                docid = item['文书ID']
                js = get_docid.call("GetJs", runeval)
                js_objs = js.split(";;")
                js1 = js_objs[0] + ';'
                js2 = re.findall(r"_\[_\]\[_\]\((.*?)\)\(\);", js_objs[1])[0]
                key = get_docid.call("EvalKey", js1, js2)
                key = re.findall(r"\"([0-9a-z]{32})\"", key)[0]
                docid = get_docid.call("DecryptDocID", key, docid)
                docids.append(docid)
            return docids
        else:
            return False

    async def getContent(self, docid):
        url = f"http://wenshu.court.gov.cn/CreateContentJS/CreateContentJS.aspx?DocID={docid}"
        try:
            self.f80t_n = ctx1.call("getCookies", self.meta, self.f80t, self.ywtu)
        except:
            logger.info("获取cookie值时失败")
            return None
        cookies = self.cookies
        cookies['wzwsvtime'] = str(int(time.time()))
        cookies['FSSBBIl1UgzbN7Nenable'] = "true"
        cookies['FSSBBIl1UgzbN7N80S'] = self.f80s
        cookies['FSSBBIl1UgzbN7N80T'] = self.f80t_n
        cookies['vjkl5'] = self.vjkl5
        headers = self.headers
        headers['Referer'] = self.url.format(parse.quote(self.conditions))
        dl = AsyncHttpDownloader(
            timeout=self.timeout, headers=headers, cookies=cookies, proxy=self.proxy)
        text, _ = await dl.fetch(url, proxy_auth=self.proxy_auth)
        if dl.error:
            logger.info("获取内容时网络请求出错")

        if dl.status == 200 and "验证码" not in text:
            return text

        logger.info("获取内容出错,code:{},若code为200，可能出现了验证码".format(dl.status))
        return None

    async def fetch(self, docid):
        logger.info("[Spider]<wscourt> fetch: %s", docid)
        self.setconditions("searchWord+2+AJLX++案件类型:民事案件")
        ok = await self.init()
        if ok:
            content = await self.getContent(docid)
            if content:
                return content, "utf-8"
        return None, None


if __name__ == '__main__':
    # # 实例化并开启调试模式，会返回报错信息
    # for i in range(10):

    spider = SpiderManager(debug=True)
    # 设置采集条件
    spider.setconditions("searchWord+2+AJLX++案件类型:民事案件")
    # 初始化
    init_status = spider.init()
    if init_status:
        spider.getContent("4a5c7734-fbb6-447b-a036-02191d3ee2b7")
