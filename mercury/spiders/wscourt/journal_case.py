"""
http://www.spp.gov.cn/spp/jczdal/index.shtml
http://www.spp.gov.cn/gjgb/
"""
import re

from pyquery import PyQuery

from mercury.utils import siphash
from mercury.settings import logger
from mercury.libs.useragent import random_ua
from mercury.spiders._base import Spider as BaseSpider
from mercury.models import GovCase


RE_CASE = re.compile(".+案\n")
RE_CASENO = re.compile("（检例第\d+号）\n")
RE_LABEL = re.compile("【[\u4e00-\u9fa5]+】\n")


class JournalCaseSpider(BaseSpider):
    name = "journal_case"
    Model = GovCase

    domain = "http://www.spp.gov.cn"

    def setup_configs(self):
        configs = {
            "headers": {
                'Accept': ('text/html,application/xhtml+xml,application/xml;'
                           'q=0.9,image/webp,image/apng,*/*;'
                           'q=0.8,application/signed-exchange;v=b3'),
                'Accept-Encoding': 'gzip, deflate',
                'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                'Cache-Control': 'no-cache',
                'Connection': 'keep-alive',
                'DNT': '1',
                'Host': 'www.spp.gov.cn',
                'Pragma': 'no-cache',
                'Upgrade-Insecure-Requests': '1',
                "User-Agent": random_ua(),
            },
            "uniqid": "urlmd5",
            "save_html": True,
        }
        return configs

    async def check_repeat(self, data):
        content_hash = siphash(data["content"])
        key = f"job:crawled:{self.name}:{content_hash}"
        if await self.queue.get(key):
            logger.info("[Spider]<%s> repeat page: %s",
                        self.name, content_hash)
            return True
        await self.queue.set(key, 1)
        data["content_hash"] = content_hash
        return False

    async def handle_index(self, page):
        details, next_link = [], None
        pq = PyQuery(page)
        items = pq("ul.li_line > li")
        for item in items:
            e = PyQuery(item)
            title = e("a").text()
            href = e("a").attr("href")
            label = e("span").text()
            detail = {"url": self.domain + href,
                      "extra_data": {**self.job.extra_data,
                                     "title": title, "label": label}}
            logger.info("[Spider]<%s> detail url: %s",
                        self.name, detail["url"])
            details.append(detail)

        return details, next_link

    async def handle_detail(self, page, encoding, extra_data=None):
        pq = PyQuery(page)
        anchor = pq("div.BreadcrumbNav").text()
        content = pq("div#fontzoom").text()
        if content[:content.find("\n")].startswith(".TRS_Editor"):
            content = "\n".join(content.split("\n")[1:])
        editor = pq("div.zrbj.zrbj_r").text().lstrip(
            "[责任编辑：").rstrip("]").strip()
        title = extra_data["title"]
        label = extra_data["label"]
        detail = pq("div.detail_extend1.fl").text()
        pubtime, author, source = detail.split("\u3000\u3000")
        pubtime = pubtime.lstrip("时间：").strip()
        author = author.lstrip("作者：").strip()
        source = source.lstrip("来源：").strip()
        data = {
            "url": self.job.url,
            "urlmd5": self.job.urlmd5,
            "anchor": anchor,
            "label": "指导性案例",
            "tag": title,
            "content": content,
            "editor": editor,
            "pubtime": pubtime,
            "author": author,
            "source": source,
            "getway": self.name,
            "sync": 0,
        }

        items = self.parse_content(content)
        try:
            for item in items:
                retval = data.copy()
                retval.update(item)
                case = GovCase(**retval)
                await case.save()
        except Exception as e:
            logger.error("[Spider]<%s> %s", self.name, e)

        return None

    @staticmethod
    def parse_content(content):
        titles = RE_CASE.findall(content)
        titles = [title.rstrip('\n').strip() for title in titles]
        casenos = RE_CASENO.findall(content)
        casenos = [caseno.rstrip('\n').lstrip('（').rstrip(
            '）').strip() for caseno in casenos]
        parts = RE_CASE.split(content)
        parts = [p for p in parts if p.strip("\n").strip()]
        for i, part in enumerate(parts):
            labels = RE_LABEL.findall(part)
            labels = [label.rstrip('\n').lstrip('【').rstrip('】')
                      for label in labels]
            arr = RE_LABEL.split(part)
            assert len(labels) + 1 == len(arr)
            content = []
            for label, p in zip(labels, arr[1:]):
                content.append({label: p})
            _caseno, text = part.split('\n', 1)
            if re.search(r"（检例第\d+号）", _caseno):
                pass
            else:
                text = part
            content_hash = siphash(text)
            yield {"title": titles[i], "案件字号": casenos[i],
                   "content": text, "content_hash": content_hash}


class JournalSpider(JournalCaseSpider):
    name = "journal"
