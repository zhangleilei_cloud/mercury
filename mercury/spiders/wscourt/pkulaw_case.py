
import re
from functools import partial
from concurrent.futures import ThreadPoolExecutor

from pyquery import PyQuery
from bs4 import BeautifulSoup

from mercury.utils import md5, timestamp
from mercury.libs.proxy import get_proxy
from mercury.libs.useragent import random_ua
from mercury.downloader.http import AsyncHttpDownloader
from mercury.settings import logger, MAX_JOBS
from mercury.models import PkulawCase, JobQueue
from mercury.spiders import BaseSpider


class PkulawCaseSpider(BaseSpider):
    executor = ThreadPoolExecutor(max_workers=MAX_JOBS)
    name = 'pkulaw_case'

    Model = PkulawCase

    domain = "http://law.china-aseanlawinfo.org"

    index_headers = {
        'Accept': '*/*',
        'Accept-Encoding': 'gzip, deflate',
        'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'DNT': '1',
        'Host': 'law.china-aseanlawinfo.org',
        'Origin': 'http://law.china-aseanlawinfo.org',
        'Pragma': 'no-cache',
        'Referer': 'http://law.china-aseanlawinfo.org',
        'User-Agent': random_ua(),
        'X-Requested-With': 'XMLHttpRequest'
    }

    detail_headers = {
        'Accept': ('text/html,application/xhtml+xml,application/xml;'
                   'q=0.9,image/webp,image/apng,*/*;'
                   'q=0.8,application/signed-exchange;v=b3'),
        'Accept-Encoding': 'gzip, deflate',
        'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'DNT': '1',
        'Host': 'law.china-aseanlawinfo.org',
        'Pragma': 'no-cache',
        'Referer': 'http://law.china-aseanlawinfo.org',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': random_ua()
    }

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": None,
            "cookies": {},
            "uniqid": "urlmd5",
            "proxy_enable": True,
            "save_html": True,
        }
        return configs

    async def before_request(self):
        if self.job.is_seed:
            self.job.method = 'post'
            self.job.ctype = 'formdata'
            self.configs['headers'] = self.index_headers
        else:
            self.configs['headers'] = self.detail_headers
        return True

    async def handle_download_failure(self, error):
        # 将下载失败的任务重新加入任务队列
        if not self.job.is_seed:
            await JobQueue.rpush(self.job)

    async def handle_index(self, page):
        details, next_page = [], {}
        pq = PyQuery(page)
        links = pq(
            "div.list-wrap > ul > li > div.block > div.list-title > h4")
        details = []
        for link in links:
            e = PyQuery(link)
            title = e("a").text()
            href = e("a").attr("href")
            label = e("span").text()
            url = self.domain + href
            detail = {"url": url, "extra_data": {
                **self.job.extra_data, "title": title, "label": label}}
            details.append(detail)

        pager = pq("div.pagination > ul > li > a")
        for pg in pager:
            e = PyQuery(pg)
            if e.text() == "下一页":
                pageindex = e.attr("pageindex")
                payload = self.job.payload
                payload["Pager.PageIndex"] = pageindex
                next_page = {"url": self.job.url, "payload": payload, 
                             "extra_data": self.job.extra_data}

        logger.info("[Spider]<%s> details: %s", self.name, details)
        logger.info("[Spider]<%s> next: %s", self.name, next_page)
        return details, next_page

    async def handle_detail(self, page, encoding='utf-8', extra_data=None):
        pq = PyQuery(page)
        anchor = pq('div.crumbs > div.container > a').text()
        fields = pq("div.fields").text()
        values = {}
        lastkey = ""
        for f in fields.splitlines():
            vals = f.split("：")
            if len(vals) == 2:
                k, v = vals
                k, v = k.strip(), v.strip()
                values[k] = v
                lastkey = k
            elif len(vals) == 1:
                values[lastkey] = vals[0].strip()
            else:
                pass

        content = pq("div#divFullText").text()
        title = extra_data["title"]
        label = extra_data["label"]

        data = {
            "url": self.job.url,
            "anchor": anchor,
            "title": title,
            "label": label,
            "content": content,
            "getway": self.name,
            "create_time": timestamp(),
            **values,
        }
        return data
