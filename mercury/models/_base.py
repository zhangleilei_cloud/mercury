# -*- coding: utf-8 -*-

from bson.objectid import ObjectId
from pymongo.errors import DuplicateKeyError

from mercury.utils import timestamp
from mercury.settings import logger
from mercury.monitor import cnt_dup_data


class Model:

    def __init__(self):
        """所有集合的共有字段"""
        self.sync = 0
        self.update_time = timestamp()
        self.create_time = timestamp()

    @classmethod
    async def get_by_nid(cls):
        """通过nid从数据库中查询新闻，并返回 News 对象
        """
        pass

    async def load_to_cache(self):
        """将新闻linkmd5加载入缓存

        ps. 暂时写这里，之后考虑移到专门的缓存管理模块
        """
        pass

    @property
    def uniqid(self):
        return getattr(self, self._uniqid)

    def to_mongo(self):
        """返回实例在 MongoDB 中的存储数据结构
        """
        if self._id is None:
            del self._id
        elif isinstance(self._id, str):
            self._id = ObjectId(self._id)
        data = self.__dict__.copy()
        return data

    @classmethod
    async def find_one(cls, conds={}, projection=None):
        clsname = cls.__name__
        logger.info("[DB]<%s> conds: %s", clsname, conds)
        doc = await cls._db.find_one(conds, projection=projection)
        return doc

    @classmethod
    async def delete_one(cls, conds):
        clsname = cls.__name__
        logger.info("[DB]<%s> conds: %s", clsname, conds)
        await cls._db.delete_one(conds)

    @classmethod
    async def update_one(cls, conds, data):
        clsname = cls.__name__
        logger.info("[DB]<%s> conds: %s", clsname, conds)
        await cls._db.update_one(conds, {"$set": data})

    async def update(self, filters=None, data=None):
        if not filters:
            filters = {'_id': self._id}
        if not data:
            data = self.to_mongo()
        data["update_time"] = timestamp()
        clsname = type(self).__name__
        logger.debug("[DB]<{0}> updating: {1}".format(clsname, data))
        retval = await self._db.update_one(filters, {'$set': data}, upsert=True)
        logger.debug("[DB]<{0}> updated: {1}".format(
            clsname, filters.get(getattr(self, '_uniqid', None), filters)))
        return data

    async def save(self):
        clsname = type(self).__name__
        data = self.to_mongo()
        try:
            logger.info("[DB]<{0}> inserting: {1}".format(
                clsname, data["url"]))
            result = await self._db.insert_one(data)
            self._id = result.inserted_id
        except DuplicateKeyError as e:
            cnt_dup_data.labels(getway=self.getway).inc()
            logger.info("[DB]<{0}> already exists {1} {2}".format(
                clsname, self.urlmd5, self.url))
            retval = e
        else:
            data["_id"] = str(self._id)
            retval = data
            logger.debug("[DB]<{0}> inserted: {1}".format(clsname, retval))
        return retval

    @classmethod
    async def get_by_uniqid(cls, uniqid):
        if not uniqid:
            return None
        clsname = cls.__name__
        logger.info("[DB]<{0}> finding: {1}".format(clsname, uniqid))
        data = await cls._db.find_one({cls._uniqid: uniqid})
        logger.debug("[DB]<{0}> finding: {1}".format(clsname, data))
        inst = cls(**data)
        return inst

    async def remove(self):
        pass
