# -*- coding: utf-8 -*-

"""
搜狗微信搜索
"""

from ._base import Model as BaseModel
from mercury.libs.mongo import mgdb
from mercury.utils import md5, timestamp, siphash


class WxNews(BaseModel):

    _db = mgdb.wxnews

    _uniqid = "mixid"

    def __init__(self, url, urlmd5, **kwargs):
        super().__init__()
        self._id = None
        self.url = url                                  # 新闻URL
        self.urlmd5 = urlmd5                            # URL MD5哈希值
        self.create_time = timestamp()                  # 此新闻在本系统的创建时间
        self.title = kwargs.pop("title", None)          # 正文标题
        self.content = kwargs.pop("content", None)      # 正文内容
        self.link_url = kwargs.pop("link_url", None)    # 文章中的图片和视频链接
        self.share_link = kwargs.pop('share_link', None)  # 分享链接
        self.page = kwargs.pop("page", None)            # HTML原始页面存储位置
        self.author = kwargs.pop("author", None)        # 文章作者
        self.publisher = kwargs.pop("publisher", None)  # 文章出版者
        self.getway = kwargs.pop("getway", None)        # 获取/发现途径 eg. 搜狗新闻
        self.pubtime = kwargs.pop("pubtime", None)      # 文章发布时间
        self.sid = kwargs.pop("sid", None)              # seed id
        self.mixid = kwargs.pop('mixid')                # 计算得出的文章唯一ID
        self.content_hash = kwargs.pop("content_hash") or siphash(self.content)

