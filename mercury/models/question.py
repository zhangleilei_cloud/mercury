"""华律网问题解答
"""

from mercury.libs.mongo import mgdb
from ._base import Model as BaseModel
from mercury.utils import  timestamp


class Question(BaseModel):
    _db = mgdb.laws_question

    _uniqid = 'qid'

    #                 问题ID  名称      手机号     地址
    _fields = ['_id', 'qid', 'title', 'phone', 'address',
    #          发布日期   所属领域   详细描述    回复
              'pubtime', 'field', 'detail', 'replies']

    def __init__(self, url, urlmd5, **kwargs):
        for field in self._fields:
            setattr(self, field, kwargs.pop(field, None))

        self.url = url
        self.urlmd5 = urlmd5
        self.create_time = timestamp()
        self.update_time = timestamp()
        self.sync = 0

    @property
    def uniqid(self):
        uid = getattr(self, self._uniqid)
        return f'question_{uid}'

