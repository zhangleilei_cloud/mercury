"""华律网解决方案
"""

from mercury.libs.mongo import mgdb
from ._base import Model as BaseModel
from mercury.utils import timestamp


class Solution(BaseModel):
    _db = mgdb.laws_solution
    _fields = [
        #       标题      点赞数         浏览数          分类        内容
        '_id', 'title', 'like_count', 'visit_count', 'category', 'content']

    def __init__(self, url, urlmd5, **kwargs):
        for field in self._fields:
            setattr(self, field, kwargs.pop(field, None))
        self.url = url
        self.urlmd5 = urlmd5
        self.sync = 0
        self.is_updated = 0
        self.create_time = timestamp()
        self.update_time = timestamp()
