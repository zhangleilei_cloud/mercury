# -*- coding: utf-8 -*-
from mercury.libs.mongo import mgdb
from mercury.settings import logger
from mercury.utils import timestamp
from ._base import Model as BaseModel


class HighFrequency(BaseModel):
    _db = mgdb.high_frequency
    _uniqid = "urlmd5"

    def __init__(self, url, urlmd5, **kwargs):
        super().__init__()
        self._id = None
        self.url = url
        self.urlmd5 = urlmd5
        self.getway = kwargs.pop("getway", None)
        self.title = kwargs.pop("title", None)
        self.content = kwargs.pop("content", None)
        self.publish_date = kwargs.pop("publish_date", None)
        self.author = kwargs.pop("author", None)
        self.siphash = kwargs.pop("siphash", None)
        self.sid = kwargs.pop("sid", None)
        self.publisher = kwargs.pop("publisher", None)

    async def update(self, filters=None, data=None):
        if not filters:
            filters = {'_id': self._id}
        data = self.to_mongo()
        data["update_time"] = timestamp()
        data["create_time"] = data.get("create_time") or timestamp()
        clsname = type(self).__name__
        logger.debug("[DB]<{0}> updating: {1}".format(clsname, data))
        retval = await self._db.update_one(filters, {'$set': data}, upsert=True)
        logger.debug("[DB]<{0}> updated: {1}".format(
            clsname, filters.get(getattr(self, '_uniqid', None), filters)))
        return data
