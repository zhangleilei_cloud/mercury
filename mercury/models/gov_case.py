"""
【案例】权威案例数据抓取（指导案例、公报案例、参考案例）

最高法指导案例：http://www.court.gov.cn/fabu-xiangqing-104292.html （某篇详情，无统一的种子）
最高法重大案件：http://www.court.gov.cn/fabu-gengduo-15.html

最高法公报-裁判文书选登：http://gongbao.court.gov.cn/ArticleList.html?serial_no=cpwsxd
最高法公报-案例：http://gongbao.court.gov.cn/ArticleList.html?serial_no=al

最高检指导案例：http://www.spp.gov.cn/spp/jczdal/index.shtml

最高检公报：http://www.spp.gov.cn/gjgb/
"""


from mercury.utils import timestamp
from mercury.libs.mongo import mgdb
from ._base import Model as BaseModel


class GovCase(BaseModel):
    _db = mgdb.auth_cases
    _uniqid = "urlmd5"

    _fields = ["_id", "anchor", "source", "pubtime", "author",
               "title", "label", "tag", "content", "content_hash",
               "editor", "getway"]

    def __init__(self, url, urlmd5, **kwargs):
        for field in self._fields:
            setattr(self, field, kwargs.pop(field, None))

        self.url = url
        self.urlmd5 = urlmd5
        self.create_time = timestamp()
        self.update_time = timestamp()
        self.sync = 0

class PkulawCase(GovCase):
    _db = mgdb.pkulaw_cases
