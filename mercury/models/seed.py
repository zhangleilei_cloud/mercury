# -*- coding: utf-8 -*-

import demjson
import asyncio
import logging

from bson.objectid import ObjectId
from pymongo.errors import DuplicateKeyError

from mercury.libs.mongo import mgdb
from mercury.utils import md5, lazyproperty, timestamp
from mercury.settings import logger


class Seed:
    _db = mgdb.seeds

    def __init__(self, *args, **kwargs):
        """种子对象

        一个种子url，代表可以产生详情页/内容页链接的页面。
        例如新闻子频道，如果是搜索结果，那每个关键词的搜
        索结果都是一个种子

        :param url: 种子URL，不要把翻页等参数放到这里，应
        该在spider的index_page中处理

        """
        self._id = kwargs.pop('_id', None)
        self.url = kwargs.pop('url', None)
        # 获取途径, eg. 百度新闻
        self.getway = kwargs.pop("getway", None)
        self.publisher = kwargs.pop("publisher", None)
        # 种子调度间隔，每隔 N 秒调度一次，默认每天一次
        self._sched_interval = kwargs.pop('_sched_interval', 86400)
        # 该种子被抓取过的次数，每抓一次加1
        self.crawled_times = kwargs.pop("crawled_times", 0)
        # 反爬配置
        self.anti_cfg = kwargs.pop(
            'anti_cfg',
            # 默认无需代理、关闭cookie、无需随机user-agent
            {"proxy": False, "cookie": False, "random_ua": False})
        self.status = kwargs.pop('status', 'OK')
        self.create_time = kwargs.pop("create_time", timestamp())
        # 上次被调度的时间戳，默认为0，没调度过
        self.last_sched_time = kwargs.pop("last_sched_time", 0)
        # 种子被调度的优先级, 数字越大优先级越高
        self.priority = kwargs.pop('priority', 0)
        # 种子url中携带的参数, 默认为空字符串
        self.query = kwargs.pop('query', '')
        payload = kwargs.pop('payload', '')
        # 种子唯一性标志md5值
        self.urlmd5 = kwargs.pop('urlmd5', md5(
            self.url + self.query + payload))
        # 种子post请求中携带的数据, 默认为空字典
        self.payload = demjson.decode(payload) if payload else {}
        # 种子需要传递给任务的额外数据
        self.extra_data = kwargs.pop('extra_data', None) or {}

    def to_mongo(self):
        """返回实例在 MongoDB 中的存储数据结构
        """
        if self._id is None:
            del self._id
        elif isinstance(self._id, str):
            self._id = ObjectId(self._id)
        data = self.__dict__.copy()
        data['payload'] = demjson.encode(data['payload'])
        return data

    async def update(self, data=None):
        if not data:
            data = self.to_mongo()
        retval = await self._db.update_one(
            {'_id': self._id}, {'$set': data})
        logger.info("[DB]Seed updated: {0}".format(data))
        return data

    async def save(self):
        data = self.to_mongo()
        try:
            result = await self._db.insert_one(data)
            self._id = result.inserted_id
        except DuplicateKeyError as e:
            retval = e
        else:
            data["_id"] = str(self._id)
            retval = data
        logger.info("[DB]Seed inserted: {0}".format(retval))
        return retval

    @classmethod
    async def create_by_urlmd5(cls, urlmd5):
        result = await cls._db.find_one({"urlmd5": urlmd5})
        seed = cls(**result) if result else None
        return seed

    @classmethod
    async def find(cls, conds, sort=None, limit=2, skip=0):
        """查找满足cond条件的seed记录

        :param dict conds: 查询条件，跟pymongo查询语法一致
        :param list sort: 由 (key, direction) 组成的列表
        :param int limit: 限制个数，默认一次查10个
        :param int skip: 跳过前skip个结果
        """
        cursor = cls._db.find(conds)
        cursor.sort(sort).limit(limit).skip(skip)
        async for seed_data in cursor:
            yield cls(**seed_data)

    @classmethod
    async def find_one(cls, conds):
        data = await cls._db.find_one(conds)
        if data:
            return cls(**data)

    @classmethod
    async def delete_one(cls, conds):
        resval = await cls._db.delete_one(conds)
        return resval

    @property
    def sched_interval(self):
        return self._sched_interval

    @sched_interval.setter
    def sched_interval(self, value):
        self._sched_interval = value
        # 此处更新 seed 的抓取job
        # 例如value 为0时，从任务队列中移除本seed产生的任务
        # 或更新调度器的频率控制
