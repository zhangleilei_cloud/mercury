from mercury.libs.mongo import mgdb
from mercury.utils import timestamp
from ._base import Model as BaseModel


class BSCase(BaseModel):
    """
        裁判文书Model
        """
    _db = mgdb.bs_case
    _uniqid = "docid"

    def __init__(self, url, urlmd5, **kwargs):
        super().__init__()
        self._id = None
        self.url = url
        self.urlmd5 = urlmd5
        self.create_time = timestamp()
        self.getway = kwargs.get('getway')
        self.docid = kwargs.get('docid')
        self.title = kwargs.get('title')
        self.sections = kwargs.get('sections')
        self.sid = kwargs.get('sid')
        self.basic_info = kwargs.get('basic_info')
