"""北大法宝V6"""

from mercury.utils import md5, timestamp
from mercury.libs.mongo import mgdb
from ._base import Model as BaseModel


class Law(BaseModel):
    """pkulaw数据源"""
    _db = mgdb.laws
    _uniqid = "fbid"

    def __init__(self, url, urlmd5, **kwargs):
        self._id = None
        self.url = url
        self.urlmd5 = urlmd5
        self.fbid = kwargs.pop('fbid')
        self.title = kwargs.pop("title", None)
        self.lib = kwargs.pop('lib', None)
        self.documentno = kwargs.pop('documentno', None)
        self.category = kwargs.pop('category', None)
        self.issuedate = kwargs.pop('issuedate', None)
        self.ratifydate = kwargs.pop('ratifydate', None)
        self.implementdate = kwargs.pop('implementdate', None)
        self.timelinessdic = kwargs.pop('timelinessdic', None)
        self.effectivenessdic = kwargs.pop('effectivenessdic', None)
        self.issuedepartment = kwargs.pop('issuedepartment', None)
        self.ratifydepartment = kwargs.pop('ratifydepartment', None)
        self.changes = kwargs.pop('changes', [])
        self.content = kwargs.pop('content', None)
        self.lasttimelinesschangedate = kwargs.pop(
            'lasttimelinesschangedate', None)
        self.mediatypes = kwargs.pop('mediatypes', [])
        self.hasattachments = kwargs.pop('hasattachments', False)
        self.modifybasis = kwargs.pop('modifybasis', None)
        self.invalidbasis = kwargs.pop('invalidbasis', None)
        self.partinvalidbasis = kwargs.pop('partinvalidbasis', None)
        self.pkulawinfos = kwargs.pop('pkulawinfos', {})
        self.getway = kwargs.pop('getway', None)
        # 是否已经被同步字段 0 未同步 1 已同步
        self.sync = 0
        self.create_time = timestamp()
        self.sid = kwargs.pop('sid', None)
