# -*- coding: utf-8 -*-

from pymongo.errors import DuplicateKeyError
from bson.objectid import ObjectId
from mercury.libs.mongo import mgdb
from mercury.utils import md5, timestamp
from mercury.settings import logger
from ._base import Model as BaseModel

class Case(BaseModel):
    """
    裁判文书Model
    """

    _db = mgdb.cases
    _uniqid = "docid"

    def __init__(self, url, urlmd5, **kwargs):
        super().__init__()
        self._id = None
        self.url = url
        self.urlmd5 = urlmd5
        self.create_time = timestamp()
        self.getway = kwargs.get('getway', 'wscourt')
        self.docid = kwargs.get('docid')
        self.title = kwargs.get('title')
        self.court = kwargs.get('court')
        self.casetype = kwargs.get('casetype')
        self.proceedings = kwargs.get('proceedings')
        self.doctype = kwargs.get('doctype')
        self.cn = kwargs.get('cn')
        self.cause = kwargs.get('cause')
        self.caseperson = kwargs.get('caseperson')
        self.admins = kwargs.get('admins')
        self.mends = kwargs.get('mends')
        self.pubdate = kwargs.get('pubdate')
        self.uploaddate = kwargs.get('uploaddate')
        self.closeway = kwargs.get('closeway')
        self.trialdate = kwargs.get('trialdate')
        self.legalbasis = kwargs.get('legalbasis')
        # 如果裁判文书全文没有内容, 则存储裁判文书的原始Html
        self.content = kwargs.get('content')
        if not self.content:
            self.html = kwargs.get('html')
        self.litigants = kwargs.get('litigants')
        self.lawoffice = kwargs.get('lawoffice')
        self.lawyers = kwargs.get('lawyers')
        self.nonpublicreason = kwargs.get('nonpublicreason')
        self.effectlevel = kwargs.get('effectlevel')
        self.doccontent = kwargs.get('doccontent')
        self.adminscope = kwargs.get('adminscope')
        self.adminact = kwargs.get('adminact')
        self.prosecutor = kwargs.get('prosecutor')
        self.relatedoc = kwargs.get('relatedoc')
