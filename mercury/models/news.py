# -*- coding: utf-8 -*-

"""
新闻数据的存储模型
"""
import logging

from bson.objectid import ObjectId
from pymongo.errors import DuplicateKeyError

from ._base import Model as BaseModel
from mercury.libs.mongo import mgdb
from mercury.utils import md5, timestamp, lazyproperty, siphash
from mercury.settings import API_VIBRIO_GEN, logger


class News(BaseModel):

    _db = mgdb.news

    _uniqid = "urlmd5"

    def __init__(self, url, urlmd5, **kwargs):
        super().__init__()
        self._id = None
        # self.nid = "create_from_vibrio"                 # 统一ID
        self.url = url                                    # 新闻URL
        self.urlmd5 = urlmd5                              # URL MD5哈希值
        self.title = kwargs.pop("title", None)            # 正文标题
        self.content = kwargs.pop("content", None)        # 正文内容
        self.page = kwargs.pop("page", None)              # HTML原始页面存储位置
        self.author = kwargs.pop("author", None)          # 文章作者 eg. 张三
        self.publisher = kwargs.pop("publisher", None)    # 文章出版者 eg. 人民日报
        self.getway = kwargs.pop("getway", None)          # 获取/发现途径 eg. 百度新闻
        self.pubtime = kwargs.pop("pubtime", None)        # 文章发布时间
        # self.origin = kwargs.pop("origin")              # 文章原始出处
        self.create_time = timestamp()                    # 此新闻在本系统的创建时间
        self.smhash = None                                # 正文内容的superminhash值
        self.sid = kwargs.pop("sid", None)                # seed id
        self.content_hash = kwargs.pop("content_hash") or siphash(self.content)
