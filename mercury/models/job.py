# -*- coding: utf-8 -*-

"""
抓取任务模型，供调度模块使用
"""
import demjson
import logging

from mercury.libs.redis import redis_pool
from mercury.utils import lazyproperty, timestamp, md5
from mercury.settings import logger, SPIDER_CONF
from mercury.monitor import cnt_dup_job


class JobQueue:
    """基于 Redis list 的下载任务队列，分为三个优先级队列.
    对应redis key 分别为 "mercury:jobq:high"
                        "mercury:jobq:mid"
                        "mercury:jobq:low"
    """
    pool = redis_pool
    queues = ["mercury:jobq:high", "mercury:jobq:mid", "mercury:jobq:low"]

    @classmethod
    async def blpop(cls, timeout=5):
        """返回一个 job 对象
        """
        retval = await cls.pool.blpop(*cls.queues, timeout=timeout)
        logger.info("[Queue] BLPOP: {0}".format(retval))
        if retval:
            qname, job_str = retval
            return Job.deserialization(job_str)

    @classmethod
    async def rpush(cls, job):
        # 检查job是否已经被抓取过
        spider_name = job.spider_name
        uniqid = job.uniqid
        if not job.extra_data.get("isupdate"):
            is_crawled = await cls.pool.get(f"job:crawled:{spider_name}:{uniqid}")
            if is_crawled:
                cnt_dup_job.labels(getway=job.spider_name).inc()
                logger.info(f"[Queue] DUPLICATE: {uniqid} this job already crawled")

                from mercury.scheduler.scheduler import SPIDER_CONF
                from mercury import spiders
                spider_name = job.spider_name
                Spider = getattr(spiders, SPIDER_CONF[spider_name]['spider'])
                await Spider(job).handle_dup_detail()
                return None

        data = job.__dict__.copy()
        data["queued_time"] = timestamp(is_int=True)
        data = demjson.encode(data)
        await cls.pool.rpush(cls.queues[job.priority], data)
        logger.info("[Queue] RPUSH: {0}".format(data))

    @classmethod
    async def total_len(cls):
        counter = 0
        for key in cls.queues:
            counter += await cls.pool.llen(key)
        logger.info("[Queue] Total number of jobs remaining: {0}"
                    .format(counter))
        return counter


class Job:
    def __init__(self, *, url, seed_urlmd5, spider_name,
                 anti_cfg, is_seed=False, max_tries=2,
                 priority=1, **kwargs):
        """抓取任务

        :param url: 待抓取任务的url
        :param priority: 表示抓取优先级，PRI_LOW|PRI_MID|PRI_HIGH
        """
        self.url = url
        # job http url中携带的参数, 默认为空字符串
        self.query = kwargs.pop('query', '')
        # job http post请求携带的数据, 默认为None
        self.payload = kwargs.pop('payload', None)
        # urlmd5 = md5(url + query + payload)
        self.urlmd5 = kwargs.pop('urlmd5', None) or md5(
            self.url + self.query + (demjson.encode(self.payload) if self.payload else ''))
        self.seed_urlmd5 = seed_urlmd5
        self.spider_name = spider_name
        self.anti_cfg = anti_cfg
        self.is_seed = is_seed
        self.priority = priority
        self.max_tries = max_tries
        self.queued_time = kwargs.pop("queued_time", None)
        # job用完即销，暂时不用记录状态
        # self.status = kwargs.pop("status", None)
        # http请求方法, 默认为get
        self.method = kwargs.pop('method', 'get')
        # http请求返回的数据类型方法 text, json, read
        self.rtype = kwargs.pop('rtype', 'text')
        # http请求发送的数据的类型:json, formurl, formdata
        self.ctype = kwargs.pop('ctype', None)
        # handle_index传递给handle_detail的数据
        self.extra_data = kwargs.pop('extra_data', None) or {}
        # http请求完毕后是否应该立即关闭当前session
        self.auto_close = kwargs.pop('auto_close', True)
        # job唯一性标志, 取代urlmd5功能, 默认为urlmd5
        self.uniqid = kwargs.pop('uniqid', None) or self.urlmd5

    @classmethod
    def deserialization(cls, obj_str):
        data = demjson.decode(obj_str)
        return cls(**data)

    # 暂时按三个优先级区分

    # @property
    # def priority(self):
        # return self._priority

    # @priority.setter
    # def priority(self, value):
        # """检查value的有效性，整数
        # 更新任务队列里的既有配置
        # """
        # self._priority = value
