# -*- coding: utf-8 -*-

from mercury.libs.mongo import mgdb
from mercury.models._base import Model
from mercury.utils import timestamp


class ZhihuColumnSummary(Model):
    _db = mgdb.zhihucolumn_summary

    _uniqid = "urlmd5"

    def __init__(self, url, urlmd5, **kwargs):
        super().__init__()
        self._id = None
        self.url = url
        self.urlmd5 = urlmd5
        self.create_time = timestamp()
        self.update_time = timestamp()
        self.column_id = kwargs.pop("column_id", None)
        self.title = kwargs.pop("title", None)
        self.description = kwargs.pop("description", None)
        self.followers = kwargs.pop("followers", None)
        self.articles_count = kwargs.pop("articles_count", None)
        self.author = kwargs.pop("author", None)
        self.getway = kwargs.pop("getway", None)
        self.sid = kwargs.pop("sid", None)


class ZhihuColumnArticle(Model):
    _db = mgdb.zhihucolumn_article

    _uniqid = "urlmd5"

    def __init__(self, url, urlmd5, **kwargs):
        super().__init__()
        self._id = None
        self.url = url
        self.urlmd5 = urlmd5
        self.updated = kwargs.pop("updated", None)
        self.created = kwargs.pop("created", None)
        self.column_id = kwargs.pop("column_id", None)
        self.title = kwargs.pop("title", None)
        self.article_id = kwargs.pop("article_id", None)
        self.content = kwargs.pop("content", None)
        self.comment_count = kwargs.pop("comment_count", None)
        self.voteup_count = kwargs.pop("voteup_count", None)
        self.cover_image_url = kwargs.pop("cover_image_url", None)
        self.tags = kwargs.pop("tags", None)
        self.images_urls = kwargs.pop("images_urls", None)
        self.video_urls = kwargs.pop("video_urls", None)
        self.getway = kwargs.pop("getway", None)
        self.sid = kwargs.pop("sid", None)
