"""北大法宝API接口的数据模型"""

from mercury.utils import md5, timestamp
from mercury.libs.mongo import mgdb
from ._base import Model as BaseModel


class _Pkulaw(BaseModel):
    _uniqid = "Gid"

    _fields = ['_id', 'Gid', 'Title', 'Category', 'DocumentNO', 'RatifyDepartment',
        'RatifyDate', 'IssueDepartment', 'IssueDate', 'ImplementDate', 'HisTiaos',
        'EffectivenessDic', 'FullText', 'RevisedBasis', 'TimelinessDic',
        'PartialFailureBasis', 'FailureBasis', 'FaBaoTips', 'IsProcess', 'Sort',
        'IsHaveEng', 'IsHaveBackInfo', 'IsHaveHistory', 'LastTimelinessChangeDate',
        'IsBeReferenced', 'ReferenceArticleGidTiaoNum', 'NavCatalog', 'HasSyTiaos']

    def __init__(self, url, urlmd5, **kwargs):
        for field in self._fields:
            setattr(self, field, kwargs.pop(field, None))
        self.url = url
        self.urlmd5 = urlmd5
        self.create_time = timestamp()
        self.update_time = timestamp()


class LawPkulawMeta(_Pkulaw):
    _db = mgdb.laws_pkulawmeta


class LawPkulawDetail(_Pkulaw):
    _db = mgdb.laws_pkulawdetail
