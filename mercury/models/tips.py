# -*- coding: utf-8 -*-

"""
北京政务服务中心数据的存储模型
"""

from ._base import Model as BaseModel
from mercury.libs.mongo import mgdb


class Tips(BaseModel):

    _db = mgdb.tips
    _uniqid = "urlmd5"

    def __init__(self, url, urlmd5, **kwargs):
        super().__init__()
        self._id = None
        self.url = url
        self.urlmd5 = urlmd5
        self.cate_xz = kwargs.pop("cate_xz", None)          # 按性质分类类型名称
        self.cate_parent = kwargs.pop("cate_parent", None)  # 父文章节点名称
        self.title = kwargs.pop("title", None)              # 正文标题
        self.reference = kwargs.pop("reference", None)      # 来源url
        self.site_level = kwargs.pop("site_level", None)    # 来源地域信息
        self.base_info = kwargs.pop("base_info", None)
        self.materials = kwargs.pop("materials", None)
        self.fees = kwargs.pop("fees", None)
        self.basis = kwargs.pop("basis", None)
        self.work_flow = kwargs.pop("work_flow", None)
        self.results = kwargs.pop("results", None)
        self.visits = kwargs.pop("visits", None)
        self.faq = kwargs.pop("faq", None)
        self.extras = kwargs.pop("extras", None)
        self.department = kwargs.pop("department", None)
        self.organization = kwargs.pop("organization", None)
        self.telephone = kwargs.pop("telephone", None)
        self.getway = kwargs.pop("getway", None)            # "爬虫名称"
        self.status = "New"                                 # 数据状态 "New"和"Deprecated"
        self.sync = kwargs.pop("sync", 0)
