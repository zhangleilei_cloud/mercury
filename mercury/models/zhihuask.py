# -*- coding: utf-8 -*-
from mercury.libs.mongo import mgdb
from mercury.models._base import Model
from mercury.utils import timestamp


class ZhihuAsk(Model):
    _db = mgdb.zhihuask

    _uniqid = "urlmd5"

    def __init__(self, url, urlmd5, **kwargs):
        super().__init__()
        self._id = None
        self.url = url
        self.urlmd5 = urlmd5
        self.create_time = timestamp()
        self.title = kwargs.pop("title", None)
        self.tags = kwargs.pop("tags", None)
        self.detail = kwargs.pop("detail", None)
        self.detail_imgs = kwargs.pop("detail_imgs", None)
        self.pubtime = kwargs.pop("pubtime", None)
        self.reptime = kwargs.pop("reptime", None)
        self.answers = kwargs.pop("answers", None)
        self.getway = kwargs.pop("getway", None)
        self.sid = kwargs.pop("sid", None)
