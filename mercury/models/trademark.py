# -*- coding: utf-8 -*-

"""
商标模型: 以商标局字段为主
"""
from mercury.libs.mongo import mgdb
from mercury.utils import timestamp
from ._base import Model as BaseModel


class Trademark(BaseModel):

    _db = mgdb.trademarks
    _uniqid = "urlmd5"

    def __init__(self, url, urlmd5, **kwargs):
        super().__init__()
        # 通用字段
        self._id = kwargs.pop("_id", None)
        self.url = url                                                              # 商标url
        self.urlmd5 = urlmd5
        self.create_time = timestamp()
        self.name = kwargs.pop("name", None)                                        # 商标名称
        self.features = kwargs.pop("features", [])                                  # 图片特征
        self.url_image = kwargs.pop("url_image", None)                              # 商标链接
        self.url_image_s3 = kwargs.get("url_image_s3")                              # s3上的存储位置
        self.services = kwargs.pop("services", None)                                # 商品服务
        self.services_list = kwargs.pop("services_list", None)                      # 服务列表
        self.similar_group = kwargs.pop("similar_group", [])                        # 类似群
        self.reg_code = kwargs.pop("reg_code", None)                                # 注册码
        self.date_apply = kwargs.pop("date_apply", None)                            # 申请日期
        self.inter_class = kwargs.pop("inter_class", None)                          # 国际分类
        self.reg_person_zh = kwargs.pop("reg_person_zh", None)                      # 注册人中文
        self.reg_person_en = kwargs.pop("reg_person_en", None)                      # 注册人英文
        self.reg_addr_zh = kwargs.pop("reg_addr_zh", None)                          # 注册地址中文
        self.reg_addr_en = kwargs.pop("reg_addr_en", None)                          # 注册地址英文
        self.notice_no_first_trial = kwargs.pop("notice_no_first_trial", None)      # 初审公告期号
        self.notice_no_reg = kwargs.pop("notice_no_reg", None)                      # 注册公告期号
        self.is_shared = kwargs.pop("is_shared", None)                              # 是否共有商标
        self.date_notice_first_trial = kwargs.pop("date_notice_first_trial", None)  # 初审公告日期
        self.date_notice_reg = kwargs.pop("date_notice_reg", None)                  # 注册公告日期
        self.type = kwargs.pop("type", None)                                        # 商标类型
        self.date_proprietary = kwargs.pop("date_proprietary", None)                # 专有权期限
        self.form = kwargs.pop("form", None)                                        # 商标形式
        self.date_inter_reg = kwargs.pop("date_inter_reg", None)                    # 国际注册日期
        self.date_late_desig = kwargs.pop("date_late_desig", None)                  # 后期指定日期
        self.date_priority = kwargs.pop("date_priority", None)                      # 优先权日期
        self.agency = kwargs.pop("agency", None)                                    # 代理组织
        self.reg_process = kwargs.pop("reg_process", [])                            # 注册流程
        self.status = kwargs.pop("status", None)                                    # 商标状态
        self.getway = kwargs.pop("getway", None)                                    # 获取途径
        self.sid = kwargs.pop("sid", None)

        # 专用字段：
        self.remarks = kwargs.pop("remarks", None)                                  # 备注 (subiao)
        self.joint_agent = kwargs.pop("joint_agent", None)                          # 商标共同申请人(iptop)
        self.colors = kwargs.pop("colors", None)                                    # 颜色组合(iptop)

