"""北大法宝V6"""

from mercury.utils import md5, timestamp
from mercury.libs.mongo import mgdb
from ._base import Model as BaseModel


class LawUpstream(BaseModel):
    """pkulaw数据源"""
    _db = mgdb.laws_upstream
    _uniqid = "fbid"

    def __init__(self, url, urlmd5, **kwargs):
        self._id = None
        self.url = url
        self.urlmd5 = urlmd5
        self.create_time = timestamp()
        self.fbid = kwargs.pop('fbid', None) or urlmd5
        self.getway = kwargs.pop('getway', None)
        self.title = kwargs.pop("title", None)
        self.lib = kwargs.pop('lib', None)
        self.issuedepartment = kwargs.pop('issuedepartment', None)
        self.documentno = kwargs.pop('documentno', None)
        self.ratifydate = kwargs.pop('ratifydate', None)
        self.issuedate = kwargs.pop('issuedate', None)
        self.implementdate = kwargs.pop('implementdate', None)
        self.timelinessdic = kwargs.pop('timelinessdic', None)
        self.modifybasis = kwargs.pop('modifybasis', None)
        self.ratifydepartment = kwargs.pop('ratifydepartment', None)
        self.effectivenessdic = kwargs.pop('effectivenessdic', None)
        self.category = kwargs.pop('category', None)
        self.invalidbasis = kwargs.pop('invalidbasis', None)
        self.partinvalidbasis = kwargs.pop('partinvalidbasis', None)
        self.changes = kwargs.pop('changes', [])
        self.content = kwargs.pop('content', None)
        self.mediatypes = kwargs.pop('mediatypes', [])
        self.hasattachments = kwargs.pop('hasattachments', False)
        self.pkulawinfos = kwargs.pop('pkulawinfos', {})
        self.needlogin = kwargs.pop('needlogin', False)
        # 是否已经被同步字段 0 未同步 1 已同步
        self.sync = 0
        self.is_updated = False
        self.sid = kwargs.pop('sid', None)
