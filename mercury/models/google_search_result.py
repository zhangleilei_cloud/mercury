# -*- coding: utf-8 -*-

"""
google搜索的存储模型
"""

from ._base import Model as BaseModel
from mercury.libs.mongo import mgdb


class GoogleSearchResult(BaseModel):

    _db = mgdb.google_search_result
    _uniqid = "urlmd5"

    def __init__(self, url, urlmd5, **kwargs):
        super().__init__()
        self._id = None
        self.url = url
        self.urlmd5 = urlmd5
        self.title = kwargs.pop("title", None)              # 标题
        self.abstract = kwargs.pop("abstract", None)        # 摘要
        self.reference = kwargs.pop("reference", None)      # 来源url
        self.keywords = kwargs.pop("keywords", None)        # 搜索所用关键字
        self.getway = kwargs.pop("getway", None)            # "爬虫名称"
        self.status = "New"                                 # 数据状态 "New"和"Deprecated"


