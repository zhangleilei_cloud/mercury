# -*- coding: utf-8 -*-

"""
基于prometheus的监控指标
"""

from prometheus_client import Counter, Gauge, Histogram, Summary


data_amount = Gauge(
    'data_amount', 'Total number of data crawled', ["getway"])

jobs_in_redis = Gauge(
    'jobs_in_redis', 'The number of jobs to be crawled in redis queue')

seeds_amount = Gauge(
    'seeds_amount', 'Total number of seeds added to the database', ["getway"])

zero_seeds_amount = Gauge(
    'zero_seeds_amount', 'Number of seeds that have never been crawled', ["getway"])

jobs_inprogress = Gauge(
    'jobs_inprogress', 'The number of jobs being crawled', ["getway"])

# ############### Counter ##############
cnt_download_failures = Counter(
    'cnt_download_failures', 'Total number of failures on http download', ["getway"])

cnt_extract_fail = Counter(
    'cnt_extract_fail', 'Total number of failures on detail page extraction', ["getway"])

cnt_job_started = Counter(
    'cnt_job_started', 'The total number of enter the crawler processing', ["getway"])

# cnt_extract_err = Counter(
    # 'cnt_extract_err', 'Number of execptions when extracting detail page', ["getway"])

cnt_db_saved = Counter(
    'cnt_db_saved', 'The total number of data saved to mongodb', ["getway"])

cnt_dup_data = Counter(
    'cnt_dup_data', 'Duplicated when insert to db', ["getway"])

cnt_dup_job = Counter(
    'cnt_dup_job', 'Enter the schedule but crawled before', ["getway"])


# ############### Summary ##############
time_on_download = Summary(
    'time_on_download', 'Time spent on downloading a webpage', ["getway"])

time_on_extract_detail = Summary(
    'time_on_extract_detail', 'Time spent on extract a detail webpage', ["getway"])

time_on_extract_index= Summary(
    'time_on_extract_index', 'Time spent on extract a index webpage', ["getway"])

time_on_upload_s3 = Summary(
    'time_on_upload_s3', 'Time spent on upload webpage raw html to ourown s3', ["getway"])

time_on_write_mongo = Summary(
    'time_on_write_mongo', 'Time spent on write data to mongodb', ["getway"])
