# -*- coding: utf-8 -*-

import asyncio
import logging
import random
import itertools
from collections import defaultdict
from dateutil.relativedelta import relativedelta
from mercury.models import (Seed, Job, JobQueue, News, Case, LawPkulawDetail,
                            Trademark, WxNews, Law, Tips, GoogleSearchResult,
                            GovCase)
from mercury.models.bs_case import BSCase
from mercury.models.high_frequency import HighFrequency
from mercury.models.zhihuask import ZhihuAsk
from mercury.models.zhihucolumn import ZhihuColumnSummary, ZhihuColumnArticle
from mercury.utils import timestamp, calc_percent_weights, probability_select
from mercury import spiders
from mercury.settings import (logger, MAX_JOBS, JOBQUEUE_MAXSIZE,
                              SEED_INTERVAL, SPIDER_CONF)
from mercury.monitor import (jobs_in_redis,
                             data_amount,
                             seeds_amount,
                             zero_seeds_amount,
                             jobs_inprogress,
                             cnt_job_started)
from mercury.spiders.laws.pkulawapi import convert_url

default_conf = SPIDER_CONF.get("default")


class Scheduler:
    """调度类
    """

    def __init__(self):
        pass

    async def _get_jobs(self, num=30):
        """批量从抓取队列获取抓取任务
        """
        pass

    @classmethod
    async def dispatch(cls, job, spider_conf):
        """将下载任务分发给对应的spider
        """
        spider_name = job.spider_name
        uniqid = job.uniqid
        if not job.extra_data.get("isupdate"):
            # 分发任务前进行一次重复检查，若重复则不分发
            is_crawled = await JobQueue.pool.get(f"job:crawled:{spider_name}:{uniqid}")
            if is_crawled:
                logger.info(f"[Sched] DUPLICATE: job.uniqid:{uniqid}")
                return None

        Spider = getattr(spiders, spider_conf["spider"])
        logger.info(
            f"[Sched] Dispatch job.uniqid:{uniqid} to spider: {spider_name}")
        jobs_inprogress.labels(getway=spider_name).inc()
        cnt_job_started.labels(getway=spider_name).inc()
        await Spider(job).start()
        jobs_inprogress.labels(getway=spider_name).dec()

    @classmethod
    async def check_new_seeds(cls, interval=SEED_INTERVAL):
        """检查数据库中是否有新种子加入，如果有则将
        新种子载入下载队列

        :param int interval: 每隔 interval 秒检查一次
        """
        # (当前时间 - 调度间隔)) >= 上次调度时间
        # 且 状态为OK的种子
        # ps. MongoDB 中Date对象为毫秒级
        should_sched = ("(new Date(ISODate().getTime()"
                        "- this._sched_interval*1000)"
                        ">= this.last_sched_time)"
                        "&& this.status=='OK'")

        # 判断当前配置文件中 status 为 OK 的爬虫
        spider_confs = dict(
            filter(lambda info: info[1]["status"] == "OK", SPIDER_CONF.items())
        )
        logger.debug("[Sched]Spiders Configuration: {}".format(spider_confs))

        for count in itertools.count(1):
            await asyncio.sleep(interval)

            # 队列中下载作业数大于JOBQUEUE_MAXSIZE时不调度新种子
            jobs_left = await JobQueue.total_len()
            if jobs_left > JOBQUEUE_MAXSIZE:
                continue
            logger.info("[Sched]Checking new seeds ...")

            seed_limit = cls.get_seed_config(count, spider_confs)
            for name, seed_limit in seed_limit.items():
                # 应该调度种子时，先随机sleep一段时间，一定程度避免多个节点同时发起mongo查询请求而调度过多种子
                await asyncio.sleep(random.uniform(0, 20))
                async for seed in Seed.find(
                    {"$where": should_sched, "getway": name},
                    # 抓取次数少的优先，创建时间早的优先
                    sort=[("crawled_times", 1), ("create_time", 1)],
                    limit=seed_limit
                ):
                    if seed.getway == "pkulawapi":
                        seed.url = convert_url(seed.url)
                    job = Job(url=seed.url,
                              urlmd5=seed.urlmd5,
                              seed_urlmd5=seed.urlmd5,
                              spider_name=seed.getway,
                              anti_cfg=seed.anti_cfg,
                              priority=spider_confs[name]["priority"],
                              query=seed.query,
                              payload=seed.payload,
                              extra_data=seed.extra_data,
                              is_seed=True)
                    logger.info("[Sched]Seed to queue, URL {0}, GETWAY {1}"
                                .format(seed.url, seed.getway))
                    await JobQueue.rpush(job)
                    seed.last_sched_time = timestamp()
                    seed.crawled_times += 1
                    if isinstance(seed.extra_data, dict) and \
                            int(seed.extra_data.get('max_crawled_times', 0)) == int(seed.crawled_times):
                        seed.status = 'STOP'
                    await seed.update()

    @staticmethod
    def get_seed_config(count, spider_confs):
        # 取出爬虫种子的周期配置 {"a": 3, "b": 4} 判断爬虫在此次循环是否需要取新的种子
        frequencies = {}
        for name, conf in spider_confs.items():
            frequencies[name] = conf["seed_frequency"]

        # 本周期可以调度的爬虫名称 spiders = ["iptop", "subiao", ...]
        spiders = [n for n, f in frequencies.items() if count % f == 0]

        # 计算spiders权重: spiders_weights = {"a": 0.9898, "b": 0.1111}
        weights = {}
        for name in spiders:
            weights[name] = spider_confs[name]["weights"]
        # calc_percent_weights 返回值为[(A, 0.9898), ...]
        spiders_weights = dict(calc_percent_weights(weights))
        logger.debug("[Sched]Spiders weights: {}".format(spiders_weights))

        # 计算爬虫是否真正的参与调度 spider_names = [A, B, ...]
        spider_names = [info[0]
                        for info in probability_select(spiders_weights)]
        logger.debug("[Sched]Spiders should schedule: {}".format(spider_names))

        # seed_limits = {"A": 5, "B": 3, ...}
        seed_limits = {}
        for name in spider_names:
            seed_limits[name] = spider_confs[name]["seed_limit"]
        logger.debug("[Sched]Seeds limit setting: {}".format(seed_limits))
        return seed_limits

    @classmethod
    async def run(cls):
        while True:
            logger.info("[Sched]Getting jobs from queue")
            # 给种子检查、API等协程多点执行机会
            await asyncio.sleep(0.1)
            jobs = await asyncio.gather(
                *[JobQueue.blpop() for _ in range(MAX_JOBS)])
            jobs = list(filter(None, jobs))
            if jobs:
                jobs_batch = cls.job_dispatch_policy(jobs)
                for batch in jobs_batch:
                    await asyncio.gather(*[cls.dispatch(
                        job, SPIDER_CONF[job.spider_name]) for job in batch])
            else:
                logger.info("[Sched]No download job yet")

            # 记录数据整体抓取量
            # TODO: 事实上，当前数据库中的量由某个节点检测即可，无需每各节点都
            # 重复地执行相同的查库命令，随着集群规模扩大，反而给数据库增加了压力
            # 现阶段无配置中心，故而暂时采取生成随机数对比的办法，让集群中所有
            # 节点有10%的概率去记录状态，集群节点数量大于10时，理想情况下就一直
            # 有节点在进行记录
            # 后续引入配置中心组件，明确地指派某一节点做记录即可
            if random.randint(0, 9) == 1:
                await cls._record_status()

    @classmethod
    def job_dispatch_policy(cls, jobs):
        jobs_group = defaultdict(list)
        for job in jobs:
            jobs_group[job.spider_name].append(job)

        jobs_batch = []
        for spdier_name in jobs_group:
            spider_conf = default_conf.copy()
            spider_conf.update(SPIDER_CONF[spdier_name])
            jc = spider_conf["job_concurrency"]
            each_group_jobs = jobs_group[spdier_name]
            count = len(each_group_jobs)
            jobs_batch.append([each_group_jobs[i:i + jc]
                               for i in range(0, count, jc)])

        retval = cls.generate_batch_jobs(jobs_batch)
        return retval

    @classmethod
    def generate_batch_jobs(cls, jobs_batch):
        retval = []
        while jobs_batch:
            buf = []
            for jobs_group in jobs_batch:
                buf.extend(jobs_group.pop())
            retval.append(buf)
            jobs_batch = list(filter(None, jobs_batch))
        return retval

    @staticmethod
    async def _record_status():
        """recording prometheus metrics
        """
        jobs_in_redis.set(await JobQueue.total_len())

        data_amount.labels(getway="baidunews").set(await News._db.estimated_document_count())
        data_amount.labels(getway="wscourt").set(await Case._db.estimated_document_count())
        data_amount.labels(getway="trademark").set(await Trademark._db.estimated_document_count())
        data_amount.labels(getway="wxnews").set(await WxNews._db.estimated_document_count())
        data_amount.labels(getway="pkulaw").set(await Law._db.estimated_document_count())
        data_amount.labels(getway="pkulaw_chl").set(await Law._db.count_documents({"lib": "chl"}))
        data_amount.labels(getway="pkulaw_lar").set(await Law._db.count_documents({"lib": "lar"}))
        data_amount.labels(getway='pkulawapi').set(await LawPkulawDetail._db.estimated_document_count())
        data_amount.labels(getway='pkulawapi_chl').set(await LawPkulawDetail._db.count_documents({"lib": "chl"}))
        data_amount.labels(getway='pkulawapi_lar').set(await LawPkulawDetail._db.count_documents({"lib": "lar"}))
        data_amount.labels(getway='zhihuask').set(await ZhihuAsk._db.estimated_document_count())
        data_amount.labels(getway='tips_beijing').set(await Tips._db.estimated_document_count())
        data_amount.labels(getway='google_search_result_keywords_top10').set(await GoogleSearchResult._db.estimated_document_count())
        data_amount.labels(getway='zhihucolumn_summary').set(await ZhihuColumnSummary._db.estimated_document_count())
        data_amount.labels(getway='zhihucolumn_article').set(await ZhihuColumnArticle._db.estimated_document_count())
        data_amount.labels(getway='high_frequency').set(await HighFrequency._db.estimated_document_count())
        data_amount.labels(getway='roll_news').set(await News._db.count_documents({"getway": 'roll_news'}))
        data_amount.labels(getway='guiding_case').set(await GovCase._db.estimated_document_count())
        data_amount.labels(getway='bs_case').set(await BSCase._db.count_documents({"getway": 'bs_case'}))

        for spider in SPIDER_CONF:
            if spider == "default":
                continue
            seeds_amount.labels(getway=spider).set(
                await Seed._db.count_documents({"getway": spider}))
            zero_seeds_amount.labels(getway=spider).set(
                await Seed._db.count_documents({"getway": spider, "crawled_times": 0}))

        # 统计最近各个源前1、3、7天前入库情况
        await _record_recent_status("baidunews", News)
        await _record_recent_status("wscourt", Case)
        await _record_recent_status("trademark", Trademark)
        await _record_recent_status("wxnews_", WxNews)
        await _record_recent_status("laws", Law)
        await _record_recent_status("zhihuask", ZhihuAsk)
        await _record_recent_status("tips_beijing", Tips)
        await _record_recent_status("google_search_result_keywords_top10", GoogleSearchResult)
        await _record_recent_status("zhihucolumn_summary", ZhihuColumnSummary)
        await _record_recent_status("zhihucolumn_article", ZhihuColumnArticle)
        await _record_recent_status("high_frequency", HighFrequency)
        await _record_recent_status("bs_case", BSCase)

        # 统计最近3天**发布**的新闻数量
        await _record_recent_published("baidunews", News)
        await _record_recent_published("wxnews", WxNews)


async def _record_recent_status(getway, model, conds=None):
    if random.randint(0, 10) == 1:
        conds = conds or {}
        param_1 = dict(
            **{"create_time": {"$gte": timestamp() - relativedelta(days=1)}, **conds})
        data_amount.labels(getway=f'{getway}_before_1day').set(await model._db.count_documents(param_1))
        param_3 = dict(
            **{"create_time": {"$gte": timestamp() - relativedelta(days=3)}, **conds})
        data_amount.labels(getway=f'{getway}_before_3day').set(await model._db.count_documents(param_3))
        param_7 = dict(
            **{"create_time": {"$gte": timestamp() - relativedelta(days=7)}, **conds})
        data_amount.labels(getway=f'{getway}_before_7day').set(await model._db.count_documents(param_7))


async def _record_recent_published(getway, model):
    """统计最近3天发布的新闻数量"""
    if random.randint(0, 10) == 1:
        conds = {"pubtime": {"$gte": timestamp() - relativedelta(days=3)}}
        data_amount.labels(getway=f"{getway}_recent_published").set(await model._db.count_documents(conds))
