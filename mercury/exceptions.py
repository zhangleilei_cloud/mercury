# -*- coding: utf-8 -*-


class GooseEncodeError(Exception):
    pass


class PkulawApiMaxAccessTimes(Exception):
    """超出最大访问次数
    """
    pass
