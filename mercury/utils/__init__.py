# -*- coding: utf-8 -*-

import base64
import hashlib
import random
from binascii import hexlify
from datetime import datetime
from Crypto.Cipher import AES
from urllib.parse import urlparse, parse_qsl, urlencode, urlunparse

import demjson
from csiphash import siphash24


def md5(obj):
    """计算字符串的 md5 值，可接受 Unicode 或 bytes 对象，
    避免代码中多次自行做类型转换
    """
    if isinstance(obj, str):
        obj = obj.encode('utf8')
    elif not isinstance(obj, bytes):
        raise TypeError("MD5 input must be string.")
    return hashlib.md5(obj).hexdigest()


def siphash(obj):
    """利用SipHash-2-4算法生成固定长度(64bit)哈希值
    该算法兼顾性能和安全性，是Python、Rust、Redis等
    组件内部Hash Table 实现过程中所采用的算法，稳定
    性有保证。
       效率上，经实测，比利用Python 3.7 内置的 md5
    算法对字符串(约5KB)计算摘要会快 3 倍，数据越大
    其加速效果越明显。
       非密码和安全用途的哈希计算建议采用此算法。
    """
    # 计算哈希所用的key，在此采用固定值
    key = b'71d3b56f89a4c2e0'
    if isinstance(obj, str):
        obj = obj.encode('utf8')
    elif not isinstance(obj, bytes):
        raise TypeError("SipHash input must be string.")
    return hexlify(siphash24(key, obj)).decode()


def timestamp(is_utc=True, is_int=False):
    """返回当前秒级时间戳

    :param is_utc: 是否返回UTC时间，否则返回当地(根据系统配置)时间
    :type is_utc: bool

    :param is_int: 是否返回整数形式，否则返回 datetime 对象
    :type is_int: bool

    :return: 整数或 datetime 对象形式的时间戳
    :rtype: int or datetime
    """
    t = datetime.utcnow() if is_utc else datetime.now()
    return int(t.timestamp()) if is_int else t


class lazyproperty:
    def __init__(self, func):
        self.func = func

    def __get__(self, instance, cls):
        if instance is None:
            return self
        else:
            value = self.func(instance)
            setattr(instance, self.func.__name__, value)
            return value


def calc_percent_weights(item_scores):
    """根据 scores 按百分比计算每个 item 的比重

    :param dict item_scores: 由 "元素名称:元素分数" 键值对组成的字典
    :return: 由 "(元素名, 比重值)" 二元对组成的列表
    :rtype: list

    例如 item_scores = {'A': 2, 'B': 5}
    则 'A' 的 weights 为 2/(2+5) = 0.285714286
    则 'B' 的 weights 为 5/(2+5) = 0.714285714
    返回值为 [('A', 0.285714286), ('B', 0.714285714)]
    """
    scores = item_scores.values()
    total_score = sum(scores)
    return list(zip(item_scores.keys(), [s/total_score for s in scores]))


def probability_select(item_probabilities):
    """按照给定概率选择元素，所有元素的概率和不强制等于1，每个元素是否被选择都是独立的。

    :param dict item_probabilities: 由 "元素名称:元素选中概率" 键值对组成的字典
    :return: 由被选中元素们的 "(元素名, 概率值)" 二元对组成的列表
    :rtype: list
    """
    return list(filter(
        lambda x: (random.uniform(0, 1) <= x[1]), item_probabilities.items()))


class AESCipher:

    def __init__(self, key):
        self.key = key[0:16]  # 只截取16位
        # 16位字符，用来填充缺失内容，可固定值也可随机字符串，具体选择看需求。
        self.iv = "0182068209920300"

    def __pad(self, text):
        """填充方式，加密内容必须为16字节的倍数，若不足则使用self.iv进行填充"""
        text_length = len(text)
        amount_to_pad = AES.block_size - (text_length % AES.block_size)
        if amount_to_pad == 0:
            amount_to_pad = AES.block_size
        pad = chr(amount_to_pad)
        return text + pad * amount_to_pad

    def __unpad(self, text):
        pad = ord(text[-1])
        return text[:-pad]

    def encrypt(self, raw):
        """加密"""
        raw = self.__pad(raw)
        cipher = AES.new(self.key, AES.MODE_CBC, self.iv)
        return base64.b64encode(cipher.encrypt(raw))

    def decrypt(self, enc):
        """解密"""
        enc = base64.b64decode(enc)
        cipher = AES.new(self.key, AES.MODE_CBC, self.iv)
        return self.__unpad(cipher.decrypt(enc).decode("utf-8"))


def batch(iterable, batch_size=1):
    """将可迭代对象按 batch_size 指定的大小分批次遍历
    即每次取出处理 batch_size 个元素
    """
    l = len(iterable)
    for ndx in range(0, l, batch_size):
        yield iterable[ndx:min(ndx + batch_size, l)]


def fast_json_decode(text, encoding='utf-8'):
    try:
        return json.loads(text, encoding=encoding)
    except:
        return demjson.decode(text, encoding=encoding)


def url_concat(url, args):
    """将url与查询参数拼接起来，支持原url已带参数或片段。

    ``args`` 可以是字典，也可以是(key, value)二元组构成的列表。
    后者用于支持同一参数名有多个值。

    >>> url_concat("http://example.com/foo", dict(c="d"))
    'http://example.com/foo?c=d'
    >>> url_concat("http://example.com/foo?a=b", dict(c="d"))
    'http://example.com/foo?a=b&c=d'
    >>> url_concat("http://example.com/foo?a=b", [("c", "d"), ("c", "d2")])
    'http://example.com/foo?a=b&c=d&c=d2'
    """
    if args is None:
        return url
    parsed_url = urlparse(url)
    if isinstance(args, dict):
        parsed_query = parse_qsl(parsed_url.query, keep_blank_values=True)
        parsed_query.extend(args.items())
    elif isinstance(args, list) or isinstance(args, tuple):
        parsed_query = parse_qsl(parsed_url.query, keep_blank_values=True)
        parsed_query.extend(args)
    else:
        err = "'args' parameter should be dict, list or tuple. Not {0}".format(
            type(args))
        raise TypeError(err)
    final_query = urlencode(parsed_query, safe=":/#?&=@[]!$&'()*+,;%")
    url = urlunparse((
        parsed_url[0],
        parsed_url[1],
        parsed_url[2],
        parsed_url[3],
        final_query,
        parsed_url[5]))
    return url
