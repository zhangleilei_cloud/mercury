"""借助ES查找文书中存在乱码的文书ID并记录并重新下载
"""

import redis
import elasticsearch

redis_client = redis.Redis(
    host='172.16.0.223', port=9379, db=7, password='acJ6Ue*4')

es_client = elasticsearch.Elasticsearch(
    [{'host': '172.16.0.75', 'port': '9222'}])

request_body = {
    "query": {
        "bool": {
            "filter": [],
            "must": [
                {
                    "query_string": {
                        "analyze_wildcard": True,
                        "default_field": "*",
                        "query": "seg_content: %s"
                    }
                }
            ],
            "must_not": [],
            "should": []
        }
    },
    "_source": ["docid"],
    "size": 2000,
}

DOCIDS = set()


def get_docids(rs):
    cases = rs['hits']['hits']
    docids = map(lambda doc: doc['_source']['docid'], cases)
    for docid in docids:
        DOCIDS.add(docid)
    print('[Total] docids: {}'.format(len(DOCIDS)))


def scroll(scroll_id):
    while scroll_id:
        rs = es_client.scroll(scroll_id, params={'scroll': '100m'})
        get_docids(rs)
        scroll_id = rs.get('_scroll_id')


def search(code):
    request_body['query']['bool']['must'][0]['query_string']['query'] = "seg_content: %s" % code
    rs = es_client.search(index='pipeline-cases-20181130',
                   doc_type='cases',
                   body=request_body,
                   params={'scroll': '100m'})

    total = rs['hits']['total']
    get_docids(rs)
    scroll_id = rs.get('_scroll_id')
    scroll(scroll_id)

def delete_keys(docids):
    counter = 0
    buffer = []
    for docid in docids:
        buffer.append(f'job:crawled:{docid}')
        if len(buffer) == 1024:
            r = redis_client.delete(*buffer)
            buffer.clear()
            counter += r
            print('delete keys: {}'.format(counter))

    if buffer:
        r = redis_client.delete(*buffer)
        buffer.clear()
        counter += r
        print('delete keys: {}'.format(counter))


def add_to_queue(docids):
    counter = 0
    buffer = []
    for docid in docids:
        buffer.append(docid)
        if len(buffer) == 1024:
            r = redis_client.sadd('mercury:wscourt:uncrawled:docids', *buffer)
            buffer.clear()
            counter += r
            print('add to re download queue: {}'.format(counter))

    if buffer:
        r = redis_client.sadd('mercury:wscourt:uncrawled:docids', *buffer)
        buffer.clear()
        counter += r
        print('add to re download queue: {}'.format(counter))


def main():
    invalid_codes = ('�', '', '\"??\"')
    for code in invalid_codes:
        search(code)

    delete_keys(DOCIDS)
    add_to_queue(DOCIDS)


if __name__ == '__main__':
    main()
