# -*- coding: utf-8 -*-

import sys
import os
import pprint
import demjson
import random
import requests
from datetime import datetime, timedelta


sys.path.insert(0, '../mercury')
from mercury.settings import DIR_PROJECT


def read(path):
    with open(path, 'r', encoding='utf-8') as f:
        for line in f:
            yield line.rstrip()


path_court = os.path.join(DIR_PROJECT, 'extras/case_seeds_court_fields.txt')
path_keyword = os.path.join(
    DIR_PROJECT, 'extras/case_seeds_keyword_fields.txt')

# 法院名称
courts = list(map(lambda x: '法院名称:{}'.format(x), read(path_court)))
# 关键词
keywords = list(map(lambda x: '关键词:{}'.format(x), read(path_keyword)))
# 案件类型
casetype = ['刑事案件', '民事案件', '行政案件', '赔偿案件', '执行案件']
# 法院层级
courtlevel = ['最高法院', '高级法院', '中级法院', '基层法院']
# 审判程序
trialprocess = ['一审', '二审', '再审', '非诉执行审查', '复核', '刑罚变更', '再审审查与审判监督', '其他']
# 文书类型
doctype = ['判决书', '裁定书', '调解书', '决定书', '通知书', '批复', '答复', '函', '令', '其他']


def _generate_days(start_day, end_day):
    start = datetime.strptime(start_day, '%Y-%m-%d')
    end = datetime.strptime(end_date, '%Y-%m-%d')
    days = (end - start).days + 1
    for day in range(1, days):
        today = end - timedelta(days=day)
        yield '裁判日期:{}'.format(today.strftime('%Y-%m-%d'))


def _generate_query(dates):
    for day in dates:
        random.shuffle(courts)
        for court in courts:
            yield '{},{}'.format(court, day)


def excute(querys, seedsnum):
    url = 'http://127.0.0.1:8888/seeds'
    payload = {
        "url": "http://wenshu.court.gov.cn/List/ListContent",
        "getway": "wscourt",
        "_sched_interval": 86400,
        "status": "OK",
        "priority": 3,
        "query": "",
        "payload": {
            "Index": 1,
            "Page": 20,
            "Order": "裁判日期",
            "Direction": "desc",
            "Param": None,
            "guid": None,
            "number": None,
            "vl5x": None
        },
        "anti_cfg": {
            "proxy": False,
            "cookie": False,
            "random_ua": False
        }
    }

    counter = 0
    for query in querys:
        payload['payload']['Param'] = query
        pprint.pprint(payload, depth=2)
        payload['payload'] = demjson.encode(payload['payload'])
        data = demjson.encode(payload, encoding='utf-8')
        try:
            r = requests.post(url, data=data)
            resp = r.json()
        except (KeyboardInterrupt, Exception) as e:
            break
        else:
            pprint.pprint(resp, depth=2)
            if resp['data']:
                counter += 1
                print('\n{}-th seed inserted\n'.format(counter))
                if counter == seedsnum:
                    break
        payload['payload'] = demjson.decode(payload['payload'])

    print('\ntotal seeds inserted: {}\n\n'.format(counter))


def main(start_date, end_date, seedsnum):
    dates = _generate_days(start_date, end_date)
    querys = _generate_query(dates)
    excute(querys, seedsnum)


if __name__ == '__main__':
    """
    2008-01-01 -> 2013-01-01: 886506
    2013-01-01 -> now: 4850W+
    2014-01-01 -> now: 4700W+
    2015-01-01 -> now: 4000W+
    """
    # $ python case_seeds_generate.py  开始日期(包含)  结束日期(不包含)  要插入的种子数量
    # $ python case_seeds_generate.py  2008-01-01    2018-01-01     100
    start_date, end_date, seednums = '2013-01-01', '2018-07-25', 999999999
    if len(sys.argv) == 4:
        start_date = sys.argv[1]
        end_date = sys.argv[2]
        seednums = int(sys.argv[3])
    main(start_date, end_date, seednums)
