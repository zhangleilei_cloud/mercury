# -*- coding: utf-8 -*-

import re
import sys
import asyncio
import demjson

from os.path import dirname, abspath, join

sys.path.insert(0, abspath(dirname(dirname(dirname(__file__)))))

import requests
from urllib.parse import quote

from mercury.libs import s3


def send_request(url):
    data = {
        "url": url,
        "getway": "baidunews",
        "_sched_interval": 86400,
        "status": "OK",
        "priority": 5,
        "query": "",
        "anti_cfg": {
            "proxy": True,
            "cookie": True,
            "random_ua": False
        }
    }
    dest_url = "http://127.0.0.1:8888/seeds"

    response = requests.post(dest_url, data=demjson.encode(data))
    print("[Response] %s" % url)
    return response.text


def load_keywords(path):
    with open(path, 'r', encoding='utf-8') as f:
        for line in f:
            keyword = line.strip()
            if keyword:
                yield keyword


def download_keywords(src, des):
    async def _download(src, des):
        try:
            await s3.download_file(src, des)
        except FileExistsError:
            pass

    loop = asyncio.get_event_loop()
    loop.run_until_complete(_download(src, des))


def process(keywords):
    for i, keyword in enumerate(keywords):
        keyword = quote(keyword)
        url = f"http://www.baidu.com/s?tn=news&rtt=4&bsst=1&cl=2&wd={keyword}&x_bfe_rqs=&x_bfe_tjscore=&tngroupname=organic_news&ac_drop=%28L%29&pn=0"
        print("[Request: <%d>]: %s" % (i, url))
        send_request(url)


def main():
    src = "extras/baidunews_keywords.txt"
    des = "/data/ftp/baidunews_keywords.txt"
    download_keywords(src, des)
    keywords = load_keywords(des)

    process(keywords)

if __name__ == "__main__":
    main()
