"""华律网问题恢复种子生成
"""

import pprint
import requests

import demjson

url_66law = 'http://www.66law.cn'

URL = 'http://127.0.0.1:8888/seeds'
POST_DATA = {
    "url": "",
    "getway": "hualv_question",
    "_sched_interval": 86400,
    "status": "OK",
    "priority": 5,
    "query": "",
    "anti_cfg": {
        "proxy": False,
        "cookie": True,
        "random_ua": False
    },
    "extra_data": {}
}

fields = {
    '安徽': '/question/ah/',
    '澳门': '/question/am/',
    '北京': '/question/bj/',
    '重庆': '/question/cq/',
    '福建': '/question/fj/',
    '甘肃': '/question/gs/',
    '广东': '/question/gd/',
    '广西': '/question/gx/',
    '贵州': '/question/gz/',
    '海南': '/question/hn/',
    '河北': '/question/hbei/',
    '黑龙江': '/question/hlj/',
    '河南': '/question/henan/',
    '湖北': '/question/hb/',
    '湖南': '/question/hun/',
    '江苏': '/question/js/',
    '江西': '/question/jx/',
    '吉林': '/question/jl/',
    '辽宁': '/question/ln/',
    '内蒙古': '/question/nmg/',
    '宁夏': '/question/nx/',
    '青海': '/question/qh/',
    '山东': '/question/sd/',
    '上海': '/question/sh/',
    '山西': '/question/sx/',
    '陕西': '/question/shanx/',
    '四川': '/question/sc/',
    '台湾': '/question/tw/',
    '天津': '/question/tj/',
    '香港': '/question/xg/',
    '西藏': '/question/xz/',
    '新疆': '/question/xj/',
    '云南': '/question/yn/',
    '浙江': '/question/zj/',
    '离婚': '/question/divorce/',
    '交通事故': '/question/jtsg/',
    '刑事辩护': '/question/xsbh/',
    '婚姻家庭': '/question/hyjt/',
    '工伤赔偿': '/question/shpc/',
    '劳动纠纷': '/question/ldjf/',
    '房产纠纷': '/question/fcjf/',
    '土地纠纷': '/question/nccb/',
    '债权债务': '/question/zwzt/',
    '医疗纠纷': '/question/yljf/',
    '拆迁安置': '/question/cqaz/',
    '合同纠纷': '/question/htjf/',
    '抵押担保': '/question/dydb/',
    '公司解散': '/question/gsqs/',
    '人身损害': '/question/rssh/',
    '保险理赔': '/question/bxlp/',
    '继承': '/question/ycjc/',
    '消费权益': '/question/xfqy/',
    '网络法律': '/question/wlfl/',
    '侵权': '/question/qinquan/',
    '综合': '/question/qt/',
    '行政': '/question/xz/',
    '建设工程': '/question/gcjz/',
    '招标投标': '/question/zbtb/',
    '知识产权': '/question/zscq/',
    '合伙联营': '/question/hhly/',
    '加盟维权': '/question/lsjm/',
    '金融证券': '/question/jrzq/',
    '银行': '/question/yhbx/',
    '反不正当竞争': '/question/bdjz/',
    '经济仲裁': '/question/jjzc/',
    '票据': '/question/pj/',
    '火灾赔偿': '/question/hzpc/',
    '取保候审': '/question/qshb/',
    '刑事自诉': '/question/xszs/',
    '行政复议': '/question/xsfy/',
    '行政诉讼': '/question/xzss/',
    '国家赔偿': '/question/gjpc/',
    '海关商检': '/question/hgsj/',
    '公安国安': '/question/gaga/',
    '公司犯罪': '/question/gsfz/',
    '毒品犯罪': '/question/dpfz/',
    '经济犯罪': '/question/jjfz/',
    '暴力犯罪': '/question/blfz/',
    '死刑辩护': '/question/sxbh/',
    '职务犯罪': '/question/zwfz/',
    '税务': '/question/gssw/',
    '兼并收购': '/question/gssg/',
    '股权纠纷': '/question/gfzr/',
    '改制重组': '/question/qygz/',
    '破产清算': '/question/pcjs/',
    '资产拍卖': '/question/zcpm/',
    '法律顾问': '/question/cngw/',
    '公司法': '/question/gsf/',
    '公司上市': '/question/gsss/',
    '融资借款': '/question/rzjk/',
    '新三板': '/question/xsb/',
    '股权激励': '/question/gqjl/',
    '海事海商': '/question/hsss/',
    '国际贸易': '/question/gjmy/',
    '招商引资': '/question/zsyz/',
    '外商投资': '/question/wstz/',
    '合资合作': '/question/hzhz/',
    'WTO事务': '/question/wto/',
    '涉外仲裁': '/question/swzc/',
    '涉外法律': '/question/shewai/',
    '新闻侵权': '/question/xwqq/',
    '旅游': '/question/lvyou/',
    '邮政储蓄': '/question/yzcx/',
    '水利电力': '/question/sldl/',
    '电信通讯': '/question/dxtx/',
    '高新技术': '/question/gxjs/',
    '个人独资': '/question/grdz/',
    '期货交易': '/question/qhjy/',
    '广告宣传': '/question/ggxc/',
    '经销代理': '/question/jxdl/',
    '求学教育': '/question/qxjy/',
    '环境污染': '/question/hjwr/',
    '倾销补贴': '/question/cxbt/',
    '工商查询': '/question/gscx/',
    '资信调查': '/question/zxdc/',
    '合同审查': '/question/htsc/',
    '调解谈判': '/question/tjtp/',
    '私人律师': '/question/srls/',
    '法律文书代写': '/question/wsdl/',
    '移民留学': '/question/ymlx/',
    '污染损害': '/question/wrsh/',
    '矿产资源': '/question/kuangchan/',
    '不限': '/question/list.aspx'}


def generate_urls():
    for (_, v) in fields.items():
        yield f'{url_66law}{v}'


def generate_querys(urls):
    for url in urls:
        POST_DATA['url'] = url
        yield POST_DATA


counter = 0


def make_request(querys):
    for query in querys:
        data = demjson.encode(query, encoding='utf-8')
        try:
            r = requests.post(URL, data=data)
            resp = r.json()
        except (KeyboardInterrupt, Exception) as e:
            return
        else:
            pprint.pprint(resp, depth=2)
            if resp['data']:
                global counter
                counter += 1
                print(f'\n{counter}-th seed inserted\n')


def main():
    urls = map(lambda url: f'{url_66law}{url}', generate_urls())
    querys = generate_querys(urls)
    make_request(querys)


if __name__ == '__main__':
    main()
