"""华律网-详情页面解析
"""

import re
import os
import hashlib
import logging
import concurrent.futures
from datetime import datetime

import pymongo
from pyquery import PyQuery

URI = 'mongodb://mercury:MercurY4dm1n!@172.16.0.224:38017/mercury'
CLIENT = pymongo.MongoClient(URI)
DB = CLIENT.get_database('mercury')['laws_question']

RE_PUB_TIME = re.compile(r'(?P<time>\d{4}-\d{2}-\d{2} \d{2}:\d{2})')
RE_REPLY_TIME = re.compile(r'(?P<time>\d{4}.\d{2}.\d{2} \d{2}:\d{2})')
RE_LIKE_COUNT = re.compile(r'''(?P<count>\d+)''')

url_66law = 'http://www.66law.cn'
url_detail = 'http://www.66law.cn/question/{0}.aspx'
base_path = '/data/storage/hualv/questions/'


logging.basicConfig(filename='parse_page.log',
                    filemode='w',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logFormatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger = logging.getLogger(__file__)
logger.setLevel(logging.DEBUG)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
logger.addHandler(consoleHandler)


def parse_detail(page):
    pq = PyQuery(page)

    alink = pq('a#js-collect')
    qid = alink.attr('data-objid')
    title = pq('a#js-collect').attr('data-title')

    phone, address, pubtime = None, None, None
    spans = pq('div.mt10.f12.s-ca > span.mr30')
    for span in spans:
        e = PyQuery(span)
        text = e.text()
        if e('i'):
            phone = text
        elif RE_PUB_TIME.search(text):
            pubtime = text
        elif '-' in text:
            address = text

    field = pq('div.mt10.f12.s-ca > span:last').text()
    detail = pq('p.mt10.f18.lh32.s-c6').text()
    replies = []
    lis = pq('ul.reply-list > li')
    for li in lis:
        e = PyQuery(li)
        reply = e('div.a.clearfix > div.fr.tr > p.s-cb.f12').text()
        r = RE_REPLY_TIME.search(reply)
        reply_time = r.group(0) if r else None
        img_url = e('div.a.clearfix > a.img-block.fl > img').attr('src')
        lawyer = e('div.a.clearfix > p.ovh > a.name.heiti-std').text()
        name = lawyer[:-2] if lawyer.endswith('律师') else lawyer
        refer_me = e(
            'div.a.clearfix > p.ovh > a.btn.btn-red.zx-btn').attr('href')
        refer = f'{url_66law}{refer_me}' if refer_me else None
        content = e('p.b').text()
        like = e('div.mt20.lh28.clearfix > span.s-cb').text()
        r = RE_LIKE_COUNT.search(like)
        like_count = int(r.group(0)) if r else None
        lawyer = {
            'name': name,
            'photo': f'http:{img_url}',
            'refer': refer,
            'content': content,
            'reply_time': reply_time,
            'like_count': like_count,
        }
        replies.append(lawyer)

    retval = {
        'qid': qid,
        'title': title,
        'phone': phone,
        'address': address,
        'pubtime': pubtime,
        'field': field,
        'detail': detail,
        'replies': replies,
        'getway': 'hualv_question',
    }
    return retval


def log_failed(filename):
    with open('parse_failed.log', 'a', encoding='utf-8') as f:
        f.write(f'{filename}\n')


def get_page(path):
    with open(path, 'r', encoding='utf-8') as f:
        page = f.read()
        return page


def insert_data(data):
    try:
        DB.insert_one(data)
    except Exception as e:
        logger.error(e, exc_info=True)
    else:
        return True


def process(i):
    path = f'{base_path}{i}.html'
    page = get_page(path)
    if page:
        data = parse_detail(page)
        if data:
            url = url_detail.format(i)
            urlmd5 = hashlib.md5(url.encode('utf8')).hexdigest()
            create_time = datetime.utcnow()
            update_time = create_time
            data['url'] = url
            data['urlmd5'] = urlmd5
            data['create_time'] = create_time
            data['update_time'] = update_time
            data['sync'] = 0
            insert_data(data)
            return True
    return False


def dispatch(filenames, worker):
    with concurrent.futures.ThreadPoolExecutor(max_workers=len(filenames)) as executor:
        for (fname, ret) in zip(filenames, executor.map(worker, filenames)):
            if ret:
                logger.debug(f'[Succeed]: {fname}')
            else:
                log_failed(fname)


def get_files(start=1, end=20000000):
    for i in range(start, end):
        if os.path.exists(f'{base_path}{i}.html'):
            yield i


def main(batch_size=8):
    files = get_files()
    buffer = []
    for i in files:
        buffer.append(i)
        if len(buffer) == batch_size:
            dispatch(buffer, process)
            buffer.clear()

    if buffer:
        dispatch(buffer, process)


if __name__ == '__main__':
    main()
