"""批量下载华律网问答页面
http://www.66law.cn/question/*
"""

import sys
import logging
import concurrent.futures

import requests

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'}

url_66law = 'http://www.66law.cn/question/{i}.aspx'



logging.basicConfig(filename='download.log',
                    filemode='a',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logFormatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger = logging.getLogger(__file__)
logger.setLevel(logging.DEBUG)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
logger.addHandler(consoleHandler)

proxies = {'http': 'http://172.16.0.222:3128'}

def fetch(url):
    try:
        r = requests.get(url, headers=headers, timeout=30)
    except:
        pass
    else:
        return r.text if r.status_code == 200 else None
    return None


def save(path, page):
    with open(path, 'w', encoding='utf-8') as f:
        f.write(page)


def log_failed(i):
    with open('failed.log', 'a', encoding='utf-8') as f:
        f.write(f'{i}\n')


def process(i):
    url = url_66law.format(i=i)
    page = fetch(url)
    if page:
        path = f'questions/{i}.html'
        save(path, page)
        logger.debug('[Download]: {0} - {1}'.format(i, len(page)))
        return True
    logger.debug('[Skip]: {0}'.format(i))
    return False


def dispatch(start, batch_size, worker):
    ids = range(start, start + batch_size)
    with concurrent.futures.ThreadPoolExecutor(max_workers=batch_size) as executor:
        for (i, r) in zip(ids, executor.map(worker, ids)):
            if not r:
                log_failed(i)


def main(start, end, batch_size=128):
    for i in range(start, end + 1, batch_size):
        dispatch(i, batch_size, process)


if __name__ == "__main__":
    start = int(sys.argv[1])
    end = int(sys.argv[2])
    main(start, end)
