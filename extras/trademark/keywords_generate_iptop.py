# -*- coding: utf-8 -*-

"""
    生成智高点商标网查询关键字
"""
import os
import re

import requests

from lxml import etree

DIR_SCRIPT = os.path.dirname(os.path.abspath(__file__))
# iptop 包含 {1.1.1：图像要素}的文件
features_iptop = os.path.join(DIR_SCRIPT, "features_iptop.txt")
# 只有图像要素的文件 将图像要素写入的文件
keywords_iptop = os.path.join(DIR_SCRIPT, "keywords_iptop.txt")

features_url = "https://www.iptop.cn/trademark/getElements.do"
headers = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 "
                  "(KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36"
}
proxy = {
    "http": "172.16.0.222:3128"
}


def make_request():
    response = requests.get(features_url, headers=headers, proxies=proxy)
    page = response.text
    html = etree.HTML(page)
    result_list = html.xpath("//div[@class='fig-three']//a/text()")

    feature_dict = {}
    for info in result_list:
        pattern = re.compile("\d+\.\d+\.\d+")
        num = pattern.match(info).group()
        feature = info.split(num)[-1]
        feature = feature.split("注:")[0].strip()
        feature_dict.update({num: feature})
    return feature_dict


# 解析iptop的{1.1.1: 图像要素}, return 图像要素
def extract_features():
    feature_dict = make_request()
    for keywords in feature_dict.values():
        yield keywords


def write_features():
    with open(keywords_iptop, "w+", encoding="utf-8") as ff:
        for features in extract_features():
            ff.write("{}\n".format(features))


if __name__ == "__main__":
    write_features()
