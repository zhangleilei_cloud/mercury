# -*- coding: utf-8 -*-

"""
    用于生成速标网查询关键字的文件
"""

import os

import demjson


DIR_SCRIPT = os.path.dirname(os.path.abspath(__file__))
keywords_path = os.path.join(DIR_SCRIPT, "baitu_feature.json")


def extract_features():
    with open(keywords_path, "r", encoding="utf-8") as ff:
        keywords_list = ff.read()
    keywords_list = demjson.decode(keywords_list)
    for keywords in keywords_list.values():
        yield keywords


def write_keywords():
    remove_dup = set()
    for line in extract_features():
        keywords_list = line.split("注:")[0].strip()
        keywords_list = keywords_list.split(",")
        for keywords in keywords_list:
            remove_dup.add(keywords)
    if "" in remove_dup:
        remove_dup.remove("")
    with open("keywords_subiao.txt", "w", encoding="utf-8") as f:
        for keywords in remove_dup:
            info = "{}\n".format(keywords)
            f.write(info)


if __name__ == "__main__":

    write_keywords()
