# -*- coding: utf-8 -*-

import os
import requests
from urllib import parse

import demjson


DIR_SCRIPT = os.path.dirname(os.path.abspath(__file__))
keywords_path = os.path.join(DIR_SCRIPT, "keywords_all.txt")


# 提取图片特征作为查询关键字
def extract_features():
    with open(keywords_path, "r", encoding="utf-8") as ff:
        for line in ff:
            yield line.rstrip()


def make_seedurl(keywords):
    base_url = "http://www.subiao.com/content"
    params = {"type": 4, "keywords": keywords}
    params = parse.urlencode(params)
    return base_url + "?" + params


def make_requests(seed_url):
    data = {
        "url": seed_url,
        "getway": "subiao",
        "_sched_interval": 86400,
        "status": "OK",
        "priority": "5",
        "anti_cfg": {
            "proxy": True,
            "cookie": False,
            "random_ua": False
            }
    }
    dest_url = "http://127.0.0.1:8888/seeds"
    data = demjson.encode(data, encoding="utf-8")
    print(" make_request:", seed_url)
    response = requests.post(dest_url, data=data)
    return response


# 生成指定数量的关键字查询
def generate_part(start, stop):
    count = 0
    all_seed_urls = []
    for keywords in extract_features():
        count += 1
        if count < start:
            continue
        origin_seed_url = make_seedurl(keywords)
        all_seed_urls.append(origin_seed_url)
        if count >= stop:
            return all_seed_urls


if __name__ == "__main__":

    # 从 start 开始 到stop 结束的  从1开始 （1，10） 10个关键字
    url_seeds = generate_part(1, 1)

    for url in url_seeds:
        make_requests(url)
