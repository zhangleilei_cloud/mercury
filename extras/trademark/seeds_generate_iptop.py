# -*- coding: utf-8 -*-

import os
import requests
from urllib import parse

import demjson

PAGE_SIZE = 20
DIR_SCRIPT = os.path.dirname(os.path.abspath(__file__))
keywords_path = os.path.join(DIR_SCRIPT, "keywords_all.txt")


def extract_keywords():
    with open(keywords_path, "r", encoding="utf-8") as ff:
        for line in ff.readlines():
            yield line.strip()


def make_seedurl(keywords, category=0, law_status=0, page=1):
    base_url = "https://www.iptop.cn/trademark/s"
    params = {
        "queryString": keywords,
        "type": 5,
        "sort": 0,
        "asc": "false",
        "page": page,
        "pageSize": 20,
        "year": "",
        "sell": "false",
        "registerEnable": "false",
    }
    # category 是从1开始的，有category一定有law_status(从0开始的，不能拿来判断)
    if category:
        params.update({
            "multiCat1Name": category,
            "lawStatus": law_status,
        })
    params = parse.urlencode(params)
    return base_url + "?" + params


def make_requests(seed_url):
    data = {
        "url": seed_url,
        "getway": "iptop",
        "_sched_interval": 86400,
        "status": "OK",
        "priority": "5",
        "anti_cfg": {
            "proxy": True,
            "cookie": False,
            "random_ua": False
        }
    }
    dest_url = "http://127.0.0.1:8888/seeds"
    data = demjson.encode(data, encoding="utf-8")
    print(" make_request:", seed_url)
    response = requests.post(dest_url, data=data)
    return response


def generate_part(start, stop):
    count = 0
    all_seed_urls = []
    for keywords in extract_keywords():
        count += 1
        if count < start:
            continue
        # 国际分类有45种，法律状态有0-14共15种
        for category in range(1, 46):
            for lawStatus in range(0, 15):
                origin_seed_url = make_seedurl(keywords, category, lawStatus)
                all_seed_urls.append(origin_seed_url)
        if count >= stop:
            return all_seed_urls


def extract_keywords_page_sum():
    with open(keywords_path, "r", encoding="utf-8") as ff:
        for words_dict in ff.readlines():
            words_dict = demjson.decode(words_dict)
            yield words_dict["keywords"], int(words_dict["page_sum"])


def generate_part_page_sum(start, stop):
    count = 0
    all_seed_urls = []
    for keywords, page_sum in extract_keywords_page_sum():
        count += 1
        if count < start:
            continue
        # 国际分类有45种，法律状态有0-14共15种
        if page_sum > 500:
            for category in range(1, 46):
                for lawStatus in range(0, 15):
                    origin_seed_url = make_seedurl(keywords, category, lawStatus)
                    all_seed_urls.append(origin_seed_url)
        else:
            origin_seed_url = make_seedurl(keywords)
            all_seed_urls.append(origin_seed_url)
        if count >= stop:
            return all_seed_urls


if __name__ == "__main__":

    # 关键字的格式: 每行只有词语或者一个字
    url_seeds = generate_part(1, 1)

    # 关键字的格式: 每行 {"keywords": "", "page_sum": ""} ==> 提前知道该关键字下的查询数量
    # url_seeds = generate_part_page_sum(1, 1)
    i = 0
    for url in url_seeds:
        if i == 0:
            i += 1
            continue
        make_requests(url)
        break
