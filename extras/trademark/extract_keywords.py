"""
文档使用：
    1, 运行 keywords_generate_iptop.py 在线得到 iptop 的 keywords
    2, 运行 keywords_generate_subiao.py 得到 subiao 的 keywords
    3, iptop 和 subiao 拆分成单个汉字的处理
    4, 最后的关键字 = iptop(词组 + 单个字) + subiao(词组 + 单个字) and 去重
"""

import os


DIR_SCRIPT = os.path.dirname(os.path.abspath(__file__))
# 将词组或者短语解析成单个汉字：keywords_subiao/keywords_iptop
keywords_path_iptop = os.path.join(DIR_SCRIPT, "keywords_iptop.txt")
single_keywords_path_iptop = os.path.join(DIR_SCRIPT, "single_keywords_iptop.txt")
keywords_path_subiao = os.path.join(DIR_SCRIPT, "keywords_subiao.txt")
single_keywords_path_subiao = os.path.join(DIR_SCRIPT, "single_keywords_subiao.txt")

# 包含以上四个的全部关键词文档
keywords_all_path = os.path.join(DIR_SCRIPT, "keywords_all.txt")


# 将一个词组或者句子差分成一个一个的单词
def extract_keywords_single(keywords_path):
    with open(keywords_path, "r", encoding="utf-8") as ff:
        for line in ff.read():
            yield line.rstrip()


# 将解析出的字做去重处理
def extract_keywords(keywords_path):
    remove_dup = []
    for keywords in extract_keywords_single(keywords_path):
        for code in keywords:
            remove_dup.append(code)
    remove_dup = set(remove_dup)
    return remove_dup


# 最后的关键字处理结果写入文件
def write_file(keywords_list, single_keywords_path):
    with open(single_keywords_path, "w", encoding="utf-8") as ff:
        for word in keywords_list:
            ff.write("{}\n".format(word))


def all_keywords(path_list):
    keywords_list = set()
    # 读出四个文件的关键词
    for path in path_list:
        with open(path, "r", encoding="utf-8") as ff:
            for line in ff.readlines():
                keywords_list.add(line.strip())
    return keywords_list


def clean_word(words_list, fina_path):
    remove_list = [',', '.', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', ')', '）',
                   '(', '（', '，', ':', '：', '@', ' ', '；', '\n', '', ';', '*', '+', '/',
                   '*', '?', '？', '"']
    for key in remove_list:
        if key in words_list:
            words_list.remove(key)

    with open(fina_path, "w", encoding="utf-8") as ff:
        for words in words_list:
            ff.write("{}\n".format(words))


def clean_file(clean_path_list):
    for path in clean_path_list:
        os.remove(path)


if __name__ == "__main__":

    # keywords_iptop.txt ==> 单个汉字 single_keywords_iptop.txt
    keywords_list = extract_keywords(keywords_path_iptop)
    write_file(keywords_list, single_keywords_path_iptop)

    # keywords_subiao.txt ==> 单个汉字 single_keywords_subiao.txt
    keywords_list = extract_keywords(keywords_path_subiao)
    write_file(keywords_list, single_keywords_path_subiao)

    # 目前出现了四个关键词的文档，所有的词做去重处理后合成最终的一个文档 keywords_all.txt
    all_path_list = [keywords_path_iptop, keywords_path_subiao,
                     single_keywords_path_iptop, single_keywords_path_subiao]
    words_list = all_keywords(all_path_list)
    clean_word(words_list, keywords_all_path)
    # 删除中间文件
    clean_file(all_path_list)
