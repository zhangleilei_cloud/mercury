# -*- coding: utf-8 -*-

import datetime
import json
import re
import demjson
import requests
from urllib import parse
from bs4 import BeautifulSoup as bs


def make_url(keywords):
    params = {
        "type": "2",
        "s_from": "input",
        "query": keywords,
        "ie": "utf8",
        "_sug_": "n",
        "_sug_type": "",
    }
    domain = "https://weixin.sogou.com/weixin?"
    result = parse.urlencode(params)
    return domain + result


def get_data_num(url):
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko)"
                      " Chrome/68.0.3440.106 Safari/537.36"
    }

    response = requests.get(url, headers=headers)
    soup = bs(response.text, "lxml")
    num_tag = soup.select("div.news-box  div.mun")
    # 查无结果时没有 num_tag 避免解析错误
    if num_tag:
        data_text = num_tag[0].get_text()
        # 找到约6,442条结果  6,442 不能直接用 \d+ 匹配
        data_num = re.search(r"找到约(.*)条结果", data_text).group(1)
        data_num = int("".join(data_num.split(",")))
        return data_num


def _make_index_url(data_num, origin_url):
    # 解析到数量计算该以什么划分方式请求：天？周？年？
    # 主要是进行自定义的查询，一天内，一周内和一个月内，都只能进行从当天算起往前的一段时间
    # 根据数量计算这个关键词需要发出多少次请求
    if data_num <= 100:
        # 不拆分小请求，本次直接解析
        return [origin_url]
    elif 100 < data_num <= 300:
        # 按年请求 请求最近三年的  ft = 2017-8-17 to et = 2018-8-16
        return _make_request_url(origin_url, request_num=3, delta=2)
    elif 300 < data_num <= 3600:
        # 按月请求 请求最近三年 36个月的 ft = 2018-8-16 to et = 2018-7-17
        return _make_request_url(origin_url, request_num=36, delta=30)
    elif 3600 < data_num <= 10800:
        # 按周请求 请求最近两年 52 * 2
        return _make_request_url(origin_url, request_num=104, delta=7)
    else:
        # 按天请求 请求最近一年的，365天的请求
        return _make_request_url(origin_url, request_num=365, delta=0)


def _make_request_url(origin_url, request_num, delta=0, fromtime=datetime.datetime.now()):
    request_urls = []
    next_time = fromtime + datetime.timedelta(days=1)
    # 拿到查询的query
    query = re.search(r"query=(.*?)&", origin_url).group(1)
    for num in range(request_num):
        fromtime = next_time - datetime.timedelta(days=1)
        et = fromtime.strftime("%Y-%m-%d")
        next_time = fromtime - datetime.timedelta(days=delta)
        ft = next_time.strftime("%Y-%m-%d")
        # tsn 字段 1: 一天内  2: 一周内 3: 一月内 4: 一年内 5: 自定义时间
        params = {
            "type": "2",
            "ie": "utf8",
            "query": parse.unquote(query),
            "tsn": 5,
            "ft": ft,
            "et": et,
            "interation": "",
            "wxid": "",
            "usip": ""
        }
        domain = "https://weixin.sogou.com/weixin?"
        request_url = domain + parse.urlencode(params)
        request_urls.append(request_url)
    return request_urls


def send_request(url):
    data = {
        "url": url,
        "getway": "wxnews",
        "_sched_interval": 86400,
        "status": "OK",
        "priority": 3,
        "query": "",
        "anti_cfg": {
            "proxy": True,
            "cookie": True,
            "random_ua": False
        }
    }
    dest_url = "http://127.0.0.1:8888/seeds"
    data = demjson.encode(data, encoding="utf-8")
    response = requests.post(dest_url, data=data)
    print("send_request:", url)
    return response.text

def start(begin, stop):
    count = 0
    with open("keyword_data.txt", "r", encoding="utf-8") as ff:
        for words_dict in ff.readlines():
            count = count + 1
            if count < begin:
                continue
            elif count > stop:
                break
            else:
                words_dict = json.loads(words_dict)
                keywords = words_dict.get("keywords")
                data_num = words_dict.get("data_sum")
                origin_url = make_url(keywords)
                requests_urls = _make_index_url(int(data_num), origin_url)
                if requests_urls:
                    for url in requests_urls:
                        send_request(url)


if __name__ == "__main__":

    start(1, 1)
