# -*- coding: utf-8 -*-

import re
import sys
import asyncio
import demjson

from os.path import dirname, abspath, join

sys.path.insert(0, abspath(dirname(dirname(dirname(__file__)))))

import requests
from urllib.parse import quote

from mercury.libs import s3


def send_request(url):
    data = {
        "url": url,
        "getway": "wxnews",
        "_sched_interval": 86400,
        "status": "OK",
        "priority": 5,
        "query": "",
        "anti_cfg": {
            "proxy": True,
            "cookie": True,
            "random_ua": False
        }
    }
    dest_url = "http://127.0.0.1:8888/seeds"

    response = requests.post(dest_url, data= demjson.encode(data))
    print("[Response] %s" % url)
    return response.text


def load_keywords(path):
    RE_SPACE = re.compile(r'\s+')
    with open(path, 'r', encoding='utf-8') as f:
        for line in f:
            if line:
                keyword, num = RE_SPACE.split(line.strip())
                yield keyword

def download_keywords(src, des):
    async def _download(src, des):
        try:
            await s3.download_file(src, des)
        except FileExistsError:
            pass

    loop = asyncio.get_event_loop()
    loop.run_until_complete(_download(src, des))


def process(keywords):
    for i, keyword in enumerate(keywords):
        keyword = quote(keyword)
        url = f"https://weixin.sogou.com/weixin?type=2&s_from=input&query={keyword}&ie=utf8&_sug_=n&_sug_type_="
        print("[Request: <%d>]: %s" % (i, url))
        send_request(url)


def main():
    src = "extras/THUOCL_law.txt"
    # des = "/opt/metasota/mercury/extras/wxsg/THUOCL_law.txt"
    des = "/data/ftp/THUOCL_law.txt"
    download_keywords(src, des)
    keywords = load_keywords(des)

    process(keywords)

if __name__ == "__main__":
    main()
