import demjson
import requests

if __name__ == '__main__':

    roll_url = {"http://news.163.com/special/0001220O/news_json.js": {"content_type": "js",
                                                                      "publisher": "网易新闻"},
                "http://news.baidu.com/": {"content_type": "html",
                                           "publisher": "百度新闻"},

                "https://feed.mix.sina.com.cn/api/roll/get?pageid=153&lid=2509&k=&num=50&page=1": {
                    "content_type": "js",
                    "publisher": "新浪新闻"},
                "https://feed.mix.sina.com.cn/api/roll/get?pageid=153&lid=2509&k=&num=50&page=2": {
                    "content_type": "js",
                    "publisher": "新浪新闻"},
                "https://feed.mix.sina.com.cn/api/roll/get?pageid=153&lid=2509&k=&num=50&page=3": {
                    "content_type": "js",
                    "publisher": "新浪新闻"},
                "https://feed.mix.sina.com.cn/api/roll/get?pageid=153&lid=2509&k=&num=50&page=4": {
                    "content_type": "js",
                    "publisher": "新浪新闻"},
                "https://feed.mix.sina.com.cn/api/roll/get?pageid=153&lid=2509&k=&num=50&page=5": {
                    "content_type": "js",
                    "publisher": "新浪新闻"},
                "http://news.people.com.cn/210801/211150/index.js": {"content_type": "js",
                                                                     "publisher": "人民网"},
                "http://world.people.com.cn/GB/157278/index.html": {"content_type": "html",
                                                                    "publisher": "人民网"},
                "http://www.chinanews.com/scroll-news/news1.html": {"content_type": "html",
                                                                    "publisher": "中国新闻网"},
                "http://world.huanqiu.com/roll.html": {"content_type": "html",
                                                       "publisher": "环球网"},
                "http://china.huanqiu.com/roll.html": {"content_type": "html",
                                                       "publisher": "环球网"}
                }

    for url in roll_url:
        parm = {
            "url": url,
            "getway": "roll_news",
            "_sched_interval": 3600,
            "status": "OK",
            "priority": "10",
            "publisher": roll_url[url]['publisher'],
            "anti_cfg": {
                "proxy": True,
                "cookie": False,
                "random_ua": False
            },
            "extra_data": roll_url[url]
        }
        seed_url = "http://127.0.0.1:8888/seeds"
        data = demjson.encode(parm, encoding="utf-8")
        response = requests.post(seed_url, data=data)
