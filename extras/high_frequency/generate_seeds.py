# -*- coding: utf-8 -*-
import re
import demjson
import requests

if __name__ == '__main__':

    with open("index_url.txt", "r") as f:
        for line in f.readlines():
            url = re.sub(r'[\n\r]', "", line).strip()
            parm = {
                "url": url,
                "getway": "high_frequency",
                "_sched_interval": 86400,
                "status": "OK",
                "priority": "10",
                "anti_cfg": {
                    "proxy": True,
                    "cookie": False,
                    "random_ua": False
                }
            }
            seed_url = "http://127.0.0.1:8888/seeds"
            data = demjson.encode(parm, encoding="utf-8")
            print(" make_request:", seed_url)
            response = requests.post(seed_url, data=data)
