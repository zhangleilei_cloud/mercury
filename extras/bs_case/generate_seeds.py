# -*- coding: utf-8 -*-
import json
import demjson

if __name__ == '__main__':
    import requests

    session = requests.session()

    headers = {"Accept": "application/json, text/javascript, */*; q=0.01",
               "Accept-Encoding": "gzip, deflate",
               "Accept-Language": "zh-CN,zh;q=0.9",
               "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
               "Host": "www.lawsdata.com",
               "Origin": "http://www.lawsdata.com",
               "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36",
               "X-Requested-With": "XMLHttpRequest"}
    q = {'q': '{"all": "", "pageNo": 1, "sortField": "judgementDate", "sortOrder": "desc"}'}
    res = session.post("http://www.lawsdata.com/s/indictmentSearch", headers=headers, data=q, cookies={})
    count = 0
    if res.status_code == 200:
        datas = res.json()['facetFields']
        indictmentTypeId = datas['indictmentTypeId']
        judgementYear = datas['judgementYear']
        procuratorateLevel = datas['procuratorateLevel']
        procuratorateProvinceId = datas['procuratorateProvinceId']
        reason_ids = []
        querys = []
        for big_reason in datas['reasonId']:
            for reason in big_reason['children']:
                reason_ids.append(reason['id'])
        for reason_id in reason_ids:
            for level in procuratorateLevel:
                for province in procuratorateProvinceId:
                    q = {
                        'q': f'{"{"}"all": "诉", "reasonId": ["{reason_id}"], '
                        f'"procuratorateId": ["{level["id"]}"], '
                        f'"fuzzyMeasure": 0,'
                        f'"procuratorateProvinceId": ["{province["id"]}"],'
                        f' "pageNo": 1,"sortField": "procuratorateLevel","sortOrder": "desc"{"}"}'}
                    if count < 0:
                        continue
                    count += 1
                    payload = json.dumps(q, ensure_ascii=False)

                    url = "http://www.lawsdata.com/s/indictmentSearch"
                    parm = {
                        "url": url,
                        "getway": "bs_case",
                        "_sched_interval": 86400,
                        "status": "OK",
                        "priority": "10",
                        "ctype": "formurl",
                        "anti_cfg": {
                            "proxy": True,
                            "cookie": True,
                            "random_ua": False
                        },
                        "payload": payload
                    }
                    seed_url = "http://127.0.0.1:8888/seeds"
                    data = demjson.encode(parm)
                    response = requests.post(seed_url, data=data)
                    if count >= 500:
                        exit()
