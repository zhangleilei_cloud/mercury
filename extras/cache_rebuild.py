"""为指定爬虫重建缓存"""

from os.path import abspath, dirname
import sys
import asyncio

sys.path.insert(0, abspath(dirname(dirname(__file__))))

from mercury.settings import SPIDER_CONF
from mercury.libs.mongo import mgdb
from mercury.libs.redis import redis_pool
from mercury import spiders


async def load_crawled_jobs_to_cache(spider_class):
    """从指定的集合中获取唯一索引, 写入Redis中
    :param Spider spider_class: 爬虫类
    """

    Model = spider_class.Model
    uniqid = Model._uniqid
    db = Model._db

    cursor = db.find({}, {uniqid: 1, '_id': 0}, no_cursor_timeout=True)
    cursor.batch_size(4096)
    counter = 0
    async for doc in cursor:
        await redis_pool.set(f'job:crawled:{spider_name}:{doc[uniqid]}', 1)
        counter += 1
        if counter % 1000 == 0:
            print("{} items was inserted".format(counter))

    print('total inserted {} items'.format(counter))


def main(spider_name):
    """
    :param str spider_name: 爬虫名称
    """
    spider_conf = SPIDER_CONF[spider_name]
    Spider = getattr(spiders, spider_conf["spider"])
    loop = asyncio.get_event_loop()
    loop.run_until_complete(load_crawled_jobs_to_cache(Spider))


if __name__ == '__main__':
    spider_name = sys.argv[1]
    main(spider_name)
