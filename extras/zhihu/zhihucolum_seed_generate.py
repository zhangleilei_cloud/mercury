# -*- coding: utf-8 -*-
import re
import demjson
import requests

if __name__ == '__main__':
    with open("THUOCL_law.txt", "r", encoding="utf-8") as f:
        i = 0
        for line in f.readlines():
            if i < 0:
                i += 1
                continue
            i += 1
            if i >= 100:
                break
            keyword = re.sub(r'[0-9a-z]+', "", line).strip()
            parm = {
                "url": f"https://api.zhihu.com/search_v3?advert_count=0&correction=1&limit=20&offset=0&q={keyword}&show_all_topics=0&t=column&vertical_info=0,0,0,0,0,0,0,0,0,0",
                "getway": "zhihucolumn_summary",
                "_sched_interval": 86400,
                "status": "OK",
                "priority": "10",
                "anti_cfg": {
                    "proxy": True,
                    "cookie": False,
                    "random_ua": False
                }
            }
            seed_url = "http://127.0.0.1:8888/seeds"
            data = demjson.encode(parm, encoding="utf-8")
            print(" make_request:", seed_url)
            response = requests.post(seed_url, data=data)
