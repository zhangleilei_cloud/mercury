# -*- coding: utf-8 -*-
import re
import demjson
import requests

if __name__ == '__main__':
    with open("words_all.txt", "r", encoding="utf-8") as f:
        i = 0
        for line in f.readlines():
            if i < 40000:
                i += 1
                continue
            i += 1
            if i >= 60000:
                break
            keyword = re.sub(r'[0-9a-z]+', "", line).strip()
            parm = {
                "url": "https://www.zhihu.com/api/v4/search_v3?t=general&q=" + keyword + "&correction=1&offset=0&limit=10&show_all_topics=0&vertical_info=0,0,0,0,0,0,0,0,0,0",
                "getway": "zhihuask",
                "_sched_interval": 86400,
                "status": "OK",
                "priority": "10",
                "anti_cfg": {
                    "proxy": False,
                    "cookie": False,
                    "random_ua": False
                }
            }
            seed_url = "http://127.0.0.1:8888/seeds"
            data = demjson.encode(parm, encoding="utf-8")
            print(" make_request:", seed_url)
            response = requests.post(seed_url, data=data)
