
from flask import Flask, jsonify, Response, request
app = Flask(__name__)

import demjson


@app.route('/Db/LibraryRecordList', methods=['GET'])
def query():
    library = request.args.get('Library')
    pagesize = request.args.get('PageSize')
    pageindex = request.args.get('PageIndex')
    issuedate = request.args.get('Model.IssueDate')
    impldate = request.args.get('Model.ImplementDate')
    lastdate = request.args.get('Model.LastTimelinessChangeDate')
    token = request.args.get('token')
    data = {'libraray': library, 'pagesize': pagesize,
            'pageindex': pageindex,
            'issuedate': issuedate,
            'impldate': impldate,
            'lastdate': lastdate,
            'token': token}
    json = demjson.encode(data)
    resp = Response(json, mimetype='application/json')
    resp.headers['Access-Control-Allow-Headers'] = '*'
    resp.headers['Access-Control-Allow-Methods'] = 'GET, POST'
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Cache-Control'] = 'private'
    resp.headers['Content-Type'] = 'application/json; charset=utf-8'
    return resp


@app.route('/Db/GetSingleRecord', methods=['GET'])
def detail():
    library = request.args.get('Library')
    gid = request.args.get('Gid')
    token = request.args.get('token')
    data = {'library': library, 'gid': gid, 'token': token}
    json = demjson.encode(data)
    resp = Response(json, mimetype='application/json')
    resp.headers['Access-Control-Allow-Headers'] = '*'
    resp.headers['Access-Control-Allow-Methods'] = 'GET, POST'
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Cache-Control'] = 'private'
    resp.headers['Content-Type'] = 'application/json; charset=utf-8'
    return resp


@app.route('/add', methods=['GET'])
def add():
    a = int(request.args.get('a'))
    b = int(request.args.get('b'))
    token = request.args.get('token')
    c = a + b
    json = demjson.encode({'status': 'OK', 'result': c, 'token': token})
    resp = Response(json, mimetype='application/json')
    resp.headers['Access-Control-Allow-Headers'] = '*'
    resp.headers['Access-Control-Allow-Methods'] = 'GET, POST'
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Cache-Control'] = 'private'
    resp.headers['Content-Type'] = 'application/json; charset=utf-8'
    return resp


@app.route("/", methods=['GET'])
def hello():
    json = demjson.encode({'status': 'OK', 'msg': 'how are you'})
    resp = Response(json, mimetype='application/json')
    resp.headers['Access-Control-Allow-Headers'] = '*'
    resp.headers['Access-Control-Allow-Methods'] = 'GET, POST'
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Cache-Control'] = 'private'
    resp.headers['Content-Type'] = 'application/json; charset=utf-8'
    return resp


if __name__ == '__main__':
    app.run(debug=True)
