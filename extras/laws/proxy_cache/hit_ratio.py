"""统计北大法宝API的请求中缓存命中率
"""

import sys


def load_log(path):
    with open(path, 'r', encoding='utf-8') as f:
        for line in f:
            yield line


def statistics(lines):
    counter = 0
    hit = 0
    for line in lines:
        counter += 1
        status = line.rsplit(' ', 1)[-1]
        if status == '"HIT"':
            hit += 1

    print('total query: {}, hit: {}, ratio: {}'.format(
        counter, hit, hit / counter))


def main(path):
    lines = load_log(path)
    statistics(lines)


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except Exception:
        path = '/data/log/nginx/pkulaw.access.log'
    main(path)
