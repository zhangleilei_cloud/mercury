"""每天自动更新laws数据库中时效性
"""

from datetime import datetime
import pymongo

uri = 'mongodb://mercury:MercurY4dm1n!@172.16.0.224:38017,172.16.0.225:38017/mercury?replicaSet=rs0'
client = pymongo.MongoClient(uri)
LAWS = client.get_database('mercury')['laws']


def timestamp():
    return datetime.utcnow()


def main():
    today = datetime.today().strftime('%Y-%m-%d')
    filters = {'timelinessdic': '尚未生效', 'implementdate': {'$lte': today}}
    fields = {'_id': 1}
    cursor = LAWS.find(filters, fields)
    for i, doc in enumerate(cursor):
        _id = doc['_id']
        doc['timelinessdic'] = '现行有效'
        doc['sync'] = 0
        doc['update_time'] = timestamp()
        LAWS.update_one({'_id': _id}, {'$set': doc})
        print('[Updated]: %s - %d' % (_id, i + 1))

if __name__ == '__main__':
    main()
