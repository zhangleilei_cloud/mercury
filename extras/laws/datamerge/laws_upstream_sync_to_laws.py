"""
批处理
将通过贵州财经补全的laws_upstream中的数据移动到laws数据库中
"""

from os.path import abspath, dirname
import sys

sys.path.insert(0, abspath(dirname(dirname(dirname(dirname(__file__))))))

from mercury.spiders.laws.laws_helper import LAWS_UPSTREAM, LAWS
from mercury.settings import logger


def main():
    counter = 0
    filters = {'is_updated': False, 'needlogin': False}
    cursor = LAWS_UPSTREAM.find(filters).batch_size(16)
    for doc in cursor:
        _id = doc['_id']
        doc['sync'] = 0
        LAWS.update_one({"fbid": doc["fbid"]}, {"$set": doc})
        LAWS_UPSTREAM.update_one({'_id': _id}, {'$set': {'is_updated': True}})
        counter += 1
        print('[SYNC]: {}'.format(counter))


if __name__ == '__main__':
    main()
