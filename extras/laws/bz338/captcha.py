import time
import random
import hashlib

import requests

base_url = 'http://vpnlib.imu.edu.cn/captcha?{}'


def md5(obj):
    return hashlib.md5(obj).hexdigest()


def get_img(url):
    with requests.Session() as sess:
        r = sess.get(url)
        if r.status_code == 200:
            img = r.raw.read()
            return img


def save_img(path, data):
    with open(path, 'wb') as f:
        f.write(data)


def main():
    while True:
        time.sleep(1)
        rand = random.random()
        url = base_url.format(rand)
        img = get_img(url)
        md5val = md5(img)
        save_img('images/{}.jpg'.format(md5val), img)
        print('md5: {}'.format(md5val))


if __name__ == '__main__':
    main()
