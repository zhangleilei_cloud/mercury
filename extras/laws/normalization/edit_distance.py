import sys
import csv
from functools import partial
from collections import defaultdict


def log_result(path, result):
    with open(path, 'w', encoding='utf-8') as f:
        for item in result:
            title, title_group = item
            for (title2, distance) in title_group:
                f.write('{},{},{}\n'.format(title, title2, distance))
            f.write('=' * 100 + '\n')


def distance(word1, word2):
    m = len(word1)
    n = len(word2)
    table = [[0] * (n + 1) for _ in range(m + 1)]
    for i in range(m + 1):
        table[i][0] = i
    for j in range(n + 1):
        table[0][j] = j
    for i in range(1, m + 1):
        for j in range(1, n + 1):
            if word1[i - 1] == word2[j - 1]:
                table[i][j] = table[i - 1][j - 1]
            else:
                table[i][j] = 1 + min(table[i - 1][j],
                                      table[i][j - 1],
                                      table[i - 1][j - 1])
    return table[-1][-1]


def title_bucket(path):

    with open(path, 'r', encoding='utf-8') as f:
        reader = csv.reader(f)
        data = list(reader)

        bucket = defaultdict(list)
        invalid = []
        for item in stream:
            title, issuedate, implementdate = item
            if issuedate:
                bucket[issuedate].append(title)
            elif implementdate:
                bucket[implementdate].append(title)
            else:
                invalid.append(title)

        return bucket, invalid


def main(lib):

    laws_bucket, laws_invalid = title_bucket(
        '../laws_{}_normalize_title_not_in_legals.csv'.format(lib))
    legals_bucket, legals_invalid = title_bucket(
        '../legals_{}_normalize_title_not_in_laws.csv'.format(lib))

    print('laws bucket : {}, invalid: {}'.format(
        len(laws_bucket), len(laws_invalid)))
    print('legals bucket: {}, invalid: {}'.format(
        len(legals_bucket), len(legals_invalid)))

    retval = []
    for bucket in laws_bucket:
        if bucket in legals_bucket:
            title_src = laws_bucket[bucket]
            title_des = legals_bucket[bucket]
            for title in title_src:
                disfunc = partial(distance, word2=title)
                buffer = []
                for (title_cmp, dis) in zip(title_des, map(disfunc, title_des)):
                    buffer.append((title_cmp, dis))
                retval.append((title, buffer))

    log_result('laws_{}_legals_distance_result.log'.format(lib), retval)


if __name__ == '__main__':
    lib = sys.argv[1]
    main(lib)
