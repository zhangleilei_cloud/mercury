#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from os.path import abspath, dirname, join

suffix_path = join(abspath(dirname(__file__)), 'suffix2remove.txt')

class SegInfo:

    def __init__(self, text, words, tags):
        self.text = text
        self.words = words
        self.tags = tags

        offset = 0
        offset2indx = dict()
        for indx, word in enumerate(words):
            r = text[offset:].find(word)
            if r != 0:
                assert text[offset:][:r].strip() == ""
            assert r != -1
            for slot in range(offset + r, offset + r + len(word)):
                offset2indx[slot] = indx
            offset += r + len(word)
        self.offset2indx = offset2indx
        pass

    def offset_tag(self, offset):
        try:
            return self.tags[self.offset2indx[offset]]
        except Exception as e:
            return

    @classmethod
    def collapse(cls, text, seg):
        words = []
        tags = []
        offset = 0
        for word, tag in zip(seg["word"], seg["tag"]):
            r = text[offset:].find(word)
            if r == -1:
                break
            offset = offset + r + len(word)
            words.append(word)
            tags.append(tag)
        return dict(word=words, tag=tags)
    pass


class TitleNormalizer:

    CHAR_MAPPING = {
        "０": "0", "１": "1", "２": "2", "３": "3", "４": "4", "５": "5", "６": "6",
        "７": "7", "８": "8", "９": "9", "（": "(", "）": ")", "［": "[", "］": "]",
        "〔": "[", "〕": "]", "％": "%", "＋": "+", "／": "/", "＃": "#", "∕": "/",
        "＆": "&", "Ο": "〇", "О": "〇", "﹟": "#", "﹝": "[", "﹞": "]", "﹑": "、",
        "〖": "[", "〗": "]", "○": "〇",
        "ａ": "a", "ｂ": "b", "ｃ": "c", "ｄ": "d", "ｅ": "e", "ｆ": "f", "ｇ": "g",
        "ｈ": "h", "ｉ": "i", "ｊ": "j", "ｋ": "k", "ｌ": "l", "ｍ": "m", "ｎ": "n",
        "ｏ": "o", "ｐ": "p", "ｑ": "q", "ｒ": "r", "ｓ": "s", "ｔ": "t", "ｕ": "u",
        "ｖ": "v", "ｗ": "w", "ｘ": "x", "ｙ": "y", "ｚ": "z",
        'Ａ': 'A', 'Ｂ': 'B', 'Ｃ': 'C', 'Ｄ': 'D', 'Ｅ': 'E', 'Ｆ': 'F', 'Ｇ': 'G',
        'Ｈ': 'H', 'Ｉ': 'I', 'Ｊ': 'J', 'Ｋ': 'K', 'Ｌ': 'L', 'Ｍ': 'M', 'Ｎ': 'N',
        'Ｏ': 'O', 'Ｐ': 'P', 'Ｑ': 'Q', 'Ｒ': 'R', 'Ｓ': 'S', 'Ｔ': 'T', 'Ｕ': 'U',
        'Ｖ': 'V', 'Ｗ': 'W', 'Ｘ': 'X', 'Ｙ': 'Y', 'Ｚ': 'Z',
        '～': '~', '―': '-', '—': '-', '－': '-', '─': '-', '￣': '-',
        '﹤': '〈', '﹥': '〉', '＜': '〈', '＞': '〉'
    }

    def __init__(self, suffix_path):
        self.suffix_regex = [line.strip().split(" ", 1)
                             for line in open(suffix_path)]
        pass

    @staticmethod
    def char_map(x):
        if not x:
            return x
        x = ''.join([TitleNormalizer.CHAR_MAPPING.get(c, c) for c in x])
        return x

    def normalize_hyphen(self, x):
        x = x.replace('------', '--').replace('-----', '--')
        x = x.replace('----', '--').replace('---', '--')
        x = x.replace("＜＜", "《").replace("＞＞", "》")
        x = x.replace("<<", "《").replace(">>", "》")
        x = x.replace("&#8226;", '·').replace("&szlig;", 'ß')
        x = x.replace("&#12539;", '·').replace("&#8203;", '')
        x = x.replace("&#14294;", '㟖').replace("&reg;", "®")
        x = x.replace('&Auml;', 'Ä').replace('&Uuml;', 'Ü')
        x = x.replace('&shy;', '-').replace('&amp;', '&')
        while True:
            l, r = x.find("<"), x.find(">")
            if l == -1 or r == -1:
                break
            if "《" in x[:l] and "》" in x[r:]:
                x = x.replace("<", '〈').replace(">", "〉")
            else:
                break
        return x

    def normalize_batch(self, hits):
        for hit in hits:
            title = hit.nm_title
            seg_title = hit.seg_title
            nm_title, succ = self.normalize(title)
            hit.nm_title = nm_title
            if succ:
                hit.seg_title = SegInfo.collapse(nm_title, seg_title)
            if not succ:
                hit.warnings += "title normlization error\n"
        pass

    TAIL_PUNCTS = set("""＂』X！|$﹑>×=﹟／ъε:”μ―：〖＜,＋-О〉の％「﹥+﹝〕‰［?éΔ【》○，ω】(＆`．φ¨●『－≤…﹤￣*/)《Ο&’～"★±＞Ф＃Ι·？］[〈Ш〗─;、；∶〔\\β。<‘~∕]α）#＇–γ.—“（ü""")

    def normalize(self, title):

        if not title:
            return title, False

        if len(set(title) & TitleNormalizer.TAIL_PUNCTS) == 0:
            return TitleNormalizer.char_map(title), True

        if title[-1] not in TitleNormalizer.TAIL_PUNCTS:
            return self.normalize_hyphen(TitleNormalizer.char_map(title)), True

        title = TitleNormalizer.char_map(title)
        offset = len(title)
        while True:
            end = len(title[:offset].strip())
            if end == 0:
                return self.normalize_hyphen(title), False
            if title[end - 1] not in TitleNormalizer.TAIL_PUNCTS:
                return self.normalize_hyphen(title[:end]), True
            hit = False
            for action, reg in self.suffix_regex:
                m = re.search(reg, title[:end])
                if m:
                    hit = True
                    offset = m.span()[0]
                    break
                pass
            if hit and action == "k":
                return self.normalize_hyphen(title[:end]), True
            if not hit:
                m = re.search("的(通知|通告|批复|决定|公告|意见|复函|通报|函|答复|答复意见)\([^\(^\).]+\)$", title[:end])
                m1 = re.search("的(通知|通告|批复|决定|公告|意见|复函|通报|函|答复|答复意见)\([^\(^\).]+\([^\(^\).]+\)[^\(^\).]+\)$", title[:end])
                m2 = re.search("的(通知|通告|批复|决定|公告|意见|复函|通报|函|答复|答复意见)各.*$", title[:end])
                if m:
                    title = title[:m.span()[0] + 1 + len(m.group(1))]
                    return self.normalize_hyphen(title), True
                elif m1:
                    title = title[:m1.span()[0] + 1 + len(m1.group(1))]
                    return self.normalize_hyphen(title), True
                elif m2:
                    title = title[:m2.span()[0] + 1 + len(m2.group(1))]
                    return self.normalize_hyphen(title), True
                else:
                    return self.normalize_hyphen(title), False
                pass
            pass
        return "", False

    pass


title_normalizer = TitleNormalizer(suffix_path)


def normalize_title(title):
    nm_title, succ = title_normalizer.normalize(title)
    return nm_title if succ else title
