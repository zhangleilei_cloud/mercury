"""法规数据备份脚本
crontab 任务
0 4 * * * cd /data/backup/ && python /data/backup/backup.py backup
"""

import re
import os
import sys
import logging
import socket
import subprocess
from datetime import datetime, timedelta

import pymongo

base_path = "/data/backup/mercury"

mongodump_str = ('mongodump --host=127.0.0.1 --port=38017 --username=mercury '
                 '--password="MercurY4dm1n!" --db=mercury --collection=laws '
                 '--out={0} --forceTableScan')
mongorestore_str = ('mongorestore --port=38017 --username=mercury '
                    '--password="MercurY4dm1n!" --db=mercury --collection=laws '
                    '--dir={0}')


RE_BACKUP = re.compile(r"laws_\d{8}\.tar\.gz")
BACKUP_INTERVAL = 15


mongo_uri = "mongodb://mercury:MercurY4dm1n!@{0}:38017/mercury"


def _get_local_ip():
    return socket.gethostbyname(socket.gethostname())


def is_secondary_node():
    ip = _get_local_ip()
    mongo_client = pymongo.MongoClient(mongo_uri.format(ip))
    db = mongo_client.get_database("mercury")
    result = db.command("ismaster")
    return result["secondary"]


def _utc_8_time():
    return datetime.utcnow() + timedelta(hours=+8)


def _two_week_before():
    return _utc_8_time() + timedelta(days=-14)


def _today():
    return _utc_8_time().strftime("%Y%m%d")


today = _today()


logging.basicConfig(
    filename='laws_backup_{0}.log'.format(today),
    filemode='w',
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger = logging.getLogger(__file__)
logger.setLevel(logging.DEBUG)


def execute(cmd_str):
    logger.info("command: %s", cmd_str)
    p = subprocess.Popen(cmd_str, shell=True, stdout=subprocess.PIPE)
    ret = p.wait()
    if ret == 0:
        logger.info("command execute succeed")
    else:
        logger.info("command execute failed")
    return ret == 0


def delete_old_backup():
    logger.info("delete old backup")
    backups = [f for f in os.listdir(base_path) if RE_BACKUP.search(f)]
    for fname in backups:
        before = datetime.strptime(
            fname.split(".")[0].split("_")[-1], "%Y%m%d")
        if before + timedelta(BACKUP_INTERVAL) < _utc_8_time():
            os.remove(os.path.join(base_path, fname))
            logger.info("delete backup %s", fname)


def remove_temp_backup():
    logger.info("remove temp backup")
    command = "rm -rf {}/mercury".format(base_path)
    execute(command)


def zip_backup():
    logger.info("zip backup")
    command = "cd {0} && tar -zcvf laws_{1}.tar.gz mercury".format(
        base_path, today)
    retval = execute(command)
    if retval:
        logger.info("zip backup succeed")
        remove_temp_backup()
    else:
        logger.info("zip backup failed")


def decompress_backup():
    logger.info("decompress backup")
    command = "cd {0} && tar -xzvf laws_{1}.tar.gz".format(base_path, today)
    retval = execute(command)
    if retval:
        logger.info("decompress backup succeed")
        return True
    else:
        logger.info("decompress backup failed")
        return False


def backup():
    logger.info("start backup")
    if os.path.exists("{}/mercury".format(base_path)):
        execute("rm -rf {}/mercury".format(base_path))

    command = mongodump_str.format(base_path)
    retval = execute(command)
    if retval:
        logger.info("backup succeed")
        zip_backup()
    else:
        logger.info("backup failed")
    delete_old_backup()


def restore(date_str):
    logger.info("start restore")
    if os.path.exists("{}/mercury".format(base_path)):
        remove_temp_backup()
    path = "{0}/laws_{1}.tar.gz".format(base_path, date_str)
    if not os.path.exists(path):
        logger.info("backup didn't exists")
        return

    if decompress_backup():
        command = mongorestore_str.format(
            "{}/mercury/laws.bson".format(base_path))
        retval = execute(command)
        if retval:
            logger.info("restore succeed")
        else:
            logger.info("restore failed")
    remove_temp_backup()


def main(args):
    if not is_secondary_node():
        logger.info("not secondary node, can't backup or restore")
        return

    if len(args) == 3 and args[1] == "restore":
        restore(args[2])
    elif len(args) == 2 and args[1] == "backup":
        backup()
    else:
        logger.info("nothing to do")


if __name__ == "__main__":
    # 恢复某一天的数据 python main.py restore 20190308
    # 备份当前的数据 python main.py backup
    main(sys.argv)
