# -*- coding: utf-8 -*-

"""
扫描 lawupdate.chinalawinfo.com 站点上的更新包
"""

import requests

from dateutil import parser


# 已确定 Law15 路径下的包是主要更新源
dbs = {"Law12", "Law15", "Law16", "Law17", "Law18"}
base_url = "http://lawupdate.chinalawinfo.com/UploadPacket/{0}/{1}.rar"


def scan_one(db, packindex):
    url = base_url.format(db, packindex)

    # 用 HTTP head 动词请求包路径, 获取资源的meta信息
    resp = requests.head(url, headers={
        "User-Agent": ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) "
                       "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69"
                       ".0.3497.100 Safari/537.36"}))

    headers = resp.headers

    is_ok = resp.status_code == 200
    is_package = headers.get("Content-Type") == "application/octet-stream"

    if  is_ok and is_package:
        line = "{datetime}, {url}, {etag}, {length} \n"
        d = parser.parse(headers.get("Last-Modified"))
        line = line.format(
            datetime=d.strftime("%Y%m%d.%X.%Z"),
            url=url,
            etag=headers.get("ETag"),
            length=headers.get("Content-Length")
        )
        # 将扫描到的合法下载路径写入文件
        fname = "urls_{}.txt".format(db.lower())
        with open(fname, "a+") as f:
            f.write(line)
            print(line)


def main(start, stop):
    for db in dbs:
        for index in range(start, stop):
            scan_one(db, index)
        print("========={} END ===========".format(db))


if __name__ == "__main__":
    # 扫描第0~3000号包
    main(0, 3000)
