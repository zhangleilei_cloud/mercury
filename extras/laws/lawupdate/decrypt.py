# -*- coding: utf-8 -*-
#!/usr/bin/env python3
"""
北大法宝 4.0 网络版 trsj 文件解密
"""
import re
from pathlib import Path


def decrypt_pkulaw(src, des):
    """将 trsj 格式的目标文件 src 解密, 输出到 trs 格式的 des 文件中
    """
    fsrc = open(src, "rb")
    fdes = open(des, "wb+")
    encrypt_chunk = fsrc.read(1024)
    while encrypt_chunk:
        decrypt_chunk = _decrypt(encrypt_chunk)
        fdes.write(decrypt_chunk)
        encrypt_chunk = fsrc.read(1024)
    fsrc.close()
    fdes.close()


def _decrypt(data):
    data = bytearray(data)
    loop_counter = len(data) // 16 - 1
    offset = 6
    while loop_counter > 0:
        data[offset    ], data[offset + 4] = data[offset + 4], data[offset]
        data[offset + 5], data[offset + 1] = data[offset + 1], data[offset + 5]
        data[offset - 1], data[offset + 2] = data[offset + 2], data[offset - 1]
        data[offset + 3], data[offset + 6] = data[offset + 6], data[offset + 3]
        data[offset - 4], data[offset + 8] = data[offset + 8], data[offset - 4]
        data[offset - 3], data[offset + 9] = data[offset + 9], data[offset - 3]
        offset += 16
        loop_counter -= 1
    return bytes(data)


def decrypt_all(dir_src):
    """将 dir_src 目录下所有已解压的文件夹全部解密,
    解密后的文件放置于同级别路径下的 pkulaw_decrypts 文件夹中
    """
    p_src = Path(dir_src)
    for p_trsj in p_src.glob("29[789][0-9]/**/*.trsj"):
        p_trs = Path(str(p_trsj).replace(
            "pkulaw_unpacks", "pkulaw_decrypts")).with_suffix(".trs")
        p_trs.parent.mkdir(parents=True, exist_ok=True)
        decrypt_pkulaw(p_trsj, p_trs)
        print("Decrypt: {}".format(p_trsj))


counter = 0
def statistic(db="chl"):
    """根据 xxUpGidList.trs 统计某类法规的 gid 数量
    """
    global counter
    dir_src = "/Users/ju/Desktop/pkulaw_decrypts/"
    p_src = Path(dir_src)
    for p in p_src.glob("**/{0}UpGidList.trs".format(db)):
        with p.open(encoding="GB18030") as f:
            rets = []
            for line in f:
                res = re.search(r"<Gid>=(\d+\n)", line)
                if res:
                    gid = res.group(1)
                    rets.append(gid)
                    counter += 1
            with open("gids_of_{0}.txt".format(db), "a+") as ff:
                ff.writelines(rets)
        print("=========={} Finished========".format(p))
    print(counter)



if __name__ == '__main__':
    # statistic("lar")
    decrypt_all("/Users/ju/Desktop/pkulaw_unpacks/")
