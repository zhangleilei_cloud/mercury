"""法律数据一键恢复
"""

import sys
from os.path import abspath, join, dirname

sys.path.insert(0, abspath(dirname(dirname(dirname(dirname(__file__))))))

from mercury.spiders.laws.laws_helper import LAWS_ORIGIN, LAWS_UPSTREAM, LAWS

PKULAW_PATH = '/data/minio/mercury/pkulaw'
PKULAW_GZCJ_PATH = '/data/minio/mercury/pkulaw_gzcj'
PKULAW_XNDX_PATH = '/data/minio/mercury/pkulaw_xndx'
PKULAW_API_PATH = '/data/minio/mercury/pkulawapi'
TAXLAW_PATH = '/data/storage/pkulaw'
LAWPACKS_PATH = '/data/storage/pkulaw_decrypts'

BATCH_SIZE = 32


def parse_taxlaw():
    """解析从税务局抓取的数据
    """
    # from .taxlaws import run
    import taxlaws
    taxlaws.run(TAXLAW_PATH, BATCH_SIZE)


def parse_packs():
    """解析从更新包抓取的数据
    """
    # from .lawpacks import run
    import lawpacks
    lawpacks.run(LAWPACKS_PATH, BATCH_SIZE)


def generate_laws_origin():
    """解析税务局/更新包中的数据, 生成laws_origin集合
    """

    # 解析从税务局抓取的数据
    parse_taxlaw()

    # 解析从更新包抓取的数据
    parse_packs()


def generate_laws_upstream():
    """解析北大法宝v6数据, 生成laws_upstream集合
    """

    # from .pkulaw import run
    import pkulaw
    pkulaw.run(PKULAW_PATH, BATCH_SIZE)
    pkulaw.run(PKULAW_GZCJ_PATH, BATCH_SIZE)
    pkulaw.run(PKULAW_XNDX_PATH, BATCH_SIZE)
    pkulaw.run(PKULAW_API_PATH, BATCH_SIZE)


def parse():
    """页面解析
    """

    generate_laws_upstream()
    generate_laws_origin()


def merge():
    """数据合并
    """
    # from .match import run
    import match
    match.run()


def log_invalid(msg):
    with open('invalid_data.log', 'w', encoding='utf-8') as f:
        f.write(msg + "\n")


def validate_effectivenessdis():
    """check effectivenessdic
    """
    cursor = LAWS.find({}, {'_id': 1, 'title': 1, 'url': 1, 'effectivenessdic': 1}).batch_size(256)
    for doc in cursor:
        effectivenessdic = doc.get('effectivenessdic')
        if effectivenessdic and " " not in effectivenessdic:
            continue

        _id = doc['_id']
        title = doc.get('title')
        url = doc.get('url')
        log_invalid(f'[effectivenessdic]\t{str(_id)}\t{title}\t{url}')


def validate():
    """检查数据, 处理特殊数据
    """

    validate_effectivenessdis()


def index():
    """建索引
    """
    LAWS_ORIGIN.create_index([("gid", 1)], background=True, unique=True)
    LAWS_ORIGIN.create_index([("lib", 1)], background=True)
    LAWS_ORIGIN.create_index([("sync", 1)], background=True)

    LAWS_UPSTREAM.create_index([("fbid", 1)], background=True, unique=True)
    LAWS_UPSTREAM.create_index([("lib", 1)], background=True)
    LAWS_UPSTREAM.create_index([("sync", 1), ("needlogin", 1)], background=True)
    LAWS_UPSTREAM.create_index([("needlogin", 1)], background=True)


def main():
    # 0. 建立索引
    # 1. 页面解析
    # 2. 数据合并
    # 3. 特殊数据处理
    index()
    parse()
    merge()
    validate()


if __name__ == '__main__':
    main()
