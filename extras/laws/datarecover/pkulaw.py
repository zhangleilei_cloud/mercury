"""北大法宝V6页面提取
"""

import os
import sys
import gzip
import json
import logging
import concurrent.futures
from operator import itemgetter
from os.path import join, abspath, dirname
from pyquery import PyQuery

sys.path.insert(0, abspath(dirname(dirname(dirname(dirname(__file__))))))

from mercury.utils import md5, timestamp
from mercury.spiders.laws.laws_helper import (
    extract_content, LAWS_UPSTREAM, MEDIA_TYPES, RE_ATTACHMENT,
    convert_date_format, extract_sorted_field, validate_sorted_field,
    extract_pkulaw_v6_metainfo, extract_table_pre_img_link_content)


# 自定义logger对象
logging.basicConfig(filename='pkulaw.log', filemode='w',
                    format='%(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter(
    '%(asctime)s %(name)-4s %(levelname)-4s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


PKULAW_API_DETAIL_URL = 'http://api.pkulaw.com/Db/GetSingleRecord?Library={lib}&Gid={gid}'


def load_file(path):
    if path.endswith('.html'):
        with open(path, 'r', encoding='utf-8') as f:
            page = f.read()
            return page, 'html'
    elif path.endswith('.html.gz'):
        with gzip.open(path, 'rb') as f:
            page = f.read()
            return page, 'html'
    elif path.endswith('.json.gz'):
        with gzip.open(path, 'rb') as f:
            page = f.read()
            return page, 'json'
    return None, None

def extract_json(page):
    """从JSON数据中获取信息
    """

    Data = json.loads(page)
    data = Data['Data']

    # 正文内容为空, 跳过
    if not data.get('FullText') or not data.get('CheckFullText'):
        return None

    gid = data['Gid']
    lib = 'chl' if len(gid) < 36 else 'lar'
    url = PKULAW_API_DETAIL_URL.format(gid=gid, lib=lib)
    urlmd5 = md5(url)

    retval = {
        'url': url,
        'lib': lib,
        'getway': 'pkulawapi',
        'sid': urlmd5,
        'gid': gid,
        'title': data['Title'],
        'documentno': data['DocumentNO'],
        'issuedate': convert_date_format(data['IssueDate']),
        'implementdate': convert_date_format(data['ImplementDate']),
        'ratifydate': convert_date_format(data['RatifyDate']),
        'lasttimelinesschangedate': convert_date_format(data['LastTimelinessChangeDate']),
        'modifybasis': data['RevisedBasis'],
        'invalidbasis': data['FailureBasis'],
        'partinvalidbasis': data['PartialFailureBasis'],
        'ratifydepartment': validate_sorted_field(extract_sorted_field(data['RatifyDepartment']), 'd'),
        'issuedepartment': validate_sorted_field(extract_sorted_field(data['IssueDepartment']), 'd'),
        'category': validate_sorted_field(extract_sorted_field(data['Category']), 'c'),
        'effectivenessdic': validate_sorted_field(extract_sorted_field(data['EffectivenessDic']), 'e'),
        'timelinessdic': extract_sorted_field(data['TimelinessDic']),
    }

    retval['effectivenessdic'] = retval['effectivenessdic'].split(' ')[-1]
    content = data.get('FullText') or data.get('CheckFullText')
    if not content:
        return None
    res = extract_content(content)
    if res:
        res['pkulawinfos']['tips'] = data['FaBaoTips']
        retval.update(res)
        # convert gid to fbid
        retval['fbid'] = retval['gid']
        # set not need login
        retval['needlogin'] = False
        return retval
    return None


def extract_data(page, path):
    pq = PyQuery(page)
    # 验证页面
    if pq('#verify'):
        return None
    fbid = pq('input#ArticleId').val()
    if not fbid:
        fbid = path.rsplit('/', 1)[-1].rstrip('.gz').rstrip('.html')
    # lib
    lib = pq('input#DbId').val()
    # eg: 全国人大常委会关于修改《中华人民共和国个人所得税法》的决定(2018)-北大法宝V6官网
    title = pq('title').text().rsplit('-', 1)[0]

    # 非法页面
    if not fbid or not lib or not title:
        return None
    url = f'http://www.pkulaw.com/{lib}/{fbid}.html'
    urlmd5 = md5(url)

    _timestamp = timestamp()
    retval = {
        'url': url,
        'urlmd5': urlmd5,
        'sid': urlmd5,
        'getway': 'pkulaw',
        'fbid': fbid,
        'lib': lib,
        'title': title,
        'create_time': _timestamp,
        'update_time': _timestamp,
    }
    # 需要先登录才能查看完整信息
    if pq('#WeChatPayUrl').val():
        retval['needlogin'] = True

    mainblock = pq('div#divFullText')
    html = mainblock.html()
    content = extract_table_pre_img_link_content(html)

    mediatypes = [t for t in MEDIA_TYPES if mainblock(t)]
    hasattachments = bool(RE_ATTACHMENT.search(content))
    retval['content'] = content
    retval['mediatypes'] = mediatypes
    retval['hasattachments'] = hasattachments
    # 正文内容为空
    if not content:
        return None
    metainfo = extract_pkulaw_v6_metainfo(page, lib)
    retval.update(metainfo)
    return retval


def insert_doc(data):
    data['sync'] = 0
    LAWS_UPSTREAM.update_one({'fbid': data['fbid']}, {
                             '$set': data}, upsert=True)
    return True


def process_data(path):
    page, rtype = load_file(path)
    if page:
        if rtype == 'html':
            data = extract_data(page, path)
        elif rtype == 'json':
            data = extract_json(page)
        if data:
            return insert_doc(data)
    return False


def mutiple_process(worker, jobs):
    with concurrent.futures.ThreadPoolExecutor(max_workers=len(jobs)) as executor:
        for (job, ret) in zip(jobs, executor.map(worker, jobs)):
            if ret:
                logger.info(f'[Success]: {job}')
            else:
                logger.info(f'[Failed]: {job}')


def dispatch(sub_path, files, batach_size):
    buffer = []
    for fname in files:
        path = os.path.join(sub_path, fname)
        buffer.append(path)
        if len(buffer) == batach_size:
            mutiple_process(process_data, buffer)
            buffer.clear()

    if buffer:
        mutiple_process(process_data, buffer)
        buffer.clear()


def main(base_path, batch_size):
    folders = os.listdir(base_path)
    for folder in folders:
        sub_path = os.path.join(base_path, folder)
        files = os.listdir(sub_path)
        total = len(files)
        logger.info('[Total Files]: %s', total)
        dispatch(sub_path, files, batch_size)


def run(base_path='/data/minio/mercury/pkulaw', batch_size=32):

    main(base_path, batch_size)


if __name__ == '__main__':
    base_path = '/data/minio/mercury/pkulaw'
    batch_size = 32
    main(base_path, batch_size)
