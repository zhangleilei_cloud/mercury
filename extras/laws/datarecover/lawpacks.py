
import re
import os
import sys
import concurrent.futures
from datetime import datetime
from functools import partial
from os.path import join, dirname, abspath

sys.path.insert(0, abspath(dirname(dirname(dirname(dirname(__file__))))))

import ftfy
import demjson

from mercury.spiders.laws.laws_helper import (
    LAWS_ORIGIN, JS_FIELDS, TIMELINESSDIC, strip_expired_title, convert_date_format, extract_content)

RE_TRS_FIELDS = re.compile(r'<(?P<key>\w+)>=(?P<val>.*)', re.MULTILINE)


# 自定义logger对象
import logging
logging.basicConfig(filename='lawpacks.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s %(name)-4s %(levelname)-4s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


# 更新包中的字段名称和镜像站的字段名称不一致,需要重新指定
# 字段中英文对照
COMMON_FIELDS = {
    '法规标题': 'title',
    '类别': 'category',
    '发文字号': 'documentno',
    '批准部门': 'issuedepartment',
    '批准日期': 'ratifydate',
    '发布部门': 'issuedepartment',
    '发布日期': 'issuedate',
    '实施日期': 'implementdate',
    '时效性': 'timelinessdic',
    '效力级别': 'effectivenessdic',
    '唯一标志': 'gid',
    '全文': 'content'
}

FIELDS_MAPPING = {
    'chl': COMMON_FIELDS,
    'lar': COMMON_FIELDS,
    'iel': COMMON_FIELDS,
    'fmt': {
        '文书编号': 'documentno',
        '文书分类': 'category',
        '发布日期': 'issuedate',
        '发布部门': 'issuedepartment',
        '时效性': 'timelinessdic',
        '发文名称': 'documenttitle',
        '唯一标志': 'gid',
        '全文': 'content',
    },
    'hkd': {
        '分类': 'category',
        '英文标题': 'englishtitle',
        '唯一标志': 'gid',
        '全文': 'content',
    },
    'aom': {
        '法规分类': 'legalcategory',
        '类别': 'category',
        '刊登日期': 'issuedate',
        '相关信息': 'relationinfo',
        '唯一标志': 'gid',
        '全文': 'content',
    },
    'twd': {
        '类别': 'category',
        '颁布日期': 'issuedate',
        '唯一标志': 'gid',
        '全文': 'content',
    },
    'con': {
        '合同分类': 'category',
        '合同条数': 'number',
        '发布日期': 'issuedate',
        '发文字号': 'documentno',
        '时效性': 'timelinessdic',
        '行业类别': 'industrycategory',
        '字数区间': 'wordnumberrange',
        '发文名称': 'documenttitle',
        '合同编号': 'contractno',
        '官方合同': 'officialcontract',
        '唯一标志': 'gid',
        '全文': 'content',
    },
    'lfbj': {
        '标题': 'title',
        '发布部门': 'issuedepartment',
        '类别': 'category',
        '发布日期': 'issuedate',
        '唯一标志': 'gid',
        '全文': 'content',
        '效力级别': 'effectivenessdic',
    },
    'eag': COMMON_FIELDS,
    'eagn': {
        **COMMON_FIELDS,
        '种类': 'kind',
        '签订地点': 'signplace',
        '保存机关': 'preservedepartment',
        '交存时间': 'deliverydate',
        '中国生效时间': 'chinaeffectivedate',
        '适用于港澳': 'applyforhkmo',
        '失效日期': 'expiratedate',
        '部分失效日期': 'partialexpiredate',
        '签署时间': 'signdate',
        '声明与保留': 'declarationreservation',
        '修订日期': 'revisiondate',
        '广为熟知': 'widelyknown',
        '呈批部门': 'ratifydepartment',
    },
    'news': {
        '发布日期': 'issuedate',
        '来源': 'source',
        '作者': 'author',
        '关键词语': 'keywords',
        '唯一标志': 'gid',
        '全文': 'content',
    },
}


def convert_default(f, collection={}):
    return f


def convert_field(field, collection):
    retval = []
    vals = field.split('#')
    for val in vals:
        if val in collection:
            retval.append(collection[val])
        else:
            retval.append(val)
    return ' '.join(retval).strip()


def trs_parse_date(date_str):

    try:
        d = datetime.strptime(date_str, '%Y%m%d')
    except:
        try:
            d = datetime.strptime(date_str, '%Y%m')
        except:
            try:
                d = datetime.strptime(date_str, '%Y')
            except Exception as e:
                return convert_date_format(date_str)
            else:
                return d.strftime('%Y')
        else:
            return d.strftime('%Y-%m')
    else:
        return d.strftime('%Y-%m-%d')


def load_trs(path):
    with open(path, 'r', encoding='gb18030') as f:
        try:
            page = f.read()
        except:
            pass
        else:
            return page
    return None


def split_trs(trs):
    buffer = trs.split('<REC>')
    return map(lambda x: x.strip(), filter(lambda x: x.strip(), buffer))


def insert_doc(data, lib, i):
    data['sync'] = 0
    LAWS_ORIGIN.update_one({'gid': data['gid']}, {'$set': data}, upsert=True)
    return True


def update_doc(data, i, lib, uniqid):
    data['sync'] = 0
    retval = LAWS_ORIGIN.update_one({'gid': data['gid']}, {"$set": data}, upsert=True)
    return True


def convert_tr_shixiao_date(date_str):
    try:
        d = datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")
    except Exception as e:
        logger.error(e, exc_info=True)
    else:
        return d.strftime("%Y-%m-%d")
    return date_str


def change_tr_timelinessdic(tr):
    retval = dict(RE_TRS_FIELDS.findall(tr))

    fields = ('Gid', 'Name', 'Lib', 'shixiao_id_new')
    for f in fields:
        if not f in retval:
            return None
    lib = retval['Lib']
    # if not lib in LIBS:
    #     return None

    gid = retval['Gid']
    # title = retval['Name']
    shixiao = retval['shixiao_id_new']
    if shixiao:
        timelinessdic = TIMELINESSDIC[shixiao]
    else:
        timelinessdic = None
    updatetime = retval.get('ChangeTime') or retval.get('updatetime')
    if not updatetime:
        return None
    lasttimelinesschangedate = convert_tr_shixiao_date(updatetime)
    return {
        'gid': gid,
        # 'title': title,
        'timelinessdic': timelinessdic,
        'lasttimelinesschangedate': lasttimelinesschangedate
    }


def process_trs_shixiao(trs, i):
    retval = []
    for tr in trs:
        data = change_tr_timelinessdic(tr)
        if data:
            retval.append(data)
    return retval


def trs_link_lib_field_to_collection(lib, field, key):
    if lib == 'eagn':
        lib = 'eag'

    library = JS_FIELDS[lib]

    fields_processor = {
        'category': partial(convert_field, collection=library['category']),
        'ratifydepartment': partial(convert_field, collection=library['ratifydepartment']),
        'issuedepartment': partial(convert_field, collection=library['issuedepartment']),
        'timelinessdic': partial(convert_field, collection=library['timelinessdic']),
        'effectivenessdic': partial(convert_field, collection=library['effectivenessdic']),
        'ratifydate': trs_parse_date,
        'issuedate': trs_parse_date,
        'implementdate': trs_parse_date,
        'title': strip_expired_title,
        'gid': convert_default,
        'content': convert_default,
        'documentno': convert_default,
        'kind': convert_default,
        'signplace': convert_default,
        'preservedepartment': convert_default,
        'deliverydate': trs_parse_date,
        'chinaeffectivedate': trs_parse_date,
        'applyforhkmo': convert_default,
        'expiratedate': trs_parse_date,
        'partialexpiredate': trs_parse_date,
        'signdate': trs_parse_date,
        'declarationreservation': convert_default,
        'revisiondate': trs_parse_date,
        'widelyknown': convert_default,
    }
    return fields_processor[field](key)


def extract_fields(tr, lib):
    retval = dict()
    ret = RE_TRS_FIELDS.findall(tr)
    if ret:
        for (k, v) in ret:
            key, val = k.strip(), v.strip()
            if not key or not val:
                continue
            key = FIELDS_MAPPING[lib][key]
            val = val.strip('#')
            val = trs_link_lib_field_to_collection(lib, key, v.strip())
            retval[key] = val
    return retval


def extract_tr(tr, lib, i):
    tr = ftfy.fix_text(tr)
    pos = tr.find('<全文>=')
    if pos == -1:
        return None

    tr_fields = tr[:pos].strip()
    tr_fulltext = tr[pos+5:].strip()
    # 全文内容是空的 lar - 271
    if not tr_fulltext:
        return None

    if lib == 'eagn':
        pos = tr.rfind('<种类>=')
        if pos > -1:
            tr_fulltext = tr[:pos].strip()
            tr_fields += tr[pos:].strip()

    if not tr_fulltext:
        return None

    meta_fields = extract_fields(tr_fields, lib)
    content_fields = extract_content(tr_fulltext)

    retval = {}
    retval.update(meta_fields)
    retval.update(content_fields)

    retval['getway'] = 'package'
    retval['pkulawinfos']['source'] = '{i}-{lib}-{gid}'.format(
        i=i, lib=lib, gid=retval['gid'])

    return retval


def trs_process_tr(tr, lib,  i):
    data = extract_tr(tr, lib,  i)
    if data:
        r = insert_doc(data, lib, i)
        if r:
            return data['gid']
    return None


def extract_trs(worker, iterable, i, lib, uniqid=None):
    with concurrent.futures.ThreadPoolExecutor(max_workers=len(iterable)) as executor:
        worker = partial(worker, i=i, lib=lib)
        for retval in executor.map(worker, iterable):
            if retval:
                logger.info('[SUCCEED]: {}-{}-{}'.format(i, lib, retval))


def start_process_trs(lib, i, trs, batch_size):
    buffer = []
    for tr in trs:
        buffer.append(tr)
        if len(buffer) == batch_size:
            extract_trs(trs_process_tr, buffer, i, lib, uniqid='gid')
            buffer.clear()
    if buffer:
        extract_trs(trs_process_tr, buffer, i, lib, uniqid='gid')
        buffer.clear()


def newjob(base_path, lib, i):
    path = os.path.join(base_path, '{}/LawFile/{}u.trs'.format(i, lib))
    if os.path.exists(path):
        page = load_trs(path)
        if page:
            trs = split_trs(page)
            start_process_trs(lib, i, trs, 8)
        else:
            logger.info('[Invalid File]: {}'.format(path))


def process_multi_tr(func, iterable, lib, i, uniqid=None):
    with concurrent.futures.ThreadPoolExecutor(max_workers=len(iterable)) as executor:
        worker = partial(func, uniqid=uniqid, lib=lib, i=i)
        for item, ret in zip(iterable, executor.map(worker, iterable)):
            uid = item[uniqid] if uniqid else item
            if ret:
                logger.info('[SUCCEED]: {}-{}-{}'.format(i, lib, uid))
            else:
                logger.info('[FAILED]: {}-{}-{}'.format(i, lib, uid))


def timeline(base_path, i, batch_size=32):
    path = os.path.join(base_path, '{}/LawFile/synshixiao.trs'.format(i))
    if os.path.exists(path):
        page = load_trs(path)
        if page:
            trs = split_trs(page)
            retval = process_trs_shixiao(trs, i)
            if retval:
                buffer = []
                for item in buffer:
                    buffer.append(item)
                    if len(buffer) == batch_size:
                        process_multi_tr(update_doc, buffer,
                                         'timeline', i, uniqid='gid')
                        buffer.clear()
                if buffer:
                    process_multi_tr(update_doc, buffer, 'timeline', i)
                    buffer.clear()
        else:
            logger.info('[Invalid File]: {}'.format(path))


def dispatch(base_path, i, libs):
    for lib in libs:
        newjob(base_path, lib, i)
    timeline(base_path, i)


def start_task(base_path, libs, start, end):
    for i in range(start, end):
        dispatch(base_path, i, libs)


def main(base_path, libs, start, end):
    start_task(base_path, libs, start, end + 1)


def run(base_path='/data/storage/pkulaw_decrypts/', batch_size=32):
    libs = ['chl', 'lar', 'eag', 'eagn', 'iel', 'lfbj']
    start, end = 244, 3019
    main(base_path, libs, start, end)


if __name__ == '__main__':
    base_path = '/data/storage/pkulaw_decrypts/'
    # fnl 为案例指导 不录入
    libs = ['chl', 'lar', 'eag', 'eagn', 'iel', 'lfbj']
    start = int(sys.argv[1])
    end = int(sys.argv[2])
    main(base_path, libs, start, end)
