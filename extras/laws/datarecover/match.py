

import sys
import logging
from os.path import join, abspath, dirname

sys.path.insert(0, abspath(dirname(dirname(dirname(dirname(__file__))))))
sys.path.insert(0, abspath(dirname(dirname(__file__))))

import ftfy

from normalization.title_normalizer import normalize_title
from mercury.spiders.laws.laws_helper import LAWS_UPSTREAM, LAWS_ORIGIN, LAWS


# 自定义logger对象
logging.basicConfig(filename='match.log', filemode='w',
                    format='%(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter(
    '%(asctime)s %(name)-4s %(levelname)-4s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


def insert_doc(doc):
    LAWS.update_one({'_id': doc['_id']}, {'$set': doc}, upsert=True)


def mark_doc(DB, _id):
    DB.update_one({'_id': _id}, {'$set': {'sync': 1}})


def load_data(DB, filters={'sync': 0}, fields=('title', 'documentno', 'issuedate', 'implementdate')):
    retval = dict()
    cursor = DB.find(filters, fields, no_cursor_timeout=True).batch_size(128)
    fields_size = len(fields)
    for doc in cursor:
        _id = doc['_id']
        arr = []
        for field in fields:
            value = doc.get(field)
            if not value:
                break
            if field == 'title':
                value = ftfy.fix_text(value)
                value = normalize_title(value).replace(
                    '--', '-').replace('\n', '').replace(' ', '')
            if field == 'documentno':
                value = ftfy.fix_text(value)
                if value[-2:] == "发布":
                    value = value[:-2]
                value = value.replace(' ', '').strip()
            arr.append(value)
        if len(arr) == fields_size:
            retval[tuple(arr)] = _id
    return retval


def match_condition(filters={'sync': 0}, fields=('title', 'documentno', 'issuedate', 'implementdate')):
    """match condition
    """
    laws_upstream = load_data(LAWS_UPSTREAM, filters, fields)
    laws_origin = load_data(LAWS_ORIGIN, filters, fields)

    for (key, _id) in laws_upstream.items():
        _id2 = laws_origin.get(key)
        if _id2:
            doc = LAWS_UPSTREAM.find_one({'_id': _id})
            doc2 = LAWS_ORIGIN.find_one({'_id': _id2}, {'content': 1,
                                                        'mediatypes': 1,
                                                        'hasattachments': 1})
            content = doc2.get('content')
            mediatypes = doc2.get('mediatypes')
            hasattachments = doc2.get('hasattachments')
            if not content:
                continue
            doc['content'] = content
            if mediatypes:
                doc['mediatypes'] = mediatypes
            if hasattachments:
                doc['hasattachments'] = hasattachments
            doc['sync'] = 0

            insert_doc(doc)
            mark_doc(LAWS_UPSTREAM, _id)
            mark_doc(LAWS_ORIGIN, _id2)
            logger.debug('[insert]: %s', doc['fbid'])


def match_complete(DB, uniqid='fbid', filters={'sync': 0}):
    """将laws_upstream集合中正文内容完整的直接导入laws数据库
    """
    cursor = DB.find(filters, no_cursor_timeout=True).batch_size(128)
    for doc in cursor:
        insert_doc(doc)
        mark_doc(DB, doc['_id'])
        logger.debug('[insert]: %s', doc[uniqid])


def run():
    """laws_upstream & laws_origin match policy
    """

    # 将laws_upstream中正文内容完整的导入laws数据库中
    match_complete(LAWS_UPSTREAM, 'fbid', {'sync': 0, 'needlogin': {'$ne': True}})
    # 将 title, documentno, issuedate, implementdate 匹配的导入数据库中
    match_condition(fields=('title', 'documentno',
                            'issuedate', 'implementdate'))
    # 将 title, documentno, issuedate 匹配的导入数据库中
    match_condition(fields=('title', 'documentno', 'issuedate'))
    # 将 title, documentno, implementdate 匹配的导入数据库中
    match_condition(fields=('title', 'documentno', 'implementdate'))
    # 将 title, documentno 匹配的导入数据库中
    match_condition(fields=('title', 'documentno'))
    # 将 title, issuedate, implementdate 匹配的导入数据库中
    match_condition(fields=('title', 'issuedate', 'implementdate'))
    # 将 documentno, issuedate, implementdate 匹配的导入数据库中
    match_condition(fields=('documentno', 'issuedate', 'implementdate'))
    # 将 title, issuedate 匹配的导入数据库中
    match_condition(fields=('title', 'issuedate'))
    # 将 title, implementdate 匹配的导入数据库中
    match_condition(fields=('title', 'implementdate'))
    # 将 documentno, issuedate 匹配的导入数据库中
    match_condition(fields=('documentno', 'issuedate'))
    # 将 documentno, implementdate 匹配的导入数据库中
    match_condition(fields=('documentno', 'implementdate'))
    # 将 title 匹配的导入数据库中
    match_condition(fields=('title',))
    # 将 documentno 匹配的导入数据库中
    match_condition(fields=('documentno',))
    # 最后, 将laws_origin中正文内容完整的导入laws数据库中
    match_complete(LAWS_ORIGIN, 'gid', {'sync': 0})


if __name__ == '__main___':
    run()
