"""
公共部分
"""

import sys

import pymongo

sys.path.insert(0, '../../../mercury')
from mercury.utils import md5, timestamp

URI = 'mongodb://mercury:MercurY4dm1n!@172.16.0.224:38017/mercury'
CLIENT = pymongo.MongoClient(URI)
DB = CLIENT.get_database('mercury')


RE_SPACE = re.compile(r'\s+')
RE_FIELD = re.compile(r'【(?P<field>\w+)】')
RE_CLI = re.compile(r'(chl|lar|cas|eag|eagn|hnt|iel|lfbj|scs)_\d+')
RE_ATTACHMENT = re.compile('附件[:：]?[一1]?')
RE_TABLE_PRE_IMG_LINK = re.compile(
    r"""(<table.*?>[\s\S]*?<\/table>)|(<pre.*?>[\s\S]*?<\/pre>)|(<img src="(?P<url>.+?)" class="fjLink"/>)|(<a href='(?P<link>.+?)' class='fjLink' target='_blank'>(?P<explain>.+?)</a>)""", re.UNICODE)

PATH_TRS = '/data/storage/pkulaw_decrypts/'
PATH_CATEGORIES = '/work/categories.csv'
PATH_DEPARTMENTS = '/work/departments.csv'
PATH_EFFECTIVEDIS = '/work/effectivenessdic.csv'
PATH_JS_FIELDS = '/work/processdata/listfields.js'
URL_RESOURCE = 'http://resources.pkulaw.cn'

MEDIA_TYPES = ['table', 'img', 'pre', 'a']


FIELDS_MAPPING = {
    'chl': {
        '法规标题': 'title',
        '类别': 'category',
        '发文字号': 'documentno',
        '批准部门': 'issuedepartment',
        '批准日期': 'ratifydate',
        '发布部门': 'issuedepartment',
        '发布日期': 'issuedate',
        '实施日期': 'implementdate',
        '时效性': 'timelinessdic',
        '效力级别': 'effectivenessdic',
        '唯一标志': 'gid',
        '全文': 'content'
    },
    'lar': {
        '法规标题': 'title',
        '类别': 'category',
        '发文字号': 'documentno',
        '批准部门': 'issuedepartment',
        '批准日期': 'ratifydate',
        '发布部门': 'issuedepartment',
        '发布日期': 'issuedate',
        '实施日期': 'implementdate',
        '时效性': 'timelinessdic',
        '效力级别': 'effectivenessdic',
        '唯一标志': 'gid',
        '全文': 'content'
    },
    'iel': {
        '相关组织': 'originization',
        '颁布日期': 'issuedate',
        '生效日期': 'implementdate',
        '类别': 'category',
    },
    'fmt': {
        '文书编号': 'documentno',
        '文书分类': 'category',
        '发布日期': 'issuedate',
        '发布部门': 'issuedepartment',
        '时效性': 'timelinessdic',
        '发文名称': 'documenttitle'
    },
    'hkd': {
        '分类': 'category',
        '英文标题': 'englishtitle',
    },
    'aom': {
        '法规分类': 'legalcategory',
        '类别': 'category',
        '刊登日期': 'issuedate',
        '相关信息': 'relationinfo',
    },
    'twd': {
        '类别': 'category',
        '颁布日期': 'issuedate',
    },
    'con': {
        '合同分类': 'category',
        '合同条数': 'number',
        '发布日期': 'issuedate',
        '发文字号': 'documentno',
        '时效性': 'timelinessdic',
        '行业类别': 'industrycategory',
        '字数区间': 'wordnumberrange',
        '发文名称': 'documenttitle',
        '合同编号': 'contractno',
        '官方合同': 'officialcontract'
    },
    'lfbj': {
        '发布部门': 'issuedepartment',
        '类别': 'category',
        '发布日期': 'issuedate',
    },
    'eagn': {
        '国家与国际组织': 'originization',
        '条约分类': 'category',
        '签订日期': 'issuedate',
        '生效日期': 'implementdate',
        '批准日期': 'ratifydate',
        '批准机关': 'ratifydepartment',
        '时效性': 'timelinessdic',
        '条约种类': 'treatytype',
        '签订地点': 'signplace',
        '保存机关': 'preservedepartment',
        '交存时间': 'deliverydate',
        '中国生效时间': 'chinaeffectivedate',
        '适用于港澳': 'applyforhkmo',
        '失效日期': 'expiratedate',
        '签署时间': 'signdate',
        '声明与保留': 'declarationreservation',
        '修订日期': 'revisiondate',
    },
    'news': {
        '发布日期': 'issuedate',
        '来源': 'source',
        '作者': 'author',
        '关键词语': 'keywords',
    },
}


def read_csv(path):
    with open(path, 'r', encoding='utf-8') as f:
        reader = csv.reader(f)
        for row in reader:
            if row:
                yield row


def load_csv(path):
    data = read_csv(path)
    retval = dict(data)
    return retval


CATEGORIES = load_csv(PATH_CATEGOGIRES)
DEPARTMETNS = load_csv(PATH_DEPARTMENTS)
EFFECTIVENESS = load_csv(PATH_EFFECTIVENESS)

TIMELINESSDIC = {
    '01': '现行有效',
    '02': '失效',
    '03': '已被修改',
    '04': '尚未生效',
    '05': '部分失效',
}

FIELDS_COLLECTION = {
    'category': CATEGORIES,
    'issuedepartment': DEPARTMETNS,
    'ratifydepartment': DEPARTMETNS,
    'effectivenessdic': EFFECTIVENESS,
    'timelinessdic': TIMELINESSDIC,
}


def strip_expired_title(title):
    if title.endswith('[失效]'):
        title = title[:-4]
    return title.strip().replace('\n', '')


def parse_date(date_str):
    try:
        d = datetime.strptime(date_str, '%Y.%M.%d')
    except:
        try:
            d = datetime.strptime(date_str, '%Y.%M')
        except:
            try:
                d = datetime.strptime(date_str, '%Y')
            except:
                return date_str
            else:
                return d.strftime('%Y')
        else:
            return d.strftime('%Y-%M')
    else:
        return d.strftime('%Y-%M-%d')


def extract_table_pre_img_link_content(html):
    iters = RE_TABLE_PRE_IMG_LINK.finditer(html)
    if not iters:
        soup = BeautifulSoup(html, 'lxml')
        text = soup.get_text().strip()
        return text

    retval = []
    idx = 0
    for e in iters:
        start, end = e.span()
        soup = BeautifulSoup(html[idx: start], 'lxml')
        text = soup.get_text().strip()
        raw = html[start: end]
        if e.groupdict().get('url') is not None:
            url = e.groupdict()['url']
            url = url.replace('\\', '/')
            if not url.startswith(URL_RESOURCE):
                url = URL_RESOURCE + url
            raw = "<img src='{url}' class='fjLink'/>".format(url=url)
        elif e.groupdict().get('link') is not None:
            link = e.groupdict()['link']
            link = link.replace('\\', '/')
            link = URL_RESOURCE + link
            explain = e.groupdict().get('explain', '')
            raw = "<a href='{link}' class='fjLink' target='_blank'>{expalin}</a>".format(
                link=link, explain=explain)
        idx = end
        retval.append(text)
        retval.append(raw)

    soup = BeautifulSoup(html[idx:], 'lxml')
    text = soup.get_text().strip()
    retval.append(text)
    return '\n'.join(retval)


def trs_extract_content(page, gid, i, lib):
    pq = PyQuery(page)

    mainblock = pq
    # raw = mainblock.html()
    # 本法变迁
    changes = [PyQuery(e).text() for e in
               mainblock('font.MTitle').parent().prev_all('a.alink')]

    # 法宝联想
    mainblock.remove('div.TiaoYinV2')
    # 本法变迁史
    mainblock.remove('div.TiaoYin:contains("【本法变迁史】")')
    # 本法变迁史
    mainblock('font.MTitle').parent().prev_all('a.alink').remove()
    # 目录
    mainblock.remove('a.miaodian')
    # 目录
    mainblock.remove('div[@align=center]:contains("目 录")')

    # 正文内容
    html = mainblock.html()
    content = extract_table_pre_img_link_content(html)

    cli = None
    parts = content.rsplit('\n', 1)
    if len(parts) == 2:
        prev, last = parts
        ret = RE_CLI.search(last)
        if ret:
            cli = ret.group(0)
            content = prev.rstrip()

    mediatypes = [t for t in MEDIA_TYPES if mainblock(t)]
    hasattachments = True if RE_ATTACHMENT.search(content) else False

    url = URL_UPDATE.format(lib, gid)
    urlmd5 = md5(url)

    retval = {
        'gid': gid,
        'lib': lib,
        'changes': changes,
        'content': content,
        'url': url,
        'urlmd5': urlmd5,
        'mediatypes': mediatypes,
        'hasattachments': hasattachments,
        'getway': 'package',
        'time_created': timestamp(),
        'time_updated': timestamp(),
        'pkulawinfos': {
            'cli': cli,
        },
    }
    return retval


def js_list_fileds_to_dict(list_fields):
    retval = dict()
    size = len(list_fields)
    for i in range(0, size, 2):
        retval[list_fields[i]] = list_fields[i+1]
    return retval


def load_js(path):
    with open(path, 'r', encoding='utf-8') as f:
        page = f.read()
    js_compiler = execjs.compile(page)
    return js_compiler


def load_js_fields(js_compiler):
    retval = dict()
    js_fieldes = js_compiler.eval('m_ListFieldList')
    for lib in LIBS:
        lib_fieldes = js_fieldes[lib]
        shixiao = lib_fieldes.get('shixiao_id', [])
        xiaoli = lib_fieldes.get('xiaoli_id', [])
        pdep = lib_fieldes.get('pdep_id', [])
        fdep = lib_fieldes.get('fdep_id', [])
        sort = lib_fieldes.get('sort_id', [])
        timelinessdic = js_list_fileds_to_dict(shixiao)
        effectivenessdic = js_list_fileds_to_dict(xiaoli)
        category = js_list_fileds_to_dict(sort)
        pdep_dict = js_list_fileds_to_dict(pdep)
        fdep_dict = js_list_fileds_to_dict(fdep)
        pdep_dict.update(fdep_dict)
        issuedepartment = pdep_dict
        ratifydepartment = pdep_dict
        retval[lib] = {
            'timelinessdic': timelinessdic,
            'effectivenessdic': effectivenessdic or EFFECTIVEDIS,
            'category': category or CATEGORIES,
            'issuedepartment': issuedepartment or DEPARTMENTS,
            'ratifydepartment': ratifydepartment or DEPARTMENTS,
        }
    return retval


JS_COMPILER = load_js(PATH_JS_FIELDS)
JS_FIELDS = load_js_fields(JS_COMPILER)
