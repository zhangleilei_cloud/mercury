import re
import os
import sys
import logging
import concurrent.futures
from functools import partial
from os.path import join, abspath, dirname

import ftfy
from pyquery import PyQuery
from bs4 import BeautifulSoup

sys.path.insert(0, abspath(dirname(dirname(dirname(dirname(__file__))))))

from mercury.utils import md5, timestamp
from mercury.spiders.laws.laws_helper import (
    LAWS_ORIGIN, URL_TAXLAW, RE_CLI, RE_FIELD, RE_ATTACHMENT, MEDIA_TYPES,
    strip_expired_title, convert_date_format, extract_taxlaw_metainfo, extract_table_pre_img_link_content)


# 自定义logger对象
logging.basicConfig(filename='taxlaw.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s %(name)-4s %(levelname)-4s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


RE_NUMBER = re.compile(r'^(?P<gid>\d+).html$', re.DOTALL)


def extract_page(lib, gid, page):
    page = ftfy.fix_text(page)
    pq = PyQuery(page)

    title = pq('input#title').val()
    title = strip_expired_title(title)
    mainblock = pq('div#divFullText')

    # 没有该篇文章,返回None
    if not title or not mainblock:
        return None

    content = mainblock.text()
    if not content and not mainblock('table') and not mainblock('img') and not mainblock('pre'):
        return None

    ######
    if 'MTitle' in content[:50]:
        mainblock = PyQuery(content)

    # 本法变迁
    changes = [PyQuery(e).text() for e in
               mainblock('font.MTitle').parent().prev_all('a.alink')]

    # 法宝联想
    mainblock.remove('div.TiaoYinV2')
    # 本法变迁史
    mainblock.remove('div.TiaoYin:contains("【本法变迁史】")')
    # 本法变迁史
    mainblock('font.MTitle').parent().prev_all('a.alink').remove()
    # 目录
    mainblock.remove('a.miaodian')
    # 目录
    mainblock.remove('div[@align=center]:contains("目 录")')

    # 正文内容
    html = mainblock.html()
    content = extract_table_pre_img_link_content(html)

    cli = None
    parts = content.rsplit('\n', 1)
    if len(parts) == 2:
        prev, last = parts
        ret = RE_CLI.search(last)
        if ret:
            cli = ret.group(0)
            content = prev.rstrip()

    mediatypes = [t for t in MEDIA_TYPES if mainblock(t)]
    hasattachments = bool(RE_ATTACHMENT.search(content))

    url = URL_TAXLAW.format(lib, gid)
    metainfo = extract_taxlaw_metainfo(lib, gid, page)

    retval = {
        'gid': gid,
        'title': title,
        'changes': changes,
        'content': content,
        'lib': lib,
        'url': url,
        'urlmd5': md5(url),
        'mediatypes': mediatypes,
        'hasattachments': hasattachments,
        'getway': 'taxlaw',
        'create_time': timestamp(),
        'update_time': timestamp(),
        'pkulawinfos': {
            'cli': cli,
            'tips': metainfo.pop('tips', None)
        },
        **metainfo,
    }
    return retval


def load_page(path):
    with open(path, 'r', encoding='utf-8') as f:
        try:
            page = f.read()
        except:
            pass
        else:
            return page
    return None

def insert_doc(data):
    data['sync'] = 0
    LAWS_ORIGIN.update_one({'gid': data['gid']}, {'$set': data}, upsert=True)
    return True


def extract(gid, path, lib):
    path_html = os.path.join(path, '{}.html'.format(gid))
    page = load_page(path_html)
    if page:
        data = extract_page(lib, gid, page)
        if data:
            return insert_doc(data)
    return False


def mutiple_process(func, path, lib, iterable):
    with concurrent.futures.ThreadPoolExecutor(max_workers=len(iterable)) as executor:
        worker = partial(func, path=path, lib=lib)
        for item, retval in zip(iterable, executor.map(worker, iterable)):
            if retval:
                logger.info('[Succeed]: {} - {}'.format(lib, item))
            else:
                logger.info('[Failed]: {} - {}'.format(lib, item))


def dispatch(path, lib, collections, step=32):
    for i in range(0, len(collections), step):
        mutiple_process(extract, path, lib, collections[i:i+step])


def main(base_path, libs, step):
    for lib in libs:
        path = os.path.join(base_path, lib)
        files = filter(RE_NUMBER.match, os.listdir(path))
        gids = list(map(lambda x: x.split('.')[0], files))
        dispatch(path, lib, gids, step)


def run(base_path='/data/storage/pkulaw', batch_size=32):
    libs = ['chl', 'lar', 'aom', 'con', 'eagn', 'fmt', 'hkd', 'iel', 'lfbj', 'twd']
    main(base_path, libs, batch_size)


if __name__ == '__main__':
    base_path = '/data/storage/pkulaw'
    libs = ['aom', 'chl', 'con', 'eagn', 'fmt',
            'hkd', 'iel', 'lar', 'lfbj', 'twd']
    step = 32
    main(base_path, libs, step)
