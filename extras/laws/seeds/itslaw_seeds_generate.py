# -*- coding: utf-8 -*-

import json
import os
import requests
from urllib import parse

import demjson


DIR_SCRIPT = os.path.dirname(os.path.abspath(__file__))
keywords_path = os.path.join(DIR_SCRIPT, "laws.json")

searchword = "searchWord+{keywords}+1+{keywords}"
index_url_origin = ("https://www.itslaw.com/api/v1/lawRegulations?startIndex=0&countPerPage=20"
                    "&sortType=5&needCorrection=1&")


def extract_info():
    with open(keywords_path, "r", encoding="utf-8") as ff:
        for search_info in ff.readlines():
            search_info = json.loads(search_info)
            del search_info["_id"]
            yield search_info


def make_url(search_info):
    keywords = search_info.get("title")
    if keywords:
        keywords = keywords.split()[0].split("(")[0]
        params = {
            "conditions": searchword.format(keywords=keywords)
        }
        params = parse.urlencode(params)
        index_url = index_url_origin + params
        return index_url


def make_requests(index_url, search_info):
    data = {
        "url": index_url,
        "getway": "itslaw",
        "_sched_interval": 86400,
        "status": "OK",
        "priority": "5",
        "extra_data": search_info,
        "anti_cfg": {
            "proxy": True,
            "cookie": False,
            "random_ua": False
            }
    }
    dest_url = "http://127.0.0.1:8888/seeds"
    data = demjson.encode(data)
    print(" make_request:", search_info)
    response = requests.post(dest_url, data=data)
    return response


def generate_part(start, stop):
    count = 0
    all_seed_urls = []
    for info in extract_info():
        count += 1
        if count < start:
            continue
        index_url = make_url(info)
        make_requests(index_url, info)
        if count >= stop:
            return all_seed_urls


if __name__ == "__main__":

    generate_part(1, 100)
