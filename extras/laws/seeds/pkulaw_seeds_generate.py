
import sys
import pprint
from datetime import datetime, timedelta

import demjson
import requests

URL = 'http://127.0.0.1:8888/seeds'
POST_DATA = {
    "url": "http://www.pkulaw.com/law/search/RecordSearch",
    "getway": "pkulaw",
    "_sched_interval": 86400,
    "status": "OK",
    "priority": 5,
    "query": "",
    "anti_cfg": {
        "proxy": False,
        "cookie": True,
        "random_ua": False
    }
}
PAYLOAD = {
    'lar': {
        'Menu': 'law',
        'Keywords': '',
        'SearchKeywordType': 'Title',
        'MatchType': 'Exact',
        'RangeType': 'Piece',
        'Library': 'lar',
        'ClassFlag': 'lar',
        'GroupLibraries': '',
        'pdfStr': '',
        'pdfTitle': '',
        'IsAdv': 'False',
        'ClassCodeKey': ',,,,,,,,',
        'GroupByIndex': 2,
        'OrderByIndex': 0,
        'ShowType': 'Default',
        'GroupValue': '',
        'AdvSearchDic.Title': '',
        'AdvSearchDic.CheckFullText': '',
        'AdvSearchDic.IssueDepartment': '',
        'AdvSearchDic.DocumentNO': '',
        'AdvSearchDic.RatifyDepartment': '',
        'AdvSearchDic.RatifyDate': "{ 'Start': '', 'End': '' }",
        'AdvSearchDic.IssueDate': "{ 'Start': '', 'End': '' }",
        'AdvSearchDic.ImplementDate': "{ 'Start': '', 'End': '' }",
        'AdvSearchDic.TimelinessDic': '',
        'AdvSearchDic.Specialtopic': '',
        'AdvSearchDic.EffectivenessDic': '',
        'AdvSearchDic.Category': '',
        'TitleKeywords': '',
        'FullTextKeywords': '',
        'Pager.PageIndex': 0,
        'Pager.PageSize': 10,
        'QueryBase64Request': '',
        'X-Requested-With': 'XMLHttpRequest'
    },
    'chl': {
        'Menu': 'law',
        'Keywords': '',
        'SearchKeywordType': 'Title',
        'MatchType': 'Exact',
        'RangeType': 'Piece',
        'Library': 'chl',
        'ClassFlag': 'chl',
        'GroupLibraries': '',
        'QueryOnClick': 'False',
        'AfterSearch': 'False',
        'pdfStr': '',
        'pdfTitle': '',
        'IsAdv': 'False',
        'ClassCodeKey': '',
        'GroupByIndex': '2',
        'OrderByIndex': '0',
        'ShowType': 'Default',
        'GroupValue': '',
        'TitleKeywords': '',
        'FullTextKeywords': '',
        'Pager.PageIndex': 0,
        'Pager.PageSize': 10,
        'QueryBase64Request': '',
        'VerifyCodeResult': '',
        'isEng': 'chinese',
        'OldPageIndex': '',
        'X-Requested-With': 'XMLHttpRequest'
    }
}


IssueDate = "{ 'Start': '%s', 'End': '%s' }"

cluster_codes1 = [
    ['XA0101', 'XA0102', 'XA0103', 'XA0104', 'XA0105', 'XA0106', 'XA01'],
    ['XC0201', 'XC0202', 'XC0203', 'XC02'],
    ['XJ15'],
    ['XG0401', 'XG0402', 'XG0403', 'XG04'],
    ['XE0301', 'XE0302', 'XE0303', 'XE0304', 'XE03'],
    ['XI05'],
    ['XK06'],
    ['XQ0901', 'XQ0902', 'XQ0903', 'XQ09']
]

cluster_local = [
    # ['XM07', 'XM0701', 'XM0702', 'XM0703', 'XM0704'],
    'XO08', 'XP08', 'XP09', 'XP10', 'XP11'
]


def _generate_classcode(given_codes):
    for codes in given_codes:
        for code in codes:
            yield code


def _generate_days(start_day, end_day, offset=7):
    start_time = datetime.strptime(start_day, '%Y-%m-%d')
    end_time = datetime.strptime(end_day, '%Y-%m-%d')
    days = (end_time - start_time).days + 1
    for day in range(0, days, offset):
        start = start_time + timedelta(days=day)
        end = start + timedelta(days=offset)
        yield start.strftime('%Y.%m.%d'), end.strftime('%Y.%m.%d')


def _generate_lar_query(dates, cluster_code):
    for (start, end) in dates:
        issue_date = IssueDate % (start, end)
        for code in cluster_code:
            PAYLOAD['lar']['AdvSearchDic.IssueDate'] = issue_date
            PAYLOAD['lar']['ClassCodeKey'] = ',,,{0},,,,'.format(code)
            payload = demjson.encode(PAYLOAD['lar'])
            POST_DATA['payload'] = payload
            yield POST_DATA


def _generate_chl_query(dates):
    for (year, month) in dates:
        PAYLOAD['chl']['ClassCodeKey'] = '{0}.{1},,,,,,,'.format(year, month)
        payload = demjson.encode(PAYLOAD['chl'])
        POST_DATA['payload'] = payload
        yield POST_DATA

        PAYLOAD['chl']['ClassCodeKey'] = ',{0}.{1},,,,,,'.format(year, month)
        payload = demjson.encode(PAYLOAD['chl'])
        POST_DATA['payload'] = payload
        yield POST_DATA

        PAYLOAD['chl']['ClassCodeKey'] = ',,{0}.{1},,,,,'.format(year, month)
        payload = demjson.encode(PAYLOAD['chl'])
        POST_DATA['payload'] = payload
        yield POST_DATA


def _generate_codekey(codekeystr, codes):
    for code in codes:
        yield codekeystr.format(code)


def execute(querys):
    counter = 0
    for query in querys:
        data = demjson.encode(query, encoding='utf-8')
        try:
            r = requests.post(URL, data=data)
            resp = r.json()
        except (KeyboardInterrupt, Exception) as e:
            break
        else:
            pprint.pprint(resp, depth=2)
            if resp['data']:
                counter += 1
                print('\n{}-th seed inserted\n'.format(counter))

    print('\ntotal seeds inserted: {}\n\n'.format(counter))


def _generate_month(start_day, end_day):
    start_year, start_month = start_day.split('.')
    end_year, end_month = end_day.split('.')
    for year in range(int(start_year), int(end_year) + 1, 1):
        start, end = 1, 12
        if year == int(start_year):
            start = int(start_month)
        if year == int(end_year):
            end = int(end_month)

        for month in range(start, end + 1):
            yield (year, month)


def main(lib, start_time, end_time, offset):
    if lib == 'lar':
        dates = _generate_days(start_time, end_time, int(offset))
        querys = _generate_lar_query(dates, cluster_local)
        execute(querys)
    elif lib == 'chl':
        dates = _generate_month(start_time, end_time)
        querys = _generate_chl_query(dates)
        execute(querys)


if __name__ == '__main__':
    lib, start_time, end_time, offset = sys.argv[1:]
    main(lib, start_time, end_time, offset)
