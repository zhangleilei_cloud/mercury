import re
import sys
import pprint
from datetime import datetime, timedelta

import demjson
import requests

URL = 'http://127.0.0.1:8888/seeds'
POST_DATA = {
    "url": "http://www.ttfawu.com/fagui/searchFagui",
    "getway": "ttfawu",
    "_sched_interval": 86400,
    "status": "OK",
    "priority": 5,
    "query": "",
    "anti_cfg": {
        "proxy": False,
        "cookie": True,
        "random_ua": False
    }
}
PAYLOAD = {
    'keywords': '',
    'pageNO': 1,
    'orderby': 'sxx_desc'
}


def read_json(path):
    with open(path, 'r', encoding='utf-8') as f:
        for line in f:
            data = demjson.decode(line.rstrip())
            del data['_id']
            yield data


def _generate_query(path):
    data = read_json(path)
    for item in data:
        PAYLOAD['keywords'] = '标题：%s' % item['title'].split(' ')[
            0].replace('修正', '')
        payload = demjson.encode(PAYLOAD)
        POST_DATA['payload'] = payload
        POST_DATA['extra_data'] = item
        yield POST_DATA


def execute(querys):
    counter = 0
    for query in querys:
        data = demjson.encode(query, encoding='utf-8')
        try:
            r = requests.post(URL, data=data)
            resp = r.json()
        except (KeyboardInterrupt, Exception) as e:
            break
        else:
            pprint.pprint(resp, depth=2)
            if resp['data']:
                counter += 1
                print('\n{}-th seed inserted\n'.format(counter))

    print('\ntotal seeds inserted: {}\n\n'.format(counter))


def main(path):
    querys = _generate_query(path)
    execute(querys)


if __name__ == '__main__':
    path = sys.argv[1]
    main(path)
