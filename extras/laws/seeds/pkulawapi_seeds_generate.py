
import sys
import calendar
import pprint
from datetime import datetime, timedelta

import demjson
import requests

URL_API = 'http://api.pkulaw.com/Db/LibraryRecordList?Library={lib}&PageSize={pagesize}&PageIndex={pageindex}&SortField=IssueDate%20desc'

URL = 'http://127.0.0.1:8888/seeds'
POST_DATA = {
    "url": "",
    "getway": "pkulawapi",
    "_sched_interval": 86400,
    "status": "OK",
    "priority": 5,
    "query": "",
    "anti_cfg": {
        "proxy": False,
        "cookie": True,
        "random_ua": False
    },
    "extra_data": {
        'max_crawled_times': 1,
        'isupdate': True
    }
}

counter = 0

def make_request(querys):
    for query in querys:
        data = demjson.encode(query, encoding='utf-8')
        try:
            r = requests.post(URL, data=data)
            resp = r.json()
        except (KeyboardInterrupt, Exception) as e:
            return
        else:
            pprint.pprint(resp, depth=2)
            if resp['data']:
                global counter
                counter += 1
                print('\n{}-th seed inserted\n'.format(counter))


def make_query(lib, firstday, lastday):
    issuedate = "Model.IssueDate={'Start':'%s','End':'%s'}" % (
        firstday, lastday)
    impldate = "Model.ImplementDate={'Start':'%s','End':'%s'}" % (
        firstday, lastday)
    lastchangedate = "Model.LastTimelinessChangeDate={'Start':'%s','End':'%s'}" % (
        firstday, lastday)

    url = URL_API.format(lib=lib, pagesize=20, pageindex=0)

    issue_url = f"{url}&{issuedate}"
    impl_url = f"{url}&{impldate}"
    lastchange_url = f"{url}&{lastchangedate}"

    issue_post = POST_DATA.copy()
    issue_post['url'] = issue_url

    impl_post = POST_DATA.copy()
    impl_post['url'] = impl_url

    lastchange_post = POST_DATA.copy()
    lastchange_post['url'] = lastchange_url

    querys = [issue_post, impl_post, lastchange_post]
    make_request(querys)


def main(lib, start, end):
    start_date = datetime.strptime(start, '%Y-%m-%d')
    end_date = datetime.strptime(end, '%Y-%m-%d')
    days = (end_date - start_date).days
    for day in range(0, days):
        start_day = start_date + timedelta(days=day)
        end_day = start_day + timedelta(days=0)
        start_datestr = start_day.strftime('%Y-%m-%d')
        end_datestr = end_day.strftime('%Y-%m-%d')
        make_query(lib, start_datestr, end_datestr)


if __name__ == '__main__':
    lib = sys.argv[1]
    start = sys.argv[2]
    end = sys.argv[3]
    main(lib, start, end)
