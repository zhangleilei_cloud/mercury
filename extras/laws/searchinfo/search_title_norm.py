import csv
import sys

import title_normalizer as TN

BATCH_SIZE = 512


def norm_titles(titles):
    succeed, failed = [], []
    for (item, (nm_title, succ)) in zip(buffer, TN.normalize_title(titles)):
        if succ:
            succeed.append(nm_title)
        else:
            failed.append(nm_title)
    return succeed, failed


def log_csv(path, msgs):
    with open(path, 'w', encoding='utf-8') as f:
        writer = csv.writer(f)
        for msg in msgs:
            writer.writerow(msg)


def load_csv(path):
    counter = 0
    succeed, failed, buffer = [], [], []
    with open(path, 'r', encoding='utf-8') as f:
        reader = csv.reader(f)
        data = map(lambda x: x:[0], reader)

    for item in data:
        counter += 1
        if counter % 1000 == 0:
            print('[load_csv]: {}'.format(counter))
        buffer.append(item)
        if len(buffer) == BATCH_SIZE:
            succ, fail = norm_titles(buffer)
            succeed.extend(succ)
            failed.extend(fail)
            buffer.clear()
            
    if len(buffer) > 0:
        succ, fail = norm_titles(buffer)
        succeed.extend(succ)
        failed.extend(fail)

    return succeed, failed


def main():
    laws_succeed, laws_failed = load_csv(
        '/work/searchinfo/laws_upstream_all_titles.csv')
    search_succeed, search_failed = load_csv(
        '/work/searchinfo/search_titles.csv')

    print('laws: normalize succeed: {}, failed: {}, total: {}'.format(
        len(laws_succeed), len(laws_failed), len(laws_succeed) + len(laws_failed)))
    print('search: normalize succeed: {}, failed: {}, total: {}'.format(
        len(search_succeed), len(search_failed), len(search_succeed) + len(search_failed)))

    log_csv('laws_titles_normalize_succeed.csv', laws_succeed)
    log_csv('laws_titles_normalize_failed.csv', laws_failed)
    log_csv('search_titles_normalize_succeed.csv', search_succeed)
    log_csv('search_titles_normalize_failed.csv', search_failed)

    laws_succeed.extend(laws_failed)
    search_succeed.extend(search_failed)
    laws = set(laws_succeed)
    search = set(search_succeed)

    print('set(laws): {}'.format(len(laws)))
    print('set(search): {}'.format(len(search)))

    search_diff_laws = search.difference(laws)
    intersection = search.intersection(laws)

    print('search - laws: {}'.format(len(search_diff_laws)))
    print('search & laws: {}'.format(len(intersection)))

    log_csv('search_titles_normalize_title_not_in_laws.csv', search_diff_laws)
    log_csv('search_titles_normalize_title_in_laws.csv', intersection)


if __name__ == '__main__':
    main()
