import concurrent.futures

import requests

url = 'http://144.217.72.87:5611/elasticsearch/_msearch'
data = '''{"index":"pipeline-laws-reindex","ignore_unavailable":true,"timeout":30000,"preference":1540388730622}\n{"version":true,"size":50,"sort":[{"_score":{"order":"desc"}}],"_source":{"excludes":[]},"stored_fields":["*"],"script_fields":{},"docvalue_fields":["implementdate","issuedate"],"query":{"bool":{"must":[{"query_string":{"query":"title: %s","analyze_wildcard":true,"default_field":"*"}}],"filter":[],"should":[],"must_not":[]}},"highlight":{"pre_tags":["@kibana-highlighted-field@"],"post_tags":["@/kibana-highlighted-field@"],"fields":{"*":{}},"fragment_size":2147483647}}\n'''

headers = {'Accept': 'application/json, text/plain, */*',
           'Accept-Encoding': 'gzip, deflate',
           'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
           'Connection': 'keep-alive',
           'Content-Length': '646',
           'content-type': 'application/x-ndjson',
           'DNT': '1',
           'Host': '144.217.72.87:5611',
           'kbn-version': '6.3.1',
           'Origin': 'http://144.217.72.87:5611',
           'Referer': 'http://144.217.72.87:5611/app/kibana',
           'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36'}


def query(title):
    print('title: {}'.format(title))
    payload = data % title
    try:
        r = requests.post(url, data=payload.encode('utf-8'), headers=headers)
    except Exception as e:
        print(e)
        return '[NOT FOUND]'
    else:
        ret = r.json()
        try:
            hits = ret['responses'][0]['hits']['hits'][0]
        except Exception as e:
            print(e)
            return '[NOT FOUND]'
        else:
            return hits['_source']['title']


def load_titles(path):
    with open(path, 'r', encoding='utf-8') as f:
        for line in f:
            if line.rstrip():
                yield line.rstrip()


def write(path, data):
    with open(path, 'w', encoding='utf-8') as f:
        for (title, title_search) in data:
            f.write(title + '\n')
            f.write(title_search + '\n')
            f.write('\n')


def process(worker, iterable):
    buffer = []
    with concurrent.futures.ThreadPoolExecutor(max_workers=len(iterable)) as executor:
        for (title, title_search) in zip(iterable, executor.map(worker, iterable)):
            buffer.append((title, title_search))
    return buffer


def main(batch_size=32):
    titles = list(load_titles('search_titles_normalize_title_not_in_laws.csv'))
    print('total: {}'.format(len(titles)))

    retval = []
    buffer = []
    for title in titles:
        buffer.append(title)
        if len(buffer) > batch_size:
            buf = process(query, buffer)
            retval.extend(buf)
            buffer.clear()

    if len(buffer) > 0:
        buf = process(query, buffer)
        retval.extend(buf)

    found, notfound = 0, 0
    lost = []
    for (title1, title2) in retval:
        if title1 == title2:
            found += 1
        else:
            notfound += 1
            lost.append((title1, title2))

    print('found: {}, notfound: {}, total: {}'.format(
        found, notfound, found + notfound))

    write('search_titles_match_kibana.log', retval)
    write('search_titles_match_kibana_lost.log', lost)


if __name__ == '__main__':
    main()
