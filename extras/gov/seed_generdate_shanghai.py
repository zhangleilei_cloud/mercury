import requests
import demjson

from mercury.settings import logger

region_type_mapping = {"SH00SH": "上海市",
                         "SH00PD": "浦东新区",
                         "SH00HP": "黄浦区",
                         "SH00JA": "静安区",
                         "SH00XH": "徐汇区",
                         "SH00CN": "长宁区",
                         "SH00PT": "普陀区",
                         "SH00HK": "虹口区",
                         "SH00YP": "杨浦区",
                         "SH00BS": "宝山区",
                         "SH00MH": "闵行区",
                         "SH00JD": "嘉定区",
                         "SH00JS": "金山区",
                         "SH00QP": "青浦区",
                         "SH00FX": "奉贤区",
                         "SH00CM": "崇明区",
                         }

item_type_mapping = {"行政审批": "行政许可",
                     "行政给付": "行政给付",
                     "行政确认": "行政确认",
                     "行政奖励": "行政奖励",
                     "行政裁决": "行政裁决",
                     "其他类别": "其他类别", }

gf_type_list = ["个人", "法人"]


def make_seeds_url():
    return "http://zwdt.sh.gov.cn/govPortals/person.do?pageNumber=1&pageSize=7&itemType=%1&stRegion=%2&fType=%3"


def make_requests(index_url, extra_data):
    data = {
        "url": index_url,
        "getway": "tips_shanghai",
        "_sched_interval": 86400,
        "status": "OK",
        "priority": "5",
        "extra_data": extra_data,
        "anti_cfg": {
            "proxy": False,
            "cookie": False,
            "random_ua": True
            }
    }
    dest_url = "http://127.0.0.1:8888/seeds"
    data = demjson.encode(data)
    response = requests.post(dest_url, data=data)
    return response


def generate_seeds():
    seeds_url = make_seeds_url()
    for gf_type in gf_type_list:
        for region in region_type_mapping:
            for item_type in item_type_mapping:
                next_seeds_url = seeds_url.replace("%3", gf_type).replace("%2", region).replace("%1", item_type)
                extra_data = {"site_level": region, "cate_xz": item_type}
                make_requests(next_seeds_url, extra_data)
                logger.debug("[generate_seeds_shanghai]<{0}> --- ".format(next_seeds_url))

if __name__ == "__main__":
    generate_seeds()
