# -*- coding: utf-8 -*-

import os
import requests
import demjson

DIR_SCRIPT = os.path.dirname(os.path.abspath(__file__))

# beij_category.json 从文件服务器获取
category_path = os.path.join(DIR_SCRIPT, "guangdong_category.json")


def extract_category():
    with open(category_path, "r", encoding="utf-8") as fc:
        categorys = fc.read()
        categorys = demjson.decode(categorys)
    for category in categorys:
        yield category


def make_referer_url(code, number):
    return f"http://www.gdzwfw.gov.cn/portal/affairs-public-depart-matters?region={code}&orgId={number}"


def make_url(number):
    return f"http://www.gdzwfw.gov.cn/portal/item/getCommonAuditItem?DEPT_CODE={number}&pageNum=1&pageSize=10"


def generate_seeds():
    for node in extract_category():
        site_level = "{0} {1} {2}".format(node["province"], node["city"], node["county"])
        url = make_url(node["ORGNUMBER"])
        referer = make_referer_url(node["ORGAREACODE"], node["ORGNUMBER"])
        make_requests(url, dict(site_level=site_level, Referer=referer))


def make_requests(index_url, extra_data):
    data = {
        "url": index_url,
        "getway": "tips_guangdong",
        "_sched_interval": 86400,
        "status": "OK",
        "priority": "5",
        "extra_data": extra_data,
        "anti_cfg": {
            "proxy": True,
            "cookie": False,
            "random_ua": True
        }
    }
    dest_url = "http://127.0.0.1:8888/seeds"
    data = demjson.encode(data)
    print(data)
    response = requests.post(dest_url, data=data)
    return response


if __name__ == "__main__":
    generate_seeds()
