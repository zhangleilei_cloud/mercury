# -*- coding: utf-8 -*-

import os
import requests
import demjson

DIR_SCRIPT = os.path.dirname(os.path.abspath(__file__))

# beij_category.json 从文件服务器获取
category_path = os.path.join(DIR_SCRIPT, "hebei_category.json")

# 个人主题
gr_theme_data = ["07", "08", "10", "16", "17", "24", "25", "26", "27", "28", "11", "12", "13", "14", "15", "01", "05",
                 "18", "19", "20", "21", "22", "23", "06", "04", "02", "09", "03", "29", "30", "31"]

# 法人主题
fr_theme_data = ["01", "02", "11", "04", "05", "24", "03", "08", "09", "10", "07", "12", "13", "14", "15", "33", "17",
                 "18", "19", "20", "21", "22", "25", "06", "23", "26", "27", "28", "29", "30", "31", "32", "16", "34"]


def extract_category():
    with open(category_path, "r", encoding="utf-8") as fc:
        categorys = fc.read()
        categorys = demjson.decode(categorys)
    for category in categorys:
        yield category


def make_seeds_url(type, id, theme_id):
    if type == 1:
        return f"http://www.hbzwfw.gov.cn/hbzw/sxcx/itemList/gr_list.do?webId={id}&zt={theme_id}&deptid="
    elif type == 2:
        return f"http://www.hbzwfw.gov.cn/hbzw/sxcx/itemList/fr_list.do?webId={id}&enterprise_zt={theme_id}&deptid="
    elif type == 3:
        return f"http://www.hbzwfw.gov.cn/hbzw/sxcx/itemList/town_right.do?webId={id}"


def generate_seeds():
    for area in extract_category():
        site_level = area["name"]
        # 乡镇 没有个人和法人的以及主题的区分 只有一个请求
        if area["type"] == "4":
            make_requests(make_seeds_url(3, area["id"], ""), dict(site_level=site_level, type=area["type"]))
        else:
            for theme_id in gr_theme_data:
                make_requests(make_seeds_url(1, area["id"], theme_id), dict(site_level=site_level, type=area["type"]))
            for theme_id in fr_theme_data:
                make_requests(make_seeds_url(2, area["id"], theme_id), dict(site_level=site_level, type=area["type"]))


def make_requests(index_url, extra_data):
    print("index_url:" + index_url)
    data = {
        "url": index_url,
        "getway": "tips_hebei",
        "_sched_interval": 86400,
        "status": "OK",
        "priority": "5",
        "extra_data": extra_data,
        "anti_cfg": {
            "proxy": False,
            "cookie": False,
            "random_ua": True
        }
    }
    dest_url = "http://127.0.0.1:8888/seeds"
    data = demjson.encode(data)
    response = requests.post(dest_url, data=data)
    return response


if __name__ == "__main__":
    generate_seeds()

