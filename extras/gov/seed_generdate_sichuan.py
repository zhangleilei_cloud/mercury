import requests
import demjson
import os

DIR_SCRIPT = os.path.dirname(os.path.abspath(__file__))

#sichuan_site.json 从文件服务器获取
site_path = os.path.join(DIR_SCRIPT, "sichuan_site.json")


def make_seeds_url(area_code):
    return "http://www.sczwfw.gov.cn/app/powerDutyList/getThImplement?page=1&pageSize=15&eventType=1&areaCode={0}".format(area_code)


def make_requests(index_url, extra_data):
    data = {
        "url": index_url,
        "getway": "tips_sichuan",
        "_sched_interval": 86400,
        "status": "OK",
        "priority": "5",
        "extra_data": extra_data,
        "anti_cfg": {
            "proxy": True,
            "cookie": False,
            "random_ua": True
            }
    }
    dest_url = "http://127.0.0.1:8888/seeds"
    data = demjson.encode(data)
    response = requests.post(dest_url, data=data)
    return response


def extract_site():
    with open(site_path, "r", encoding="utf-8") as fc:
        sites = fc.read()
        sites = demjson.decode(sites)
    site_map = {}
    for site in sites:
        site_id = site["idForStr"]
        site_value = dict(area_code=site["areaCode"], area_name=site["areaName"], level=site["level"], parent_area_id=site["parentAreaIdForStr"])
        site_map[site_id] = site_value
    return site_map


def get_site_full_name(site_value, site_map):
    site_full_name = site_value["area_name"]
    site_level = site_value["level"]
    site_parent_id = site_value["parent_area_id"]
    while site_level > 1 and len(site_parent_id) > 0:
        parent_site = site_map[site_parent_id]
        site_full_name = parent_site["area_name"] + " " + site_full_name
        site_level = parent_site["level"]
        site_parent_id = parent_site["parent_area_id"]
    return site_full_name


def generate_seeds():
    site_map = extract_site()
    for site_id in site_map.keys():
        site_value = site_map[site_id]
        area_code = site_value["area_code"]
        seeds_url = make_seeds_url(area_code)
        site_full_name = get_site_full_name(site_value, site_map)
        extra_data = dict(site_level=site_full_name)
        make_requests(seeds_url, extra_data)


if __name__ == "__main__":
    generate_seeds()
