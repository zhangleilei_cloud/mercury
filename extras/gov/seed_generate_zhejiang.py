# -*- coding: utf-8 -*-

import os
import sys
import json
import demjson
import requests

from urllib import parse
from bs4 import BeautifulSoup

DIR_SCRIPT = os.path.dirname(os.path.abspath(__file__))

# zhejiang_dept.json 从文件服务器获取
dept_path = os.path.join(DIR_SCRIPT, "zhejiang_dept.json")


def extract_dept():
    with open(dept_path, "r", encoding="utf-8") as fc:
        depts = fc.read()
        depts = demjson.decode(depts)
    return depts


def make_seeds_url(id, category, bsfw):
    dept_id = category["dept_id"]
    name = category["name"]
    name = parse.urlencode({"deptname": name})
    return f"http://www.zjzwfw.gov.cn/zjzw/item/list/show_grrightNew.do?" \
           f"webid={id}&deptid={dept_id}&{name}&isonline=0&bsfw={bsfw}"


def generate_seeds():
    for area_key, area_value in extract_dept().items():
        for dept_key, dept_value in area_value.items():
            category = {"dept_id": dept_key, "name": dept_value["name"]}
            # bsfw为1 办事服务类
            make_requests(make_seeds_url(area_key, category, 1),
                          dict(site_level=dept_value["area"], dept_name=dept_value["name"]))
            # bsfw为2 行政监管类
            make_requests(make_seeds_url(area_key, category, 2),
                          dict(site_level=dept_value["area"], dept_name=dept_value["name"]))


def make_requests(index_url, extra_data):
    print("index_url:" + index_url)
    data = {
        "url": index_url,
        "getway": "tips_zhejiang",
        "_sched_interval": 86400,
        "status": "OK",
        "priority": "5",
        "extra_data": extra_data,
        "anti_cfg": {
            "proxy": False,
            "cookie": False,
            "random_ua": True
        }
    }
    dest_url = "http://127.0.0.1:8888/seeds"
    data = demjson.encode(data)
    response = requests.post(dest_url, data=data)
    return response


if __name__ == "__main__":
    # 生成种子
    generate_seeds()
