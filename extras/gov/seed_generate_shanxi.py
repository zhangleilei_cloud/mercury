# -*- coding: utf-8 -*-

import os
import re
import requests
import demjson

from bs4 import BeautifulSoup

DIR_SCRIPT = os.path.dirname(os.path.abspath(__file__))

# shanxi_category.json 从文件服务器获取
category_path = os.path.join(DIR_SCRIPT, "shanxi_category.json")

gr_foot_href = "/govservice/project?i=1&type=gr"
fr_foot_href = "/govservice/project?i=2&type=fr"


def extract_category():
    with open(category_path, "r", encoding="utf-8") as fc:
        categorys = fc.read()
        categorys = demjson.decode(categorys)
    for category in categorys:
        yield category


def make_seeds_url(url, bs_type):
    return f"{url}/api-v2/app.icity.govservice.GovProjectCmd/getBigMattersList?" \
           f"region_code&CAT=theme&ID&start=0&page=1&limit=10&IsInhall=1&PAGEMODEL={bs_type}"


def make_county_seeds_url(url):
    return f"{url}/api-v2/app.icity.govservice.GovProjectCmd/getMattersList?" \
           f"region_code&CAT=theme&ID&SUIT_ONLINE&SearchName&start=0&page=1&limit=10"


def generate_seeds():
    icity_pattern = re.compile(r'.*icity')
    for node in extract_category():
        site_level = node["name"]
        url = node["url"]
        if node["area_type"] is "4":
            head_url = re.match(icity_pattern, url)
            head_url = head_url.group()
            head_url = head_url.replace("/icity", "")
            signature = get_unique_signature(url)
            if signature is not "":
                make_requests(make_county_seeds_url(head_url),
                              dict(site_level=site_level, signature=signature,
                                   area_type=node["area_type"]))
        else:
            gr_url = url + gr_foot_href
            fr_url = url + fr_foot_href
            gr_signature = get_unique_signature(gr_url)
            fr_signature = get_unique_signature(fr_url)
            if gr_signature is not "":
                make_requests(make_seeds_url(url, "person"),
                              dict(site_level=site_level, bs_type="person", signature=gr_signature,
                                   area_type=node["area_type"]))
            if fr_signature is not "":
                make_requests(make_seeds_url(url, "ent"),
                              dict(site_level=site_level, bs_type="ent", signature=fr_signature,
                                   area_type=node["area_type"]))


def get_unique_signature(url):
    print("url:" + url)
    signature = ""
    response = requests.post(url)
    soup = BeautifulSoup(response.text, 'lxml')
    if soup.find('script', {'type': 'text/javascript'}):
        script_text = soup.find('script', {'type': 'text/javascript'}).get_text()
        signature = re.search(r"__signature.*;", script_text).group()
        if signature:
            signature = signature.split("=")[1].replace("\"", "")
            signature = signature.replace(";", "").strip()
    return signature


def make_requests(index_url, extra_data):
    print("index_url:" + index_url)
    data = {
        "url": index_url,
        "getway": "tips_shanxi",
        "_sched_interval": 86400,
        "status": "OK",
        "priority": "5",
        "extra_data": extra_data,
        "anti_cfg": {
            "proxy": False,
            "cookie": False,
            "random_ua": True
        }
    }
    dest_url = "http://127.0.0.1:8888/seeds"
    data = demjson.encode(data)
    response = requests.post(dest_url, data=data)
    return response


if __name__ == "__main__":
    generate_seeds()
