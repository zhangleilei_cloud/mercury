import requests
import demjson
import os
import bs4

from bs4 import BeautifulSoup

DIR_SCRIPT = os.path.dirname(os.path.abspath(__file__))

#jiangsu_site.txt 从文件服务器获取
site_path = os.path.join(DIR_SCRIPT, "jiangsu_site.txt")

# 服务领域
fwly_data = {
    "F101": "城乡规划",
    "F102": "重大建设项目",
    "F103": "财政预决算",
    "F104": "税收管理",
    "F105": "环境保护",
    "F106": "食品药品监管",
    "F107": "安全生产",
    "F108": "基本公共文化体育",
    "F109": "公共法律服务",
    "F110": "水利服务",
    "F111": "人社服务",
    "F112": "科技服务",
    "F113": "民族宗教事务",
    "F114": "测绘服务",
    "F115": "公众服务",
    "F116": "价格公共服务",
    "F117": "快递服务",
    "F118": "投诉举报服务",
    "F119": "基本公共教育",
    "F120": "基本就业创业",
    "F123": "基本社会服务",
    "F124": "基本住房保障",
    "F125": "基本公共交通",
    "F126": "残疾人基本公共服务",
    "F127": "工程建设",
    "F128": "市政公用",
    "F129": "园林绿化",
    "F132": "基本卫生计生"
}


def make_seeds_url(webid, themid):
    return "http://www.jszwfw.gov.cn/jszwfw/publicservice/itemlist/ggshowtype.do?webId={0}&themid={1}&pageno=1".format(webid, themid)


def make_requests(index_url, extra_data):
    data = {
        "url": index_url,
        "getway": "tips_jiangsu",
        "_sched_interval": 86400,
        "status": "OK",
        "priority": "5",
        "extra_data": extra_data,
        "anti_cfg": {
            "proxy": True,
            "cookie": False,
            "random_ua": True
            }
    }
    dest_url = "http://127.0.0.1:8888/seeds"
    data = demjson.encode(data)
    response = requests.post(dest_url, data=data)
    return response


def extract_site():
    with open(site_path, "r", encoding="utf-8") as f:
        site_html = f.read()

    site_soup = BeautifulSoup(site_html, 'lxml')

    # 省级 只有一个直接写死
    sites = [
        {"site_id": 1, "site_full_name": "江苏省"}
    ]

    # 市级
    site_level = {}
    sj_div = site_soup.find("div", {"class": "c_list2"})
    for sj_a in sj_div.find_all("a"):
        sj_site_id = sj_a.get("data")
        sj_site_name = sj_a.get_text().strip()
        sj_site_full_name = "江苏省 " + sj_site_name
        sites.append(dict(site_id=sj_site_id, site_full_name=sj_site_full_name))
        site_level[sj_site_name] = dict(webid=sj_site_id, full_name=sj_site_full_name)

    # 区/县级
    qxj_div = site_soup.find("div", {"class": "site_r3"}).find("div", {"class": "c2"})
    sj_site_name = ""
    for tag in qxj_div.contents:
        if isinstance(tag, bs4.element.Tag) and tag.name == "div":
            site_level_children = []
            for qxj_a in tag.find_all("a"):
                qxj_site_id = qxj_a.get("data")
                qxj_site_name = qxj_a.get_text().strip()
                qxj_site_full_name = site_level[sj_site_name]["full_name"] + " " + qxj_site_name
                sites.append(dict(site_id=qxj_site_id, site_full_name=qxj_site_full_name))
                site_level[sj_site_name][qxj_site_name] = dict(webid=qxj_site_id, full_name=qxj_site_full_name)
                site_level_children.append(dict(webid=qxj_site_id, full_name=qxj_site_full_name))
                site_level[sj_site_name]["children"] = site_level_children
        elif isinstance(tag, bs4.element.Comment):
            sj_site_name = tag.string.strip().replace("区县", "").replace("-", "")

    # 乡镇级
    xzj_div = site_soup.find("div", {"class": "site_r4"}).find("div", {"class": "c2"})
    sj_site_name = ""
    for sj_tag in xzj_div.contents:
        if isinstance(sj_tag, bs4.element.Tag) and sj_tag.name == "div":
            qxj_site_name = ""
            for qxj_tag in sj_tag.contents:
                if isinstance(qxj_tag, bs4.element.Tag) and qxj_tag.name == "div":
                    sj_site = site_level[sj_site_name]
                    qxj_site_full_name = ""
                    if qxj_site_name in sj_site.keys():
                        qx_site = sj_site[qxj_site_name]
                        qxj_site_full_name = qx_site["full_name"]
                    else:
                        index = int(qxj_site_name.replace("区", "")) - 1
                        if 0 <= index < len(sj_site["children"]):
                            qx_site = sj_site["children"][index]
                            qxj_site_full_name = qx_site['full_name']
                    if qxj_site_full_name != "":
                        for xzj_a in qxj_tag.find_all("a"):
                            xzj_site_id = xzj_a.get("data")
                            xzj_site_name = xzj_a.get_text().strip()
                            sites.append(dict(site_id=xzj_site_id, site_full_name=qxj_site_full_name + " " + xzj_site_name))
                    pass
                elif isinstance(qxj_tag, bs4.element.Comment):
                    qxj_site_name = qxj_tag.string.strip().replace("乡镇", "").replace("-", "")
        elif isinstance(sj_tag, bs4.element.Comment):
            sj_site_name = sj_tag.string.strip().replace("乡镇", "").replace("-", "")

    return sites


def generate_seeds():
    sites = extract_site()
    for site in sites:
        site_id = site["site_id"]
        site_name = site["site_full_name"]
        for fwly_id in fwly_data.keys():
            fwly_name = fwly_data[fwly_id]
            seeds_url = make_seeds_url(site_id, fwly_id)
            extra_data = dict(site_level=site_name, fwly=fwly_name)
            make_requests(seeds_url, extra_data)


if __name__ == "__main__":
    generate_seeds()
