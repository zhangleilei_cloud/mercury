# -*- coding: utf-8 -*-


import requests
import demjson


def generate_seeds():
    make_requests("http://www.lnzwfw.gov.cn/matter/DeptOther/findAllFR?pageNum=1&pageSize=10&qlObject=10",
                  dict(site_level="辽宁省  "))
    make_requests("http://www.lnzwfw.gov.cn/matter/DeptOther/findAllGR?pageNum=1&pageSize=10&qlObject=10",
                  dict(site_level="辽宁省  "))


def make_requests(index_url, extra_data):
    data = {
        "url": index_url,
        "getway": "tips_liaoning",
        "_sched_interval": 86400,
        "status": "OK",
        "priority": "5",
        "extra_data": extra_data,
        "anti_cfg": {
            "proxy": True,
            "cookie": False,
            "random_ua": True
        }
    }
    dest_url = "http://127.0.0.1:8888/seeds"
    data = demjson.encode(data)
    response = requests.post(dest_url, data=data)
    return response


if __name__ == "__main__":
    generate_seeds()
