# -*- coding: utf-8 -*-
import json
import os
import requests
import demjson

DIR_SCRIPT = os.path.dirname(os.path.abspath(__file__))

# beij_category.json 从文件服务器获取
category_path = os.path.join(DIR_SCRIPT, "tianjing_seeds_json.json")


def extract_category():
    with open(category_path, "r", encoding="utf-8") as fc:
        data = fc.read()
        json_data = json.loads(data)
        return json_data


def make_url1(code):
    return f"http://zwfw.tj.gov.cn/tj_twfw/page/Governmentservice_{code}_01_1.html"


def make_url2(code):
    return f"http://zwfw.tj.gov.cn/tj_twfw/page/Governmentservice_{code}_20_1.html"


def generate_seeds():
    datas = extract_category()
    for key in datas.keys():
        # print(key)
        # print(datas[key])

        url1 = make_url1(datas[key])
        url2 = make_url2(datas[key])

        make_requests(url1, dict(site_level=key))
        make_requests(url2, dict(site_level=key))


def make_requests(index_url, extra_data):
    data = {
        "url": index_url,
        "getway": "tips_tianjing",
        "_sched_interval": 86400,
        "status": "OK",
        "priority": "5",
        "extra_data": extra_data,
        "anti_cfg": {
            "proxy": True,
            "cookie": False,
            "random_ua": True
        }
    }
    dest_url = "http://127.0.0.1:8888/seeds"
    # dest_url = "http://172.168.1.110:27017/seeds"
    data = demjson.encode(data)
    print(data)
    response = requests.post(dest_url, data=data)
    return response


if __name__ == "__main__":
    generate_seeds()
