# -*- coding: utf-8 -*-

import os
import requests
import demjson

DIR_SCRIPT = os.path.dirname(os.path.abspath(__file__))

#beij_category.json 从文件服务器获取
category_path = os.path.join(DIR_SCRIPT, "beij_category.json")


def extract_category():
    with open(category_path, "r", encoding="utf-8") as fc:
        categorys = fc.read()
        categorys = demjson.decode(categorys)
    for category in categorys:
        yield category


def make_seeds_url(id):
    return f"http://banshi.beijing.gov.cn/guideservice/workPublishing/getWorkPublishingList?servicePlaceId={id}&themeType=&objectType=&cycleType=&deptType=&natureType=&title=&pageNum=1&serviceObject=1003&appointmentType="


def generate_seeds():
    for node in extract_category():
        site_level = node["name"]
        make_requests(make_seeds_url(node["id"]), dict(site_level=site_level))
        if node.get("childNode"):
            recursion_childs(node.get("childNode"), site_level)


def recursion_childs(child_nodes, parent_name):
    for node in child_nodes:
        if parent_name:
            site_level = "{0} {1}".format(parent_name, node["name"])
        else:
            site_level = node["name"]
        make_requests(make_seeds_url(node["id"]), dict(site_level=site_level))
        if node.get("childNode"):
            recursion_childs(node.get("childNode"), site_level)


def make_requests(index_url, extra_data):
    data = {
        "url": index_url,
        "getway": "tips_beijing",
        "_sched_interval": 86400,
        "status": "OK",
        "priority": "5",
        "extra_data": extra_data,
        "anti_cfg": {
            "proxy": False,
            "cookie": False,
            "random_ua": True
            }
    }
    dest_url = "http://127.0.0.1:8888/seeds"
    data = demjson.encode(data)
    response = requests.post(dest_url, data=data)
    return response


if __name__ == "__main__":
    generate_seeds()
