# -*- coding: utf-8 -*-

import os
import re
import time
import random
import demjson
import requests

from bs4 import BeautifulSoup

DIR_SCRIPT = os.path.dirname(os.path.abspath(__file__))

# shanxi_category.json 从文件服务器获取
category_path = os.path.join(DIR_SCRIPT, "chongqing_area.json")


def extract_category():
    with open(category_path, "r", encoding="utf-8") as fc:
        categorys = fc.read()
        categorys = demjson.decode(categorys)
    for category in categorys:
        yield category


def make_gr_url(area_spell):
    return f"http://zwfw.cq.gov.cn/{area_spell}/govservice/project?type=gr"


def make_fr_url(area_spell):
    return f"http://zwfw.cq.gov.cn/{area_spell}/govservice/project?type=gr"


def make_gg_url(area_spell):
    return f"http://zwfw.cq.gov.cn/{area_spell}/govservice/ggfwqd"


def make_seeds_url(area_spell, page_model):
    return f"http://zwfw.cq.gov.cn/{area_spell}/api-v2/cq.app.icity.govservice.GovProjectCmd/getItemListByFolderCQ?" \
           f"PAGEMODEL={page_model}&TYPE=XK&limit=8&org_code&page=1&runnum&start=0&title_name"


def make_gg_seeds_url(area_spell, region_code):
    return f"http://zwfw.cq.gov.cn/{area_spell}/api-v2/cq.app.icity.govservice.GovProjectCmd/getIndexListByPageCQ?" \
           f"FOLDER_TREE_LEVEL=0&SERVICE_OBJECT_CODE&orgCode&page=1&region_code={region_code}" \
           f"&rows=8&start=0&sub_type&type=GG"


def generate_seeds():
    for node in extract_category():
        # 目前网站上 市-个人，法人 区县-个人，法人，公共服务 乡镇-公共服务 后续若有变化，可以增加种子页
        if node["area_type"] != 3:
            gr_signature = get_unique_signature(make_gr_url(node["ref"]))
            print("gr_signature:" + gr_signature)
            if gr_signature is not "":
                make_requests(make_seeds_url(node["ref"], "person"),
                              dict(site_level=node["name"], signature=gr_signature, area_spell=node["ref"],
                                   request_type="gr"))
            else:
                print("gr_signature 为空 不抓取")

            fr_signature = get_unique_signature(make_gr_url(node["ref"]))
            if fr_signature is not "":
                make_requests(make_seeds_url(node["ref"], "ent"),
                              dict(site_level=node["name"], signature=fr_signature, area_spell=node["ref"],
                                   request_type="fr"))
            else:
                print("fr_signature 为空 不抓取")

        if node["area_type"] != 1:
            gg_signature = get_unique_signature(make_gg_url(node["ref"]))
            if gg_signature is not "":
                make_requests(make_gg_seeds_url(node["ref"], node["webRegion"]),
                              dict(site_level=node["name"], signature=gg_signature, area_spell=node["ref"],
                                   request_type="gg"))
            else:
                print("gg_signature 为空 不抓取")


def get_unique_signature(url):
    print("url:" + url)
    signature = ""
    response = requests.post(url)
    soup = BeautifulSoup(response.text, 'lxml')
    if soup.find('script', {'type': 'text/javascript'}):
        script_text = soup.find('script', {'type': 'text/javascript'}).get_text()
        signature = re.search(r"__signature.*;", script_text).group()
        if signature:
            signature = signature.split("=")[1].replace("\"", "")
            signature = signature.replace(";", "").strip()
    else:
        print(soup.prettify())
    return signature


def make_requests(index_url, extra_data):
    print("index_url:" + index_url)
    data = {
        "url": index_url,
        "getway": "tips_chongqing",
        "_sched_interval": 86400,
        "status": "OK",
        "priority": "5",
        "extra_data": extra_data,
        "anti_cfg": {
            "proxy": False,
            "cookie": False,
            "random_ua": True
        }
    }
    dest_url = "http://127.0.0.1:8888/seeds"
    data = demjson.encode(data)
    response = requests.post(dest_url, data=data)
    return response


def get_unique_timestamp(signature):
    # 根据网站规则生成t,每一次请求t不同，每次请求需要重新生成
    char_str = "0123456789abcdef"
    key = ""
    keyIndex = -1
    for i in range(0, 6):
        c = signature[keyIndex + 1]
        key += c
        keyIndex = char_str.index(c)
        if keyIndex < 0 or keyIndex >= len(signature):
            keyIndex = i
    timestamp = str(int(random.random() * (9999 - 1000 + 1) + 1000)) + "_" + key + "_" + str(
        int(time.time() * 1000))
    return timestamp


if __name__ == "__main__":
    generate_seeds()
