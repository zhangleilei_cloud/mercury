# -*- coding: utf-8 -*-

import os
import requests
import demjson

DIR_SCRIPT = os.path.dirname(os.path.abspath(__file__))

# document_keywords_source.txt 从本地文件读取
keywords_source_file = os.path.join(DIR_SCRIPT, "document_keywords_source.txt")
keywords_used_file = os.path.join(DIR_SCRIPT, "document_keywords_used.txt")
SUFFIX = "+法定+filetype%3Adocx"


def read_keywords():
    keywords_dict = dict()
    with open(keywords_source_file, "r", encoding="utf-8") as fk:
        for line in fk:
            if line.strip() == "":
                continue
            line_split = line.strip('\n').split(' ')
            del line_split[0]
            for keywords in line_split:
                if keywords not in keywords_dict and len(keywords) > 4:
                    keywords_format = keywords.replace(" ", "+").replace("(", "%28").replace(")", "%29")
                    keywords_dict.update({keywords: keywords_format})
    return keywords_dict


def write_new_used_keywords(new_used_keywords_list):
    with open(keywords_used_file, "a", encoding="utf-8") as fak:
        fak.writelines(new_used_keywords_list)


def read_used_keywords():
    used_keywords_list = []
    with open(keywords_used_file, "w", encoding="utf-8") as fak:

        if os.path.exists('test.txt'):
            with open(keywords_used_file, "r", encoding="utf-8") as fak:
                for line in fak:
                    if line.strip() == "":
                        continue
                    used_keywords_list.append(line.strip('\n'))
        else:
            with open(keywords_used_file, mode='w', encoding='utf-8') as fak:
                print("[used_keywords]: used keywords text create success")
    return used_keywords_list


def make_seeds_url(keywords_format):
    return f"https://www.google.com/search?q={keywords_format}{SUFFIX}"


def generate_seeds():
    keywords_dict = read_keywords()
    used_keywords_list = read_used_keywords()
    new_used_keywords_list = []
    for keywords in keywords_dict:
        if keywords not in used_keywords_list:
            new_used_keywords_list.append(keywords+'\n')
            print(make_seeds_url(keywords_dict[keywords]),)
            make_requests(make_seeds_url(keywords_dict[keywords]), dict(keywords=keywords))
    if len(new_used_keywords_list) > 0:
        write_new_used_keywords(new_used_keywords_list)


def make_requests(index_url, extra_data):
    data = {
        "url": index_url,
        "getway": "google_search_result_document",
        "_sched_interval": 86400,
        "status": "OK",
        "priority": "5",
        "extra_data": extra_data,
        "anti_cfg": {
            "proxy": False,
            "cookie": False,
            "random_ua": True
            }
    }
    dest_url = "http://127.0.0.1:8888/seeds"
    data = demjson.encode(data)
    response = requests.post(dest_url, data=data)
    return response


if __name__ == "__main__":
    generate_seeds()
