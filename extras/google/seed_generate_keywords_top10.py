# -*- coding: utf-8 -*-

import os
import csv
import requests
import demjson

DIR_SCRIPT = os.path.dirname(os.path.abspath(__file__))

# keywords_source.csv 从本地文件读取
keywords_file_path = os.path.join(DIR_SCRIPT, "keywords_source.csv")


def read_keywords():
    keywords_dict = dict()
    with open(keywords_file_path, "r", encoding="utf-8") as fk:
        reader = csv.reader(fk)
        keywords_list = list(reader)
        if keywords_list:
            index = 0
            for row in keywords_list:
                index += 1
                if index == 1:
                    continue
                keywords = row[2]
                keywords_format = row[2].replace(" ", "+").replace("(", "%28").replace(")", "%29")
                keywords_dict.update({keywords: keywords_format})
    return keywords_dict


def make_seeds_url(keywords_format):
    return f"https://www.google.com/search?q={keywords_format}"


def generate_seeds():
    keywords_dict = read_keywords()
    for keywords in keywords_dict:
        make_requests(make_seeds_url(keywords_dict[keywords]), dict(keywords=keywords))


def make_requests(index_url, extra_data):
    data = {
        "url": index_url,
        "getway": "google_search_result_keywords_top10",
        "_sched_interval": 86400,
        "status": "OK",
        "priority": "5",
        "extra_data": extra_data,
        "anti_cfg": {
            "proxy": False,
            "cookie": False,
            "random_ua": True
            }
    }
    dest_url = "http://127.0.0.1:8888/seeds"
    data = demjson.encode(data)
    response = requests.post(dest_url, data=data)
    return response


if __name__ == "__main__":
    generate_seeds()
